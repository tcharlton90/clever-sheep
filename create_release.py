#! /usr/bin/python
"""
Python script that creates a CleverSheep release
"""

import os
import subprocess
import shutil
import glob

# Make sure we've up to date with master
subprocess.call(["git", "fetch", "origin"])

# Get the version we are building
version = subprocess.check_output(["git", "describe", "--tags"])
version = version.rstrip()
print("Creating Version: '%s'" % version)

# Write the version out to file - this is used by setup.py
version_file = open(os.path.join("src", "CleverSheep", "version.py"), 'w')
version_file.write("#!/usr/bin/env python\n")
version_file.write("version_string='%s'" % version)
version_file.close()

# Remove the previous dist folder
# We ignore errors as the folder may not be there
shutil.rmtree(os.path.join("src", "dist"), ignore_errors=True)

# Build the package
process = subprocess.Popen(['python', 'setup.py', '-q', 'sdist'], cwd='src')
process.wait()
