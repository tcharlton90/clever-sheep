[![build status](https://gitlab.com/LCaraccio/clever-sheep/badges/master/build.svg)](https://gitlab.com/LCaraccio/clever-sheep/commits/master)
[![coverage report](https://gitlab.com/LCaraccio/clever-sheep/badges/master/coverage.svg)](https://gitlab.com/LCaraccio/clever-sheep/commits/master)

# CleverSheep


CleverSheep is a the top-level package for various other general purpose packages. It exists
in order keep the other packages tidily hidden within a single name space.

See the Wiki for more details: https://gitlab.com/LCaraccio/clever-sheep/wikis/home

# Latest Release

For the latest release notes see [Release Notes](https://gitlab.com/LCaraccio/clever-sheep/wikis/release-notes).

To download the latest release see the [release repo](https://gitlab.com/LCaraccio/clever-sheep-releases).

# Project State

To see code health information about the project see the [code analysis](https://lcaraccio.gitlab.io/clever-sheep/) page.

# Contributing

If you want to contribute see the [Contributors Guide](https://gitlab.com/LCaraccio/clever-sheep/wikis/contributors-guide)
