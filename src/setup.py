#!/usr/bin/env python
"""Setup script for the CleverSheep."""



from distutils.core import setup

import CleverSheep

packages=['CleverSheep',
        'CleverSheep.App',
        'CleverSheep.Debug',
        'CleverSheep.Extras',
        'CleverSheep.IOUtils',
        'CleverSheep.Log',
        'CleverSheep.Prog',
        'CleverSheep.Rst',
        'CleverSheep.Rst.rst2rst',
        'CleverSheep.Sys',
        'CleverSheep.TTY_Utils',
        'CleverSheep.Test',
        'CleverSheep.Test.Tester',
        'CleverSheep.Test.Mock',
        'CleverSheep.TextTools',
        'CleverSheep.VisTools',
       ]

setup(name='CleverSheep',
      version=CleverSheep.version_string,
      description='A collection of various python packages',
      author='Paul Ollis, Laurence Caraccio',
      author_email='cleversheepframework@gmail.com',
      url='https://gitlab.com/LCaraccio/clever-sheep',
      packages=packages,
      py_modules = [], #["csversion"],
      #scripts=["ctc"],
      scripts=[],
      long_description=CleverSheep.__doc__,
      # platform="Linux",

      classifiers=[
              'License :: Python',
              'Operating System :: Linux',
              'Programming Language :: Python',
              'Description :: A collection of various python packages',
              'Platform :: Linux',
          ]
     )

