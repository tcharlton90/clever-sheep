#!/usr/bin/env python
"""A small package to render reStructuredText into reStructuredText.

Yes, it it really is intended to produce reStructuredText from parsed
reStructuredText. This apparently silly idea has at least two applications.

- I wrote a tool that used Epydoc [http://epydoc.sourceforge.net/] to extract
  API documentation, but which then used Sphinx [http://sphinx.pocoo.org/]
  to generate the actual documentation. This entails taking already parsed
  reStructuredText and converting it back into plain reStructuredText for
  the Sphinx files.

- I wish to create a reStructuredText reformatter tool (mainly for use within
  the Vim editor). To do this properly requires using the Docutils
  [http://docutils.sourceforge.net/] parser to parse the text and then
  rendering the parsed reStructuredText back into plain reStructuredText.

"""
from __future__ import print_function



import sys

from docutils import nodes


class _Null(object):
    """A simple /dev/null file like object.

    Everything written to this is simply discarded.

    """
    def write(self, *args): #pragma: debug
        pass

    isatty = flush = writelines = close = write


#: The current log file.
#:
#: Initially this is a null logger, which discards all output. It can be set
#: directly or by using `setLogPath`. This is used to log each node as it is
#: visited, providing a visual picture of the document's tree.
log = _Null()


def setLogPath(path, mode="w"):
    """Set the path of the log file.

    Use this if you wish to log the renderer's activity to a file. Each
    docutils node is logged as it is visited and departed. The output uses
    indentation to show the structure of the node tree.

    :Param path:
        Defines the name of a log file to write to. Set this to ``None`` to
        stop logging.
    :Param mode:
        The file opening mode, which defaults to 'w'.
    :Return:
        The previous logger.

    """
    global log
    prevLog = log
    if not path:
        log = _Null()
    else:
        try:
            log = open(path, mode)
        except IOError: #pragma: unreachable
            log = _Null()
    return prevLog


setLogPath("z")

class BaseVisitor(nodes.NodeVisitor, object):
    """A base class for reStructuredText writers.

    This just provides a basis for a fairly generic approach I have taken when
    developing RST writers.

    :Ivar level:
        The current level within the *docUtils* document tree that is being
        walked.
    :Ivar skipLevel:
        When non-zero, we skip nodes that are deeper in the tree than this
        level.

    """
    def __init__(self, *args, **kwargs):
        nodes.NodeVisitor.__init__(self, *args, **kwargs)
        self.level = 0
        self.skipLevel = 0
        if not hasattr(self, "document"): #pragma: unreachable
            print("NO DOC!")
            self.document = None

    def unknown_visit(self, node):
        """Handler for all node visit processing.

	I let all nodes be effectively treated as unkonwn nodes. That way I can
	route all prcoessing through `processNode`, which allos me to easily
	add tracing and simplify some other stuff as well.

        :Param node:
            the node being visited.

        """
        self.level += 1
        self.processNode("enter", node, tracer=self.log_enter)

    def unknown_departure(self, node):
        """The partner method for `unknown_visit`."""
        if self.skipLevel and self.level <= self.skipLevel:
            self.skipLevel = 0
        self.level -= 1
        self.processNode("leave", node, tracer=self.log_leave)

    def processNode(self, mode, node, tracer):
        """Common processing for node visit and departure.

        This is invoked for *all* RST nodes. It looks for a method called
        ``enter_<name>`` or ``leave_<name>``, where ``<name>`` is the node's
        type (paragraph, Text, block_quote, etc). If a method can be found then
        it is normally invoked as ``method(node, self.level, ...)``. However, if
        self.skipLevel is currently non-zero then the method is not invoked.

        """
        kwargs = {}
        name = node.__class__.__name__
        if len(name) == 2 and name.endswith("_"): #pragma: unreachable
            kwargs["char"] = name[0]
            name = "Char"

        if isinstance(node, nodes.Admonition):
            # All the admonitions can be handled the same way, so we route them to
            # a single pair of generic handlers.
            admonitionType = node.__class__.__name__
            funcName = "%s_admonition" % (mode)
            func = getattr(self, funcName, None)
            if func:
                if not self.skipLevel:
                    tracer(node, self.level, "  ")
                    func(admonitionType, node, self.level, **kwargs)
                else: #pragma: unreachable
                    tracer(node, self.level, "##")
            else: #pragma: unreachable
                tracer(node, self.level, "**")
        else:
            funcName = "%s_%s" % (mode, name)
            func = getattr(self, funcName, None)
            if func:
                if not self.skipLevel:
                    tracer(node, self.level, "  ")
                    func(node, self.level, **kwargs)
                else:
                    tracer(node, self.level, "##")
            else:
                tracer(node, self.level, "**")

    def log_enter(self, node, level, prefix=""):
        """Log details of a node being entered."""
        text = ""
        if node.__class__.__name__ == "Text":
            text = " " + repr(node.astext()[:80])
        log.write("%sENTER %s %s %s%s {%s}\n" % (prefix,
                level, "  " * level,
                node.__class__.__name__, text, _getNodeAttrs(node)))

    def log_leave(self, node, level, prefix=""):
        """Log details of a node being left."""
        if 0: #pragma: unreachable
            log.write("%sLEAVE %s %s %s {%s}\n" % (prefix,
                level + 1, "  " * (level + 1),
                node.__class__.__name__, _getNodeAttrs(node)))


def _getNodeAttrs(node):
    """Get the attributes of a node.

    :Return:
        A string containing a list of ``name=value`` entries.

    """
    attrs = ""
    if hasattr(node, "attributes"):
        if node.attributes:
            for attr in node.attributes:
                value = node.attributes[attr]
                if value:
                    attrs += "%s=%r " % (attr, node.attributes[attr])
    return attrs
