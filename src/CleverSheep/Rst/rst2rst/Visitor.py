"""This needs documenting.

:Newfield omit: Omitted

"""



import re
import textwrap

from docutils import nodes, utils
from docutils.parsers.rst import roles, directives
import docutils.parsers.rst.states

from CleverSheep.Prog.Aspects import decorator

from CleverSheep.Rst import rst2rst
from CleverSheep.TextTools.Roman import decToRoman


rFootnoteId = re.compile("id[0-9]+")


_langMap = {
    "py": "python",
}


def maxRoman(v, count):
    """Work out the maximum length Roman numeral.

    :Return:
        The length of the longest Roman numeral form for the range.
    :Param v:
        The first number.
    :Param count:
        The range of numbers, i.e. v <= x < v + count.
    """
    return max([len(decToRoman(i)) for i in range(v, v + count)])


#: A bit of dodgy monkey patching.
#:
#: It is helpful to have the original block of text for a table attached to the
#: ``table`` node. To do this, we override the ``table`` method from
#: ``docutils.parsers.rst.states.Body.table`` with one
#: that has an extra line:<py>:
#:
#:    table["block"] = block
#:
#: added at the appropriate point.
#:
#: The advantages are:
#:
#: - Some tables might otherwise not re-render properly.
#: - This is the only way that it is possible to retain the original layout
#:   (such as leading spaces within cells.
#: - Both simple and grid style tables retain their form. Without this hack
#:   my code always ouptputs a grid style of table.
def table(self, isolate_function, parser_class):
    #"""Parse a table."""
    block, messages, blank_finish = isolate_function()
    if block:
        try:
            parser = parser_class()
            tabledata = parser.parse(block)
            tableline = (self.state_machine.abs_line_number() - len(block) + 1)
            table = self.build_table(tabledata, tableline)
            table["block"] = block
            nodelist = [table] + messages
        except tableparser.TableMarkupError as detail: #pragma: unreachable
            nodelist = self.malformed_table(
                block, ' '.join(detail.args)) + messages
    else:
        nodelist = messages
    return nodelist, blank_finish

docutils.parsers.rst.states.Body.table = table


#: Another bit of dodgy monkey patching.
#:
#: For custom roles, we need to know the role name, which the docutils parser
#: does not copy to inline nodes. This patches the docutils code to add this.
def generic_custom_role(role, rawtext, text, u1, u2, options={}, content=[]):
    docutils.parsers.rst.roles.set_classes(options)
    return [nodes.inline(rawtext, utils.unescape(text),
        rolename=role, **options)], []

_options = docutils.parsers.rst.roles.generic_custom_role.options
docutils.parsers.rst.roles.generic_custom_role = generic_custom_role
docutils.parsers.rst.roles.generic_custom_role.options = _options
del _options


@decorator
def skips(func, self, *args, **kwargs):
    try:
        return func(self, *args, **kwargs)
    finally:
        self.skipLevel = self.level


rRefUnderscore = re.compile(r"([`a-zA-Z0-9])_(?=[^a-zA-Z0-9])")

rQuoting1 = re.compile(r"""(?x)
   ^([*`|])
   (?=[ *`|])
""")
rQuoting4 = re.compile(r"""(?x)
       (?<=[\s\W])          # Look-behind assert space or non-word char.
       ([*|]{1,2})
       (?![\s\W])
   |
       (?<=[^a-zA-Z0-9_:])  # Look-behind assert space, non-word char or :.
       ([`]{1,2})
       (?![\s\W])
   |
       (?<![\s\W|])
       ([*`|]{1,2})
       (?=[\s\W|])
""")


def escape(s):
    """Escape special RST characters in `s`.

    :Param s:
        The text to be escaped.
    """
    orig = s
    s = s.replace("\\", "\\\\")
    s = rQuoting1.sub(r"\\1", s)
    ss = ""
    p = 0
    for m in rQuoting4.finditer(s):
        a, b = m.start(), m.end()
        ss += s[p:a]
        ss += ("\\" + s[a]) * (b - a)
        p = b
    if p:
        ss += s[p:]
        s = ss
    s = s.replace("]_", "]\\_")
    s = s.replace(" _[", " \\_[")
    s = rRefUnderscore.sub("\\1\\_", s)
    return s


class Style(object):
    """Holds information about a paragraph's style.

    This handles simple paragraph style information such as bulleted and
    numbered paragraphs.

    """
    def __init__(self, enumType=None, enumCount=None, bulletType=None,
            count=10, label=None, pad="", literal=False):
        """Constructor:

        :Param enumType, enumCount:
            These are used when the style is for a numbered paragraph.

        :Param bulletType:
            The type of bullet character, which will be one of the standard
            reStructureText bullet characters.

        """
        self.firstPara = True
        self.enumType = enumType
        self.enumCount = enumCount
        self.bulletType = bulletType
        self.label = label
        self.pad = pad
        self.literal = literal
        if self.bulletType or self.label:
            return

        # Work out the width required for the full range of numbers.
        if self.enumType in ("lowerroman", "upperroman"):
            self.width = maxRoman(self.enumCount, count)
        elif self.enumType in ["loweralpha", "upperalpha"]:
            self.width = 1
        elif enumCount is not None:
            n = self.enumCount + count - 1
            self.width = 1
            while n >= 10:
                self.width += 1
                n = n // 10

    def copy(self):
        s = Style()
        s.__dict__.update(self.__dict__)
        return s

    def peekNextPrefix(self):
        """Like `getNextPrefix`, but does not advance any counters."""
        if self.bulletType:
            return "%s " % self.bulletType
        if self.label:
            return "%s" % self.label

        if self.enumType is None:
            return ""
        if self.enumType == "lowerroman":
            s = decToRoman(self.enumCount).lower()
        elif self.enumType == "upperroman":
            s = decToRoman(self.enumCount)
        elif self.enumType in ["loweralpha", "upperalpha"]:
            s = chr(self.enumCount + ord('a') - 1)
            if self.enumType == "upperalpha":
                s = s.upper()
        else:
            s = "%d" % (self.enumCount)
        return (s + ".").ljust(self.width + 2)

    def getNextPrefix(self):
        if self.firstPara:
            prefix = self.peekNextPrefix()
            self.firstPara = False
        else:
            prefix = self.peekPad()
        return prefix

    def peekPad(self):
        return " " * len(self.peekNextPrefix())

    def step(self):
        if self.enumType:
            self.enumCount += 1
        self.firstPara = True


class StyleStack(object):
    """Used to manage the nesting of styles."""
    def __init__(self, f):
        """Constructor:

        :Param f:
            The file-like object that is being written to.

        """
        self.styles = [Style()]
        self.f = f
        self.styles[0].f = f

    def push(self, style):
        """Push a style onto the stack, making it the current style.

        :Param style:
            A `Style` instance.

        """
        if self.styles:
            ps = self.styles[-1]
            style.pad += ps.pad + ps.peekPad()
        self.styles.append(style)
        style.f = self.f
        return style

    def pop(self):
        """Pop the top-most style from the stack.

        The previous style (if any) becomes th current style.

        :Return:
            A `Style` instance, being the popped entry.

        """
        return self.styles.pop()

    def nextStyle(self):
        self.styles[-1] = self.styles[-1].copy()
        self.styles[-1].step()
        return self.styles[-1]

    def top(self):
        return self.styles[-1]


class Paragraph(object):
    """Holds the content of a paragraph."""
    def __init__(self, style, leadingBlanks=1, trailingBlanks=0,
            trailer=None):
        self.text = ""
        self.leadingBlanks = leadingBlanks
        self.trailingBlanks = trailingBlanks
        self.style = style
        self.trailer = trailer

    def addText(self, text, esc=False):
        if esc:
            self.text += escape(str(text))
        else:
            self.text += str(text)

    def flush(self, styles, wrap):
        if not self.text:
            return
        self.style.f.write("\n" * self.leadingBlanks)
        s = self.flow()
        self.style.f.write(s)
        if s and s[-1] != "\n":
            self.style.f.write("\n")
        if self.trailer is not None:
            text = "%s%s" % (self.style.peekPad(), self.trailer)
            self.style.f.write("%s\n" % text.rstrip())
        self.style.f.write("\n" * self.trailingBlanks)

    def flow(self):
        prefix, pad = self.style.getNextPrefix(), self.style.peekPad()
        ppad = self.style.pad

        if not self.style.literal:
            wrapper = textwrap.TextWrapper(width=79 - len(prefix) - len(ppad))
            lines = wrapper.wrap(self.text)
            newLines = ["%s%s%s" % (ppad, prefix, lines[0])]
            newLines += ["%s%s%s" % (ppad, pad, l) for l in lines[1:]]
            newLines = [l.rstrip() for l in newLines]
            return "\n".join(newLines)

        lines = self.text.splitlines()
        lines = ["%s%s%s" % (ppad, prefix, lines[0])] + [
                "%s%s%s" % (ppad, pad, l) for l in lines[1:]]
        lines = [l.rstrip() for l in lines]
        return "\n".join(lines)

    def empty(self):
        return not self.text


class VisitorMeta(type):
    """Metaclass for the `Visitor` class.

    This simply arranges for all the ``enter`` and ``leave`` methods to be given
    common docstrings.
    """
    def __new__(self, name, bases, members):
        for name, member in members.iteritems():
            if name.startswith("enter_"):
                member.__doc__ = "Visitor for %s\n\n:omit:" % name[6:]
            if name.startswith("leave_"):
                member.__doc__ = "Visitor for %s\n\n:omit:" % name[6:]
        return type(name, bases, members)


class Visitor(rst2rst.BaseVisitor):
    """A reStructureText visitor that outputs reStructureText.

    This is the visitor that is used to walk the reStructureText document tree
    outputting (reformatted) reStructureText.

    :Ivar linker:
        A cross-reference link resolver. This is a docutils API thing.
        TODO: Needs more detail, which requires more docutils study.

    :Ivar styles:
        A `StyleStack` instance, which both provides details about the
        currently active style and, of course, a stack of currently over-riden
        styles.

    """
    __metaclass__ = VisitorMeta

    def __init__(self, outF, *args, **kwargs):
        """Constructor:

        :Param outF:
            The file-like object to write the output to.
        :Param args, kwargs:
            Other arguments passed up to the `BaseVisitor` class constructor.

        """
        self.inPara = 0
        self.sectionLevel = 0
        self.inFigure = False
        self.escaping = True
        self.outF = outF

        # New staring point
        width = kwargs.pop("width", 79)
        self.renderMode = kwargs.pop("mode", "standard")
        rst2rst.BaseVisitor.__init__(self, *args, **kwargs)
        self.styles = StyleStack(outF)
        self.linker = None
        self.paras = [Paragraph(self.styles.top(), leadingBlanks=0)]

    @property
    def para(self):
        return self.paras[-1]

    @property
    def prevPara(self):
        try:
            return self.paras[-2]
        except IndexError:
            return self.paras[-1]

    # ------------------------------------------------------------------------
    #  Common methods to handle repetitive node operations.
    # ------------------------------------------------------------------------
    def enter_Text(self, node, level):
        self.para.addText(node, esc=self.escaping)

    def pflush(self, keep=3):
        while len(self.paras) > keep:
            para = self.paras.pop(0)
            para.flush(styles=self.styles, wrap=True)
        if not self.paras or not self.paras[-1].empty():
            self.paras.append(Paragraph(self.styles.top()))
        self.paras[-1].style = self.styles.top()

    def popStyle(self):
        self.styles.pop()
        self.para.style = self.styles.top()


    def flushAndPop(self, node, level, *args):
        """Used for a lot of the ``leave_...`` methods."""
        self.pflush()
        self.popStyle()

    def enter_problematic(self, node, level):
        self.escaping = False

    def leave_problematic(self, node, level):
        self.escaping = True

    # ------------------------------------------------------------------------
    #  Footnotes
    # ------------------------------------------------------------------------
    @skips
    def enter_footnote_reference(self, node, level):
        # Find the matching footnote and get the text.
        uid = node.get("refid", "id1")
        refname = node.get("refname")
        if node.get("auto") == 1:
            if refname:
                self.para.addText("[#%s]_" % refname)
            else:
                self.para.addText("[#]_")
        elif node.get("auto") == "*":
            self.para.addText("[*]_")
        else:
            self.para.addText("[%s]_" % node.astext())

    leave_footnote = flushAndPop
    def enter_footnote(self, node, level):
        prefix = ""
        uid = node.get("ids", ["id1"])[0]
        if node.get("auto") == 1:
            if not rFootnoteId.match(uid):
                prefix = ".. [#%s] " % uid
            else:
                prefix = ".. [#] "
        elif node.get("auto") == "*":
            prefix = ".. [*] "
        else:
            prefix = ".. [%s] " % " ".join(node.get("names"))
        self.styles.push(Style(label=prefix))
        self.pflush()

    # ------------------------------------------------------------------------
    #  Cross-referencing.
    # ------------------------------------------------------------------------
    def enter_title_reference(self, node, level):
        if self.linker is None:
            self.para.addText("`")
            return
        self.para.addText("%s" %
                self.linker.translate_identifier_xref(node.astext()))
        self.skipLevel = self.level

    def leave_title_reference(self, node, level):
        if self.linker is None:
            self.para.addText("`")
            #self.para.addText("`")

    # ------------------------------------------------------------------------
    #  Basic paragraph styles
    # ------------------------------------------------------------------------
    leave_doctest_block = flushAndPop
    def enter_doctest_block(self, node, level):
        self.styles.push(Style(literal=True))
        self.pflush()

    leave_block_quote = flushAndPop
    def enter_block_quote(self, node, level):
        self.styles.push(Style(pad="  "))
        self.pflush()

    leave_line_block = flushAndPop
    def enter_line_block(self, node, level):
        self.styles.push(Style(label="| "))
        self.pflush()
        self.prevPara.trailingBlanks = 1
        self.para.leadingBlanks = 0

    def enter_line(self, node, level):
        text = node.astext().strip()
        if not text:
            self.pflush()
            self.para.addText("|\n")

            self.pflush()
            self.para.leadingBlanks = 0
            self.styles.nextStyle()

            self.prevPara.leadingBlanks = 0
            self.prevPara.style.label = ""
        else:
            self.pflush()

    def leave_line(self, node, level):
        self.para.leadingBlanks = 0
        self.pflush()
        self.styles.nextStyle()


    # ------------------------------------------------------------------------
    #  Headings, sections and titles
    # ------------------------------------------------------------------------
    def insertSpaceAfterPara(self):
        if not self.prevPara.empty():
            self.prevPara.trailingBlanks = 1

    @skips
    def enter_title(self, node, level):
        self.insertSpaceAfterPara()
        self.styles.push(Style(literal=True))
        self.pflush()
        text = node.astext()
        ulineChars = "=-~^."
        level = min(len(ulineChars), self.sectionLevel)
        if level == 1:
            uline = "=" * len(text)
            self.para.addText("%s\n" % uline)
            self.para.addText("%s\n" % text)
            self.para.addText("%s\n" % uline)
        else:
            uline = ulineChars[level - 2] * len(text)
            self.para.addText("%s\n" % text)
            self.para.addText("%s\n" % uline)

    def leave_title(self, node, level):
        # Append two paragraphs. These act as a buffer between the
        # title-paragraph.
        self.pflush()
        self.popStyle()
        self.paras.append(Paragraph(self.styles.top()))
        self.paras.append(Paragraph(self.styles.top()))

    def enter_subtitle(self, node, level): #pragma: unreachable
        text = node.astext()
        uline = "=" * len(text)
        self.para.addText("%s\n%s" % (text, uline))
        self.skipLevel = self.level

    def enter_section(self, node, level):
        self.sectionLevel += 1

    def leave_section(self, node, level):
        self.sectionLevel -= 1


    # ------------------------------------------------------------------------
    #  Table processing.
    # ------------------------------------------------------------------------
    leave_table = flushAndPop
    @skips
    def enter_table(self, node, level):
        self.styles.push(Style(literal=True))
        self.pflush()
        block = node.get("block")
        self.para.addText("%s" % ("\n".join(block),))


    # ------------------------------------------------------------------------
    #  Field processing.
    # ------------------------------------------------------------------------
    # Docinfo does not get generated because it occurs as the result of a
    # transform.
    def enter_field_list(self, node, level):
        self.para.leadingBlanks = 1

    @skips
    def enter_field_name(self, node, level):
        self.para.addText(":%s:" % node.astext(), esc=False)

    leave_field_body = flushAndPop
    def enter_field_body(self, node, level):
        style = self.styles.push(Style(pad="  "))
        self.pflush()
        self.para.leadingBlanks = 0


    # ------------------------------------------------------------------------
    #  Option list processing.
    # ------------------------------------------------------------------------
    leave_option_list = flushAndPop
    def enter_option_list(self, node, level):
        l = 0
        for item in node.children:
            group = item.children[0]
            for option in group.children:
                l = max(l, len(option.astext()))
        self.optionWidth = l + 2
        self.pflush()
        self.prevPara.trailingBlanks = 1
        self.paras.append(Paragraph(Style()))
        self.styles.push(Style())

    @skips
    def enter_option(self, node, level):
        style = self.styles.nextStyle()
        style.label = node.astext().ljust(self.optionWidth)
        self.pflush()
        self.para.leadingBlanks = 0


    # ------------------------------------------------------------------------
    #  Embedding images
    # ------------------------------------------------------------------------
    def enter_figure(self, node, level):
        self.inFigure = True

    def leave_figure(self, node, level):
        self.inFigure = False

    leave_image = flushAndPop
    def enter_image(self, node, level, directive="Image"):
        directive = "Image"
        if self.inFigure:
            directive = "Figure"
        self.styles.push(Style(literal=True))
        self.pflush()
        self.para.addText(".. %s:: %s\n" % (directive, node.get("uri")))
        for attr, value in sorted(node.attributes.iteritems()):
            if attr == "uri" or not value:
                continue
            self.para.addText("   :%s: %s\n" % (attr, value))

    leave_caption = flushAndPop
    def enter_caption(self, node, level):
        self.styles.push(Style(pad="   "))
        self.pflush()

    leave_legend = flushAndPop
    def enter_legend(self, node, level):
        self.styles.push(Style(pad="   "))
        self.pflush()

    def enter_paragraph(self, node, level):
        self.needBlanks = 1
        self.inPara += 1
        if self.sectionLevel <= 0:
            self.sectionLevel = 1

    def leave_paragraph(self, node, level):
        self.pflush()
        self.inPara -= 1

    def enter_inline(self, node, level):
        role = node.attributes["rolename"]
        self.para.addText(":%s:" % role)
        self.para.addText("`")
        # self.para.addText(":%s:`" % role)

    def leave_inline(self, node, level):
        self.para.addText("`")
        # self.para.addText("`")

    leave_bullet_list = flushAndPop
    def enter_bullet_list(self, node, level):
        self.styles.push(Style(
            bulletType=node.attributes["bullet"]))

    leave_enumerated_list = flushAndPop
    def enter_enumerated_list(self, node, level):
        start = int(node.get("start", 1))
        self.styles.push(Style(enumType=node["enumtype"],
            enumCount=start, count=len(node.children)))

    def enter_list_item(self, node, level):
        # self.curInitPrefix = self.styles.getNextPrefix()
        self.pflush()

    def leave_list_item(self, node, level):
        self.styles.nextStyle()

    def enter_literal_block(self, node, level):
        style = node.attributes.get("style", None)
        cur_style = self.styles.top()

        if style in [1, None]:
            self.prevPara.trailer = "%s::" % (cur_style.pad)
        else:
            if self.renderMode == "sphinx":
                self.para.addText(".. code-block:: %s" % _langMap.get
                        (style, style))
            else:
                self.prevPara.trailer = "%s:<%s>:" % (cur_style.pad, style)

        outStyle = self.styles.push(Style(literal=True))
        outStyle.pad += "  "
        self.pflush()
        self.escaping = False

    def leave_literal_block(self, node, level):
        self.pflush()
        self.escaping = True
        self.popStyle()

    def enter_emphasis(self, node, level):
        self.para.addText("*")

    def leave_emphasis(self, node, level):
        self.para.addText("*")

    def enter_strong(self, node, level):
        self.para.addText("**")

    def leave_strong(self, node, level):
        self.para.addText("**")

    def enter_literal(self, node, level):
        # TODO: Should I make escaping a paragrph state?
        self.para.addText("``")
        self.escaping = False

    def leave_literal(self, node, level):
        self.para.addText("``")
        # TODO: Is here any situation where this needs to push/pop.
        self.escaping = True

    def enter_substitution_reference(self, node, level):
        self.para.addText("|")

    def leave_substitution_reference(self, node, level):
        self.para.addText("|")

    leave_definition = flushAndPop
    def enter_definition(self, node, level):
        style = self.styles.push(Style(pad="  "))
        self.pflush()
        self.para.leadingBlanks = 0


    # -------------------------------------------------------------------------
    #  Citations.
    #    These are used to reference external documents, as in see [WIZ-OZ].
    # -------------------------------------------------------------------------
    leave_citation = flushAndPop
    def enter_citation(self, node, level):
        child = node.children[0]
        self.curInitPrefix = ".. [%s] " % child.astext()
        self.styles.push(Style(label=(".. [%s] " % child.astext())))
        self.pflush()

    @skips
    def enter_label(self, node, level):
        return

    def enter_citation_reference(self, node, level):
        # self.para.addText("<%r>" % node.astext())
        self.para.addText("[")
        # self.para.addText("<%r>" % node.astext())

    def leave_citation_reference(self, node, level):
        self.para.addText("]_", esc=False)


    # -------------------------------------------------------------------------
    #  References.
    #    These can be internal, such as links to headings or explicit markers
    #    and also references to external documentation.
    # -------------------------------------------------------------------------
    @skips
    def enter_reference(self, node, level):
        text = node.astext()
        name = node.get("name")
        anonymous = node.get("anonymous", False)
        refuri = node.get("refuri")
        refname = node.get("refname")
        target = None
        if refname:
            uid = self.document.nameids.get(refname, None)
            if uid:
                target = self.document.ids.get(uid, None)
        if name is not None and refuri:
            self.inline_ref = True
            self.para.addText("`%s" % (name, ))
            return

        if name is None and refuri:
            self.para.addText(refuri)
            return
        if name and refname and target is None:
            self.para.addText("`%s`_"% (name, ))
            return

        if " " in name:
            name = "`%s`" % name
        if anonymous:
            self.para.addText("%s__" % name)
        else:
            self.para.addText("%s_" % name)

    @skips
    def enter_target(self, node, level):
        ids = node.get("ids")
        names = node.get("names")
        refid = node.get("refid")
        refuri = node.get("refuri", "")
        anonymous = node.get("anonymous", False)

        if getattr(self, "inline_ref", None) and refuri:
            # self.pflush()
            self.para.addText(" <%s>`_" % (refuri,))
            self.inline_ref = False
            return

        if anonymous:
            self.pflush()
            self.para.addText("__ %s" % (refuri,))
            return

        name = node.get("names")[0]
        if self.inPara:
            # Inline target
            self.para.addText("_`%s`" % (name, ))
            return

        self.pflush()
        if " " in name:
            name = "`%s`" % name
        if refuri:
            self.para.addText(".. _%s: %s" % (name, refuri))
        else:
            self.para.addText(".. _%s:" % (name,))

    leave_transition = flushAndPop
    def enter_transition(self, node, level):
        self.styles.push(Style(literal=True))
        self.pflush()
        self.para.addText("%s\n" % ("-" * 40))


    # -------------------------------------------------------------------------
    #  Specialised Word processing
    # -------------------------------------------------------------------------
    def enter_raw(self, node, level):
        self.styles.push(Style(literal=True))
        self.pflush()
        format = node.attributes.get("format", "")
        self.para.addText(".. raw:: %s\n" % format)
        self.styles.push(Style(pad="   ", literal=True))
        self.pflush()

    def leave_raw(self, node, level):
        self.pflush()
        self.popStyle()
        self.popStyle()


    # -------------------------------------------------------------------------
    #  Admonitions.
    # -------------------------------------------------------------------------
    leave_admonition = flushAndPop
    def enter_admonition(self, admonitionType, node, level=0):
        self.pflush()
        self.para.addText(".. %s::\n" % admonitionType.capitalize())
        self.styles.push(Style(pad="   "))
        self.pflush()
        self.para.leadingBlanks = 0


    # -------------------------------------------------------------------------
    #  Comments.
    # -------------------------------------------------------------------------
    leave_comment = flushAndPop
    def enter_comment(self, node, level):
        self.styles.push(Style(label=".. ", literal=True))
        self.pflush()

    def enter_document(self, node, level=0):
        self.document = node

    def leave_document(self, node, level=0):
        self.pflush(keep=0)
        assert len(self.styles.styles) == 1

    @skips
    def enter_substitution_definition(self, node, level):
        child = node.children[0]
        if isinstance(child, nodes.Text):
            self.para.addText(".. |%s| replace:: %s"
                    % (node["names"][0], node.astext()))
        elif isinstance(child, nodes.image):
            self.para.addText(".. |%s| image:: %s"
                    % (node["names"][0], child.get("uri")))
        else: #pragma: unreachable
            self.para.addText(".. |%s| FIXME - Handle %s node" % (
                node["names"][0], child.__class__.__name__))
