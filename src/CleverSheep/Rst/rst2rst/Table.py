#!/usr/bin/env python
"""Support for tables.

This provides classes to help handle reStructuredText tables.

"""



from Page import  Page


class Row(object):  #pragma: unsupported
    """A row within a table.

    The row is broken into a set of `cells`, each containing a list of of lines
    for the cell.

    :Ivar cells:
        The cells for this row. This is a list, where each entry is another
        list representing the lines of text in the cell.

    """
    def __init__(self, colCount):
        """Constructor:

        Sets the `cells` to a list of empty sets of lines.

        :Param colCount:
            The number of columns in the row.

        """
        self.cells = [list([]) for n in range(colCount)]
        self.linesAfter = ['|' for n in range(colCount)]
        self.isMerged = [False for n in range(colCount)]

    def addContent(self, cellIdx, lines):
        """Add content to a single cell.

        :Param cellIdx:
            The cell to add to.
        :Param lines:
            A list of the lines to put into the selected cell.

        """
        self.cells[cellIdx] = lines

    def lineCount(self):
        """The number of lines required by this row.

        :Return:
            The maximum number of lines in any of the cells. If no cell
            contains any lines then one is returned, *not* zero.

        """
        return max(1, max(len(ent) for i, ent in enumerate(self.cells)
                    if not self.isMerged[i]))

    def mergeCells(self, cellIdx, count):
        """Merge the selected cell with following cells.

        :Param cellIdx:
            The cell to add merge into.
        :Param count:
            How many of the following cells to merge.

        """
        for i in range(count):
            self.linesAfter[cellIdx + i] = ' '

    def merge(self, cellIdx):
        """Mark the specified cell as merged with the cell below.

        :Param cellIdx:
            The cell to mark.

        """
        self.isMerged[cellIdx] = True


class Table(object): #pragma: unsupported
    """A complete table.

    This stores the details of a table. Its contents get built up as the
    table's elements are processed.

    :Ivar rows:
        The rows in the table.

        Each entry oos a `Row` instance.

    :Ivar lineBelow:
        An array of the undeline characters to be used under each cell of each
        row.

        This is the same size as the `rows` member. Each entry corresponds to
        the row with the same index.

    :Ivar widths:
        The widths for each column, in characters.
    :Ivar colCount:
        The number of columns in this table.

    """
    def __init__(self):
        """Constructor:

        Initialses the table to have no heading and no rows.

        """
        # self.heading = []
        self.rows = []
        self.lineBelow = []
        self.widths = []
        self._curRowIdx = -1
        self._headingEnd = 99999

    def markHeadingEnd(self):
        """Mark the current row as a heading style row.

        This basically set the `lineBelow` entry to contain equals (``=``)
        signs.

        """
        self._headingEnd = self._curRowIdx
        self.lineBelow[self._curRowIdx] = ["=" for c in range(self.colCount)]

    def _setColumns(self, count):
        """Not used"""

    def addColWidth(self, w):
        """Add details of the witdh of a column.

        This is invoked for each ``colspec`` visited. Each time the width is
        added to the `widths` array and the `colCount` updated.

        :Param w:
            The width the new column, in characters.

        """
        self.widths.append(w)
        self.colCount = len(self.widths)

    def addRow(self):
        """Add a new row, providing the current row is the last one.

        This is invoked for ``row`` visited. Normally this means that a new
        `Row` instance is added to the table's `rows`. However, sometimes a
        call to `mergeRows` can cause rows to be added early, causing some
        subsequent call to this function to have no effect.
        """
        if self._curRowIdx + 1 >= len(self.rows):
            row = Row(self.colCount)
            self.lineBelow.append(["-" for c in range(self.colCount)])
            self.rows.append(row)
        self._curRowIdx += 1
        self._row = self.rows[self._curRowIdx]
        self._cellIdx = 0

    def mergeRows(self, count):
        """Merge one or more subsequent rows into the current row.

        This is invoked when a ``entry`` is visted and it has a value
        set for its ``morerows`` attribute. As a side effect, extra rows
        get added to the table.

        The merging is effectively achieved by setting the `lineBelow`
        characters to spaces.

        :Param count:
            How may of the following rows to merge.

        """
        count -= self._curRowIdx + 1 - len(self.rows)
        r, c = self._curRowIdx, self._cellIdx
        for n in range(count):
            self.addRow()
            self.lineBelow[r + n][c] = " "
            self.rows[r + n].merge(c)
        self._curRowIdx, self._cellIdx = r, c
        self._row = self.rows[self._curRowIdx]

    def mergeColumns(self, count):
        """Merge one or more subsequent cell into the current cell.

        This is invoked when a ``entry`` is visted and it has a value
        set for its ``morecols`` attribute.

        :Param count:
            How may of the following cells to merge.

        """
        self._row.mergeCells(self._cellIdx, count)

    def addRowContent(self, s):
        """Add a string to the row.

        This is invoked when a ``entry`` is visted. The text is split into
        lines and added to the next cell in the row, Multiple calls to this
        method fill the row cell by cell.

        :Param s:
            The string to add to a cell.

        """
        self._row.addContent(self._cellIdx, s.splitlines())
        self._cellIdx += 1

    def asStr(self):
        """Render the table as a single string.

        This uses the `Page` class to render the entire table as a single
        string.

        :Return:
            The rendered table.
        """
        def makeSpan(firstJoint, joints, connections):
            """Create horizontal span for part of the table.

            This creates lines that appear in an empty table, like::

                +-----+---------+
                |     |         |
                +=====+=========+

            :Param firstJoint:
                The first joining character (the leftmost), a ``+`` or ``|``.
            :Param joints:
                An array of the subsequent joint characters. Typically an array
                of the same character as `firstJoint`. Must be the smae length as
                `connections`.
            :Param connections:
                An array of the characters used to connect between joints.
                These are typically one of *space*, ``-`` or ``+``. The length of
                this array is the number of columns in the table.
            """
            parts = [(c * w) + j
                    for c, w, j in zip(self.widths, connections, joints)]
            return firstJoint + "".join(parts) + "\n"

        # Create the page and draw the top line.
        p = Page()
        p.write(makeSpan("+", "+" * self.colCount, "-" * self.colCount))

        # Now add the empty rows, plus following lines. This gives an empty table.
        # The lineBelow and Row.linesAfter are used to draw the dividing lines,
        # thus ensuring than spanned cells appear correctly.
        for i, r in enumerate(self.rows):
            hSpan = makeSpan("|", r.linesAfter, " " * self.colCount)
            for c in range(r.lineCount()):
                p.write(hSpan)
            p.write(makeSpan("+", "+" * self.colCount, self.lineBelow[i]))

        # Now write the contents, to each cell in turn.
        rCoord = 0
        for rowIdx, row in enumerate(self.rows):
            rCoord += 1
            c = 0
            for colIdx, ent in enumerate(row.cells):
                c += 1
                r = rCoord
                for line in ent:
                    p.gotoRowColumn(r, c)
                    p.write(line)
                    r += 1
                c += self.widths[colIdx]
            rCoord += row.lineCount()
        return p.asStr()
