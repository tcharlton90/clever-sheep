#!/usr/bin/env python
"""Support for rendering pages in memory.

This module provides the `Page` class, which allows a page to be drawn as
if to a cell addressable screen.

"""



class Page(object): #pragma: unsupported
    """A cell-addressable page.

    This acts like a ``file`` object, but is also effectively organised
    like a grid of characters. You can `write` as if to a file, but
    also used `gotoRowColumn` to seek to a characeter position.

    :Ivar lines:
        The lines of text that have been written so far.
    :Ivar pos:
        The current character position as a tuple of ``(r, c)``.

    """
    def __init__(self):
        self.lines = []
        self.pos = (0, 0)

    def gotoRowColumn(self, r, c):
        """Position to a specific character.

        The next `write` or `writeLine` operation will start from this
        position.

        """
        self.pos = (r, c)

    def writeLine(self, s):
        """Write a single line to the page, starting at the current position.

        This writes the line at the current position. If the position is
        currently beyond the end of the current line then it is first extended
        with first.

        If `s` does not hae a trailing newline character then `pos` is left on
        the same line. Otherwise, hte `pos` is move to the firs column of the
        next line of the page.

        :Param s:
            The single line. This *must not* contain any embedded newline
            characters. However, it may have a single trailing newline
            character.

        """
        r, c = self.pos

        while r + 1 > len(self.lines):
            self.lines.append("")
        line = self.lines[r]

        lLen = len(line)
        if lLen < c:
            line += " " * (c - lLen)

        haveNewline = False
        if s[-1] == "\n":
            haveNewline = True
            s = s[:-1]

        self.lines[r] = line[:c] + s + line[c + len(s):]
        c += len(s)
        if haveNewline:
            c = 0
            r += 1
        self.pos = (r, c)

    def write(self, s):
        """Write a string to the page.

        The `writeLine` method is used to write each line in `s` to the
        page.

        :Param s:
            The string to write. This may contain new line characters.
        """
        if not s:
            return
        for l in s.splitlines(1):
            self.writeLine(l)

    def asStr(self):
        """Return the who page as a string.

        :Return:
            All the lines, joined using newlines.
        """
        return "\n".join(self.lines)
