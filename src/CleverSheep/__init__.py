#!/usr/bin/env python
"""A collection of re-usable packages.

This is the top-level package for various other general purpose packages. It
exists in order keep the other packages tidily hidden within a single name
space. The sub-packages available are:

`Prog`
    Provides various general programming tools.

`TTY_Utils`
    Provides utilities assocated with logging to terminals and related
    activities.

`Test`
    Provides general support for testing.

`Debug`
    Some debug aids.

`Log`
    Useful things for logging support.

`VisTools`
    Tools to help visualisation. Most notably, at the moment,
    the sequence chart drawing module.

`IOUtils`
    Provides things like clever redirection.

The "Clever Sheep" was called Harold.

"""


# Try to import the version string but don't fail if we can't
try:
    from version import version_string
except:
    pass

# Todo what is this? Is it used?
magic = ("This is magic")
