#!/usr/bin/env python
"""Deprecated version of Process.

Use CleverSheep.Prog.Process rather than CleverSheep.VisTools.Process.
"""


import os
import warnings
import inspect

csPath = os.path.join("CleverSheep", "Test", "Tester")
for count, frame in enumerate(inspect.stack()):
    if count == 0:
        continue
    if csPath not in frame[1]:
        break

warnings.warn("""
The 'CleverSheep.VisTools.Process' module is deprecated. It will nolonger be
supported from version 0.5 onwards and will be removed in version 0.6. Please
use the 'CleverSheep.Prog.Process' instead.
----------------------------------------------------------------------
""", PendingDeprecationWarning, stacklevel=count + 1)

from CleverSheep.Prog.Process import *
