"""Deleted code, which I have not yet had the corouge to completely remove.

"""


# TODO: This should be in CleverSheep
class Enum(int):
    _known = {}
    def __new__(cls, *args):
        if len(args) == 3:
            name, v, altName = args
        else:
            name, v = args
        if name in Enum._known:
            inst = Enum._known[name]
            assert inst == v
            return inst

        try:
            v = args[1]
        except IndexError:
            v = args[0]
        inst = super(Enum, cls).__new__(cls, v)
        Enum._known[name] = inst
        return inst

    def __init__(self, name, value, altName=None):
        self.name = name
        self.altName = altName or name

    def __repr__(self):
        return "Enum(%r, %s)" % (self.name, super(Enum, self).__str__())

    def __str__(self):
        return self.name

    def __getnewargs__(self):
        return self.name, int(self)


#: Test states. Any test or suite can be in one of these states. In the
#: case of a suite, the state depends on its children's states.
#:
NOT_RUN            = Enum("NOT_RUN", 0)
PASS               = Enum("PASS", 1)
FAIL               = Enum("FAIL", 2)
ERROR              = Enum("ERROR", 3)
BAD_SETUP          = Enum("BAD_SETUP", 4)
BAD_TEARDOWN       = Enum("BAD_TEARDOWN", 6)
CHILD_FAIL         = Enum("CHILD_FAIL", 7)
INCOMPLETE         = Enum("INCOMPLETE", 8)
SKIPPED            = Enum("SKIPPED", 9)
DISABLED           = Enum("DISABLED", 10)
BROKEN             = Enum("BROKEN", 11)
BAD_SUITE_SETUP    = Enum("BAD_SUITE_SETUP", 12, "BAD_SETUP")
BAD_SUITE_TEARDOWN = Enum("BAD_SUITE_TEARDOWN", 13, "BAD_TEARDOWN")
SUITE_RUNNING      = Enum("SUITE_RUNNING", 14)
