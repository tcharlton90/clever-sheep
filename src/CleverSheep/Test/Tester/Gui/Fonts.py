#!/usr/bin/env python
"""Some font munging support.

This module is used to provide a consistent appearance within the Gui.
The principle function is `getFont`, which takes a generic name, such as
``"normal"`` and returns a wx.Font.
"""


import wx

_fontSet = {}

def _selectFonts():
    fixedFamily, propFamily = None, None
    for family in (wx.FONTFAMILY_DEFAULT, wx.FONTFAMILY_DECORATIVE,
                   wx.FONTFAMILY_ROMAN, wx.FONTFAMILY_SCRIPT,
                   wx.FONTFAMILY_SWISS, wx.FONTFAMILY_MODERN,
                   wx.FONTFAMILY_TELETYPE):
        font = wx.Font(10, family, 
                wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL)
        if font.IsFixedWidth():
            if fixedFamily is None:
                fixedFamily = family
        else:
            if propFamily is None:
                propFamily = family
    if not fixedFamily:
        fixedFamily = wx.FONTFAMILY_SWISS
        print("No fixed width font found")

    _fontSet["normalFont"] = wx.Font(8, propFamily, 
            wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL)
    _fontSet["boldFont"] = wx.Font(8, propFamily, 
            wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)
    _fontSet["fixedNormalFont"] = wx.Font(8, fixedFamily, 
            wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL)
    _fontSet["fixedBoldFont"] = wx.Font(8, fixedFamily, 
            wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)


def getFont(name):
    if not _fontSet:
        _selectFonts()
    return _fontSet.get(name, _fontSet["normalFont"])

