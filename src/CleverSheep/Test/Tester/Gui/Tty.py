#!/usr/bin/env python
"""Terminal redirection support for the Gui.

<+Detailed multiline documentation+>
"""


import wx

myEVT_ADD_TEXT = wx.NewEventType()
EVT_ADD_TEXT = wx.PyEventBinder(myEVT_ADD_TEXT, 1)


class AddTextEvent(wx.PyCommandEvent):
    def __init__(self, evtType, id, text, fg=None, bg=None, bold=False):
        wx.PyCommandEvent.__init__(self, evtType, id)
        self.fg = fg
        self.bg = bg
        self.bold = bold
        self.text = text


class GuiColourStream(object):
    def __init__(self, console, **kwargs):
        self.console = console
        self.fg = kwargs.get("fg", "black")
        self.bg = kwargs.get("bg", "white")
        self.bold = kwargs.get("bold", False)
        self.l = open("raw.log", "wb")

    def write(self, s):
        self.l.write(s)
        self.l.flush()
        event = AddTextEvent(myEVT_ADD_TEXT, 0, s, fg=self.fg, bg=self.bg,
                bold=self.bold)
        wx.PostEvent(self.console, event)
        return


class Tty(object):
    def __init__(self, console, **kwargs):
        self.console = console
        self.softspace = 0
        self.dead = False

    def write(self, s):
        if self.dead:
            return
        event = AddTextEvent(myEVT_ADD_TEXT, 0, s)
        wx.PostEvent(self.console, event)

    def setStyle(self, **kwargs):
        if self.dead:
            return
        event = AddTextEvent(myEVT_ADD_TEXT, 0, None, 
                fg=kwargs.get("fg", None), 
                bg=kwargs.get("bg", None), 
                bold=kwargs.get("bold", None))
        wx.PostEvent(self.console, event)

    def resetStyle(self):
        if self.dead:
            return
        event = AddTextEvent(myEVT_ADD_TEXT, 0, None, 
                fg="black", bg="white", bold=False)
        wx.PostEvent(self.console, event)

    def flush(self):
        pass

    def close(self):
        self.dead = True

    def getDims(self):
        return 24, 80

    def __getattr__(self, name):
        import sys
        raise AttributeError(name)


