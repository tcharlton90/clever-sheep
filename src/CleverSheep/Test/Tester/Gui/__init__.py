#!/usr/bin/env python
"""A GUI for the tester framework.

<+Detailed multiline documentation+>
"""



import sys
import textwrap
import time

import wx

from CleverSheep.Test import Tester

# Local Gui imports
import Tty
import Keys
import Fonts
import TestIf

def makeItemText(testObj):
    title = Tester.getDocSummary(Tester.getDocLines(testObj.doc))
    state = testObj.getState()
    cmdLineNumber = getattr(testObj, "cmdLineNumber", None)
    if cmdLineNumber is not None:
        text = "[%-12s] %-4d: %s" % (state, cmdLineNumber, title)
    else:
        text = "[%-12s]: %s" % (state, title)
    return text


class Visitor(object):
    def __init__(self, tree):
        self.tree = tree
        self.stack = []
        
    def enterSuite(self, suite):
        if not self.stack:
            child = self.tree.AddRoot(makeItemText(suite))
        else:
            child = self.tree.AppendItem(self.stack[-1], makeItemText(suite))
        self.tree.SetPyData(child, suite)
        self.stack.append(child)

    def leaveSuite(self, suite):
        self.stack.pop()

    def enterTest(self, test):
        child = self.tree.AppendItem(self.stack[-1], makeItemText(test))
        self.tree.SetPyData(child, test)

    def leaveTest(self, test):
        pass
    
    def getTestTitle(self, test):
        docLines = Tester.getDocLines(test.doc)
        title = Tester.getDocSummary(docLines)
        state = test.getState()
        text = "[%-12s] %-4d: %s" % (state, test.cmdLineNumber, title)
        return text


def describeItem(item):
    if item.IsOk():
        try:
            testObj = self.GetPyData(item)
            return testObj.uid()[-1]
        except TypeError:
            return "<NO DATA>"
    return "<END>"


class Tree(wx.TreeCtrl):
    def __init__(self, parent, statusBar, *args, **kwargs):
        self.statusBar = statusBar
        wx.TreeCtrl.__init__(self, parent, *args, **kwargs)
        self.SetOwnFont(Fonts.getFont("normalFont"))
        self.Bind(wx.EVT_TREE_KEY_DOWN, self.onKey)
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.onSelectionChanged)

    def children(self, item):
        def _iter(child, wxCookie):
            first = child
            while True:
                if not child.IsOk():
                    return
                yield child
                child = self.GetNextSibling(child)
        child, wxCookie = self.GetFirstChild(item)
        return _iter(child, wxCookie)

    def allChildren(self, item=None):
        if item is None:
            item = self.GetRootItem()
            yield item
        for child in self.children(item):
            yield child
            for grandChild in self.allChildren(child):
                yield grandChild

    def findItemById(self, uid):
        for item in self.allChildren():
            testObj = self.GetPyData(item)
            if testObj.uid() == uid:
                return item

    def onSelectionChanged(self, event):
        item = event.GetItem()
        testObj = self.GetPyData(item)
        if not testObj:
            self.statusBar.SetStatusText("", 0)
        else:
            self.statusBar.SetStatusText(str(testObj.uid()), 0)

    def onKey(self, event):
        key = Keys.event2keyName(event.GetKeyEvent())
        # Note that event.GetItem() will not work!
        item = self.GetSelection()

        if not item.IsOk():
            return
        if key == "Ctrl-O":
            self.Expand(item)
        elif key == "Ctrl-C":
            self.Collapse(item)
        elif key == "Ctrl-T":
            self.Toggle(item)
        elif key == "Ctrl-RIGHT":
            self.Expand(item)
            child, it = self.GetFirstChild(item)
            self.SelectItem(child)
        elif key == "Ctrl-LEFT":
            if self.IsExpanded(item):
                self.Collapse(item)
            else:
                parent = self.GetItemParent(item)
                if not parent:
                    parent = item
                self.Collapse(parent)
                self.SelectItem(parent)
        else:
            event.Skip()


class Console(wx.TextCtrl):
    def __init__(self, parent, *args, **kwargs):
        style = kwargs.get("style", 0)
        style |= wx.TE_MULTILINE | wx.HSCROLL | wx.TE_READONLY
        kwargs["style"] = style
        wx.TextCtrl.__init__(self, parent, *args, **kwargs)
        self.SetBackgroundColour("White")
        self.Bind(Tty.EVT_ADD_TEXT, self.onAddText)
        self.initFont()

    def initFont(self):
        self.normalFont = Fonts.getFont("fixedNormalFont")
        self.boldFont = Fonts.getFont("fixedBoldFont")
        self.SetDefaultStyle(wx.TextAttr(font=self.normalFont))

    def onAddText(self, event):
        if event.text is not None:
            self.AppendText(event.text)
            return

        font = self.normalFont
        fg = event.fg or wx.NullColour
        bg = event.bg or wx.NullColour
        if event.bold:
            font = self.boldFont
        self.SetDefaultStyle(wx.TextAttr(font=font, colText=fg, colBack=bg))


class Frame(wx.Frame):
    def __init__(self, parent, *args, **kwargs):
        wx.Frame.__init__(self, parent, *args, **kwargs)

        self.makeMenu()
        self.makeStatus()
        self.setupShortCuts()

        tree = self.tree = Tree(self, self.GetStatusBar(),
               style=wx.TR_HAS_BUTTONS) # | wx.TR_EDIT_LABELS)
        console = self.console = Console(self, size=(600, 600))

        sizer = self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(tree, proportion=45, flag=wx.EXPAND)
        sizer.Add(console, proportion=55, flag=wx.EXPAND)

        self.SetSizer(sizer)
        self.Fit()

        self.Bind(wx.EVT_CLOSE, self.onClose)
        self.Bind(TestIf.EVT_TEST_STATE_CHANGED, self.onTestStateChanged)

        # We have a set of consoleStreams.
        self.tty = Tty.Tty(console)
        self._firstActivation = True

    def setKeepFocus(self):
        self.focusTimer = wx.Timer(self)
        self.focusTimer.Start(50)
        self._focusCheckStart = time.time()
        self.Bind(wx.EVT_TIMER, self.onFocusTimer, self.focusTimer)

    def onFocusTimer(self, event):
        delay = time.time() - self._focusCheckStart
        w = wx.Window_FindFocus()
        if w is not None and delay < 3.0:
            return
        if delay < 3.0:
            self.Raise()
        self.Unbind(wx.EVT_TIMER, self.focusTimer)
        self.focusTimer.Stop()
        del self.focusTimer

    def onTestStateChanged(self, event):
        if event.testId is not None:
            item = self.tree.findItemById(event.testId)
            if item is not None:
                testObj = self.tree.GetPyData(item)
                if testObj is not None:
                    self.tree.SetItemText(item, makeItemText(testObj))
                    self.tree.EnsureVisible(item)
        if event.runState is not None:
            self.GetStatusBar().SetStatusText(event.runState, 1)

    def notifyTestStateChanged(self, testId, oldState, newState):
        event = TestIf.TestStateChangedEvent(testId=testId, 
                oldState=oldState, newState=newState)
        wx.PostEvent(self.tree, event)

    def makeStatus(self):
        # Set up the status bar
        statusBar = self.CreateStatusBar(2)

    def setupShortCuts(self):
        # Arrange to handle short-cut keys.
        self.Bind(wx.EVT_KEY_DOWN, self.onKey)

    def makeMenu(self):
        # Set up the menu
        menuBar = wx.MenuBar()
        fileMenu = wx.Menu()
        menuBar.Append(fileMenu, "&File")
        iQuit = fileMenu.Append(wx.NewId(), "&Quit")
        self.SetMenuBar(menuBar)

        self.Bind(wx.EVT_ACTIVATE, self.onActivate)
        self.Bind(wx.EVT_MENU, self.onClose, iQuit)

    def onActivate(self, event):
        # Force focus onto the tree so that we can handle keybard short-cuts.
        if not self._firstActivation:
            return
        self._firstActivation = False
        event.Skip()
        if wx.Window_FindFocus() in (self, None):
            self.tree.SetFocus()
        self.testThread = TestIf.TestThread(self, self.suite, self.options)
        if not self.options.get_option_value("skip_run"):
            self.testThread.start()

    def onKey(self, event):
        key = Keys.event2keyName(event)
        if key == "Ctrl-Q":
            self.onClose(event)
        event.Skip()

    def onClose(self, event):
        # print("Closing...")
        dlg = wx.MessageDialog(self, "Quit Application", caption="Quit",
                style=wx.OK | wx.CANCEL, pos=wx.DefaultPosition)
        result = dlg.ShowModal()
        if result != wx.ID_OK:
            # event.Veto()
            return
        self.tty.close()
        self.Destroy()


class App(wx.App):
    def OnInit(self):
        frame = self.frame = Frame(parent=None, title="CleverSheep Tester")
        frame.Show()
        self.SetTopWindow(frame)
        return True

    def OnExit(self):
        # print("Exiting...")
        pass

    def fixConsoleStream(self, name):
        stream = getattr(Tester, name)
        newStream = Tty.GuiColourStream(self.frame.console, **stream.args)
        setattr(Tester, name, newStream)
        stream = getattr(Tester, name)


def init(suite, options):
    """The main for this module.

    <+Details+>
    """
    global app
    app = App()
    visitor = Visitor(app.frame.tree)
    suite.walk(visitor)
    root = app.frame.tree.GetRootItem()
    app.frame.tree.Expand(root)
    app.suite = suite
    app.frame.suite = suite
    app.frame.options = options

    Tester.registerForTestStateChanged(app.frame.notifyTestStateChanged)
    return app


def start():
    """Called to start the Gui, i.e. run the main loop."""
    app.MainLoop()


def keepFocus():
    """Tells the GUI to hang on to the focus.

    This ca be used in the test thread to let the GUI know that it will shortly
    lose focus (for example because a new window is being created). The Gui will
    monitor its focus for a short while and, if it loses it, try to force focus
    back and keep the GUI as the top window.
    """
    app.frame.setKeepFocus()

