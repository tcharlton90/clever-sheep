#!/usr/bin/env python
"""The cs command library.

This package contains all the code for the cs sub-commands.

"""



import os
import sys
import glob
import optparse

from CleverSheep.Prog.Curry import Curry

SHOW_HELP = "HELP"


def do_cst_version(context, args):
    """The cs version sub-command.

    Usage: cs version

    """
    import CleverSheep
    print CleverSheep.versionStr


def do_cst_help(context, args):
    """The cs help sub-command.

    Usage: cs help [cmd]

    With no arguments, prints a list of the defined commands.
    With an argument, prints more detailed help for that command.

    The cmd may be abbreviated.

    """
    if not args:
        subCmds = getSubCommands(context.namespace)
        for name in subCmds:
            print(name)
        return

    subCommand = getSubCommand(args[0])
    subCommand.parse([subCommand.name, "-h"])


def do_cst_run(context, args):
    """Run a test script

    Usage: TODO

    """
    name, ext = os.path.splitext(args[0])
    print os.getcwd()
    for p in sys.path:
        print "   ", p
    mod = __import__(name)
    print mod


def getUsageFromDoc(doc):
    """Use a docstring to create a usage message.

    The first line of the docstring is a description which will be ignore.
    The second line should be blank, to conform to normal Python coding
    conventions.

    The third line is the start of the usage string.

    Trailing blanks lines are stripped.

    """
    lines = doc.splitlines()[2:]
    while lines and not lines[-1].strip():
        lines.pop()
    return "\n".join(lines)


class SubCommand(object):
    """Holds all details of a sub-command.

    This is a base class, which is no intended to be used directly.
    See `ModSubCommand` and `FuncSubCommand` for concrete versions.

    """
    def __init__(self, name, inst, context):
        """Constructor:

        :Parameters:
          name
            The sub-command name.
          inst
            The object that provides the sub-command function.
            Details TODO.
          context
            The context for the sub-command.
            Details TODO.

        """
        self.name = name
        self.context = context
        self.usage = getUsageFromDoc(inst.__doc__)
        if hasattr(inst, "makeParser"):
            self.parser = inst.makeParser()
        else:
            self.parser = optparse.OptionParser(self.usage)

    def parse(self, args):
        options, args = self.parser.parse_args(args)
        ret = self.execute(args)
        if ret is SHOW_HELP:
            self.parser.print_help()


class ModSubCommand(SubCommand):
    def __init__(self, name, _ignore, context):
        mod = __import__("CleverSheep.Test.Tester.Cst.%s" % name,
                globals(), locals(), [name])
        super(ModSubCommand, self).__init__(name, mod, context)
        self.execute = Curry(mod.execute, self.context)


class FuncSubCommand(SubCommand):
    def __init__(self, name, inst, context):
        super(FuncSubCommand, self).__init__(name, inst, context)
        self.execute = Curry(inst, self.context)


class Context(object):
    def __init__(self, namespace):
        self.namespace = namespace


def getSubCommands(namespace):
    """Get all the sub-command names at this level.

    """
    subCommands = {}

    # Look look for locally defined do_cst_... functions.
    prefix = "do_cst_"
    for name, value in namespace.iteritems():
        if not name.startswith(prefix) or not hasattr(value, "__call__"):
            continue
        subCommands[name[7:]] = FuncSubCommand, value

    # Look for python modules
    for name in glob.glob(os.path.join(os.path.dirname(__file__), "*.py")):
        base = os.path.basename(name)
        if base == "__init__.py":
            continue
        if base[:-3] in subCommands:
            continue
        subCommands[base[:-3]] = ModSubCommand, None

    return subCommands


def getSubCommand(cmd, namespace=None):
    """Get a unique sub-command at this level.

    The name may be abbreviated provided that it is unique within the current
    context.

    """
    if namespace is None:
        namespace = globals()
    context = Context(namespace)
    subCommands = getSubCommands(namespace)
    candidates = [name for name in subCommands if name.startswith(cmd)]
    if not candidates:
        sys.exit("Unrecognised command %r" % cmd)
    elif len(candidates) > 1:
        sys.exit("Ambiguous command %r, candidates are: %s" % (
            cmd, " ".join(f.func_name[7:] for f in candidates)))

    name = candidates[0]
    klass, value = subCommands[name]
    return klass(name, value, context)


def handleComposite(args, context=None):
    context = context or globals()
    cmd, args = args[0], args[1:]
    subCommand = Cst.getSubCommand(cmd)
    subCommand.parse(args)




# BELOW THIS IS HOPEFULLY DEAD
if 0:
    # TODO: Got several of these now!
    def _getCallerDict(level=2):
        frame = inspect.stack()[level][0]
        try:
            return frame.f_globals
        finally:
            del frame


    def _getCallerName(level=2):
        return inspect.stack()[level][3]


    def xmakeParser():
        d = _getCallerDict()
        funcName = _getCallerName()
        func = d[funcName]
        help = inspect.getdoc(func) % {"func": funcName[7:]}
        return optparse.OptionParser(help)
