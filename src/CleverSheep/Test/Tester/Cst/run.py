"""The run sub-command.

Usage: run [run-options] [test-script]

"""



import os
import optparse

from CleverSheep.Test.Tester import Execution


def execute(context, args):
    if not args or args[0].startswith("-"):
        allTests = "all_tests.py"
        if not os.path.exists(allTests):
            sys.exit("You need an all_test.py script - sorry!")
        os.execv("./%s" % allTests, [allTests] + args)
    else:
        os.execv("./%s" % args[0], args)


def runHelp(option, opt, value, parser):
    help = parser.format_help()
    print(help)
    parser.exit()


def makeParser():
    parser = optparse.OptionParser(__doc__)
    parser.disable_interspersed_args()
    parser.set_conflict_handler("resolve")

    parser.add_option("-h", "--help", action="callback",
            callback=runHelp,
        help="Dump a summary of the loaded tests")
    Execution.addCommonOptions(parser)
    Execution.addDebugOptions(parser)
    Execution.addDocumentationOptions(parser)
    Execution.addDelimOption(parser)

    return parser
