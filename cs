#!/usr/bin/env python
"""CleverSheep test framework driver tool.

This tool can be used to run tests, get information about tests and configure
the way tests run.

This was added to CleverSheep in version 0.5 when it became clear that the
the set of automatic options was starting to become a bit unwieldy.

"""



import sys
import optparse

from CleverSheep.Test.Tester import Cst


def main(args):
    """The main command execution dispatch point.

    Treats the first argument as a sub-command and everything else as
    options/arguments for the sub-command.

    """
    cmd, args = args[0], args[1:]
    subCommand = Cst.getSubCommand(cmd)
    subCommand.parse(args)


usage = """Usage: %prog [glob-opts] command [cmd-opts] [cmd-args]

Useful starting points:

   %prog help
       List all availaible commands

   %prog help command
       Show help for a given command.
"""

if __name__ == "__main__":
    parser = optparse.OptionParser(usage)
    parser.disable_interspersed_args()
    options, args = parser.parse_args()
    if len(args) < 1:
        parser.print_help()
        sys.exit(0)
    
    main(args)
