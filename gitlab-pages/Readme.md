This folder contains a templates dir which contains template html pages, these are copied into generated and
values such as the pylint score inserted before then being hosted via gitlab pages. See
 https://lcaraccio.gitlab.io/clever-sheep/ for the pages themselves.