#!/usr/bin/env bash
# Script to run the CleverSheep tests with code coverage

# Trap any error so we can return an error status
err=0
trap "err=1" ERR

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

# cd into this folder so we know where we are
cd $BASEDIR

# Go into the test folder
pushd ../test

# We want to ignore the test folder files as some of the files generated and
# then deleted during the tests cause issues for the coverage command
IGNORE_PATH="$(pwd)"

# Run the tests with coverage
coverage erase
coverage run --omit="$IGNORE_PATH/*"  all_tests.py --keep-going
coverage combine
coverage report | tee $BASEDIR/generated_files/coverage_report.txt
coverage html -d $BASEDIR/generated_files/coverage_html

popd

# Store the coverage summary for later use
summary_line="$(tail -1 $BASEDIR/generated_files/coverage_report.txt)"
coverage_percent="$(echo $summary_line | awk '{print $NF}')"

echo -n $coverage_percent > $BASEDIR/generated_files/coverage_percent.txt

# Return non-zero if any command in the script has failed
test $err -eq 0