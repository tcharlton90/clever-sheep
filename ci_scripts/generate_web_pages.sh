#!/usr/bin/env bash

# Trap any error so we can return an error status
err=0
trap "err=1" ERR

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

# cd into this folder so we know where we are
cd $BASEDIR

# Store the plyint score for later use
PYLINT_SCORE=$(<$BASEDIR/generated_files/pylint_score.txt)

# Store the number of pep8 issues for later use
PEP8_ISSUES=$(<$BASEDIR/generated_files/pep8_issue_count.txt)

# Store the coverage percent for later use
COVERAGE_PERCENT=$(<$BASEDIR/generated_files/coverage_percent.txt)

# Copy the templates into place
pushd ../gitlab-pages > /dev/null
cp templates/* generated/

# Set the coverage percent
sed -i "s/COVERAGE_PERCENT/$COVERAGE_PERCENT/g" generated/index.html

# Set the pylint score
sed -i "s/PYLINT_SCORE/$PYLINT_SCORE/g" generated/index.html

# Set the number of pep8 issues
sed -i "s/PEP8_ISSUES/$PEP8_ISSUES/g" generated/index.html

# Set the last updated time
LAST_UPDATED="$(date)"

sed -i "s/LAST_UPDATED/$LAST_UPDATED/g" generated/index.html

mkdir -p api

pushd api > /dev/null
# Generate API docs

# Clean the API directory
rm -rf ./*

# Generate api and config blacklist
DIRS=`find ../../src/CleverSheep/ -type d` 
APIBLACKLIST=()
CONFBLACKLIST=()
CONFBLACKLIST+="'_build'"
CONFBLACKLIST+=", 'Thumbs.db'"
CONFBLACKLIST+=", '.DS_Store'"
for file in `find ../../src/CleverSheep -regex .*.py`; do
  if ! grep -q $file ../../ci_scripts/api_whitelist.txt; then
    CONFBLACKLIST+=", '$file'"
    APIBLACKLIST+=" $file"
  fi
done

for dir in $DIRS; do
  if ! grep -q $dir ../../ci_scripts/api_whitelist.txt; then
    CONFBLACKLIST+=", '$dir'"
    APIBLACKLIST+=" $dir"
  fi
done

# Generate config
sphinx-quickstart -q -p "CleverSheep" -a "L Caraccio" -v "" --ext-autodoc --ext-coverage --makefile --no-batchfile
sed -i "s|# import os|import os\nimport sys\nsys.path.insert(0, os.path.abspath('../../src'))|" conf.py
sed -i "s|'_build', 'Thumbs.db', '.DS_Store'|$CONFBLACKLIST|" conf.py
sed -i "s|alabaster|classic|" conf.py
sed -i "s|html_sidebars|not_used|" conf.py
 
# Generate restructed text files
sphinx-apidoc -o ./ ../../src/CleverSheep/ $APIBLACKLIST 

# Generate html and copy to appropriate location
make html
cp -r _build/html ../generated/api

popd > /dev/null

popd > /dev/null

# Copy the coverage html into place
cp -r $BASEDIR/generated_files/coverage_html ../gitlab-pages/generated/

# Return non-zero if any command in the script has failed
test $err -eq 0
