#!/usr/bin/env bash

# Trap any error so we can return an error status
err=0
trap "err=1" ERR

touch ~/.pylintrc

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

$BASEDIR/pylint_checker.py $BASEDIR/../src/CleverSheep

# Return non-zero if any command in the script has failed
test $err -eq 0