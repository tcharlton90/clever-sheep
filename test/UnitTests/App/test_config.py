#!/usr/bin/env python
"""Test the classes defined in Config.py"""

import mock

from CleverSheep.Test.Tester import Suite
from CleverSheep.Test.Tester import test
from CleverSheep.Test.Tester import runModule
from CleverSheep.Test.Tester import failUnless
from CleverSheep.Test.Tester import failUnlessEqual

from CleverSheep.App import Config


class ConfigManagerTestSuite(Suite):
    """Test the ConfigManager behaviour"""

    @test
    def test_has_option_unknown_arg(self):
        """Test that when an has_option is called for an unknown argument False
        is returned"""
        # Arrange
        config_manager = Config.ConfigManager()

        # Act
        has_option = config_manager.has_option("unknown_option")

        # Assert
        failUnlessEqual(False, has_option)

    @test
    def test_has_option_default(self):
        """Test that when an has_option is called for an argument that has had
        a default set True is returned"""
        # Arrange
        option = "default_option"
        config_manager = Config.ConfigManager()
        config_manager.set_default_for_option(option, 1)

        # Act
        has_option = config_manager.has_option(option)

        # Assert
        failUnlessEqual(True, has_option)

    @test
    def test_has_option_config_file(self):
        """Test that when an has_option is called for an argument that has been
        set via a config file True is returned"""
        # Arrange
        option = "config_file_option"
        config_manager = Config.ConfigManager()

        config_file_options = Config.ConfigFile()
        setattr(config_file_options, option, 1)
        config_manager.set_config_file_options("/path/to/file/",
                                               config_file_options)

        # Act
        has_option = config_manager.has_option(option)

        # Assert
        failUnlessEqual(True, has_option)

    @test
    def test_has_option_command_line(self):
        """Test that when an has_option is called for an argument that has been
        set via the command line True is returned"""
        # Arrange
        option = "cmd_line_option"
        config_manager = Config.ConfigManager()

        cmd_line_option_manager = mock.MagicMock()
        cmd_line_option_manager.has_arg.return_value = True

        config_manager.set_command_line_option_manager(cmd_line_option_manager)

        # Act
        has_option = config_manager.has_option(option)

        # Assert
        cmd_line_option_manager.has_arg.assert_called_once_with(option)
        failUnlessEqual(True, has_option)

    @test
    def test_has_option_programmatically(self):
        """Test that when an has_option is called for an argument that has been
        explicitly set True is returned"""
        # Arrange
        option = "programmatically_set_option"
        config_manager = Config.ConfigManager()
        config_manager.set_option(option, 1)

        # Act
        has_option = config_manager.has_option(option)

        # Assert
        failUnlessEqual(True, has_option)

    @test
    def test_get_option_value_no_value_found(self):
        """Test that if an option for the requested value can't be found None
         is returned"""
        # Arrange
        config_manager = Config.ConfigManager()

        # Act
        value = config_manager.get_option_value("unknown_option")

        # Assert
        failUnlessEqual(None, value)

    @test
    def test_get_option_value_no_value_found_passed_in_default(self):
        """Test that if an option for the requested value can't be found but a
         default has been given to use if it can't be found this default is
         returned"""
        # Arrange
        config_manager = Config.ConfigManager()
        passed_in_default = 99

        # Act
        value = config_manager.get_option_value("unknown_option",
                                                default=passed_in_default)

        # Assert
        failUnlessEqual(passed_in_default, value)

    @test
    def test_get_option_value_for_default_value(self):
        """Test that if a default value is found for the option this value is
         returned"""
        # Arrange
        option = "valid_option"
        expected_value = 1
        config_manager = Config.ConfigManager()
        config_manager.set_default_for_option(option, expected_value)

        # Act
        value = config_manager.get_option_value(option)

        # Assert
        failUnlessEqual(expected_value, value)

    @test
    def test_get_option_value_for_config_file_value(self):
        """Test that if a config file value is found for the option this value
         is returned overriding the default"""
        # Arrange
        option = "valid_option"
        default_value = 1
        config_file_value = 2
        config_file_options = Config.ConfigFile()
        setattr(config_file_options, option, config_file_value)

        config_manager = Config.ConfigManager()
        config_manager.set_default_for_option(option, default_value)

        config_manager.set_config_file_options("/path/to/file",
                                               config_file_options)

        # Act
        value = config_manager.get_option_value(option)

        # Assert
        failUnlessEqual(config_file_value, value)

    @test
    def test_get_option_value_for_cmd_line_value(self):
        """Test that if a command line value is found for the option this value
         is returned overriding the default and config file values"""
        # Arrange
        option = "valid_option"
        default_value = 1
        config_file_value = 2
        cmd_line_value = 3

        config_file_options = Config.ConfigFile()
        setattr(config_file_options, option, config_file_value)

        cmd_line_option_manager = mock.MagicMock()
        cmd_line_option_manager.has_arg.return_value = True
        cmd_line_option_manager.get_arg_value.return_value = cmd_line_value

        config_manager = Config.ConfigManager()
        config_manager.set_default_for_option(option, default_value)

        config_manager.set_config_file_options("/path/to/file",
                                               config_file_options)

        config_manager.set_command_line_option_manager(cmd_line_option_manager)

        # Act
        value = config_manager.get_option_value(option)

        # Assert
        cmd_line_option_manager.has_arg.assert_called_once_with(option)
        cmd_line_option_manager.get_arg_value.assert_called_once_with(option)
        failUnlessEqual(cmd_line_value, value)

    @test
    def test_get_option_value_for_explicit_value(self):
        """Test that if an explicit value is found for the option this value
         is returned overriding the default, config file and command line
         values"""
        # Arrange
        option = "valid_option"
        default_value = 1
        config_file_value = 2
        cmd_line_value = 3
        explicit_value = 4

        config_file_options = Config.ConfigFile()
        setattr(config_file_options, option, config_file_value)

        cmd_line_option_manager = mock.MagicMock()
        cmd_line_option_manager.has_arg.return_value = True
        cmd_line_option_manager.get_arg_value.return_value = cmd_line_value

        config_manager = Config.ConfigManager()
        config_manager.set_default_for_option(option, default_value)

        config_manager.set_config_file_options("/path/to/file",
                                               config_file_options)

        config_manager.set_command_line_option_manager(cmd_line_option_manager)

        config_manager.set_option(option, explicit_value)

        # Act
        value = config_manager.get_option_value(option)

        # Assert
        failUnlessEqual(explicit_value, value)

    @test
    def test_using_the_global_config(self):
        """Test using the global Config instance."""
        c = Config.config_manager
        failUnless(c.get_option_value("fred") is None)

        # Just verify that we can set a value
        c.set_option("fred", 10)
        failUnlessEqual(10, c.get_option_value("fred"))

        # Re-initialise the module
        Config.config_manager.reset()


if __name__ == "__main__":
    runModule()
