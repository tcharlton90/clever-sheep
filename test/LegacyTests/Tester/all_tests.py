#!/usr/bin/env python
"""Tests for the Tester module itself.

This is naturally a bit weird because these tests are intended to test the
very thing that is running the tests. However, it should be possible for the
CleverSheep test framework to be self hosting.

"""

import CheckEnv

from CleverSheep.Test import Tester

_exclDirs_ = ["mocktests"]

if __name__ == "__main__":
    Tester.runTree()
