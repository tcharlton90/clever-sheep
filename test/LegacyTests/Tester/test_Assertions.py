#!/usr/bin/env python
"""CleverSheep.Test.Tester.Assertions unit tests"""

import os
import re
import sys
import struct

import CheckEnv
import support

from CleverSheep.Test.Tester import *
from CleverSheep.Test.Tester import Errors
from CleverSheep.Test import DataMaker

from TestSupport.Files import FileBasedTests, ensureNoFile

# The module under test.
module_under_test = "../../CleverSheep/Test/Tster/Assertions.py"
from CleverSheep.Test.Tester import Assertions


class Bang(Exception):
    """Bang!"""

class Pop(Exception):
    """Pop!"""


def go_bang(a, b=3):
    raise Bang("Bang %s %s" % (a, b))

def go_pop(a, b=3):
    raise Pop("Pop %s %s" % (a, b))

def go_nop(a, b=3):
    return


class Test_CoreAssertions(Suite):
    """Tests for the core assertion functions."""
    @test
    def fail_unlessRaises(self):
        """Test the failUnlessRaises assertion.

        We test this first, because the remaining tests rather depend on it.
        """
        # Check we get the exception for a passing test.
        exc = failUnlessRaises(Bang, go_bang, 1, b=5)
        failUnlessEqual("Bang 1 5", str(exc))

        # Check we detect failure to raise the exception. For this we need to
        # explicitly handle the Errors.Failure exception.
        try:
            failUnlessRaises(Bang, go_nop, 1, b=5)
        except Errors.Failure as exc:
            pass
        else:
            raise fail("The failUnlessRaises did not detect failure!")
        failUnlessEqual(
            "failUnlessRaises: Did not raise expected 'Bang' exception",
            str(exc))

        # Check we detect the wrong exception being raised.
        try:
            failUnlessRaises(Bang, go_pop, 1, b=5)
        except Errors.Failure as exc:
            pass
        else:
            raise fail("The failUnlessRaises did not detect failure!")
        failUnlessEqualStrings(
            "failUnlessRaises: Raised 'Pop' instead of 'Bang'\n"
            "  str(exc) = 'Pop 1 5'",
            str(exc), simple=True)

    @test
    def fail_unlessRaisesMsg(self):
        """Test the failUnlessRaisesMsg assertion."""
        #> Check we detect failure to raise an exception with the correct
        #> message contents. For this we need to explicitly handle the
        #> Errors.Failure exception.
        try:
            failUnlessRaisesMsg(Bang, go_bang, "Bang 1 4", 1, b=5)
        except Errors.Failure as exc:
            pass
        else:
            raise fail("The failUnlessRaises did not detect failure!")
        failUnlessEqualStrings(
            "failUnlessRaisesMsg: Raises 'Bang' OK, but message was wrong\n"
            "  Expected: 'Bang 1 4'\n"
            "  Got     : 'Bang 1 5'",
            str(exc))

        #> Also run with a non-failure to verify that the exception raised
        #> is then returned.
        exc = failUnlessRaisesMsg(Bang, go_bang, "Bang 1 5", 1, b=5)
        #print "EXC", exc, exc is not None
        failUnless(exc is not None)
        failUnless(isinstance(exc, Bang))

    @test
    def fail_unadorned(self):
        """The unconditional fail assertion, no message."""
        failUnlessRaises(Errors.Failure, Assertions.fail)

    @test
    def fail_with_message(self):
        """The unconditional fail assertion, with a message."""
        exc = failUnlessRaisesMsg(Errors.Failure, Assertions.fail,
                "Oops", "Oops")

    @test
    def fail_with_extra_details(self):
        """You can provide extra details for an assertion, using a function.

        """
        def details():
            return "Wibble"

        exc = failUnlessRaisesMsg(Errors.Failure, Assertions.fail,
                "Oops\nWibble",
                "Oops", addDetails=details)

    @test
    def fail_unlessEqual(self):
        """Test basic failUnlessEqual assertion."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqual, 1, 0)
        failUnlessEqual("failUnlessEqual: values do not match\n"
            "Expected: 1\n"
            "Actual:   0", exc.message)

    @test
    def fail_unlessInRange(self):
        """Test basic failUnlessInRange assertion."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessInRange, 0, 4, 5)
        failUnlessEqual("failUnlessInRange: 0 <= 5 <= 4", exc.message)

    @test
    def provide_simple_message1(self):
        """Verify we can provide a simple (over-riding) message, arg=msg"""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqual, 1, 0, msg="Not equal")
        failUnlessEqual("Not equal", exc.message)

    def provide_simple_message2(self):
        """Verify we can provide a simple (over-riding) message,
        arg=message.

        """
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqual, 1, 0, message="Not equal")
        failUnlessEqual("Not equal", exc.message)

    @test
    def provide_generated_message(self):
        """Verify we can provide a message using a function."""
        x = 3
        def gen():
            return "X = %d" % x
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqual, 1, 0, makeMessage=gen)
        failUnlessEqual("X = 3", exc.message)

    @test
    def generated_message_over_rides_simple_message(self):
        """Verify that the makeMessage argument over-rides the message
        argument."""
        x = 3
        def gen():
            return "X = %d" % x
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqual, 1, 0,
                message="Should not be seen", makeMessage=gen)
        failUnlessEqual("X = 3", exc.message)

    @test
    def augment_basic_message(self):
        """The augmentMessage function can modify the basic message."""
        x = 3
        def augment(msg):
            return "Added: %s" % msg
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqual, 1, 0, augmentMessage=augment)
        failUnlessEqual("Added: failUnlessEqual: values do not match"
                        "\nExpected: 1"
                        "\nActual:   0", exc.message)

    @test
    def provide_augmented_message(self):
        """The augmentMessage function can modify the final message."""
        x = 3
        def gen():
            return "X = %d" % x
        def augment(msg):
            return "Added: %s" % msg
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqual, 1, 0, makeMessage=gen,
                augmentMessage=augment)
        failUnlessEqual("Added: X = 3", exc.message)

    @test
    def fail_ifEqual(self):
        """Test basic failIfEqual assertion."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failIfEqual, 1, 1)
        failUnlessEqual("failIfEqual: 1 == 1", exc.message)

    @test
    def fail_If(self):
        """Test basic failIf assertion."""
        exc = failUnlessRaises(Errors.Failure, Assertions.failIf,
                True)
        failUnlessEqual("Condition is True, but should be False",
                exc.message)

    @test
    def fail_Unless(self):
        """Test basic failUnless assertion."""
        exc = failUnlessRaises(Errors.Failure, Assertions.failUnless,
                False)
        failUnlessEqual("Condition is False, but should be True",
                exc.message)

    @test
    def fail_UnlessReMatches(self):
        """Test basic failUnlessReMatches assertion."""
        m = failUnlessReMatches("hi", "Say 'hi' Harold")
        failUnless(m)
        failUnlessEqual(m.group(0), "hi")

        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessReMatches, re.compile("ho"),
                "Say 'hi' Harold")
        failUnlessEqual('''Regexp 'ho' does not match "Say 'hi' Harold"''',
                exc.message)

    @test
    def fail_IfReMatches(self):
        """Test basic failIfReMatches assertion."""
        failIfReMatches("ho", "Say 'hi' Harold")
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failIfReMatches, "hi", "Say 'hi' Harold")
        failUnlessEqual(
                '''Regexp 'hi' unexpectedly matches "Say 'hi' Harold"''',
                exc.message)

    @test
    def fail_unlessEqualStrings(self):
        """Test basic failUnlessEqualStrings assertion."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualStrings,
                "a\nb", "a\nc", simple=True)
        failUnlessEqual("The two strings do not match\n"
                "Expected:\n"
                "     1: 'a\\n'\n"
                "     2:>'b'\n"
                "Actual:\n"
                "     1: 'a\\n'\n"
                "     2:>'c'",
                exc.message)

    @test
    def fail_unlessEqualStrings_with_getTitle(self):
        """Test basic failUnlessEqualStrings assertion, with getTitle arg"""
        def title():
            return "A made up title"

        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualStrings,
                "a\nb", "a\nc", getTitle=title, simple=True)
        failUnlessEqual("A made up title\n"
                "Expected:\n"
                "     1: 'a\\n'\n"
                "     2:>'b'\n"
                "Actual:\n"
                "     1: 'a\\n'\n"
                "     2:>'c'",
                exc.message)

    @test
    def fail_unlessEqualStrings_new1(self):
        """Test basic failUnlessEqualStrings assertion, new diff format,
        simple.

        """
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualStrings,
                "a\nb", "a\nc")
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | The two strings do not match
        | == expected ===================================================================
        |   0:  a
        |   1:> b
        |       ^
        | -- actual   -------------------------------------------------------------------
        |   0:  a
        |   1:> c
        |       ^
        """), exc.message)

    @test
    def fail_unlessEqualStrings_new_del_line(self):
        """Test basic failUnlessEqualStrings assertion, line deleted."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualStrings,
                DataMaker.literalText2Text("""
                | Line 1
                | Line 2
                | Line 3
                | Line 4
                | Line 5
                """),
                DataMaker.literalText2Text("""
                | Line 1
                | Line 2
                | Line 4
                | Line 5
                """))
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | The two strings do not match
        | == expected ===================================================================
        |   0:  Line 1
        |   1:  Line 2
        |   2:- Line 3
        |   3:  Line 4
        |   4:  Line 5
        | -- actual   -------------------------------------------------------------------
        |   0:  Line 1
        |   1:  Line 2
        |   2:  Line 4
        |   3:  Line 5
        """), exc.message)

    @test
    def fail_unlessEqualStrings_new_add_line(self):
        """Test basic failUnlessEqualStrings assertion, line deleted."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualStrings,
                DataMaker.literalText2Text("""
                | Line 1
                | Line 2
                | Line 4
                | Line 5
                """),
                DataMaker.literalText2Text("""
                | Line 1
                | Line 2
                | Line 3
                | Line 4
                | Line 5
                """))
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | The two strings do not match
        | == expected ===================================================================
        |   0:  Line 1
        |   1:  Line 2
        |   2:  Line 4
        |   3:  Line 5
        | -- actual   -------------------------------------------------------------------
        |   0:  Line 1
        |   1:  Line 2
        |   2:+ Line 3
        |   3:  Line 4
        |   4:  Line 5
        """), exc.message)

    @test
    def fail_unlessEqualStrings_new_chars_add_del(self):
        """Test basic failUnlessEqualStrings assertion, chars added and
        deleted.

        """
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualStrings,
                DataMaker.literalText2Text("""
                | Line 1
                | Line 2
                | Line 3 - plus stuff
                | Line 4
                | Line 5
                """),
                DataMaker.literalText2Text("""
                | Line 1
                | Line 2
                | Line 3
                | Line
                | Line 5
                """))
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | The two strings do not match
        | == expected ===================================================================
        |   0:  Line 1
        |   1:  Line 2
        |   2:> Line 3 - plus stuff
        |             ^^^^^^^^^^^^^
        |   3:> Line 4
        |           ^^
        |   4:  Line 5
        | -- actual   -------------------------------------------------------------------
        |   0:  Line 1
        |   1:  Line 2
        |   2:> Line 3
        |   3:> Line
        |   4:  Line 5
        """), exc.message)

    @test
    def fail_unlessEqualStrings_new_chars_add_del_same_line(self):
        """Test basic failUnlessEqualStrings assertion, chars added and
        deleted within the same line.

        """
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualStrings,
                DataMaker.literalText2Text("""
                | Line 1
                | Line 2
                | Line 3 plus stuff
                | Line 4
                | Line 5
                """),
                DataMaker.literalText2Text("""
                | Line 1
                | Line 2
                | ine 3
                | Line 4
                | Line 5
                """))
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | The two strings do not match
        | == expected ===================================================================
        |   0:  Line 1
        |   1:  Line 2
        |   2:> Line 3 plus stuff
        |       ^     ^^^^^^^^^^^
        |   3:  Line 4
        |   4:  Line 5
        | -- actual   -------------------------------------------------------------------
        |   0:  Line 1
        |   1:  Line 2
        |   2:> ine 3
        |   3:  Line 4
        |   4:  Line 5
        |
        """), exc.message, simple=True)


class Test_FloatAssertions(Suite):
    """Tests for the floating point comparison assertions.

    """
    def floatToInt(self, x):
        ival = struct.unpack('I', struct.pack('f', x))[0]
        if ival == 0:
            ival = 0x80000000
        elif ival < 0x80000000:
            ival += 0x80000000
        else:
            ival = 0x100000000 - ival
        return ival

    def intToFloat(self, q):
        if q == 0x80000000:
            q = 0
        elif q < 0x80000000:
            q = 0x100000000 - q
        else:
            q = q - 0x80000000
        return struct.unpack('f', struct.pack('I', q))[0]

    def addULPs(self, x, ulps):
        """Add a number of ULPs to a floating pointing number.

        """
        v = self.floatToInt(x)
        v += ulps
        if v < 0:
            v = 0
        if v > 0xffffffff:
            v = 0xffffffff
        return self.intToFloat(v)

    @test
    def equalityNearZero(self):
        """Verify that numbers near zero can compare equal.

        This test uses the default ULP tolerance of 4.

        """
        Assertions.failUnlessEqualFloats(0.0, 0.0)
        Assertions.failUnlessEqualFloats(0.0, self.addULPs(0.0, 1))
        Assertions.failUnlessEqualFloats(0.0, self.addULPs(0.0, 2))
        Assertions.failUnlessEqualFloats(0.0, self.addULPs(0.0, 3))
        Assertions.failUnlessEqualFloats(0.0, self.addULPs(0.0, 4))
        Assertions.failUnlessEqualFloats(0.0, self.addULPs(0.0, -1))
        Assertions.failUnlessEqualFloats(0.0, self.addULPs(0.0, -2))
        Assertions.failUnlessEqualFloats(0.0, self.addULPs(0.0, -3))
        Assertions.failUnlessEqualFloats(0.0, self.addULPs(0.0, -4))

    @test
    def inequalityNearZero(self):
        """Verify that numbers near zero can compare not-equal.

        This test uses the default ULP tolerance of 4.

        """
        Assertions.failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualFloats, 0.0, self.addULPs(0.0, 5))
        Assertions.failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualFloats, 0.0, self.addULPs(0.0, -5))

    @test
    def equalityNearInfinities(self):
        """Verify that numbers near infinities can compare equal.

        This test uses the default ULP tolerance of 4.

        """
        inf = float("inf")
        Assertions.failUnlessEqualFloats(inf, inf)
        Assertions.failUnlessEqualFloats(inf, self.addULPs(inf, -1))
        Assertions.failUnlessEqualFloats(inf, self.addULPs(inf, -2))
        Assertions.failUnlessEqualFloats(inf, self.addULPs(inf, -3))
        Assertions.failUnlessEqualFloats(inf, self.addULPs(inf, -4))

        inf = -float("inf")
        Assertions.failUnlessEqualFloats(inf, inf)
        Assertions.failUnlessEqualFloats(inf, self.addULPs(inf, 1))
        Assertions.failUnlessEqualFloats(inf, self.addULPs(inf, 2))
        Assertions.failUnlessEqualFloats(inf, self.addULPs(inf, 3))
        Assertions.failUnlessEqualFloats(inf, self.addULPs(inf, 4))

    @test
    def inequalityNearInfinities(self):
        """Verify that numbers near infinities can compare not-equal.

        This test uses the default ULP tolerance of 4.

        """
        inf = float("inf")
        Assertions.failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualFloats, inf, self.addULPs(inf, 5))

        inf = -float("inf")
        Assertions.failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualFloats, inf, self.addULPs(inf, -5))

    @test
    def nanIsNeverEqual(self):
        """Verify that NaN never compares equal.

        """
        nan = float("nan")
        Assertions.failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualFloats, nan, -0.0)
        Assertions.failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualFloats, nan, +0.0)
        Assertions.failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualFloats, nan, 1.0)
        Assertions.failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualFloats, nan, -1.0)
        Assertions.failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualFloats, nan, nan)
        inf = float("inf")
        Assertions.failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualFloats, nan, inf)
        inf = -float("inf")
        Assertions.failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualFloats, nan, inf)


class Test_MessageGen(Suite):
    """Message generation behaviour.

    Principally this set of test verify that, for most assertions, an
    extra non-keyword argument can be either a failure message or the
    makeMessage argument.

    """
    def oops(self):
        return "The cat ate it!"

    @test
    def fail(self):
        """fail: Message/makeMessage."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.fail, "Oh dear!")
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.fail, self.oops)
        failUnlessEqual("The cat ate it!", exc.message)

    @test
    def fail_ifEqual(self):
        """failIfEqual: Message/makeMessage."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failIfEqual, 1, 1, "Oh dear!")
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failIfEqual, 1, 1, self.oops)
        failUnlessEqual("The cat ate it!", exc.message)

    @test
    def fail_unlessEqual(self):
        """failUnlessEqual: Message/makeMessage."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqual, 0, 1, "Oh dear!")
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqual, 0, 1, self.oops)
        failUnlessEqual("The cat ate it!", exc.message)

    @test
    def fail_if(self):
        """failIf: Message/makeMessage."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failIf, True, "Oh dear!")
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failIf, True, self.oops)
        failUnlessEqual("The cat ate it!", exc.message)

    @test
    def fail_unless(self):
        """failIf: Message/makeMessage."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnless, False, "Oh dear!")
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnless, False, self.oops)
        failUnlessEqual("The cat ate it!", exc.message)

    @test
    def fail_unlessInRange(self):
        """failUnlessInRange: Message/makeMessage."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessInRange, 1, 3, 4, "Oh dear!")
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessInRange, 1, 3, 4, self.oops)
        failUnlessEqual("The cat ate it!", exc.message)

    @test
    def fail_unlessEqualStringsOld(self):
        """failUnlessEqualStrings - simple: Message/makeMessage."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualStrings, "a", "b", "Oh dear!",
                simple=True)
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualStrings, "a", "b", self.oops,
                simple=True)
        failUnlessEqual("The cat ate it!", exc.message)

    @test
    def fail_unlessEqualStringsNew(self):
        """failUnlessEqualStrings - new: Message/makeMessage."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualStrings, "a", "b", "Oh dear!")
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessEqualStrings, "a", "b", self.oops)
        failUnlessEqual("The cat ate it!", exc.message)

    @test
    def fail_unlessReMatches(self):
        """failUnlessReMatches: Message/makeMessage."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessReMatches, "xx", "1", "Oh dear!")
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessReMatches, "xx", "1", self.oops)
        failUnlessEqual("The cat ate it!", exc.message)

    @test
    def fail_ifReMatches(self):
        """failIfReMatches: Message/makeMessage."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failIfReMatches, "1", "1", "Oh dear!")
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failIfReMatches, "1", "1", self.oops)
        failUnlessEqual("The cat ate it!", exc.message)


class Test_MessageGenFile(FileBasedTests):
    """Message generation behaviour - file based tests.

    Principally this set of test verify that, for most assertions, an
    extra non-keyword argument can be either a failure message or the
    makeMessage argument.

    """
    def oops(self):
        return "The cat ate it!"

    @test
    def fail_unlessFilesMatch(self):
        """failUnlessFilesMatch: Message/makeMessage."""
        self.addFile("a", "line 1\nline 2")
        self.addFile("b", "line 1\nline 3")

        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessFilesMatch, "a", "b", "Oh dear!")
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessFilesMatch, "a", "b", self.oops)
        failUnlessEqual("The cat ate it!", exc.message)

    @test
    def fail_UnlessFileModesMatch(self):
        """failUnlessFileModesMatch: Message/makeMessage."""
        self.addFile("a", "line 1\nline 2")
        self.addFile("b", "line 1\nline 2")
        a = os.stat("a").st_mode
        os.chmod("a", a ^ 1)

        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessFileModesMatch, "a", "b", "Oh dear!")
        failUnlessEqual("Oh dear!", exc.message)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessFileModesMatch, "a", "b", self.oops)
        failUnlessEqual("The cat ate it!", exc.message)


class Test_FileAssertions(FileBasedTests):
    """Tests for the file based assertion functions."""
    @test
    def fail_IfExists(self):
        """Test failIfExists assertion."""
        failUnlessExists(__file__)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failIfExists, __file__, "no-such-file-at-all")
        failUnlessEqual(
                "The file '%s' should not exist" % __file__, exc.message)

    @test
    def fail_UnlessExists(self):
        """Test failUnlessExists assertion."""
        failIfExists("no-such-file-at-all")
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessExists, __file__, "no-such-file-at-all")
        failUnlessEqual(
                "The file 'no-such-file-at-all' should exist", exc.message)

    @test
    def fail_UnlessFilesMatch(self):
        """Test failUnlessFilesMatch assertion."""
        self.addFile("a", "line 1\nline 2")
        self.addFile("b", "line 1\nline 2")
        failUnlessFilesMatch("a", "b")

        ensureNoFile("b")
        self.addFile("b", "line 1\nline 3")
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessFilesMatch, "a", "b")
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | The files a and b do not match
        | == expected ===================================================================
        |   0:  line 1
        |   1:> line 2
        |            ^
        | -- actual   -------------------------------------------------------------------
        |   0:  line 1
        |   1:> line 3
        |            ^
        """), exc.message)

    # TODO: For some reason I removed the failIfFilesMatch assertion.
    #@test
    def fail_IfFilesMatch(self):
        """Test failIfFilesMatch assertion."""
        self.addFile("a", "line 1\nline 2")
        self.addFile("b", "line 1\nline 3")
        failIfFilesMatch("a", "b")

        ensureNoFile("b")
        self.addFile("b", "line 1\nline 2")
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failIfFilesMatch, "a", "b", simple=True)
        failUnlessEqual("The files a and b should not match", exc.message)

    @test
    def fail_UnlessFileModesMatch(self):
        """Test failUnlessFileModesMatch assertion."""
        self.addFile("a", "line 1\nline 2")
        self.addFile("b", "line 1\nline 2")
        failUnlessFileModesMatch("a", "b")


        a = os.stat("a").st_mode
        os.chmod("a", a ^ 1)
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failUnlessFileModesMatch, "a", "b")
        failUnlessEqual(
                "The modes (permissions) of files a and b do not match",
                exc.message)


class Test_UserErrorr(FileBasedTests):
    """When the user provides faulty error reporting code the framework trys to
    be helpful.
    """
    @test
    def fail_IfExists(self):
        """Test failIfExists assertion."""
        def badOops():
            assert 0

        exc = failUnlessRaises(Errors.Failure,
                Assertions.fail, makeMessage=badOops)
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | !! Cannot correctly describe failure !!
        | The function <function badOops at 0x1a076e0> raised the exception
        |     AssertionError()
        """), exc.message, lineWrapper=support.clean)


if __name__ == "__main__":
    runModule()

