#!/usr/bin/env python
"""TODO:

"""

import os
import commands

import CheckEnv
import sys
import exec_test_support

from CleverSheep.Test.Tester import *
from CleverSheep.Test import DataMaker
from CleverSheep.Test.DataMaker import literalText2Text, makeFile

import support

class BugsToBeFixed(exec_test_support.TestExecutor):
    """Place holder for bugs I have spotted.

    """


class SpecialImportBehaviour(support.TestRunner):
    """Tests for the special import behaviour controlled by the test framework.

    """
    @test(uid="import-01")
    def prefer_module_in_script_dir(self):
        """Imports for a test script prefer the script's directory.

        Given a tree like::

            -o-<root>
             |---all_tests.py
             |---helper.py
             |-o-a/
             | |---all_tests.py
             | |---test_a.py
             | `---helper.py
             `-o-b/
               |---all_tests.py
               |---test_b.py
               `---helper.py

        Where test_a, test_b and <root>/all_tests.py import 'helper'.

        - <root>/all_tests should import <root>/helper.py.
        - test_a should import a/helper.py.
        - test_b should import b/helper.py.

        The above rules should hold true regardless of how a test scrtipt is
        executed.

        """
        #> Run the test_mod_ns.py. The attribute error in the build test suite
        #> should be gracefully handled.
        os.chdir("localimp")

        data = support.run_test("all_tests.py")
        failUnlessEqualStrings(literalText2Text(r"""
        | Root uses Helper at testroot
        | Test A uses Helper A
        | Test B uses Helper B
        """), data.terminal)

        os.chdir("a")
        support.run_test("all_tests.py")
        failUnlessEqualStrings(literalText2Text(r"""
        | Test A uses Helper A
        """), data.terminal)

        support.run_test("test_a.py")
        failUnlessEqualStrings(literalText2Text(r"""
        | Test A uses Helper A
        """), data.terminal)

        os.chdir("../b")
        support.run_test("all_tests.py")
        failUnlessEqualStrings(literalText2Text(r"""
        | Test B uses Helper B
        """), data.terminal)

        support.run_test("test_b.py")
        failUnlessEqualStrings(literalText2Text(r"""
        | Test B uses Helper B
        """), data.terminal)


class AddingTests(support.TestRunner):
    """Tests for the loadTestsFromTestSource function.

    """
    @test(issue="76", uid="mod-ns")
    def ns_mod_during_load(self):
        """Test suites are robust against adding/removing items from the
        enclosing namespace.

        """
        #> Run the test_mod_ns.py. The attribute error in the build test suite
        #> should be gracefully handled.
        data = support.run_test("./test_mod_ns.py")
        failUnlessEqualStrings(literalText2Text(r"""
        | A test script that modifies its namespace during loading.
        |   Suite A
        |     1   : test a...................................................        PASS
        |   Suite B
        |     2   : test a...................................................        PASS
        """), data.terminal)

    @test(issue="84", uid="first-is-broken")
    def broken_first_test(self):
        """We should be able to mark the very first test as broken.

        """
        support.makePySingleTestFile("test_broken.py",
            suiteA='''
            | class SuiteA(Suite):
            |     """Suite A"""
            ''',
            tests=['''
            | @test("broken")
            | def a(self):
            |    """Test A"""
            |    pass
            |
            | @test
            | def b(self):
            |    """Test B"""
            |    pass
            |''', '''
            | def a(self):
            |    """Test A"""
            |    pass
            '''])
        data = support.run_test("test_broken.py", exitCode=0)

        support.checkTerminalOutput('''
        | Testing the tester.
        |   Suite A
        |     2   : Test B..........        PASS
        |   Suite B
        |     3   : Test A..........        PASS
        ''', data.terminal)


if __name__ == "__main__":
    runModule()
