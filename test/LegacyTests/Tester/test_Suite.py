#!/usr/bin/env python
"""Test for the the Test.Suite class.

The `CleverSheep.Test.Tester.Suite` module provides a replacement for
the more clunky `CleverSheep.Test.Tester.PollTester`. However it does require
stackless python.

This script provides tests for this test module, which is obviously little bit
strange. What is more strange is that these tests use the Suite
module to construct the tests. However, as a finished piece of work this pretty
much makes sense; there is no good reason "why not!". However, there is no
prize for guessing that these test do not form part of a pure TDD development.
In fact they may be an interesting exception to any rule that says "any piece
of software can be developed using TDD" [#]_.

That said, once I had gotten beyond the initial bootstrapping problem, I was
able to switch to a more TDD like approach [#]_.

.. [#] I cannot claim to know of any such rule.

.. [#] I tend to fail to do pure TDD, due to personal failings. My style seems
       to be characterised by periods of TDD mixed with periods of hacking
       code, followed by catching up on the testing.
"""
from __future__ import print_function

import time

import CheckEnv

from CleverSheep.Test import Tester
from CleverSheep.Test.Tester import *

from CleverSheep.Test.Tester import Manager
from CleverSheep.Test.Tester import Core, Collection, ReportMan
from CleverSheep.Test.Tester import Coordinator

# TODO: Rework these tests.
raise Collection.Unsupported()

# The module under test.
module_under_test = "../../CleverSheep/Test/Tester/Suites.py"
from CleverSheep.Test.Tester import Suites


def log(s, *args):
    f = open("log", "a")
    if args:
        s = s % args
    f.write("%s\n" % s)
    f.close()


class Stub(object):
    def __getattr__(self, name):
        log("Bum! %r, %r", self.__class__.__name__, name)


class UI(Stub):
    def start(self): pass
    def stop(self): pass
    def setMode(self, *args, **kwargs): pass
    def enterSuite(self, *args, **kwargs): pass
    def logFailure(self, *args, **kwargs): pass
    def prepareToRunTest(self, *args, **kwargs): pass
    def putResult(self, *args, **kwargs): pass
    def leaveSuite(self, *args, **kwargs): pass
    def finish(self, *args, **kwargs): pass
    def write(self, *args, **kwargs): pass


class Logger(Stub):
    def fatal(self, *args, **kwargs): pass
    def error(self, *args, **kwargs): pass
    def warning(self, *args, **kwargs): pass
    def info(self, *args, **kwargs): pass
    def debug(self, *args, **kwargs): pass

    def putResult(self, *args, **kwargs): pass
    def logFailure(self, *args, **kwargs): pass
    def setMode(self, *args, **kwargs): pass
    def stop(self, *args, **kwargs): pass


class TestManager(Manager.Manager):
    pass


class StdTerm(Stub):
    def flush(self): pass
    def write(self, *args): pass
    def writelines(self, *args): pass
    def sol(self, *args): pass
    def up(self, *args): pass
    def right(self, *args): pass
    def isatty(self, *args): pass
    def setLog(self, *args): pass
    def setStyle(self, *args, **kwargs): pass
    def resetStyle(self, *args, **kwargs): pass


class Tty(Stub):
    def __init__(self):
        self.stdout = StdTerm()
        self.stderr = StdTerm()
        self.out = StdTerm()
        self.err = StdTerm()


class Base(object):
    def __init__(self, uid=1, parent=None):
        self.results = {}
        self.parent = parent
        self.uid = uid
        if parent:
            self._parentUid = parent.uid
        else:
            self._parentUid = None
        self.hasFailed = False
        self.exited = False

    def setPhase(self, *args):
        pass

    def setCollection(self, c):
        pass

    def addResult(self, name, result):
        print("Add result", name, str(result.state))
        self.results[name] = result
        if result.hasFailed:
            self.hasFailed = True

    def getResult(self, name):
        return self.results.get(name, None)


class MyTest(Base):
    summary = "The test summary"
    docLines = ["The test summary", "", "The details"]
    result = Core.Result()

    def __init__(self, uid, func, parent):
        super(MyTest, self).__init__(uid, parent)
        self.func = func
        self.number = 1
        self.isBroken = False
        self.isRunnable = True
        self.result = Core.Result()

    def hasFailingAncestor(self):
        return False


class ClassSuite(Base):
    summary = "The suite summary"
    docLines = ["The suite summary", "", "The details"]
    result = Core.Result()

    @property
    def suiteSetUp(self):
        return lambda: None
    suiteTearDown = suiteSetUp
    setUp = suiteSetUp
    tearDown = suiteSetUp

    def hasTests(self):
        return True

    def noteFailure(self):
        print("Noting failure")
        self.hasFailed = True
        result = Core.Result()
        result.state = Core.CHILD_FAIL
        self.addResult("suite", result)


class Test_Suite(Suites.Suite):
    """Tests for the Suite class"""
    @test
    def delay_does_delay(self):
        """The Suite provides a delay method.

        Basically we merely need to delay for a 0.2 second and ensure that at
        least a 0.2 second delay occurs.
        """
        #> Save the starting time then delay for 0.2s.
        start = time.time()
        self.control.delay(0.2)
        #> When the delay ends, verify that the elapsed time is long enough.
        end = time.time()
        failUnlessInRange(0.195, 0.3, end - start)

    # TODO: This depends on old internal functions. Proabably not worth keeping since
    #       we need test that verify the screen output.
    #@test
    def fail_gives_good_traceback(self):
        """We wish to ensure that, on failure, we get a useful traceback"""
        myTest = Tester.Test(self.failingTest, self)
        result, stopFlag = self.executeTest(myTest)
        for sIdx, ((src, lnum, func, ctx, n), expectFunc) in enumerate(zip(
                result.func.exc.stack, ("failMe", "x", "failingTest"))):
            print(expectFunc, func)
        failUnlessEqual(3, len(result.func.exc.stack))
        for sIdx, ((src, lnum, func, ctx, n), expectFunc) in enumerate(zip(
                result.func.exc.stack,
                    ("failMe", "x", "failingTest"))):
            failUnlessEqual(expectFunc, func,
                msg="Expected to see %r, not %r in stack at index %d" % (
                    expectFunc, func, sIdx))

    def failingTest(self):
        """This is run as a test by `fail_gives_good_traceback`.

        Note this is not actually a test method.
        """
        def x():
            self.failMe()
        x()

    def failMe(self):
        fail("This is an expected failure")

    def executeTest(self, func):
        Coordinator.Registry.save()
        try:
            Coordinator.registerProvider("test", "report_manager", 
                    ReportMan.ReporterAPI(None, None))
            Coordinator.registerProvider("test", "logging", Logger())
            manager = Manager.Manager()
            Coordinator.registerProvider("test", "manager", manager)
            Coordinator.registerProvider("test", "tty", Tty())
            collection = Collection.Collection()
            manager.setCollection(collection)

            suite = ClassSuite(101)
            test = MyTest(1, func, suite)
            collection.addSuite(suite.uid, suite)
            collection.addTest(test)
            a = time.time()
            manager.run("Aardvaark")
            log("")
            b = time.time()
            self.runTime = a, b
            return test, suite
        finally:
            Coordinator.Registry.restore()

    @test
    def expect_passes(self):
        """Expect check can pass OK."""
        test, suite = self.executeTest(self.expectPass)
        failIf(suite.hasFailed)
        a, b = self.runTime
        failUnlessInRange(0.5, 0.9, b - a)
        return

    @test
    def expect_fails(self):
        """Expect check can fail OK."""
        test, suite = self.executeTest(self.expectFail)
        failUnless(suite.hasFailed)
        a, b = self.runTime
        failUnless((b - a) > 0.9,
                msg="Test time T should be in range T > 0.9\n"
                    "Actual time was %.2f seconds" % (b - a))

    @test
    def dont_expect_passes(self):
        """Don't expect check can pass OK."""
        test, suite = self.executeTest(self.dontExpectPass)
        failIf(suite.hasFailed)
        a, b = self.runTime
        failUnless((b - a) > 0.9,
                msg="Test time T should be in range T > 0.9\n"
                    "Actual time was %.2f seconds" % (b - a))

    @test
    def dont_expect_fails(self):
        """Don't expect check can fail OK."""
        test, suite = self.executeTest(self.dontExpectFail)
        failUnless(suite.hasFailed)
        a, b = self.runTime
        failUnless(0.5 <= (b - a) < 0.9,
                msg="Test time T should be in range 0.5 <= T < 0.9\n"
                    "Actual time was %.2f seconds" % (b - a))

    def x_executeTest(self, func):
        return Tester.executeTest(func)

        # TODO: Junk this!
        pollMan, self.poll = self.poll, None
        try:
            return Tester.executeTest(func)
        finally:
            self.poll = pollMan

    @test
    def simple_timeout(self):
        """Check we can set up a PollManager one-shot timeout."""
        self.count = 0
        def tick(inc):
            self.count += inc

        self.control.addTimeout(0.1, tick, 2)
        self.control.delay(0.2)
        failUnlessEqual(2, self.count,
                msg="Count should be 2\n"
                    "Actual count is %d" % self.count)

    @test
    def work_function(self):
        """Check we can set up a PollManager work function"""
        self.count = 0
        def tick(inc=1):
            self.count += inc

        self.control.addTimeout(0.1, tick, 2)
        self.control.addWorkFunction(tick)
        self.control.delay(0.2)
        failUnless(self.count > 2,
                msg="Count should be 2\n"
                    "Actual count is %d" % self.count)

    @test
    def repeating_timeout(self):
        """Check we can set up a PollManager repeating timeout."""
        self.count = 0
        def tick(inc):
            self.count += inc

        tid = self.control.addRepeatingTimeout(0.1, tick, 2)
        self.control.delay(0.5)
        failUnless(self.count in [8, 10],
                msg="Count should be 8 or 10\n"
                    "Actual count is %d" % self.count)
        self.control.removeTimeout(tid)
        self.control.delay(0.2)
        failUnless(self.count in [8, 10],
                msg="Count should be 8 or 10\n"
                    "Actual count is %d" % self.count)

    def expectPass(self):
        def oops(*args, **kwargs):
            return "Oops! %r %r" % (args, kwargs)
        def check(period, x=1):
            f.write("CH %s %s\n" % (time.time(), "x"))
            f.flush()
            a, b = runTime[0], time.time()
            runTime[1] = b
            if b - a > period:
                return True

        f = open("/tmp/paul", "w")
        self.runTime = runTime = [time.time(), time.time()]
        self.control.expect(1.0, check, oops, 0.5, x=2, checkInterval=0.01)

    def expectFail(self):
        def oops(*args, **kwargs):
            return "Oops! %r %r" % (args, kwargs)
        def check(period, x=1):
            runTime[1] = time.time()

        self.runTime = runTime = [time.time(), time.time()]
        self.control.expect(1.0, check, oops, 0.5, x=2, checkInterval=0.01)

    def dontExpectPass(self):
        def oops(*args, **kwargs):
            return "Oops! %r %r" % (args, kwargs)
        def check(period, x=1):
            runTime[1] = time.time()

        self.runTime = runTime = [time.time(), time.time()]
        self.control.dontExpect(1.0, check, oops, 0.5, x=2)

    def dontExpectFail(self):
        def oops(*args, **kwargs):
            return "Oops! %r %r" % (args, kwargs)
        def check(period, x=1):
            a, b = runTime[0], time.time()
            runTime[1] = b
            if b - a > period:
                return True

        self.runTime = runTime = [time.time(), time.time()]
        self.control.dontExpect(1.0, check, oops, 0.5, x=2)


if __name__ == "__main__":
    runModule()

