#!/usr/bin/env python
"""Test for the control object within CleverSheep's test framework.

"""
from __future__ import print_function

import time

import CheckEnv

from CleverSheep.Test.Tester import *


class ControlDelay(Suite):
    """Tests for the control object's delay method.

    The delay method can be used to pause the main thread of control in a test
    for a minimum period of time (typically in units of 0.01 seconds). During a
    call to `delay` the framework handles other interactions, such a file input
    and timer callbacks. This means that there are some interesting situations
    where the framework needs to provide consistent, albeit arbitrary,
    behaviour.

    """
    # TODO: Plausibly do-able with built in code, but with third party
    #       code, not do sure. The Python 3 asyncio will almost certainly make
    #       it difficult or impossible.
    #@test(testID="nest-delay")
    def delays_can_nest(self):
        """A shorter inner delay works as most people would expect; i.e. it
        has no effect on the outer delay.

        """
        data = []
        st = time.time()

        def innerDelay():
            print("Inner start: %.2f" % (time.time() - st))
            data.append(time.time())
            self.control.delay(0.2)
            print("Inner delayed end: %.2f" % (time.time() - st))
            data.append(time.time())

        print("Start: %.2f" % (time.time() - st))
        self.control.addTimeout(0.1, innerDelay)
        data.append(time.time())
        self.control.delay(0.6)
        print("Outer delayed end: %.2f" % (time.time() - st))
        data.append(time.time())

        inner = data[1:3]
        outer = [data[0], data[-1]]

        a, b = inner
        failUnless(b - a >= 0.2,
                msg="Inner delay should have been >= 0.2 seconds, but was"
                    " %.3f seconds" % (b - a))

        failUnless(b - a <= 0.4,
                msg="Inner delay should have been <= 0.4 seconds, but was"
                    " %.3f seconds" % (b - a))

        c, d = outer
        failUnless(d - c >= 0.6,
                msg="Outer delay should have been > 0.6 seconds, but was"
                    " %.3f seconds" % (d - c))

    @test("todo")
    def inner_delay_extends_outer_delay(self):
        """A longer inner delay will extend the outer delay.

        """
        data = []

        def innerDelay():
            data.append(time.time())
            self.control.delay(0.5)
            data.append(time.time())

        self.control.addTimeout(0.1, innerDelay)
        data.append(time.time())
        self.control.delay(0.5)
        data.append(time.time())

        inner = data[1:3]
        outer = [data[0], data[-1]]

        a, b = inner
        failUnless(b - a >= 0.5,
                msg="Inner delay should have been >= 0.5 seconds, but was"
                    " %.3f seconds" % (b - a))

        failUnless(b - a <= 0.6,
                msg="Inner delay should have been <= 0.6 seconds, but was"
                    " %.3f seconds" % (b - a))

        c, d = outer
        failUnless(d - c >= 0.6,
                msg="Outer delay should have been >= 0.6 seconds, but was"
                    " %.3f seconds" % (d - c))

        failUnless(d - c < 0.7,
                msg="Outer delay should have been < 0.7 seconds, but was"
                    " %.3f seconds" % (d - c))

    @test("todo")
    def outer_callback_quits(self):
        """If a callback from the outer delay quits then the polling finishes
        when the inner delay finishes.

        """
        data = []

        def innerDelay():
            data.append(time.time())
            self.control.delay(0.2)
            data.append(time.time())

        def quitter():
            self.control.poll.quit()

        self.control.addTimeout(0.1, innerDelay)
        self.control.addTimeout(0.15, quitter)
        data.append(time.time())
        self.control.delay(0.6)
        data.append(time.time())

        inner = data[1:3]
        outer = [data[0], data[-1]]

        a, b = inner
        failUnless(b - a >= 0.2,
                msg="Inner delay should have been >= 0.2 seconds, but was"
                    " %.3f seconds" % (b - a))

        failUnless(b - a <= 0.4,
                msg="Inner delay should have been <= 0.4 seconds, but was"
                    " %.3f seconds" % (b - a))

        c, d = outer
        failUnless(d - c <= 0.4,
                msg="Outer delay should have been <= 0.4 seconds, but was"
                    " %.3f seconds" % (d - c))


if __name__ == "__main__":
    runModule()

