#!/usr/bin/env python
"""Tests for CleverSheep.Test.Tester.Errors.py.

This targets areas of the code that cannot easily be system tested.

"""

from CleverSheep.Test.Tester import *

# The module under test.
from CleverSheep.Test.Tester import Errors


class Problem(Suite):
    """Errors. Problem specific tests.

    """
    @test
    def problem_str(self):
        """Conversion of a ``Problem`` to a string.

        This simply verifies that the ``Problem`` class provides string
        conversion.

        """
        p = Errors.Problem([1, 2, 3])
        failUnlessEqual("[1, 2, 3]", str(p))


if __name__ == "__main__":
    runModule()



