#!/usr/bin/env python
"""Multiple suites and tests for testing the Tester.

"""


import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester


Tester.add_argument("--fail-at", action="append", type=int, default=[],
    metavar="T", help="Fail at test T")
Tester.add_argument("--fail-setup", action="store_true",
    help="Fail setup for first suite")
Tester.add_argument("--fail-teardown", action="store_true",
    help="Fail teardown for first suite")
Tester.add_argument("--fail-postcheck", action="store_true",
    help="Fail post-check for first test")


class Base(Suite):
    def common(self):
        info = Tester.currentTestInfo()
        if info.cs_test_num in Tester.userOptions.fail_at:
            fail("Selected test")


class A(Base):
    """A"""
    def setUp(self):
        if Tester.userOptions.fail_setup:
            fail("Fail setup")

    def tearDown(self):
        if Tester.userOptions.fail_teardown:
            fail("Fail teardown")

    def postCheck(self):
        if Tester.userOptions.fail_postcheck:
            fail("Fail post check")

    @test
    def a(self):
        """A-a"""
        self.common()

    @test
    def b(self):
        """A-b"""
        self.common()

    @test
    def c(self):
        """A-c"""
        self.common()


class B(Base):
    """B"""
    @test
    def a(self):
        """B-a"""
        self.common()

    @test
    def b(self):
        """B-b"""
        self.common()

    @test
    def c(self):
        """B-c"""
        self.common()


if __name__ == "__main__":
    runModule()
