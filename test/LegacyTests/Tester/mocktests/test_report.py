#!/usr/bin/env python
"""A test demonstrating the reporter plug-in feature.

"""

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester

class Reporter(Tester.Reporter):
    def __init__(self):
        self.f = open("report.txt", "w")

    def enter_suite(self, has_tests, title, doc_lines):
        self.f.write("Enter suite %s\n" % title)

    def leave_suite(self, has_tests, state):
        self.f.write("Leave suite\n")

    def start_test(self, number, title, doc_lines):
        self.f.write("Enter %s\n" % title)

    def end_test(self, number, title, doc_lines, state, error):
        self.f.write("Leave %s\n" % title)

    def finish(self):
        self.f.close()
        

Tester.registerReporter(Reporter())


class TestIt(Suite):
    """This is a test suite"""

    @test
    def aaa(self):
        """The first test."""

    @test
    def bbb(self):
        """The second test."""


if __name__ == "__main__":
    runModule()
