import CheckEnv

from CleverSheep.Test.Tester import *


# Pretend to be testing Python code
modules_under_test = ["Hello.py"]

# Pretend to be testing C code.
sources_under_test = ["hello.c"]


@test
def hello():
    """Hello"""


class A(Suite):
    """A"""
    @test
    def a(self):
        """a"""
    @test
    def b(self):
        """b"""


class B(Suite):
    """A"""
    @test
    def a(self):
        """a"""
    @test
    def b(self):
        """b"""
