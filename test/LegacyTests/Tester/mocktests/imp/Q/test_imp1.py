#!/usr/bin/env python
"""A test demonstrating some special import behaviour.

"""

from CleverSheep.Test.Tester import *
import sys
for p in sys.path:
    if not p.startswith("/usr"):
        print("A>>", p)

import package.main
import util

print("TEST", util.name)
