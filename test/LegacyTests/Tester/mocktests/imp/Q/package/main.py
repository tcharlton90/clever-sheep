"""A main module in a package."""

import sys
import imp
for p in sys.path:
    if not p.startswith("/usr"):
        print("B>>", p)

print(imp.find_module("util"))
import util

print("MAIN", util.name)
