#!/usr/bin/env python
"""A test demonstrating the exit_all feature.

"""

import CheckEnv

from CleverSheep.Test.Tester import *


class TestIt(Suite):
    """This is a test suite"""

    @test
    def aaa(self):
        """The first test."""
        exit_all()
        fail("Cannot happen\n")

    @test
    def bbb(self):
        """The second test."""
        fail("Cannot happen\n")


class TestHim(Suite):
    """This is another test suite"""

    @test
    def aaa(self):
        """The first test."""
        fail("Cannot happen\n")


if __name__ == "__main__":
    runModule()
