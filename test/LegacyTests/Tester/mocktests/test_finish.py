#!/usr/bin/env python
"""A test demonstrating the exit_suite and exit_all features.

"""

import CheckEnv

from CleverSheep.Test.Tester import *


class TestIt(Suite):
    """This is a test suite"""

    @test
    def aaa(self):
        """The first test."""
        #exit_suite()
        fail("Cannot happen\n")

    @test
    def bbb(self):
        """The second test."""


class TestHim(Suite):
    """This is another test suite"""

    @test
    def aaa(self):
        """The first test."""


if __name__ == "__main__":
    runModule()
