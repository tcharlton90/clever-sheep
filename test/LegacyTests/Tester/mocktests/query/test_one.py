#!/usr/bin/env python
"""One set of tests.

"""



from CleverSheep.Test.Tester import *


class Suite1(Suite):
    """A set of tests.

    """
    @test
    def one(self):
        """The first test.

        This is one of many.

        """

    @test
    def two(self):
        """The second test.

        This is the second of many.

        """


if __name__ == "__main__":
    runModule()
