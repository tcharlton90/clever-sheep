#!/usr/bin/env python
"""Another set of tests.

"""



from CleverSheep.Test.Tester import *


class Suite2(Suite):
    """Another set of tests.

    """
    @test
    def one(self):
        """The first test in the second set.

        Will normally be third.

        """

    @test
    def two(self):
        """The second test in the second set.

        Will normally be fourth.

        """


if __name__ == "__main__":
    runModule()
