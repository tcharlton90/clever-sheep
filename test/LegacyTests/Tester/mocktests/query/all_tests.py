#!/usr/bin/env python
"""All the querying tests.

This provides a tree of tests that test the test framework.

"""


import CheckEnv

from CleverSheep.Test.Tester import *

if __name__ == "__main__":
    runTree()
