#!/usr/bin/env python
"""Check that the test environment is correct.

When we run the CleverSheep test, we need to be using the local CleverSheep
library, not any parts of an installed version. This is because we otherwise
end up not testing the local code or with duplicate modules loaded, which
causes test to fail in strange ways. For example, the ``reload`` built-in
fails in the RichTerm tests.

"""


import os
import sys

from CleverSheep.Test import ImpUtils

if 0:
    projDir = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../.."))
    cleverSheepDir = os.path.join(projDir, 'src')
    pypath = os.environ.get("PYTHONPATH", "").split()
    pypath.append(cleverSheepDir)
    os.environ["PYTHONPATH"] = ":".join(pypath)
    if sys.path[0] != cleverSheepDir:
        sys.path[0:0] = [cleverSheepDir]
else:
    ImpUtils.addPathAtRoot("src/CHANGES.txt")


