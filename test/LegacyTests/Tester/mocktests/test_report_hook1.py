#!/usr/bin/env python
"""Example script to test the reporting hooks.

"""


import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester


class Reporter(object):
    def __init__(self):
        import os
        self.f = open("report.log", "w")

    def enter_suite(self, has_tests, title, doc_lines):
        self.f.write("Enter suite %s\n" % title)

    def leave_suite(self, has_tests, state):
        self.f.write("Leave suite\n")

    def start_test(self, number, title, doc_lines):
        self.f.write("Enter test %s: %s\n" % (number, title))

    def end_test(self, number, title, doc_lines, state, error):
        self.f.write("Leave test\n")

    def finish(self):
        self.f.write("DONE\n")
        self.f.close()


Tester.registerReporter(Reporter())


class A(Suite):
    """B
    """
    @test
    def a(self):
        """a
        """
        pass


if __name__ == "__main__":
    runModule()



