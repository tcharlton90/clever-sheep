#!/usr/bin/env python
import os
import sys

projDir = os.path.abspath(os.path.join(os.path.dirname(__file__),
                          "../../../.."))
pypath = os.environ.get("PYTHONPATH", "").split()
pypath.append(projDir)
os.environ["PYTHONPATH"] = ":".join(pypath)

if sys.path[0] != projDir:
    sys.path[0:0] = [projDir]
