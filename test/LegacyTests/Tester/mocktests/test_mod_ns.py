#!/usr/bin/env python
"""A test script that modifies its namespace during loading.

"""

import CheckEnv

from CleverSheep.Test.Tester import *

class TestA(Suite):
    """Suite A"""
    def __init__(self):
        global x
        x = 5

    @test
    def a(self):
        """test a"""


class TestB(Suite):
    """Suite B"""
    @test
    def a(self):
        """test a"""


if __name__ == "__main__":
    runModule()
