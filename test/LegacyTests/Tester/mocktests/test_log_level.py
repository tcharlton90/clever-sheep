#!/usr/bin/env python
"""A test demonstrating the logging levels.

"""

import CheckEnv

import logging

from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester


class TestIt(Suite):
    """This is a test suite"""

    @test
    def aaa(self):
        """Default logging."""
        self.log_a_bit()

    @test
    def bbb(self):
        """Errors and above."""
        log.setLevel(logging.ERROR)
        self.log_a_bit()

    def log_a_bit(self):
        log.critical("Mushroom cloud")
        log.error("Bang")
        log.warning("Pop")
        log.info("Ready to pop")
        log.debug("Getting ready")

if __name__ == "__main__":
    runModule()
