#!/usr/bin/env python
"""Multiple suites and tests for testing the Tester.

"""


import CheckEnv

from CleverSheep.Test.Tester import *


class A(Suite):
    """A"""
    @test
    def a(self):
        """A-a"""

    @test
    def b(self):
        """A-b"""

    @test
    def c(self):
        """A-c"""


class B(Suite):
    """B"""
    @test
    def a(self):
        """B-a"""

    @test
    def b(self):
        """B-b"""

    @test
    def c(self):
        """B-c"""


if __name__ == "__main__":
    runModule()




