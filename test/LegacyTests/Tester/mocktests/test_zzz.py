#!/usr/bin/env python
"""Some tests to exercise the watchdog feature.

"""


import sys
sys.stdout.flush()
import time

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester


Tester.add_option("--fail-at", action="append", type="int", default=[],
    metavar="T", help="Fail at test T")
Tester.add_option("--fail-setup", action="store_true",
    help="Fail setup for first suite")
Tester.add_option("--fail-teardown", action="store_true",
    help="Fail teardown for first suite")


class Base(Suite):
    def common(self):
        info = Tester.currentTestInfo()
        if info.cs_test_num in Tester.userOptions.fail_at:
            fail("Selected test")


import signal, os, time

class A(Base):
    """A"""
    def __init__(self):
        def q(*args):
            print ":::Q:::", args

        signal.signal(signal.SIGINT, q)
        os.kill(os.getpid(), signal.SIGINT)
        time.sleep(1)

    @test
    def a(self):
        """A-a"""


if __name__ == "__main__":
    runModule()
