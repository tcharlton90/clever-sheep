#!/usr/bin/env python
"""Dummy test module for testing failure tracebacks.

"""
import CheckEnv

from CleverSheep.Test.Tester import *

class A(Suite):
    """A"""
    @test(testID="ID-1")
    def a(self):
        """a"""
        self.b()

    def b(self):
        """b"""
        fail("Oops")


if __name__ == "__main__":
    runModule()
