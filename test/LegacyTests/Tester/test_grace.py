#!/usr/bin/env python
"""Test for graceful handling of user errors.

"""
from __future__ import print_function


import os
import re
import functools

import CheckEnv

from CleverSheep.Test import DataMaker
from CleverSheep.Test.Tester import *

import support


class TestLoading(support.TestRunner):
    """Graceful behaviour of test loading issues.

    """
    @test(testID="missing-docstring-01", issue=83)
    def missing_suite_docstring(self):
        """A missing suite docstring should provoke a friendly and useful error
        message.

        """
        support.makePySingleTestFile("test_broken.py", suiteADoc="", text='''
        | def a(self):
        |    """Test A"""
        |    def err():
        |        raise TypeError("CoderError")
        ''')
        data = support.run_test("test_broken.py", exitCode=1)

        support.checkTerminalOutput('''
        | Test loading error
        | Suite 'SuiteA' in file ./test_broken.py does not have a docstring
        ''', data.terminal)

    @test(testID="missing-docstring-02")
    def missing_test_docstring(self):
        """A missing test docstring should provoke a friendly and useful error
        message.

        """
        support.makePySingleTestFile("test_broken.py", text='''
        | def a(self):
        |    def err():
        |        raise TypeError("CoderError")
        ''')
        data = support.run_test("test_broken.py", exitCode=1)

        support.checkTerminalOutput('''
        | Test loading error
        | Test 'a' in file ./test_broken.py does not have a docstring
        ''', data.terminal)

    @test(testID="missing-docstring-03")
    def missing_module_docstring(self):
        """A missing module docstring should provoke a friendly and useful
        error message.

        """
        support.makePySingleTestFile("test_broken.py", modDoc="", text='''
        | def a(self):
        |    """Test A"""
        |    def err():
        |        raise TypeError("CoderError")
        ''')
        data = support.run_test("test_broken.py", exitCode=1)

        support.checkTerminalOutput('''
        | Test loading error
        | Module '<-->/test/LegacyTests/Tester/mocktests/test_broken.py' does not have a docstring
        ''', data.terminal)


class TestRuntime(support.TestRunner):
    """Graceful behaviour of unpleasant run-time failures.

    """
    @test(testID="issue-81", issue=81)
    def bad_error_message_function(self):
        """A bad error message function should not give a cryptic failure.

        This was added for issue 81.

        """
        support.makePySingleTestFile("test_broken.py", text='''
        | def a(self):
        |    """Test A"""
        |    def err():
        |        raise TypeError("CoderError")
        |
        |    fail("Oops", makeMessage=err)
        ''')
        data = support.run_test("test_broken.py", exitCode=1)

        support.checkFailureOutput('''
        | !! Cannot correctly describe failure !!
        | The function <function err at IGNORED> raised the exception
        |     TypeError('CoderError',)
        | ./test_broken.py: a
        |    11  :        def err():
        |    12  :            raise TypeError("CoderError")
        |    13  :
        | ==>14  :        fail("Oops", makeMessage=err)
        |    15  :
        |    16  : if __name__ == "__main__":
        |    17  :     runModule()
        ''', data.terminal)

    @test(testID="random-exception")
    def non_cs_exception(self):
        """Non-cleversheep exceptions should result in an informative failure.

        """
        support.makePySingleTestFile("test_broken.py", text='''
        | def a(self):
        |    """Test A"""
        |    import os
        |    os.chdir("this is not a directory")
        ''')
        data = support.run_test("test_broken.py", exitCode=1)

        support.checkFailureOutput('''
        | Unhandled exception occurred:
        |   OSError:[Errno 2] No such file or directory: 'this is not a directory'
        | ./test_broken.py: a
        |    9   :     def a(self):
        |    10  :        """Test A"""
        |    11  :        import os
        | ==>12  :        os.chdir("this is not a directory")
        |    13  :
        |    14  : if __name__ == "__main__":
        |    15  :     runModule()
        ''', data.terminal)


if __name__ == "__main__":
    runModule()
