#!/usr/bin/env python
"""Tests for the early exit features.

"""

import os

import CheckEnv

from CleverSheep.Prog import Files
from CleverSheep.Test import DataMaker, ImpUtils
from CleverSheep.Test.Tester import *

import support

# The module under test.
module_under_test = "../../CleverSheep/Test/Tester/Execution.py"
from CleverSheep.Test.Tester import Execution


class Test_early_exits(Suite):
    """Tests for early exit from tests."""
    def cleanUp(self):
        #Files.rmFile("subtest.log")
        pass

    def setUp(self):
        # Change to the mock tests directory.
        os.chdir(os.path.join(os.path.dirname(__file__), "mocktests"))
        self.cleanUp()

    def tearDown(self):
        # Change to the mock tests directory.
        #os.chdir(os.path.join(os.path.dirname(__file__), "mocktests"))

        self.cleanUp()

    @test
    def exit_suite(self):
        """Verify we can exit a suite early, without failing.

        This runs a test, which has 2 suites. The first has two tests, but the
        the first test invokes ``exit_suite``. So the second test in the suite
        should be skipped, but the test in the second suite still be executed.

        """
        support.run_test("test_exit_suite.py", exitCode=1)

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | A test demonstrating the exit_suite feature.
        |   This is a test suite
        |     1   : The first test...........................................  EXIT_SUITE
        |   This is another test suite
        |     3   : The first test, second suite.............................        PASS
        |
        |
        | Summary of the failures
        | A test demonstrating the exit_suite feature........................    PART_RUN
        |   This is a test suite.............................................    PART_RUN
        |     1   : The first test...........................................  EXIT_SUITE
        |
        """), open("subtest.log").read(), addDetails=support.details)

    @test
    def exit_all(self):
        """Verify we can exit all tests early, without failing.

        This is like `exit_suite`, but the fisrt test invokes ``exit_all``.
        This means all following tests should be skipped.

        """
        support.run_test("test_exit_all.py", exitCode=1)

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | A test demonstrating the exit_all feature.
        |   This is a test suite
        |     1   : The first test...........................................    EXIT_ALL
        |
        |
        | Summary of the failures
        | A test demonstrating the exit_all feature..........................    PART_RUN
        |   This is a test suite.............................................    PART_RUN
        |     1   : The first test...........................................    EXIT_ALL
        |
        """), open("subtest.log").read())


if __name__ == "__main__":
    runModule()

