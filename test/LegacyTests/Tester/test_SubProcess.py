#!/usr/bin/env python
"""Test for the the Tester.SubProcess class.

The `CleverSheep.Test.Tester.SubProcess` module provides alternative
replacements for ``subprocess.Popen``, ``commands.getstatusoutput``,
``commands.getoutput`` and ``os.system``. Each of these is designed to
generally behave in the same way, but tie into the test framework; in
particular to allow the test-runner status line to be updated.

In common with much of the Tester speccific test scripts, this does not try to
comprehensively test the framework; it just covers the difficult to reach
areas. Other parts of the framework are considered well enough exercised by the
mere fact that the full CleverSheep test suite runs successfully.

"""

import os
import commands
import subprocess
import errno

import CheckEnv

from CleverSheep.Test.Tester import *

# The module under test.
module_under_test = "../../CleverSheep/Test/Tester/SubProcess.py"
from CleverSheep.Test.Tester import SubProcess


class Test_SubProcess(Suite):
    """Tests for the SubProcess module itslf."""
    @test
    def monkeying(self):
        """Verify that monkey patching is implemented.

        The SubProcess module can monkey patch some standard modules to replace
        certain classes and functions.

        """
        #> Now invoked the patch function and verify the expected functions
        #> have been patched.
        try:
            SubProcess.egregiouslyMonkeyPatch()
            failUnlessEqual(os.system, SubProcess.system)
            failUnlessEqual(commands.getoutput, SubProcess.getoutput)
            failUnlessEqual(commands.getstatusoutput, SubProcess.getstatusoutput)
            failUnlessEqual(subprocess.Popen, SubProcess.Popen)

        finally:
            SubProcess.unPatch()

    @test
    def output_io_errors1(self):
        """Output EAGAIN errors should be gracefully handled.
        
        Failure to write all required characters should be handled correctly.
        Note that the subprocess object writes to the ``stdin`` file.

        """
        #> We use a file-like object that only initially accepts 4 characters.
        class F(object):
            def __init__(self):
                self.c = 0

            def fileno(self):
                return 0

            def write(self, s):
                if self.c == 0:
                    self.c +=1
                    err = IOError()
                    err.args = (errno.EAGAIN,)
                    err.characters_written = 4
                    raise err

            def close(self):
                pass

        p = SubProcess.Popen("cat", stdin=SubProcess.PIPE)
        p.stdin = F()
        p.communicate(input="hello")
        p.wait()

    @test
    def output_io_errors2(self):
        """Output EAGAIN errors should be gracefully handled, for python2.4
        
        This is like `output_io_errors1`, but simulates the error conditions
        that occure when using Python2.4. The difference is that IOError
        never has a ``characters_written`` attribute.

        """
        #> We use a file-like object that only initially accepts 4 characters.
        class F(object):
            def __init__(self):
                self.c = 0

            def fileno(self):
                return 0

            def write(self, s):
                if self.c == 0:
                    self.c +=1
                    err = IOError()
                    err.args = (errno.EAGAIN,)
                    raise err

            def close(self):
                pass

        p = SubProcess.Popen("cat", stdin=SubProcess.PIPE)
        p.stdin = F()
        p.communicate(input="hello")
        p.wait()

    @test
    def output_io_errors3(self):
        """Output I/O errors should be gracefully handled, for python2.4
        
        This simulates a hard output error.

        """
        #> We use a file-like object that only initially accepts 4 characters.
        class F(object):
            def __init__(self):
                self.c = 0

            def fileno(self):
                return 0

            def write(self, s):
                if self.c == 0:
                    self.c +=1
                    err = IOError()
                    err.args = (errno.EBADF,)
                    raise err

            def close(self):
                pass

        p = SubProcess.Popen("cat", stdin=SubProcess.PIPE)
        p.stdin = F()
        p.communicate(input="hello")
        p.wait()

    # TODO: This is unfinished.
    #@test
    def input_io(self):
        """Input I/O errors should be gracefully handled.
        
        Failure to write all required characters should be handled correctly.
        Note that the subprocess object reads from the ``stdout`` and
        ``sdterr`` files.

        """

    @test
    def system_returns_exit_code(self):
        """The replacement system function should return the subprocess exit
        status.
        
        """
        stat = SubProcess.system("true")
        failUnlessEqual(0, stat)

        stat = SubProcess.system("false")
        failUnlessEqual(1, stat)

        stat = SubProcess.system("exit 123")
        failUnlessEqual(123, stat)



if __name__ == "__main__":
    runModule()

