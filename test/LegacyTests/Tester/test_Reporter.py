#!/usr/bin/env python
"""Tests for plug-in reporting.

"""

import os

import CheckEnv

from CleverSheep.Prog import Files
from CleverSheep.Test import DataMaker
from CleverSheep.Test.Tester import *

# The module under test.
from CleverSheep.Test.Tester import Execution


def run_test(script):
    cmd = "./%s --columns=80 >subtest.log" \
          " 2>&1 </dev/null" % script
    os.system(cmd)


class Test_reporter(Suite):
    """Tests for reporter to file."""
    def cleanUp(self):
        Files.rmFile("subtest.log")
        Files.rmFile("report.txt")

    def setUp(self):
        # Change to the mock tests directory.
        os.chdir(os.path.join(os.path.dirname(__file__), "mocktests"))
        self.cleanUp()

    def tearDown(self):
        self.cleanUp()

    @test
    def basic_reporting(self):
        """Verify that basic reporting works OK.

        Runs a test with a single reporter plug-in, which logs minimally to
        a file.

        """
        run_test("test_report.py")

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | Enter suite A test demonstrating the reporter plug-in feature.
        | Enter suite This is a test suite
        | Enter The first test.
        | Leave The first test.
        | Enter The second test.
        | Leave The second test.
        | Leave suite
        | Leave suite
        |
        """), open("report.txt").read())

    @test
    def state_reporting(self):
        """Verify that test and suite state reporting works.

        Runs a test with a single reporter plug-in, which logs the details
        of the test and suite at exit.

        The test and suite enter methods are left undefined to veirfy that
        only the required methods need to be over-ridden.

        """
        run_test("test_report2.py")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | Leave: The first test.
        | Result = PASS
        | Leave: The second test.
        | Result = FAIL
        | Details = Oops
        | ./test_report2.py: bbb
        |    37  :     @test
        |    38  :     def bbb(self):
        |    39  :         """The second test."""
        | ==>40  :         fail("Oops")
        |    41  :
        |    42  :
        |    43  : if __name__ == "__main__":
        | Leave suite
        | Result = CHILD_FAIL
        | Leave suite
        | Result = CHILD_FAIL
        |
        '''), open("report.txt").read())


if __name__ == "__main__":
    runModule()

