#!/usr/bin/env python
"""Tests for logging to file.

"""

import os

import CheckEnv
from Tester_utils import LogLine

from CleverSheep.Prog import Files
from CleverSheep.Test import DataMaker, ImpUtils
from CleverSheep.Test.Tester import *

import support

# The module under test.
module_under_test = "../../CleverSheep/Test/Tester/Execution.py"
from CleverSheep.Test.Tester import Execution


class Test_logging(Suite):
    """Tests for logging to file."""
    def cleanUp(self):
        Files.rmFile("subtest.log")

    def setUp(self):
        # Change to the mock tests directory.
        os.chdir(os.path.join(os.path.dirname(__file__), "mocktests"))
        self.cleanUp()

    def tearDown(self):
        self.cleanUp()

    @test
    def default_logging_level(self):
        """Verify correct behaviour for the default logging level.

        The default level is debug, which means everything should appear in the
        log file. In addition, print(statements should appear at the DEBUG)
        level.

        """
        support.run_test("test_log_level.py", "1", exitCode=1)
        # TODO: Why the early return?
        return

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | 23:29:21.829 INFO    Started on 13/08/2010\n
        | 23:29:21.961 INFO    Suite: A test demonstrating graceful handling of bad attempts to\n
        | |                           log.\n
        | 23:29:21.985 INFO    Suite:   This is a test suite\n
        | 23:29:22.009 INFO    Test:      1  : A test\n
        | 23:29:22.046 DEBUG       Format but missing args %d %d, (1,)\n
        | 23:29:22.066 INFO    Test:      Passed!\n
        | 23:29:22.105 INFO    \n
        | 23:29:22.106 INFO    Summary of the entire run\n
        | 23:29:22.217 INFO    Suite: A test demonstrating graceful handling of bad attempts to\n
        | |                           log..........................................................        PASS\n
        | 23:29:22.221 INFO      Suite: This is a test suite.......................................        PASS\n
        | 23:29:22.223 INFO        Test: 1   : A test..............................................        PASS\n
        """), open("test.log").read())

    @test
    def bad_log_calls_get_handled_nicely(self):
        """A bad call to the logger should not break the test.

        The underlying logger expects to have a format string followed by the
        interpolation arguments. If the test writer gets this wrong, the test
        should still pass and the log output shuld be best effort.

        """
        support.run_test("test_bad_logging.py")

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | 18:48:10.986 INFO    Started on 15/08/2010
        | 18:48:11.120 INFO    Suite: A test demonstrating graceful handling of bad attempts to
        | |                           log.
        | 18:48:11.144 INFO    Suite:   This is a test suite
        | 18:48:11.169 INFO    Test:      1   : A test
        | 18:48:11.208 DEBUG                    Format but missing args %d %d, (1,)
        | 18:48:11.229 INFO    Test:            PASS
        | 18:48:11.270 INFO
        | 18:48:11.270 INFO    Summary of the entire run
        | 18:48:11.383 INFO    Suite: A test demonstrating graceful handling of bad attempts to
        | |                           log.........................................................        PASS
        | 18:48:11.386 INFO    Suite:   This is a test suite......................................        PASS
        | 18:48:11.388 INFO    Test:      1   : A test............................................        PASS
        |
        """), open("temp.log").read(), lineWrapper=LogLine)

    @test(uid="log-no-prop")
    def log_no_prop(self):
        """The standard cs_test logger should not propagate to the root logger.

        """
        #> Run the test_mod_ns.py. The attribute error in the build test suite
        #> should be gracefully handled.
        support.makePySingleTestFile("test_broken.py",
            tests=['''
            | def a(self):
            |    """Test A"""
            |    import logging, sys
            |    log = logging.getLogger()
            |    log.addHandler(logging.StreamHandler(sys.stderr))
            |    fail("Oops")
            |'''])
        data = support.run_test("test_broken.py", exitCode=1)

        support.checkTerminalOutput('''
        | Testing the tester.
        |   Suite A
        |     2   : Test B..........        PASS
        |   Suite B
        |     3   : Test A..........        PASS
        ''', data.terminal)

    @test(uid="log-no-prop")
    def log_no_prop(self):
        """The standard cs_test logger should not propagate to the root logger.

        It is only necessary that this test runs OK. This was added for a
        regression and failure will cause the sub-test to be killed, causing a failure
        in run_test.

        """
        #> Run the test_mod_ns.py. The attribute error in the build test suite
        #> should be gracefully handled.
        support.makePySingleTestFile("test_broken.py",
            tests=['''
            | def a(self):
            |    """Test A"""
            |    import logging, sys
            |    log = logging.getLogger()
            |    log.addHandler(logging.StreamHandler(sys.stderr))
            |    fail("Oops")
            |'''])
        data = support.run_test("test_broken.py", exitCode=1)


if __name__ == "__main__":
    runModule()

