#!/usr/bin/env python
"""CleverSheep.Test.Tester.Assertions unit tests"""

import os
import re
import sys

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test.Tester import Errors
from CleverSheep.Test import DataMaker

from TestSupport.Files import FileBasedTests, ensureNoFile

# The module under test.
module_under_test = "../../CleverSheep/Test/Tester/Assertions.py"
from CleverSheep.Test.Tester import Assertions


class Test_CoreAssertions(Suite):
    """Tests for the core assertion functions."""
    @test(testID="ulp-001")
    def fail_ifFloatEqual1(self):
        """Test basic failIfFloatEqual assertion."""
        exc = failUnlessRaises(Errors.Failure,
                Assertions.failIfEqual, 1, 1)
        failUnlessEqual("failIfEqual: 1 == 1", exc.message)


if __name__ == "__main__":
    runModule()

