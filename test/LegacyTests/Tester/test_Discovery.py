#!/usr/bin/env python
"""CleverSheep.Test.Tester.Assertions unit tests"""

import os
import sys
import commands
from cStringIO import StringIO

import CheckEnv

from CleverSheep.Test.Tester import *
#from CleverSheep.Test.Tester import Fixture
from CleverSheep.Test.Tester import Collection
from CleverSheep.Test.DataMaker import literalText2Text

# TODO: Rework these tests.
raise Collection.Unsupported()

# The module under test.
module_under_test = "../../CleverSheep/Test/Tester/Discovery.py"
from CleverSheep.Test.Tester import Discovery


class ImportModuleUnderTest(Suite):
    """Tests for the importModuleUnderTest function.

    """
    @test
    def importModuleUnderTest(self):
        """Test the importModuleUnderTest function.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests")

        #> Import a module and verify that it was imported correctly.
        mod = Discovery.importModuleUnderTest("mod.py", cwd=True)
        failIf(mod is None)
        path = mod.__file__
        if path.endswith(".pyc"):
            path = path[:-1]
        path = os.path.join(os.getcwd(), path)
        expect = os.path.join(os.getcwd(), "mod.py")
        failUnlessEqual(expect, path)

    @test
    def importModuleUnderTestWitReload(self):
        """Test the importModuleUnderTest with the reload option.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests")

        #> Import without reload. Then delet the ``add`` member.
        mod = Discovery.importModuleUnderTest("mod.py", cwd=True)
        del mod.add
        failIf(hasattr(mod, "add"))

        #> Import again, with reload. This should restore the ``add`` member.
        Discovery.importModuleUnderTest("mod.py", doReload=True, cwd=True)
        failUnless(hasattr(mod, "add"))

    @test
    def badImport(self):
        """An import problem should raise ImportError.

        """
        failUnlessRaises(ImportError, Discovery.importModuleUnderTest,
                "no such module")


class AddingModulesUnderTest(Suite):
    """Tests for the addModulesUnderTest function.

    """
    def tearDown(self):
        for n in ("mod.py", "mod2.py"):
            expect = os.path.join(os.getcwd(), n)
            if expect in Discovery.modules_under_test:
                del Discovery.modules_under_test[expect]

    @test
    def addSingleModule(self):
        """Test the addModulesUnderTest with a single module string.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests")

        #> Add two modules and verify that it has been added.
        Discovery.addModulesUnderTest("mod.py")
        expect = os.path.join(os.getcwd(), "mod.py")
        failUnless(expect in Discovery.modules_under_test)

    @test
    def addMultiModules(self):
        """Test the addModulesUnderTest with a list of modules.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests")

        #> Add two modules and verify that it has been added.
        Discovery.addModulesUnderTest(
                ["mod.py", "mod2.py"])
        expect = os.path.join(os.getcwd(), "mod.py")
        failUnless(expect in Discovery.modules_under_test)

        expect = os.path.join(os.getcwd(), "mod2.py")
        failUnless(expect in Discovery.modules_under_test)

    @test
    def addWithPath(self):
        """Use the moduleDir argument.

        """
        #> Need to work in this script's sub-directory.
        os.chdir(os.path.dirname(__file__))

        #> Add two modules and verify that it has been added.
        Discovery.addModulesUnderTest("mod.py", moduleDir="mocktests")
        expect = os.path.join(os.getcwd(), "mocktests/mod.py")
        failUnless(expect in Discovery.modules_under_test)

    @test
    def provideNoneAsPath(self):
        """Paths of ``None`` should be silently ignored.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests")

        #> Add None as the only path.
        Discovery.addModulesUnderTest(None)

        #> Add None within a list of paths.
        Discovery.addModulesUnderTest([None, "mod2.py"])
        expect = os.path.join(os.getcwd(), "mod2.py")
        failUnless(expect in Discovery.modules_under_test)


class AddingTests(Suite):
    """Tests for the loadTestsFromTestSource function.

    """
    #@test - Fixture is dead
    def addFromSource(self):
        """Test the loadTestsFromTestSource.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests")

        #> Create a ModuleSource and use it with ``loadTestsFromTestSource``
        #> function.
        source = Fixture.ModuleSource("test_discover1.py")
        suite = Discovery.loadTestsFromTestSource(source)

    #@test - Fixture is dead
    def missingSourceDocString(self):
        """A missing source file doc string should cause an early exit.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests")

        #> Create a ModuleSource and use it with ``loadTestsFromTestSource``
        #> function.
        source = Fixture.ModuleSource("test_discover2.py")
        stderr, sys.stderr = sys.stderr, StringIO()
        try:
            try:
                suite = Discovery.loadTestsFromTestSource(source)
            except SystemExit as exc:
                pass
            else:
                fail("The test load operation should have tried to exit")
        finally:
            stderr, sys.stderr = sys.stderr, stderr

        data = {"root": os.path.abspath(os.path.dirname("test_discover2.py"))}
        failUnlessEqualStrings(literalText2Text(r"""
        | Missing docstring for a test script
        | Module directory: %(root)s
        | Module file: %(root)s/test_discover2.py
        |
        """ % data), stderr.getvalue())

    #@test - Fixture is dead
    def missingClassDocString(self):
        """A missing class doc string should cause an early exit.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests")

        #> Create a ModuleSource and use it with ``loadTestsFromTestSource``
        #> function.
        source = Fixture.ModuleSource("test_discover3.py")
        stderr, sys.stderr = sys.stderr, StringIO()
        try:
            try:
                suite = Discovery.loadTestsFromTestSource(source)
            except SystemExit as exc:
                pass
            else:
                fail("The test load operation should have tried to exit")
        finally:
            stderr, sys.stderr = sys.stderr, stderr

        data = {"root": os.path.abspath(os.path.dirname("test_discover3.py"))}
        failUnlessEqualStrings(literalText2Text(r"""
        | Missing docstring for a test suite
        | Module directory: %(root)s
        | Module file: %(root)s/test_discover3.py
        | Class: B
        | Line: 30
        |
        """ % data), stderr.getvalue())

    #@test - Fixture is dead
    def missingTestDocString(self):
        """A missing test docstring should cause an early exit.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests")

        #> Create a ModuleSource and use it with ``loadTestsFromTestSource``
        #> function.
        source = Fixture.ModuleSource("test_discover4.py")
        stderr, sys.stderr = sys.stderr, StringIO()
        try:
            try:
                suite = Discovery.loadTestsFromTestSource(source)
            except SystemExit as exc:
                pass
            else:
                fail("The test load operation should have tried to exit")
        finally:
            stderr, sys.stderr = sys.stderr, stderr

        data = {"root": os.path.abspath(os.path.dirname("test_discover4.py"))}
        failUnlessEqualStrings(literalText2Text(r"""
        | Missing docstring for a test
        | Module directory: %(root)s
        | Module file: %(root)s/test_discover4.py
        | Class: B
        | Function: a
        | Line: 32
        |
        """ % data), stderr.getvalue())

    #@test - Fixture is dead
    def missingAllTestsDocString(self):
        """A missing docstring in all_tests.py should cause an early exit.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests")

        #> Create a ModuleSource and use it with ``loadTestsFromTestSource``
        #> function.
        source = Fixture.TreeSource("all/all_tests.py")
        stderr, sys.stderr = sys.stderr, StringIO()
        try:
            try:
                suite = Discovery.loadTestsFromTestSource(source)
            except SystemExit as exc:
                pass
            else:
                fail("The test load operation should have tried to exit")
        finally:
            stderr, sys.stderr = sys.stderr, stderr

        data = {"root": os.path.abspath(os.path.dirname("all"))}
        failUnlessEqualStrings(literalText2Text(r"""
        | Missing docstring for a test script
        | Module directory: %(root)s/all
        | Module file: %(root)s/all/all_tests.py
        |
        """ % data), stderr.getvalue())

    @test
    def attribute_error(self):
        """Regression for a bug.

        The way test discovery loaded suite could fail in a messy way because
        of an attribute error during an import.

        """
        #> Need to work in this script's mocktests/bad_discover sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests/bad_discover")

        #> Run the all_tests.py. The attribute error in the build test suite
        #> should be gracefully handled.
        text = commands.getoutput("./all_tests.py")
        pwd = os.getcwd()
        failUnlessEqualStrings(literalText2Text(r"""
         | Error occured during test discovery
         | Traceback (most recent call last):
         |   File "%s/test_discover.py", line 18, in <module>
         |     a = x.qqq
         | AttributeError: 'X' object has no attribute 'qqq'
        """ % pwd), text)


class ImportBehaviour(Suite):
    """Tests for the special import behaviour for test scripts.

    Given a test tree like this::

        mocktests/localimp/
        |-- A
        |   |-- all_tests.py
        |   |-- helper.py
        |   |-- helper.pyc
        |   |-- test_a.py
        |   |-- test_a.pyc
        |   `-- test.log
        |-- all_tests.py
        `-- B
            |-- all_tests.py
            |-- helper.py
            |-- helper.pyc
            |-- test_b.py
            |-- test_b.pyc
            `-- test.log

    Then if you run just test_a.py, in directory A, you will expect it to
    import helper.py in directory A. Similarly, for test_b.py that should pick
    up B/helper.py. However, if you run all_tests.py at the root of the tree,
    then using the normal Python rules, neither test script will be able to
    import its preferred module. Furthermore, if you tweak the import paths in
    the two scripts, the first to be loaded will get its helper and the second
    will get the same helper - not the one you would expect.

    Therefore, when importing test scripts (during test discovery) the
    framework provides its own importer, which can ensure the intended
    behaviour.

    Of course all of this must happen without breaking the normal import
    rules in all other circumstances.

    """
    @test(testID="imp-01")
    def addFromSource(self):
        """module local to tests script directory should be preferentially
        imported.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests/localimp")

        #> Run the all_tests.py script at the root and check that both helpert
        #> scripts are imported in the expected order.
        text = commands.getoutput("./all_tests.py")
        failUnlessEqualStrings(literalText2Text(r"""
        | Helper A
        | Helper B
        | All tests
        """), text)

    @test
    def tricky_import1(self):
        """The special test framework import mechanism must not break normal
        imports.

        """
        import os
        print("...", os.environ["PYTHONPATH"])

        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests/imp")

        text = commands.getoutput("./all_tests.py")
        failUnlessEqualStrings(literalText2Text(r"""
        | MAIN package_util
        | TEST mock_util
        | Example all tests
        """), text)

    @test
    def tricky_import2(self):
        """Another special import case.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests/imp2")

        #> Create a ModuleSource and use it with ``loadTestsFromTestSource``
        #> function.
        text = commands.getoutput("./all_tests.py")
        failUnlessEqualStrings(literalText2Text(r"""
        | MAIN package_util
        | TEST mock_util
        | Example all tests
        """), text)

    @test
    def traceBack(self):
        """Test that import failure tracebacks are clean.

        Since CleverSheep provides its own import handling when importing
        discovered test modules, any exception during the import would normally
        provide a confusing traceback.

        So CleverSheep tries to fix this.

        """
        #> Need to work in this scripts mocktests sub-directory.
        os.chdir(os.path.dirname(__file__))
        os.chdir("mocktests/tb")

        #> Run the all_tests.py script at the root and check that both helpert
        #> scripts are imported in the expected order.
        text = commands.getoutput("./all_tests.py")
        data = {"root": os.path.abspath(os.path.dirname("all_tests.py"))}
        failUnlessEqualStrings(literalText2Text(r"""
         | Error occured during test discovery
         | Traceback (most recent call last):
         |   File "%(root)s/A/test_a.py", line 6, in <module>
         |     import helper
         |   File "%(root)s/A/helper.py", line 2, in <module>
         |     assert 1 == 0
         | AssertionError
         |
        """ % data), text)


if __name__ == "__main__":
    runModule()
