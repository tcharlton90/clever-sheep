"""Check that the test environment is correct.

When we run the CleverSheep test, we need to be using the local CleverSheep
library, not any parts of an installed version. This is because we otherwise
end up not testing the local code or with duplicate modules loaded, which
causes test to fail in strange ways. For example, the ``reload`` built-in
fails in the RichTerm tests.

"""


import os
import sys

from CleverSheep.Test import ImpUtils

ImpUtils.addPathAtRoot("src/CHANGES.txt")
