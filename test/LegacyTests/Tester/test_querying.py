#!/usr/bin/env python
"""Tests for the various ways we can query the test tree.

The CleverSheep test framework allows us to get different types of information
from the test tree. This includes:

- A summary of the tree.
- A detilaed description of the tree.
- A list of test ID, which is useful as a means to suppress tests without
  modifiying the scripts themselves.

"""


import os

import CheckEnv

from CleverSheep.Prog import Files
from CleverSheep.Test import DataMaker, ImpUtils
from CleverSheep.Test.Tester import *

import support

# The module under test.
module_under_test = "../../CleverSheep/Test/Tester/Execution.py"
from CleverSheep.Test.Tester import Execution

# TODO: Common code with test_Execution.py
class Common(Suite):
    def cleanUp(self):
        Files.rmFile("subtest.log")
        Files.rmFile("temp.log")

    def setUp(self):
        # Change to the mock tests directory.
        self.d = {"base": os.path.abspath(os.path.dirname(__file__))}
        os.chdir(os.path.join(os.path.dirname(__file__), "mocktests/query"))
        self.info = {
            "base": os.path.dirname(os.path.abspath(__file__)),
            "cwd": os.getcwd(),
        }
        self.cleanUp()

    def tearDown(self):
        self.cleanUp()


class Test_summary(Common):
    """Tests for the summary output of CleverSheep.

    """
    @test
    def full_summary(self):
        """A full summary of all tests in a suite tree.

        """
        support.run_test("all_tests.py", "--summary")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | All the querying tests.
        |   One set of tests.
        |     A set of tests.
        |       1   : The first test.
        |       2   : The second test.
        |   Another set of tests.
        |     Another set of tests.
        |       3   : The first test in the second set.
        |       4   : The second test in the second set.
        '''), open("subtest.log").read())

    @test
    def sel_by_num(self):
        """A summary of tests selected by number.

        """
        support.run_test("all_tests.py", "--summary", "2", "3")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | All the querying tests.
        |   One set of tests.
        |     A set of tests.
        |       2   : The second test.
        |   Another set of tests.
        |     Another set of tests.
        |       3   : The first test in the second set.
        '''), open("subtest.log").read())

    @test
    def sel_by_text(self):
        """A summary of tests selected by plain text.

        """
        support.run_test("all_tests.py", "--summary", "first")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | All the querying tests.
        |   One set of tests.
        |     A set of tests.
        |       1   : The first test.
        |   Another set of tests.
        |     Another set of tests.
        |       3   : The first test in the second set.
        '''), open("subtest.log").read())

    @test
    def sel_by_regexp(self):
        """A summary of tests selected by regular expression.

        """
        support.run_test("all_tests.py", "--summary", "~f..st")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | All the querying tests.
        |   One set of tests.
        |     A set of tests.
        |       1   : The first test.
        |   Another set of tests.
        |     Another set of tests.
        |       3   : The first test in the second set.
        '''), open("subtest.log").read())


class Test_details(Common):
    """Tests for the detailed output of CleverSheep.

    """
    # TODO: This does not verify the display of test steps.
    @test
    def full_details(self):
        """A full details of all tests in a suite tree.

        """
        support.run_test("all_tests.py", "--details")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | All the querying tests.
        | =======================
        |
        |   This provides a tree of tests that test the test framework.
        |
        |
        |   One set of tests.
        |   =================
        |
        |
        |     A set of tests.
        |     ===============
        |
        |       1   : The first test.
        |
        |             This is one of many.
        |
        |             Path:     test_one.py
        |             Class:    Suite1
        |             Function: one
        |
        |       2   : The second test.
        |
        |             This is the second of many.
        |
        |             Path:     test_one.py
        |             Class:    Suite1
        |             Function: two
        |
        |
        |   Another set of tests.
        |   =====================
        |
        |
        |     Another set of tests.
        |     =====================
        |
        |       3   : The first test in the second set.
        |
        |             Will normally be third.
        |
        |             Path:     test_two.py
        |             Class:    Suite2
        |             Function: one
        |
        |       4   : The second test in the second set.
        |
        |             Will normally be fourth.
        |
        |             Path:     test_two.py
        |             Class:    Suite2
        |             Function: two
        |
        |
        ''' % self.d), open("subtest.log").read())

    @test
    def sel_by_num(self):
        """Details of tests selected by number.

        """
        support.run_test("all_tests.py", "--details", "2", "3")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | All the querying tests.
        | =======================
        |
        |   This provides a tree of tests that test the test framework.
        |
        |
        |   One set of tests.
        |   =================
        |
        |
        |     A set of tests.
        |     ===============
        |
        |       2   : The second test.
        |
        |             This is the second of many.
        |
        |             Path:     test_one.py
        |             Class:    Suite1
        |             Function: two
        |
        |
        |   Another set of tests.
        |   =====================
        |
        |
        |     Another set of tests.
        |     =====================
        |
        |       3   : The first test in the second set.
        |
        |             Will normally be third.
        |
        |             Path:     test_two.py
        |             Class:    Suite2
        |             Function: one
        |
        |
        ''' % self.d), open("subtest.log").read())

    @test
    def sel_by_text(self):
        """Details of tests selected by plain text.

        """
        support.run_test("all_tests.py", "--details", "first")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | All the querying tests.
        | =======================
        |
        |   This provides a tree of tests that test the test framework.
        |
        |
        |   One set of tests.
        |   =================
        |
        |
        |     A set of tests.
        |     ===============
        |
        |       1   : The first test.
        |
        |             This is one of many.
        |
        |             Path:     test_one.py
        |             Class:    Suite1
        |             Function: one
        |
        |
        |   Another set of tests.
        |   =====================
        |
        |
        |     Another set of tests.
        |     =====================
        |
        |       3   : The first test in the second set.
        |
        |             Will normally be third.
        |
        |             Path:     test_two.py
        |             Class:    Suite2
        |             Function: one
        |
        |
        ''' % self.d), open("subtest.log").read())

    @test
    def sel_by_regexp(self):
        """Details of tests selected by regular expression.

        """
        support.run_test("all_tests.py", "--details", "~f..st")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | All the querying tests.
        | =======================
        |
        |   This provides a tree of tests that test the test framework.
        |
        |
        |   One set of tests.
        |   =================
        |
        |
        |     A set of tests.
        |     ===============
        |
        |       1   : The first test.
        |
        |             This is one of many.
        |
        |             Path:     test_one.py
        |             Class:    Suite1
        |             Function: one
        |
        |
        |   Another set of tests.
        |   =====================
        |
        |
        |     Another set of tests.
        |     =====================
        |
        |       3   : The first test in the second set.
        |
        |             Will normally be third.
        |
        |             Path:     test_two.py
        |             Class:    Suite2
        |             Function: one
        |
        |
        ''' % self.d), open("subtest.log").read())


class Test_ids(Common):
    """Tests for the ids-summary output of CleverSheep.

    """
    @test
    def full_ids(self):
        """A full ids-summary of all tests in a suite tree.

        """
        support.run_test("all_tests.py", "--ids-summary")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | (
        |   # All the querying tests.
        |   #   One set of tests.
        |   #     A set of tests.
        |   #       1   : The first test.
        |                 (('%(cwd)s', 'test_one.py'), 'Suite1', 'one'),
        |   #       2   : The second test.
        |                 (('%(cwd)s', 'test_one.py'), 'Suite1', 'two'),
        |   #   Another set of tests.
        |   #     Another set of tests.
        |   #       3   : The first test in the second set.
        |                 (('%(cwd)s', 'test_two.py'), 'Suite2', 'one'),
        |   #       4   : The second test in the second set.
        |                 (('%(cwd)s', 'test_two.py'), 'Suite2', 'two'),
        | )
        |
        ''' % self.info), open("subtest.log").read())

    @test
    def sel_by_num(self):
        """Details of tests selected by number.

        """
        support.run_test("all_tests.py", "--ids-summary", "2", "3")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | (
        |   # All the querying tests.
        |   #   One set of tests.
        |   #     A set of tests.
        |   #       2   : The second test.
        |                 (('%(cwd)s', 'test_one.py'), 'Suite1', 'two'),
        |   #   Another set of tests.
        |   #     Another set of tests.
        |   #       3   : The first test in the second set.
        |                 (('%(cwd)s', 'test_two.py'), 'Suite2', 'one'),
        | )
        |
        ''' % self.info), open("subtest.log").read())

    @test
    def sel_by_text(self):
        """Details of tests selected by plain text.

        """
        support.run_test("all_tests.py", "--ids-summary", "first")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | (
        |   # All the querying tests.
        |   #   One set of tests.
        |   #     A set of tests.
        |   #       1   : The first test.
        |                 (('%(cwd)s', 'test_one.py'), 'Suite1', 'one'),
        |   #   Another set of tests.
        |   #     Another set of tests.
        |   #       3   : The first test in the second set.
        |                 (('%(cwd)s', 'test_two.py'), 'Suite2', 'one'),
        | )
        |
        ''' % self.info), open("subtest.log").read())

    @test
    def sel_by_regexp(self):
        """Details of tests selected by regular expression.

        """
        support.run_test("all_tests.py", "--ids-summary", "~f..st")

        failUnlessEqualStrings(DataMaker.literalText2Text('''
        | (
        |   # All the querying tests.
        |   #   One set of tests.
        |   #     A set of tests.
        |   #       1   : The first test.
        |                 (('%(cwd)s', 'test_one.py'), 'Suite1', 'one'),
        |   #   Another set of tests.
        |   #     Another set of tests.
        |   #       3   : The first test in the second set.
        |                 (('%(cwd)s', 'test_two.py'), 'Suite2', 'one'),
        | )
        |
        ''' % self.info), open("subtest.log").read())


if __name__ == "__main__":
    runModule()
