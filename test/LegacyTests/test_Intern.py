#!/usr/bin/env python
"""Tests for the Intern module."""


from CleverSheep.Test.Tester import *

from CleverSheep.Prog import Intern


class Test_Ustr(Suite):
    """Tests for the Intern module."""
    @test
    def basic_identity(self):
        """Verify basic identity maintenance"""
        #: Simple strings, such as 'a' obviously should work.
        a = Intern.intern("a")
        b = Intern.intern("a")
        failUnless(a is b)

        #: A string such as "a" + "b" should match "ab".
        a = Intern.intern("ab")
        b = Intern.intern("a" + "b")
        failUnless(a is b)

        #: A string created from other string (s1 + s2) should work.
        s1 = "a"
        s2 = "b"
        a = Intern.intern("ab")
        b = Intern.intern(s1 + s2)
        failUnless(a is b)

    @test
    def string_conversion(self):
        """Verify using the 'internIfStr' function to convert to Intern"""
        #: Simple strings, such as 'a' obviously should work.
        a = Intern.internIfStr("a")
        b = Intern.intern("a")
        failUnless(a is b)

        #: The function should handle Intern instances.
        a = Intern.internIfStr(Intern.intern("a"))
        b = Intern.intern("a")
        failUnless(a is b)

    @test
    def function_decoration(self):
        """Exercise the internFuncStrings decorator."""
        @Intern.internFuncStrings
        def test(*args, **kwargs):
            if args:
                x, y = args
            else:
                x = kwargs["x"]
                y = kwargs["y"]
            return x, y

        #: Plain strings should become Intern instances.
        a, b = test("a", "a")
        failUnless(a is b)

        #: Non-strings should be left unmodified
        a, b = test("a", 1)

        #: Ustrs should be left unmodified
        a, b = test("a", Intern.intern("a"))
        failUnless(a is b)

        #: Keyword arguments should work as well
        a, b = test(y="a", x="a")
        failUnless(a is b)

    @test
    def method_decoration(self):
        """Exercise the ustr_method decorator.
        
        This is basically a repeat of the `function_decoration` test, but we
        use a decorated method instead.
        """
        #: Plain strings should become Intern instances.
        a, b = self.test("a", "a")
        failUnless(a is b)

        #: Non-strings should be left unmodified
        a, b = self.test("a", 1)

        #: Ustrs should be left unmodified
        a, b = self.test("a", Intern.intern("a"))
        failUnless(a is b)

        #: Keyword arguments should work as well
        a, b = self.test(y="a", x="a")
        failUnless(a is b)

    @test
    def intern_properties(self):
        """Exercise the internProperty helper."""
        class X(object):
            a = Intern.internProperty("_a")
        x = X()

        #: Simple strings, such as 'a' obviously should work.
        a = Intern.intern("a")
        x.a = "a"
        failUnless(a is x.a)

        #: A string such as "a" + "b" should match "ab".
        a = Intern.intern("ab")
        x.a = "a" + "b"
        failUnless(a is x.a)

        #: A string created from other string (s1 + s2) should work.
        s1 = "a"
        s2 = "b"
        a = Intern.intern("ab")
        x.a = s1 + s2
        failUnless(a is x.a)

    @Intern.internMethodStrings
    def test(self, *args, **kwargs):
        if args:
            x, y = args
        else:
            x = kwargs["x"]
            y = kwargs["y"]
        return x, y


if __name__ == "__main__":
    runModule()
