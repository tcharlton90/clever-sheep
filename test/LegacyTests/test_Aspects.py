#!/usr/bin/env python
"""CleverSheep.Prog.Aspects unit tests"""

import os

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import ImpUtils
from CleverSheep.Prog.Curry import Curry

# The module under test.
from CleverSheep.Prog import Aspects

const = 1000

def use2000():
    global const
    const = 2000
    return 3000

def useArg(arg):
    global const
    const = arg

def func(a, b, c=None):
    if c is None:
        c = const
    return a * b + c


class Test_CallWrapper(Suite):
    """Test for the callWrapper function."""
    def tearDown(self):
        global const
        const = 1000

    @test
    def Unwrapped(self):
        """Check unwrapped function is not effectively changed."""
        failUnlessEqual(1006, func(2, 3))
        f = Aspects.callWrapper(func)
        failUnlessEqual(1006, f(2, 3))


    @test
    def before(self):
        """Check wrapping with just a before function."""
        f = Aspects.callWrapper(func, before=use2000)
        failUnlessEqual(2006, f(2, 3))


    @test
    def after(self):
        """Check wrapping with just an after function."""
        f = Aspects.callWrapper(func, after=use2000)
        failUnlessEqual(1006, f(2, 3))
        failUnlessEqual(2000, const)


    @test
    def both(self):
        """Check wrapping with both before and after functions."""
        f = Aspects.callWrapper(func, before=use2000, after=useArg)
        failUnlessEqual(2006, f(2, 3))
        failUnlessEqual(3000, const)


class Test_decorate(Suite):
    """Test for the in-place decorate function."""
    @test
    def both(self):
        """Check in-place decoration."""
        # Create the decorator function and sanity check it.
        decor = Curry(Aspects.callWrapper, before=use2000, after=useArg)
        f = decor(func)
        failUnlessEqual(2006, f(2, 3))
        failUnlessEqual(3000, const)


        # Now try for the in-place decoration.
        failUnlessEqual(3006, func(2, 3))
        f = Aspects.decorate(func, decor)
        failUnlessEqual(2006, func(2, 3))
        failUnlessEqual(3000, const)

        # Now decorate a class method.
        class X(object):
            def func(self, a, b, c=None):
                if c is None:
                    c = const
                return a * b + c
        x = X()
        failUnlessEqual(3006, x.func(2, 3))
        f = Aspects.decorate(X.func, decor, X)

        failUnlessEqual(2006, x.func(2, 3))

    @test
    def exportedFunction(self):
        """Verify that the exportedFunction decorator works."""
        global __all__
        saves_all = None
        try:
            saved_all = __all__
            del __all__
        except NameError:
            pass

        def aaa():
            pass

        f = Aspects.exportedFunction(aaa)
        failUnless("aaa" in __all__)

        __all__.remove("aaa")
        f = Aspects.exportedFunction(aaa)
        failUnless("aaa" in __all__)


class Test_protectCwd(Suite):
    """Test for the protectCwd function."""
    @test
    def proectCwd(self):
        """Check the protextCwd decorator."""
        here = os.getcwd()

        @Aspects.protectCwd
        def f():
            os.chdir("..")
            failIfEqual(here, os.getcwd())
            return 5

        failUnlessEqual(5, f())
        failUnlessEqual(here, os.getcwd())


class Test_allSubclasses(Suite):
    """Test for the allSubclasses function."""
    @test
    def allSubclasses(self):
        """Check the allSubclasses function"""
        class A(object): pass
        class A_A(A): pass
        class A_B(A): pass
        class A_C(A): pass
        class A_B_A(A_B): pass
        class A_B_B(A_B): pass

        expect = (A, A_A, A_B, A_C, A_B_A, A_B_B)
        for i, klass in enumerate(Aspects.allSubclasses(locals(), A)):
            failUnless(klass in expect)
        failUnlessEqual(len(expect), i + 1)

        del klass # Keep the local free of extra class references.
        expect = (A_B, A_B_A, A_B_B)
        for i, klass in enumerate(Aspects.allSubclasses(locals(), A_B)):
            failUnless(klass in expect)
        failUnlessEqual(len(expect), i + 1)


class Test_decorMethods(Suite):
    """Test for the decorMethods function."""
    @test
    def decorMethods_using_pattern(self):
        """Decorate methods in a class, selecting methods using regexp matching."""
        class X(object):
            def todec_a(self, x):
                return x
            def not_todec_b(self, x):
                return x

        @Aspects.decorator
        def wrapper(func, *args):
            v = func(*args)
            return v * v

        Aspects.decorMethods(X, "todec_", wrapper)

        x = X()
        failUnlessEqual(5, x.not_todec_b(5))
        failUnlessEqual(25, x.todec_a(5))

    def decorMethods_using_predcate(self):
        """Decorate methods in a class, selecting methods using a predicate."""
        class X(object):
            def todec_a(self, x):
                return x
            def not_todec_b(self, x):
                return x

        @Aspects.decorator
        def wrapper(func, *args):
            v = func(*args)
            return v * v

        Aspects.decorMethods(X, lambda n: n.startswith("todec_"), wrapper)

        x = X()
        failUnlessEqual(5, x.not_todec_b(5))
        failUnlessEqual(25, x.todec_a(5))


class Querying(Suite):
    """The special querying functions."""
    @test
    def getCallerDict(self):
        """The _getCallerDict function.

        This is to get coverage, which takes a callerDict argument.

        """
        d = {}
        dd = Aspects._getCallerDict(callerDict=d)
        failUnless(d is dd)


class Intelliprop(Suite):
    """The intelliprop decorator."""
    def getOnly(self):
        """Just gets"""
        return 1

    def readWrite(self, value=None):
        """Reads and writes"""
        return 1

    def noDoc(self):
        return 1

    @test
    def readonly(self):
        """Basic read-only property."""
        p = Aspects.intelliprop(self.getOnly)
        failUnlessEqual("Property: read-only\n\nJust gets", p.__doc__)

    @test
    def readwrite(self):
        """Basic read-write property."""
        p = Aspects.intelliprop(self.readWrite)
        failUnlessEqual("Property: read-write\n\nReads and writes", p.__doc__)

    @test
    def docRequired(self):
        """Missing codstring causes an exception."""
        failUnlessRaises(SyntaxError,
                Aspects.intelliprop, self.noDoc)


if __name__ == "__main__":
    runModule()
