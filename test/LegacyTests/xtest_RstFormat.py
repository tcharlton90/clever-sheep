#!/usr/bin/env python
"""Tests for the rformat program.

A side effect of these tests is that the rst2rst package gets tested.
"""

import subprocess
import os

import CheckEnv

from CleverSheep.Test import Tester
from CleverSheep.Test.Tester import *
from CleverSheep.Test.DataMaker import literalText2Text
from CleverSheep.Prog import Files
from CleverSheep.Prog.Curry import Curry

try:
    import docutils
except ImportError:
    raise Tester.Unsupported

modules_under_test = [
    #"../CleverSheep/Rst/RstFormat.py",
    #"../CleverSheep/Rst/rst2rst/__init__.py",
    #"../CleverSheep/Rst/rst2rst/Roman.py",
    #"../CleverSheep/Rst/rst2rst/Visitor.py",
]

rformat = importModuleUnderTest("../CleverSheep/Rst/RstFormat.py")

makeText = Curry(literalText2Text, noTail=True)

def run(text):
    """Run the rformat program on some text.

    :Return:
        A tuple of (stdout, errors).
    """
    proc = subprocess.Popen(["../rformat"],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        errors=subprocess.PIPE)
    return proc.communicate(text)


def run(text, errorHandling="", logPath="", linker=None,
        renderMode="standard"):
    """Run the rformat program on some text.

    :Return:
        A tuple of (stdout, errors).
    """
    return rformat.formatText(text, errorHandling=errorHandling,
            logPath=logPath, linker=linker, renderMode=renderMode)


def failUnlessTextMatches(expect, actual):
    """Assertion that does a line, by line comparison."""
    def show():
        print("Expected lines:")
        for l in expectLines:
            print("  %r" % l)
        print("Actual lines:")
        for l in actualLines:
            print("  %r" % l)

    def badLen():
        show()
        return ("Expected %d lines, but got %d lines, see test.log for details"
            % (len(expectLines), len(actualLines)))

    def mismatch():
        show()
        return ("Line %d does not match\n"
                 "  Expected: %r\n"
                 "  Actual:   %r" % (i + 1, a, b))

    expectLines = expect.splitlines()
    actualLines = actual.splitlines()
    failUnlessEqual(len(expectLines), len(actualLines), makeMessage=badLen)
    for i, (a, b) in enumerate(zip(expectLines, actualLines)):
        failUnlessEqual(a, b, makeMessage=mismatch)


class Base(Suite):
    def failIfErrors(self, errors):
        if not errors:
            return
        print("Unexpected error lines:")
        for msg, level in errors:
            print("  %r" % msg)
        fail("Unexpected error messages occurred - see test.log for details")


class InlineMarkup(Base):
    """Test for inline markup."""
    @test
    def emphasis(self):
        """Check emphasis is handled."""
        stdout, errors = run(makeText("""
        | A *word* and a *phrase that is split
        | over a line* and will be re-flowed rather nicely.
        """))
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | A *word* and a *phrase that is split over a line* and will be re-flowed rather
        | nicely.
        """), stdout)

    @test
    def strong_emphasis(self):
        """Check strong emphasis is handled."""
        stdout, errors = run(makeText("""
        | A **word** and a **phrase that is split
        | over a line** and will be re-flowed.
        """))
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | A **word** and a **phrase that is split over a line** and will be re-flowed.
        """), stdout)

    @test
    def literal_text(self):
        """Check literal text is handled."""
        stdout, errors = run(makeText("""
        | A ``word`` and a ``phrase that is split
        | over a line`` and will be re-flowed.
        """))
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | A ``word`` and a ``phrase that is split over a line`` and will be re-flowed.
        """), stdout)

    @test
    def literal_does_not_escape(self):
        """Check literal text does not get escaped."""
        text = makeText("""
        | Python files compile to ``*.pyc`` files.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def interpreted_text(self):
        """Check interpreted text is handled."""
        stdout, errors = run(makeText("""
        | A `word` and a `phrase that is split
        | over a line` and will be re-flowed.
        """))
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | A `word` and a `phrase that is split over a line` and will be re-flowed.
        """), stdout)

    @test
    def interpreted_text_with_presentation(self):
        """Check interpreted text with presentation part is handled.
        
        For example::

            See `the zen of python <http://www.python.org/dev/peps/pep-0020/>`
            for guidance.

        """
        text = makeText("""
        | See `zen<http://www.python.org/dev/peps/pep-0020/>` for details.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def interpreted_text_role_and_presentation(self):
        """Check interpreted text with a undefined role and presentation part
        is handled.
        
        For example::

            See :xxx:`the zen of python
            <http://www.python.org/dev/peps/pep-0020/>` for guidance.

        """
        text = makeText("""
        | See :ref:`zen <http://www.python.org/dev/peps/pep-0020>` for details.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def word_reference(self):
        """Check a word reference is handled."""
        stdout, errors = run(makeText("""
        | A Word_ with meaning.
        |
        | .. _word:
        """))
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | A Word_ with meaning.
        |
        | .. _word:
        """), stdout)

    @test
    def phrase_reference(self):
        """Check a phrase reference is handled."""
        stdout, errors = run(makeText("""
        | A `phrase split
        | over lines`_ with meaning.
        |
        | .. _`phrase split over lines`:
        """))
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | A `phrase split over lines`_ with meaning.
        |
        | .. _`phrase split over lines`:
        """), stdout)

    @test
    def anonymous_reference(self):
        """Check a anonymous reference is handled."""
        stdout, errors = run(makeText("""
        | Tested using `my favourite language`__ .
        |
        | __ http://www.python.org
        """))
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | Tested using `my favourite language`__ .
        |
        | __ http://www.python.org
        """), stdout)

    @test
    def inline_internal_target(self):
        """Check an inline internal target is handled."""
        stdout, errors = run(makeText("""
        | Use `duck typing`_ - its great!
        |
        | Python has _`duck typing`, which I love.
        """))
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | Use `duck typing`_ - its great!
        |
        | Python has _`duck typing`, which I love.
        """), stdout)

    @test
    def text_substitution_reference(self):
        """Check a text substitution reference is handled."""
        stdout, errors = run(makeText("""
        | Use |dt| - its great!
        |
        | .. |dt| replace:: duck typing.
        """))
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | Use |dt| - its great!
        |
        | .. |dt| replace:: duck typing.
        """), stdout)

    @test
    def image_substitution_reference(self):
        """Check an image substitution reference is handled."""
        stdout, errors = run(makeText("""
        | Use |dt| - its great!
        |
        | .. |dt| image:: duck.png
        """))
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | Use |dt| - its great!
        |
        | .. |dt| image:: duck.png
        """), stdout)

    @test
    def auto_numbered_footnote_reference(self):
        """Check auto numbered footnote references are handled."""
        text = makeText("""
        | Footnotes [#]_, are handled [#hdl]_.
        |
        | .. [#hdl] All things can probably be handled.
        |
        | .. [#] Things at the end of the page.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def numbered_footnote_reference(self):
        """Check non-auto numbered footnote references are handled."""
        text = makeText("""
        | Footnotes [2]_, are handled [5]_.
        |
        | .. [5] Things at the end of the page.
        |
        | .. [2] All things can probably be handled.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def auto_symbol_footnote_reference(self):
        """Check auto symbol footnote references are handled."""
        text = makeText("""
        | Footnotes [*]_, are handled [*]_.
        |
        | .. [*] Things at the end of the page.
        |
        | .. [*] All things can probably be handled.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def citation_reference(self):
        """Check citation references are handled."""
        text = makeText("""
        | Python is discussed further in [PY]_. 
        |
        | .. [PY] The Python Language Reference.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def hyperlink(self):
        """Check standalone hyperlinkes are handled."""
        text = makeText("""
        | Python is discussed further in http://www.python.org.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class Escaping(Base):
    """Test for inline markup."""
    @test
    def escaping_quote(self):
        """Check escaping of simple 'quote' characters."""
        text = makeText(r"""
        | Text is \*escaped\* ``with`` "\\", in \|rst\|.
        |
        | For example \`\`literal\`\`, \`interpreted\`.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def escaping_underscores(self):
        """Check escaping of underscore characters."""
        text = makeText(r"""
        | Leave in_middle_of_words. Escape at the end\_.
        |
        | Not a footnote \_[#].
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class SectionStructure(Base):
    """Test for section structure markup."""
    @test
    def titles_and_sections(self):
        """Check we handle titles, sub-titles, sections, etc."""
        text = makeText(r"""
        | =====
        | Title
        | =====
        |
        | About This
        | ==========
        |
        | Some text
        |
        | 
        | The Core
        | ========
        |
        | Details
        | -------
        |
        | Some more text
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class BulletLists(Base):
    """Test for bullet lists."""
    @test
    def simple_bullet_list(self):
        """Handling of simple, non-nested bullet lists."""
        text = makeText(r"""
        | - Item 1
        |
        | - Item 2
        |
        | - Item 3
        |
        | - Item 4
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def nested_bullet_list(self):
        """Handling of nested bullet lists."""
        text = makeText(r"""
        | - Item 1
        |
        |   * Item A
        |
        |   * Item B
        |
        | - Item 2
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def bullet_list_bug(self):
        """A bug regression test."""
        text = makeText(r"""
         | - You are doing TDD and this is a way to define the API and make your tests
         |   fail during development.
         |
         | - You have decided a feature *really* is a good idea, so you want to document
         |   it in the API, but the implementation has to wait.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class EnumeratedLists(Base):
    """Test for enumerated lists."""
    @test
    def simple_enumerated_list(self):
        """Handling of simple, non-nested enumerated lists."""
        text = makeText(r"""
        | 1. Item 1
        |
        | 2. Item 2
        |
        | 3. Item 3
        |
        | 4. Item 4
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def large_numbered_enum_list(self):
        """Handling of enumerated list with large numbers."""
        text = makeText(r"""
        | 99.  Item 1
        |
        | 100. Item 2
        |
        | 101. Item 3
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def nested_enumerated_list(self):
        """Handling of simple, nested enumerated lists."""
        text = makeText(r"""
        | 1. Item 1
        |
        | 2. Item 2
        |
        |    iii. Item a
        |         
        |    iv.  Item a
        |
        | 3. Item 3
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def enumerated_list_styles(self):
        """The (other) different styles of numbering lists."""
        text = makeText(r"""
        | I.  Item 1
        |
        | II. Item 2
        |
        | A. Item 1
        |
        | B. Item 2
        |
        |    a. Item 1
        |
        |    b. Item 2
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def literal_in_list(self):
        """We can have a literal block in an enumerated list."""
        text = makeText(r"""
        | 1. This is some text
        |    ::
        |
        |      Literal
        |      Lines
        |
        |    And so on.
        |
        | 2. Another item.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class DefinitionLists(Base):
    """Test for definition lists."""
    @test
    def simple_definition_list(self):
        """Handling of simple, non-nested definition lists."""
        text = makeText(r"""
        | Definition lists:
        | 
        | what
        |   Definition lists associate a term with a definition, which says what the term
        |   means.
        | 
        | how
        |   The term is a one-line phrase.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def nested_definition_list(self):
        """Handling of nested definition lists."""
        text = makeText(r"""
        | Definition lists:
        | 
        | what
        |   Definition lists associate a term with a definition, which says what the term
        |   means.
        |
        |   whether
        |     Or not
        | 
        |   whence
        |     Or from where
        | 
        | how
        |   The term is a one-line phrase.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def definition_list_with_linked_items(self):
        """Handling of definition list, where the items are links."""
        text = makeText(r"""
        | Definition lists:
        | 
        | `what`
        |   Definition lists associate a term with a definition, which says what the term
        |   means.
        | 
        | `how`
        |   The term is a one-line phrase.
        """)
        # stdout, errors = run(text)
        stdout, errors = run(text, logPath="z")
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class FieldLists(Base):
    """Test for field lists."""
    @test
    def simple_field_list(self):
        """Handling of simple, non-nested field lists."""
        text = makeText(r"""
        | :Author:
        |   Tony J. (Tibs) Ibbs, David Goodger
        | 
        |   (and sundry other good-natured folks)
        | 
        | :Version:
        |   1.0 of 2001/08/08
        |
        | :Dedication:
        |   To my father. 
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        # TODO: Cannot see a way to make this work without a leading newline.
        failUnlessTextMatches("\n" + text, stdout)

    @test
    def nested_field_list(self):
        """Handling of nested field lists."""
        text = makeText(r"""
        | :Info:
        |
        |   :Authors:
        |     Tony J. (Tibs) Ibbs, David Goodger
        |   
        |     (and sundry other good-natured folks)
        |   
        |   :Version:
        |     1.0 of 2001/08/08
        |
        | :Dedication:
        |   To my father. 
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)

        # TODO: Cannot see a way to make this work without a leading newline.
        failUnlessTextMatches("\n" + text, stdout)


class OptionLists(Base):
    """Test for options lists."""
    @test
    def option_list(self):
        """Handling of a option list."""
        text = makeText(r"""
        | -a            command-line option "a"
        | -b file       options can have arguments and long descriptions
        | --long        options can be long also
        | --input=file  long options can also have pretty darn longish-to-long argument
        |               descriptions.
        | /V            DOS/VMS-style options too
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


    @test
    def option_list_plus(self):
        """An option list with leading and trailing text."""
        text = makeText(r"""
        | These are the options.
        |
        | -a            command-line option "a"
        | -b file       options can have arguments and long descriptions
        | --long        options can be long also
        | --input=file  long options can also have pretty darn longish-to-long argument
        |               descriptions.
        | /V            DOS/VMS-style options too
        |
        | Please use them
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class LiteralBlocks(Base):
    """Test for literal blocks."""
    @test
    def literal_block(self):
        """Handling of a literal block."""
        text = makeText(r"""
        | A plain literal block.
        | ::
        | 
        |   The Zen of Python, by Tim Peters
        |  
        |   Beautiful is better than ugly.
        |   Explicit is better than implicit.
        |   ...
        | 
        | Back to normal text.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessEqualStrings(text, stdout)

    @test
    def line_quioted_literal_block(self):
        """Handling of a line quoted literal block."""
        # TODO: This test might be wrong.
        text = makeText(r"""
        | A quoted email.
        | ::
        | 
        |   > This is a previous.
        |   > e-mail.
        | 
        | Back to normal text.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def styled_literal_block(self):
        """Handling of a styled literal block."""
        text = makeText(r"""
        | A python literal block.
        | :<py>:
        | 
        |   def func(self):
        |       '''Example function'''
        |       pass
        | 
        | Back to normal text.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def sphinx_styled_literal_block(self):
        """Handling of a sphinx styled literal block."""
        text = makeText(r"""
        | A python literal block.
        | :<py>:
        | 
        |   def func(self):
        |       '''Example function'''
        |       pass
        | 
        | Back to normal text.
        """)
        stdout, errors = run(text, renderMode="sphinx")
        self.failIfErrors(errors)
        failUnlessTextMatches(
                text.replace(":<py>:", "\n.. code-block:: python"), stdout)


class LineBlocks(Base):
    """Test for lines blocks."""
    @test
    def line_block(self):
        """Handling of a line block."""
        text = makeText(r"""
        | A line block.
        |
        | | Has each logical line started with
        |   a vertical bar.
        | |
        | | Each vertical bar introduces a new line.
        |   Reformatting should wrap continuation lines.
        | 
        | Back to normal text.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | A line block.
        |
        | | Has each logical line started with a vertical bar.
        | |
        | | Each vertical bar introduces a new line. Reformatting should wrap
        |   continuation lines.
        | 
        | Back to normal text.
        """), stdout)

    @test
    def line_block_preserve_breaks(self):
        """Specific breaks should be preserved."""
        text = makeText(r"""
        | A line block.
        |
        | | Has each logical line started with
        | | a vertical bar.
        | |
        | | Each vertical bar introduces a new line.
        | 
        | Back to normal text.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(makeText("""
        | A line block.
        |
        | | Has each logical line started with
        | | a vertical bar.
        | |
        | | Each vertical bar introduces a new line.
        | 
        | Back to normal text.
        """), stdout)


class BlockQuotes(Base):
    """Test for block quotes."""
    @test
    def line_block(self):
        """Handling of block quotes."""
        text = makeText(r"""
        | Block quotes are just:
        | 
        |   Indented paragraphs,
        | 
        |     and they may nest. Actually they may nest to any level in theory. However,
        |     we like an 80 character line length.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class DoctestBlocks(Base):
    """Test for doctest blocks."""
    @test
    def doctest_block(self):
        """Handling of a literal block."""
        text = makeText(r"""
        | Doctest blocks are interactive Python sessions. They begin with "``>>>``" and
        | end with a blank line.
        | 
        | >>> print("This is a doctest block.")
        | This is a doctest block. 
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class Tables(Base):
    """Test for tables."""
    @test
    def grid_table(self):
        """Handling of a grid table."""
        text = makeText(r"""
        | Grid table:
        | 
        | +------------+------------+-----------+
        | | Header 1   | Header 2   | Header 3  |
        | +============+============+===========+
        | | body row 1 | column 2   | column 3  |
        | +------------+------------+-----------+
        | | body row 2 | Cells may span columns.|
        | +------------+------------+-----------+
        | | body row 3 | Cells may  | - Cells   |
        | +------------+ span rows. | - contain |
        | | body row 4 |            | - blocks. |
        | +------------+------------+-----------+
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def simple_table(self):
        """Handling of a simple table."""
        text = makeText(r"""
        | Simple table:
        | 
        | =====  =====  ======
        |    Inputs     Output
        | ------------  ------
        |   A      B    A or B
        | =====  =====  ======
        | False  False  False
        | True   False  True
        | False  True   True
        | True   True   True
        | =====  =====  ======
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class Transitions(Base):
    """Test for transisions."""
    @test
    def transition(self):
        """Handling of a transition."""
        text = makeText(r"""
        | A transition marker is a horizontal line of 4 or more repeated punctuation
        | characters.
        |
        | ----
        | 
        | A transition should not begin or end a section or document, nor should two
        | transitions be immediately adjacent.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text.replace("----", "-" * 40), stdout)


class HyperlinkTargets(Base):
    """Test for various hyperlink targets.
    
    Some case are tested by `InlineMarkup.hyperlink`
    
    """
    @test
    def link_to_undefined_ref(self):
        """A link to a reference that is not known.

        This is an incomplete document, but will occur when reformatting
        parts of a document.
        
        """
        text = makeText(r"""
        | The general purpose language is `Python`_.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def external_inline_hyperlink(self):
        """Handling of an external inline hyperlink."""
        text = makeText(r"""
        | The general purpose language is `Python <http://www.python.org/>`_.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def external_hyperlink(self):
        """Handling of an external hyperlink."""
        text = makeText(r"""
        | The general purpose languyage is Python_.
        |
        | .. _python: http://www.python.org/
        """)
        stdout, errors = run(text, logPath="z")
        self.failIfErrors(errors)
        failUnlessTextMatches(text.replace("----", "-" * 40), stdout)

    @test
    def external_inline_hyperlink_bug(self):
        """Handling of an external hyperlink - bug.
        
        The bug seems 'fixed' but the code is **really** horrible. I should
        make a major clean-up effort.

        """
        text = makeText(r"""
        | The Clear Climate Code project is conducted by `Ravenbrook Limited
        | <http://www.ravenbrook.com/>`_, its staff and other volunteers, in the public
        | interest. Nick Barnes had the project idea in 2007. Nobody has commissioned
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text.replace("----", "-" * 40), stdout)


class Comments(Base):
    """Test for comments.
    
    We need this because comments should be processed, but not re-flowed.
    
    """
    @test
    def comments(self):
        """Handling of comments."""
        text = makeText(r"""
        | .. This is a comment.
        |    Which is on two lines.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class Directives(Base):
    """Test for directives."""
    @test
    def admonitions(self):
        """Handling of admonitions."""
        text = makeText(r"""
        | .. Warning::
        |    Don't do that! Really! Don't! If you continue doing that then you will
        |    regret it in the long run.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def image(self):
        """Handling of figures."""
        text = makeText(r"""
        | .. Image:: piccy.png
        |    :align: right
        |    :alt: map to buried treasure
        |    :height: 100
        |    :scale: 50
        |    :width: 200
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def figure(self):
        """Handling of figures, with a caption and legend."""
        text = makeText(r"""
        | .. Figure:: piccy.png
        |    :alt: map to buried treasure
        |    :scale: 50
        |
        |    This is the caption.
        |
        |    The legend is the descriptive bit. This can be a great big thing. It can
        |    include a table of you want.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def custom_role(self):
        """Creating and using custom roles."""
        text = makeText(r"""
        | .. role:: silly
        |    :class: daft
        |
        | This is :silly:`nuts`.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches("This is :silly:`nuts`.", stdout)

    @test
    def raw_passthrough(self):
        """The raw (passthrough) directive."""
        text = makeText(r"""
        | .. raw:: html
        |
        |    <i>Italic</i>
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)


class ErrorHandling(Base):
    """Test for how we handle markup errors.
    
    We can ignore errors or use them to abandon the reformatting.
    
    """
    err = "Error: Inline emphasis start-string without end-string."

    @test
    def ignore_errors(self):
        """Handling of problematic node, caused by markup error."""
        text = makeText(r"""
        | This is *an incomplete inline markup.
        """)
        stdout, errors = run(text, errorHandling="ignore")
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def prepend_errors(self):
        """Prepend error messages to the reformatted text"""
        text = makeText(r"""
        | This is *an incomplete inline markup.
        """)
        stdout, errors = run(text, errorHandling="prepend")
        self.failIfErrors(errors)
        failUnlessTextMatches(self.err + "\n\n" + text, stdout)

    @test
    def append_errors(self):
        """Append error messages to the reformatted text"""
        text = makeText(r"""
        | This is *an incomplete inline markup.
        """)
        stdout, errors = run(text, errorHandling="append")
        self.failIfErrors(errors)
        failUnlessTextMatches(text + "\n" + self.err, stdout)

    @test
    def abort_on_errors(self):
        """The normal default on errors behaviour."""
        text = makeText(r"""
        | This is *an incomplete inline markup.
        """)
        stdout, errors = run(text)
        failUnless(errors, msg="Expected error messages did not occur")
        failUnlessEqual(
                [('Inline emphasis start-string without end-string.', 1)],
                errors)

    @test
    def bad_table(self):
        """Give the reformatter a mal-formed table."""
        text = makeText(r"""
        | Grid table:
        | 
        | +------------+------------+-----------+
        | | Header 1   | Header 2   | Header 3  |
        | +============+============+===========+
        | | body row 1  | column 2  | column 3  |
        | +-------------------------+---------+
        """)
        stdout, errors = run(text)
        failUnless(errors, msg="Expected error messages did not occur")


class Links(Base):
    """Test the support for resolving links."""
    @test
    def resolve(self):
        """Basic resolving of links"""

        class _DummyLinker(object):
            def translate_identifier_xref(self, t):
                return "ID:%s" % t

        text = makeText(r"""
        | See also: `fred`.
        """)
        stdout, errors = run(text, linker=_DummyLinker())
        failUnlessTextMatches("See also: ID:fred.", stdout)


class Logging(Base):
    """Test the logging support in the rst2rst module."""
    @test
    def setting_a_log_file(self):
        """Set a log files and expect it to appear."""
        text = makeText(r"""
        | .. This is a comment.
        |    Which is on two lines.
        """)
        Files.rmFile("z")
        failIf(os.path.exists("z"))
        stdout, errors = run(text, logPath="z")
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)
        failUnless(os.path.exists("z"))

    @test
    def clearing_a_log_file(self):
        """Set a log file then unset it again."""
        text = makeText(r"""
        | .. This is a comment.
        |    Which is on two lines.
        """)
        Files.rmFile("z")
        failIf(os.path.exists("z"))
        stdout, errors = run(text, logPath="z")
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)
        failUnless(os.path.exists("z"))

        Files.rmFile("z")
        failIf(os.path.exists("z"))
        stdout, errors = run(text, logPath=None)
        failIf(os.path.exists("z"))

    def tearDown(self):
        Files.rmFile("z")


class Bugs(Base):
    """Regressions tests for various bugs."""
    @test
    def backslash_in_literal_block(self):
        """Backslashes in literal blocks should not be escaped."""
        text = makeText(r"""
        | Literal
        | ::
        |
        |   DOS uses the \ character as a directory separator.
        |   How singular.
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text, stdout)

    @test
    def hyperlink_1(self):
        """Hyperlink 1"""
        text = makeText(r"""
        | Specifies the byte-order used in the record format of the Fortran binary-file.
        | It should be one of the characters used by the struct module to specify the
        | byte-order and size:
        | ::
        | 
        |   '@' = native
        |   '<' = little-endian
        |   '>' = big-endian
        | 
        | The default is to use native byte order. See `struct`_ for further details.
        | 
        | .. _`struct`: http://www.python.org/doc/2.4.4/lib/module-struct.html
        """)
        stdout, errors = run(text)
        self.failIfErrors(errors)
        failUnlessTextMatches(text.replace("`struct`", "struct"), stdout)


if __name__ == "__main__":
    runModule()
