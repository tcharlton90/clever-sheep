#!/usr/bin/env python
"""Test that deprecated features are managed correctly.

Some feature get deprecated as CleverSheep evolves. A deprecated feature
goes through the following phases.

1. Mark features as pending deprecation. This is indicated in the documentation
   and the user can get warning messages by running with the python option::

       -W module::PendingDeprecationWarning

2. The deprecation warning is automatically issued, unless explicitly
   suppressed. At this stage the feature is nolonger supported.

3. The feature is removed from the code.

When a feature first becomes deprecated, the documentation and warnings
indicate when the feature will be remove. This is typically two minor releases
in the future, but may be the next major release if the loss of the feature may
have a big impact.

"""

import linecache
import sys
import os
from cStringIO import StringIO
import warnings

import CheckEnv

from CleverSheep.Prog import Files
from CleverSheep.Prog.Curry import Curry
from CleverSheep.Test.DataMaker import literalText2Text
from CleverSheep.Test.Tester import *

makeText = Curry(literalText2Text, noTail=True)

def unimport(name):
    nn = "." + name
    toDel = []
    for n in sys.modules:
        if n == name:
            toDel.append(n)
        if n.endswith(nn):
            toDel.append(n)
    for n in toDel:
        del sys.modules[n]

        
class _warnings(object):
    def __getattr__(self, name):
        raise ImportError


class Test_Deprecations(Suite):
    """Tests for all deprecated features that still exist in the library.
    
    Note that we do not test that the features work, that is tested
    elsewhere. We just test that deprecation warnings are correctly issued.

    """
    def suiteSetUp(self):
        self.origSavedFilters = list(warnings.filters)
        self.origSavedOnceregistry = warnings.onceregistry.copy()
        sys.modules["_warnings"] = _warnings()
        unimport("warnings")
        import warnings as temp

        www = sys.modules["warnings"]
        for name in dir(warnings):
            setattr(www, name, getattr(warnings, name))
        self.trackedImports = {}

    def suiteTearDown(self):
        del sys.modules["_warnings"]
        unimport("warnings")
        import warnings

    def setUp(self):
        warnings.onceregistry.clear()
        warnings.resetwarnings()
        warnings.filters[:] = list(self.origSavedFilters)
        self.ss = StringIO()
        self.env = {"cwd": os.getcwd()}

        # Python uses linecache to report exception lines. Since we keep
        # importing the same module with different content, we need to clear
        # the cache between tests.
        linecache.clearcache()

    def tearDown(self):
        warnings.filters[:] = list(self.origSavedFilters)
        warnings.onceregistry.update(self.origSavedOnceregistry)
        #Files.rmFile("xxx.py")
        Files.rmFile("xxx.pyc")

    def tryImport(self, mod):
        for m in self.trackedImports:
            unimport(m.__name__)
        self.trackedImports.clear()
        f = open("xxx.py", "w")
        f.write("import %s as testMod\n" % mod)
        f.close()
        stderr, sys.stderr = sys.stderr, self.ss
        dd = globals().copy()
        try:
            exec("import xxx", dd, dd)
        finally:
            sys.stderr = stderr
            self.trackedImports[dd["xxx"]] = None
            self.trackedImports[getattr(dd["xxx"], "testMod")] = None

    @test
    def decorator_silent(self):
        """The decorator CleverSheep.decorator module is in phase 1.

        """
        self.tryImport("CleverSheep.decorator")
        failUnlessEqualStrings("", self.ss.getvalue())

    @test
    def decorator_warn(self):
        """The decorator CleverSheep.decorator module can warn in phase 1.

        """
        warnings.filterwarnings("module", category=PendingDeprecationWarning)
        self.tryImport("CleverSheep.decorator")
        actual = self.ss.getvalue().replace("./xxx.py", "%(cwd)s/xxx.py" % self.env)
        failUnlessEqualStrings(literalText2Text("""
         | %(cwd)s/xxx.py:1: PendingDeprecationWarning: |
         | The 'CleverSheep.decorator' module is deprecated. It will no longer be
         | supported from version 0.5 onwards and will be removed in version 0.6.
         | Please use either:
         | 
         |   The official decorator (http://pypi.python.org/pypi/decorator)
         | 
         | or the convenient copy in the CleverSheep library:
         | 
         |   CleverSheep.Extras.decorator
         | ----------------------------------------------------------------------
         | 
         |   import CleverSheep.decorator as testMod
        """ % self.env), actual)

    @test
    def ustr_silent(self):
        """The CleverSheep.Prog.Ustr module is in phase 1.

        """
        self.tryImport("CleverSheep.Prog.Ustr")
        failUnlessEqualStrings("", self.ss.getvalue())

    @test
    def ustr_warn(self):
        """The CleverSheep.Prog.Ustr module can warn in phase 1.

        """
        warnings.filterwarnings("module", category=PendingDeprecationWarning)
        self.tryImport("CleverSheep.Prog.Ustr")
        actual = self.ss.getvalue().replace("./xxx.py", "%(cwd)s/xxx.py" % self.env)
        failUnlessEqualStrings(literalText2Text("""
         | %(cwd)s/xxx.py:1: PendingDeprecationWarning: |
         | The 'CleverSheep.Prog.Ustr' module is deprecated. It will nolonger be
         | supported from version 0.5 onwards and will be removed in version 0.6.
         | Please use the 'Intern' module instead :
         | ----------------------------------------------------------------------
         | 
         |   import CleverSheep.Prog.Ustr as testMod
        """ % self.env), actual)

    @test
    def ultra_tb__silent(self):
        """The CleverSheep.Debug.ultraTB module is in phase 1.

        """
        self.tryImport("CleverSheep.Debug.ultraTB")
        failUnlessEqualStrings("", self.ss.getvalue())

    @test
    def ultra_tb__warn(self):
        """The CleverSheep.Debug.ultraTB module can warn in phase 1.

        """
        warnings.filterwarnings("module", category=PendingDeprecationWarning)
        self.tryImport("CleverSheep.Debug.ultraTB")
        actual = self.ss.getvalue().replace("./xxx.py", "%(cwd)s/xxx.py" % self.env)
        failUnlessEqualStrings(literalText2Text("""
         | %(cwd)s/xxx.py:1: PendingDeprecationWarning: |
         | The 'CleverSheep.Debug.ultraTB' module is deprecated. Please use either:
         |
         |   The official ultraTB (http://www.n8gray.org/files/ultraTB.py)
         |
         | or the copy.
         |
         |   CleverSheep.Extras.ultraTB
         | ----------------------------------------------------------------------
         |
         |   import CleverSheep.Debug.ultraTB as testMod
        """ % self.env), actual)

    @test
    def process_silent(self):
        """The decorator CleverSheep.VisTools.Process module is in phase 1.

        """
        self.tryImport("CleverSheep.VisTools.Process")
        failUnlessEqualStrings("", self.ss.getvalue())

    @test
    def process_warn(self):
        """The decorator CleverSheep.VisTools.Process module can warn in phase 1.

        """
        warnings.filterwarnings("module", category=PendingDeprecationWarning)
        self.tryImport("CleverSheep.VisTools.Process")
        failUnlessEqualStrings(literalText2Text("""
         | %(cwd)s/xxx.py:1: PendingDeprecationWarning: |
         | The 'CleverSheep.VisTools.Process' module is deprecated. It will nolonger be
         | supported from version 0.5 onwards and will be removed in version 0.6. Please
         | use the 'CleverSheep.Prog.Process' instead.
         | ----------------------------------------------------------------------
         | 
         |   import CleverSheep.VisTools.Process as testMod
        """ % self.env), self.ss.getvalue())


if __name__ == "__main__":
    runModule()
