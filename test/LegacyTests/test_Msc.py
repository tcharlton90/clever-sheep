#!/usr/bin/env python
"""CleverSheep.VisTools.Msc unit tests"""

from cStringIO import StringIO

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import DataMaker
from CleverSheep.Log import FileTracker
from CleverSheep.Prog.Curry import Curry

from TestSupport.Files import FileBasedTests, ensureNoFile


# The module under test.
from CleverSheep.VisTools import Msc

makeText = Curry(DataMaker.literalText2Text, noTail=True)


class SequenceChart(Suite):
    """Tests for the SequenceChart class"""
    @test
    def empty_chart(self):
        """Create and render and empty chart."""
        chart = Msc.SequenceChart()
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        """), s.getvalue())

    @test
    def null_sequence_chart(self):
        """Create and render a chart with parties, but no messages."""
        chart = Msc.SequenceChart(parties=["A", "B"])
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | A                                                B
        |
        """), s.getvalue())

    @test
    def two_party_chart(self):
        """Create and render with two parties and some simple messages."""
        chart = Msc.SequenceChart(parties=["Bob", "Sue"])
        chart.addMessage("Bob", "Sue", "Hello Sue")
        chart.addMessage("Sue", "Bob", "Hello Bob")
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(makeText("""
        | Bob                                            Sue
        | |--Hello Sue------------------------------------>|
        | |<-Hello Bob-------------------------------------|
        |
        """), s.getvalue())

    @test
    def message_prefix(self):
        """We can add a prefix to a message.

        """
        #> Create and render with two parties and some simple messages."""
        chart = Msc.SequenceChart(parties=["Bob", "Sue"])
        chart.addMessage("Bob", "Sue", "Hello Sue")
        chart.addMessage("Sue", "Bob", "Hello Bob", prefix="TEST")
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(makeText("""
        | Bob                                            Sue
        | |--Hello Sue------------------------------------>|
        | |<-TEST Hello Bob--------------------------------|
        |
        """), s.getvalue())

    @test
    def simple_three_party_chart(self):
        """A simple chart with three parties, even spacing.

        This chart has three parties and nice short messages so that it can be
        rendered with evenly spaced lines.
        """
        chart = Msc.SequenceChart(parties=["Bob", "Alice", "Sue"])
        chart.addMessage("Bob", "Sue", "Hello Sue")
        chart.addMessage("Sue", "Bob", "Hello Bob")
        chart.addMessage("Bob", "Alice", "Hello Alice")
        chart.addMessage("Alice", "Sue", "Hello Sue")
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(makeText("""
        | Bob                    Alice                   Sue
        | |--Hello Sue------------------------------------>|
        | |<-Hello Bob-------------------------------------|
        | |--Hello Alice---------->|                       |
        | |                        |--Hello Sue----------->|
        |
        """), s.getvalue())

    @test
    def post_set_parties(self):
        """Setting the parties using the setParties method.

        We should be able to set the parties after adding the messages.
        """
        chart = Msc.SequenceChart()
        chart.addMessage("Bob", "Sue", "Hello Sue")
        chart.addMessage("Sue", "Bob", "Hello Bob")
        chart.addMessage("Bob", "Alice", "Hello Alice")
        chart.addMessage("Alice", "Sue", "Hello Sue")
        chart.setParties(["Bob", "Alice", "Sue"])
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(makeText("""
        | Bob                    Alice                   Sue
        | |--Hello Sue------------------------------------>|
        | |<-Hello Bob-------------------------------------|
        | |--Hello Alice---------->|                       |
        | |                        |--Hello Sue----------->|
        |
        """), s.getvalue())

    @test
    def uneven_spacing(self):
        """Three party chart, uneven spacing.

        This chart has three parties, but a long message between two
        neighbours. This should mean that the spacing of the lines is adjusted
        to ensure that the messages do not get truncated.

        Also, even thoug we request a chart width of 45, the actual width is
        larger to allow for the required widths.
        """
        chart = Msc.SequenceChart(parties=["Bob", "Alice", "Sue"])
        chart.addMessage("Bob", "Sue", "Hello Sue")
        chart.addMessage("Sue", "Bob", "Hello Bob")
        chart.addMessage("Bob", "Alice", "Hello Alice, smell you later")
        chart.addMessage("Alice", "Sue", "Hello Sue")
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(makeText("""
        | Bob                            Alice           Sue
        | |--Hello Sue------------------------------------>|
        | |<-Hello Bob-------------------------------------|
        | |--Hello Alice, smell you later->|               |
        | |                                |--Hello Sue--->|
        |
        """), s.getvalue())

    @test
    def simple_info_above(self):
        """Test adding information above the message line."""
        chart = Msc.SequenceChart(parties=["Bob", "Alice", "Sue"])
        msg = chart.addMessage("Bob", "Sue", "Hello Sue", labAbove="First")
        msg.addAbove("Greeting")
        chart.addMessage("Sue", "Bob", "Hello Bob")
        chart.addMessage("Bob", "Alice", "Hello Alice")
        chart.addMessage("Alice", "Sue", "Hello Sue")
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(makeText("""
        | Bob                    Alice                   Sue
        | |                        |                       |
        | |  First                 |                       |
        | |  Greeting              |                       |
        | |--Hello Sue------------------------------------>|
        | |<-Hello Bob-------------------------------------|
        | |--Hello Alice---------->|                       |
        | |                        |--Hello Sue----------->|
        |
        """), s.getvalue())

    @test
    def simple_info_below(self):
        """Test adding information below the message line."""
        chart = Msc.SequenceChart(parties=["Bob", "Alice", "Sue"])
        chart.addMessage("Bob", "Sue", "Hello Sue")
        chart.addMessage("Sue", "Bob", "Hello Bob")
        chart.addMessage("Bob", "Alice", "Hello Alice")
        msg = chart.addMessage("Alice", "Sue", "Hello Sue", labBelow="Last")
        msg.addBelow("Greeting")
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(makeText("""
        | Bob                    Alice                   Sue
        | |--Hello Sue------------------------------------>|
        | |<-Hello Bob-------------------------------------|
        | |--Hello Alice---------->|                       |
        | |                        |--Hello Sue----------->|
        | |                        |  Last                 |
        | |                        |  Greeting             |
        |
        """), s.getvalue())

    @test
    def simple_info_above_and_below(self):
        """Test adding information abve and below the message line."""
        chart = Msc.SequenceChart(parties=["Bob", "Alice", "Sue"])
        chart.addMessage("Bob", "Sue", "Hello Sue")
        chart.addMessage("Sue", "Bob", "Hello Bob")
        chart.addMessage("Bob", "Alice", "Hello Alice")
        msg = chart.addMessage("Alice", "Sue", "Hello Sue")
        msg.addAbove("Salutations")
        msg.addBelow("Last")
        msg.addBelow("Greeting")
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(makeText("""
        | Bob                    Alice                   Sue
        | |--Hello Sue------------------------------------>|
        | |<-Hello Bob-------------------------------------|
        | |--Hello Alice---------->|                       |
        | |                        |                       |
        | |                        |  Salutations          |
        | |                        |--Hello Sue----------->|
        | |                        |  Last                 |
        | |                        |  Greeting             |
        |
        """), s.getvalue())

    @test
    def add_above_below_with_append(self):
        """Test adding information abve and below the message line,
        using the append option."""
        chart = Msc.SequenceChart(parties=["Bob", "Alice", "Sue"])
        chart.addMessage("Bob", "Sue", "Hello Sue")
        chart.addMessage("Sue", "Bob", "Hello Bob")
        chart.addMessage("Bob", "Alice", "Hello Alice")
        msg = chart.addMessage("Alice", "Sue", "Hello Sue")
        msg.addAbove("Salutations")
        msg.addBelow("Last")
        msg.addBelow("Greeting", append=1)
        msg.addAbove("Lots")
        msg.addAbove("of info", append=1)
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(makeText("""
        | Bob                    Alice                   Sue
        | |--Hello Sue------------------------------------>|
        | |<-Hello Bob-------------------------------------|
        | |--Hello Alice---------->|                       |
        | |                        |                       |
        | |                        |  Salutations          |
        | |                        |  Lots of info         |
        | |                        |--Hello Sue----------->|
        | |                        |  Last Greeting        |
        |
        """), s.getvalue())

    # TODO: This facility has been broken by refactoring.
    #@test
    def get_above_below(self):
        """Test the getAbove/getBelow methods of a message.

        Using these methods, we can access the AttrString instance directly,
        allowing us to modify the text, using slicing/indexing.
        """
        chart = Msc.SequenceChart(parties=["Bob", "Alice", "Sue"])
        chart.addMessage("Bob", "Sue", "Hello Sue")
        chart.addMessage("Sue", "Bob", "Hello Bob")
        chart.addMessage("Bob", "Alice", "Hello Alice")
        msg = chart.addMessage("Alice", "Sue", "Hello Sue")
        msg.addAbove("Salutations")
        msg.addBelow("Greeting", append=1)

        r = msg.getAbove()
        r[0] = "s"

        r = msg.getBelow()
        r[0] = r[0][0].lower()
        r[0:0] = "Last "

        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(makeText("""
        | Bob                    Alice                   Sue
        | |--Hello Sue------------------------------------>|
        | |<-Hello Bob-------------------------------------|
        | |--Hello Alice---------->|                       |
        | |                        |                       |
        | |                        |  salutations          |
        | |                        |--Hello Sue----------->|
        | |                        |  Last greeting        |
        |
        """), s.getvalue())

    @test
    def message_access(self):
        """The las added message should be available.

        The method `getMsg` returns the last message added.

        """
        #> Create a chart and add two messages.
        chart = Msc.SequenceChart(parties=["Bob", "Alice", "Sue"])
        chart.addMessage("Bob", "Sue", "Hello Sue")
        chart.addMessage("Sue", "Bob", "Greetings Bob")

        #> Get the last message and check it contents.
        m = chart.getMsg()
        failUnlessEqual("Greetings Bob", m.label.str())


class SequenceChartErrors(Suite):
    """Exercise various error and corner cases."""
    @test
    def no_src_or_dst(self):
        """A message with neither source or destination is not drawn.

        """
        #> Create a chart and add a message with no parties.
        chart = Msc.SequenceChart(parties=["Bob", "Alice", "Sue"])
        chart.addMessage(None, None, "Hello Sue")

        #> Render the chart and verify the correct output.
        s = StringIO()
        renderer = Msc.TextRenderer(chart)
        renderer.render(s, chartWidth=50)
        failUnlessEqualStrings(makeText("""
        | Bob                    Alice                   Sue
        |
        """), s.getvalue())


if 0: # Broken by recent refactoring.
    class ContSequenceChart(FileBasedTests):
        """Tests for the SequenceChart in continuous mode.

        In continuous mode the chart is draw as messages are added. For these
        tests, write to a file and use a FileTracker to follow the output.
        """
        def setUp(self):
            super(ContSequenceChart, self).setUp()
            ensureNoFile("XXX")

        @test
        def simple_continuous(self):
            """Simple test of continuous output."""
            self.addFile("XXX")
            f = open("XXX", "w")
            chart = Msc.SequenceChart(parties=["Bob", "Alice", "Sue"],
                    outfile=f, cont=True, chartWidth=50)
            tracker = FileTracker.FileTracker("XXX")
            code, text = tracker.read()
            failUnless(text is None)

            chart.addMessage("Bob", "Sue", "Hello Sue")
            code, text = tracker.read()
            failUnlessEqualStrings(makeText("""
            | Bob                    Alice                   Sue
            """), text)

            chart.addMessage("Sue", "Bob", "Hello Bob")
            code, text = tracker.read()
            failUnlessEqualStrings(makeText("""
            |
            | |                        |                       |
            | |--Hello Sue------------------------------------>|
            """), text)

            chart.addMessage("Bob", "Alice", "Hello Alice")
            code, text = tracker.read()
            failUnlessEqualStrings(makeText("""
            |
            | |                        |                       |
            | |<-Hello Bob-------------------------------------|
            """), text)

            chart.addMessage("Alice", "Sue", "Hello Sue")
            code, text = tracker.read()
            failUnlessEqualStrings(makeText("""
            |
            | |                        |                       |
            | |--Hello Alice---------->|                       |
            """), text)

            chart.flush()
            code, text = tracker.read()
            failUnlessEqualStrings(makeText("""
            |
            | |                        |                       |
            | |                        |--Hello Sue----------->|
            """), text)

            f.close()

            # Check the entire file - just to be sure.
            failUnlessEqualStrings(makeText("""
            | Bob                    Alice                   Sue
            | |--Hello Sue------------------------------------>|
            | |<-Hello Bob-------------------------------------|
            | |--Hello Alice---------->|                       |
            | |                        |--Hello Sue----------->|
            |
            """), open("XXX").read())


if __name__ == "__main__":
    runModule()

