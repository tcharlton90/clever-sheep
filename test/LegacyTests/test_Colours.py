#!/usr/bin/env python
"""CleverSheep.TTY_Utils.Colours unit tests"""

import os
import sys
from cStringIO import StringIO

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import TermEmu
from CleverSheep.TTY_Utils import RichTerm
from CleverSheep.Prog.Curry import Curry


# The module under test.
from CleverSheep.TTY_Utils import Colours

colourNames = (
    "black", "red", "green", "yellow",
    "blue", "magenta", "cyan", "white")


class ColourStream(Suite):
    """Tests for the ColourStream class."""

    def suiteTearDown(self):
        """
        Tidy up a tmp file created during these tests 
        """
        os.remove("/tmp/emu.log")
        super(ColourStream, self).suiteTearDown()

    @test
    def not_a_tty(self):
        """Verify we fall back to plain text for non-TTYs."""
        f = StringIO()
        s = Colours.ColourStream(f, bold=1)
        s.write("Hello")
        failUnlessEqual("Hello", f.getvalue())
        f.close()

    @test("bug")
    def each_colour(self):
        """Check each colour can be used as foreground and background"""
        for col in colourNames:
            tty = TermEmu.TTY()
            f = RichTerm.RichTerminal(tty)
            s = Colours.ColourStream(f, fg=col)
            s.write("Hello")
            failUnlessEqual(("Hello", (col, "white", "")), next(tty.blocks()))

            tty = TermEmu.TTY()
            f = RichTerm.RichTerminal(tty)
            s = Colours.ColourStream(f, bg=col)
            s.write("Hello")
            failUnlessEqual(("Hello", ("black", col, "")), next(tty.blocks()))

    @test("bug")
    def bold(self):
        """Check bold text is supported"""
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        s = Colours.ColourStream(f, bold=True)
        s.write("Hello")
        failUnlessEqual(("Hello", ("black", "white", "b")), next(tty.blocks()))

    @test("bug")
    def blinking(self):
        """Check blinking text is supported"""
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        s = Colours.ColourStream(f, blink=True)
        s.write("Hello")
        failUnlessEqual(("Hello", ("black", "white", "B")), next(tty.blocks()))

    @test("bug")
    def reverse(self):
        """Check reverse text is supported"""
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        s = Colours.ColourStream(f, rev=True)
        s.write("Hello")
        failUnlessEqual(("Hello", ("black", "white", "r")), next(tty.blocks()))

    @test("bug")
    def underline(self):
        """Check underline text is supported"""
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        s = Colours.ColourStream(f, uline=True)
        s.write("Hello")
        failUnlessEqual(("Hello", ("black", "white", "u")), next(tty.blocks()))

    @test("bug")
    def mixedAttrs(self):
        """Check mixed bold, reverse, blinking underline text is supported"""
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        s = Colours.ColourStream(f, uline=True, bold=True, rev=True, blink=True)
        s.write("Hello")
        failUnlessEqual(("Hello", ("black", "white", "buBr")), next(tty.blocks()))

    @test("bug")
    def mixedAllAttrs(self):
        """Check foreground/background/bold/etc attributes can be combined."""
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)

        s = Colours.ColourStream(f, fg="red", bg="blue", bold=True)
        s.write("Hello")

        s = Colours.ColourStream(f, fg="red", bg="blue")
        s.write("Hello")

        s = Colours.ColourStream(f, fg="green", bold=1)
        s.write("Hello")

        blocks = tty.blocks()
        failUnlessEqual(("Hello", ("red", "blue", "b")), next(blocks))
        failUnlessEqual(("Hello", ("red", "blue", "")), next(blocks))
        failUnlessEqual(("Hello", ("green", "white", "b")), next(blocks))

    @test("bug")
    def modifyingAttrs(self):
        """Check ColourStream attributes can be modified."""
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        blocks = tty.blocks()

        s = Colours.ColourStream(f, fg="red", bg="blue", bold=True)
        s.write("Hello")

        s.setScheme(fg="red", bg="blue")
        s.write("Hello")

        s.setScheme(fg="green", bold=1)
        s.write("Hello")

        failUnlessEqual(("Hello", ("red", "blue", "b")), next(blocks))
        failUnlessEqual(("Hello", ("red", "blue", "")), next(blocks))
        failUnlessEqual(("Hello", ("green", "white", "b")), next(blocks))

    @test("bug")
    def eolHandling(self):
        """Check ColourStream.write handles end of line OK"""
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)

        s = Colours.ColourStream(f, fg="red", bg="blue", bold=True)
        s.write("Hello\n\nThere")

        blocks = tty.blocks()
        failUnlessEqual(("Hello", ("red", "blue", "b")), next(blocks))
        failUnlessEqual(("\n", (None, None, None)), next(blocks))
        failUnlessEqual(("\n", (None, None, None)), next(blocks))
        failUnlessEqual(("There", ("red", "blue", "b")), next(blocks))
        failUnlessEqual(("\n", (None, None, None)), next(blocks))

    @test("bug")
    def writelines(self):
        """Check ColourStream.writelines function."""
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        blocks = tty.blocks()

        s = Colours.ColourStream(f, fg="red", bg="blue", bold=True)
        s.writelines(["Hello\n", "There\n"])
        s.setScheme(fg="green")
        s.writelines(["The\n", "End\n"])

        failUnlessEqual(("Hello", ("red", "blue", "b")), next(blocks))
        failUnlessEqual(("\n", (None, None, None)), next(blocks))
        failUnlessEqual(("There", ("red", "blue", "b")), next(blocks))
        failUnlessEqual(("\n", (None, None, None)), next(blocks))
        failUnlessEqual(("The", ("green", "white", "")), next(blocks))
        failUnlessEqual(("\n", (None, None, None)), next(blocks))
        failUnlessEqual(("End", ("green", "white", "")), next(blocks))
        failUnlessEqual(("\n", (None, None, None)), next(blocks))


class Coverage(Suite):
    """Mopping up coverage."""
    
    def suiteTearDown(self):
        """
        Tidy up a tmp file created during these tests 
        """
        os.remove("/tmp/emu.log")
        super(Coverage, self).suiteTearDown()

    @test
    def initial_getTermCap(self):
        """Simulate part of module load, with non-tty."""
        stdout, sys.stdout = sys.stdout, StringIO()
        try:
            Colours._getTermCap()
        finally:
            sys.stdout = stdout

    @test
    def initial_getTermCap_nonFile(self):
        """Simulate part of module load, with non-file for sys.stdout."""
        stdout, sys.stdout = sys.stdout, None
        try:
            Colours._getTermCap()
        finally:
            sys.stdout = stdout

    @test
    def nonFileForColourStream(self):
        """Check ColourStream handle a non-file gracefully(ish)"""
        s = Colours.ColourStream(None)

    @test
    def atexitTidy(self):
        """Simulate invoking the _tidy function used at exit."""
        Colours._tidy()

    @test
    def reset(self):
        """Reset of a colour stream."""
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        s = Colours.ColourStream(f)
        s.reset()


class Xstream(Suite):
    """Tests for the Xstream class."""

    def suiteTearDown(self):
        """
        Tidy up a tmp file created during these tests 
        """
        os.remove("/tmp/emu.log")
        super(Xstream, self).suiteTearDown()

    @test
    def plain_string(self):
        """Check we can use a plain string."""
        tty = TermEmu.TTY()
        xs = Colours.Xstream(tty)
        xs.write("Hello")
        failUnlessEqual(("Hello", ("black", "white", "")), next(tty.blocks()))

    @test("bug")
    def mixed(self):
        """Check we can use a mixture of plain string and streams."""
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        xs = Colours.Xstream(f)
        a = Colours.ColourStream(f, fg="red", bg="blue")
        b = Colours.ColourStream(f, fg="green", bold=1)
        xs.write([a, "red", " on blue", b, " bold green"])
        blocks = tty.blocks()
        failUnlessEqual(("red on blue", ("red", "blue", "")), next(blocks))
        failUnlessEqual((" bold green", ("green", "white", "b")), next(blocks))


if __name__ == "__main__":
    runModule()
