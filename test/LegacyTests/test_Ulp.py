#!/usr/bin/env python
"""CleverSheep.Test.Tester.UlpCompare unit tests"""

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test.Tester.UlpCompare import ulpFloatDifference

import struct

# The module under test.
module_under_test = "../../CleverSheep/Test/Tester/UlpCompare.py"

# Test numeric values
NaN = float('nan')
PosInf = float('inf')
NegInf = float('-inf')
PosZero = +0.0
NegZero = -0.0


class Test_ulp(Suite):
    """Tests for ULP calculation."""

    def castIntToFloat(self, x):
        """ Convert integer to float
        
        Perform a c-style memory cast in order to achieve conversion of
        integer value into float. This should be be
        equivalent of c-language code:
        
          return *(float*)&x;

        """
        return struct.unpack('f', struct.pack('i', x))[0]
    
    @test
    def compare_normal(self):
        """Verify that normal value comparison works"""
        failUnlessEqual(0, ulpFloatDifference(1.0,1.0))
        failUnlessEqual(0, ulpFloatDifference(-2.1,-2.1))
        failIfEqual(0, ulpFloatDifference(-21.0,21.0))
        failIfEqual(0, ulpFloatDifference(123.4, -123.4))
                
    @test
    def compare_zero(self):
        """Verify that zero comparison works"""
        failUnlessEqual(0, ulpFloatDifference(0.0,0.0))
        failUnlessEqual(0, ulpFloatDifference(PosZero, PosZero))
        failUnlessEqual(0, ulpFloatDifference(NegZero, NegZero))
        
        # Also +0.0 and -0.0 should compare equal; i.e. ULP difference == 0.
        failUnlessEqual(0, ulpFloatDifference(PosZero, NegZero))
        failUnlessEqual(0, ulpFloatDifference(NegZero, PosZero))

    @test
    def compare_not_a_number(self):
        """Verify that NaN comparison works.
        
        NaN, by definition, cannot compare equal to any other float including
        NaN.

        """
        failIfEqual(0, ulpFloatDifference(NaN, NaN))
        failIfEqual(0, ulpFloatDifference(NaN, NegInf))
        failIfEqual(0, ulpFloatDifference(NaN, PosInf))
        failIfEqual(0, ulpFloatDifference(NaN, NegZero))
        failIfEqual(0, ulpFloatDifference(NaN, PosZero))

    @test
    def compare_infinity(self):
        """Verify that Infinity comparison works"""
        failUnlessEqual(0, ulpFloatDifference(PosInf, PosInf))
        failIfEqual(0, ulpFloatDifference(PosInf, NegInf))
        failIfEqual(0, ulpFloatDifference(NegInf, PosInf))

    @test
    def compare_near_zero(self):
        """Verify that comparison of numbers near zero works"""
        v1 = self.castIntToFloat(0 + 1)
        failUnlessEqual(1, ulpFloatDifference(0.0, v1))
        v2 = self.castIntToFloat(0 + 11)
        failUnlessEqual(11, ulpFloatDifference(0.0, v2))
        print(ulpFloatDifference(0.0, v1))
        print(ulpFloatDifference(0.0, v2))


if __name__ == "__main__":
    runModule()
