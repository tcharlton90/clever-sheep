#!/usr/bin/env python
"""CleverSheep.Test.PollManager, using tornado, unit tests"""
import CheckEnv

from CleverSheep.Sys import Platform
if Platform.platformType == "windows":
    from CleverSheep.Test.Tester import Collection
    raise Collection.Unsupported


import time
import os
import errno
import fcntl
import threading

from CleverSheep.Test.Tester import *


# The module under test.
from CleverSheep.Test import PollManager

import pollmanbase


class TestBase(Suite):
    pass


class PollManager_Timeouts(pollmanbase.PollManager_Timeouts, TestBase):
    """Tests for the PollManager class - timeout processing"""
    impl = "tornado"


class PollManager_SimpleCallback(pollmanbase.PollManager_SimpleCallback,
                                 TestBase):
    """Tests for the PollManager class - using simple callbacks.

    """
    impl = "tornado"


class PollManager_FileCallbacks(pollmanbase.PollManager_FileCallbacks,
                                TestBase):
    """Tests for the PollManager class - file callback processing"""
    impl = "tornado"


class CallbackRef(pollmanbase.CallbackRef, TestBase):
    """Tests for the CallbackRef class.

    These only target code that the other tests do not exercise.

    """
    impl = "tornado"


class Nesting(pollmanbase.Nesting, TestBase):
    """It should be possible to nest PollManager.run invocations.

    The `active` flag indicates if the PollManager is already executing its
    `run` method, but it is nicer to be able to simply invoke::

        pollman.run()

    and assume that on return the sub-run has finished.

    """
    impl = "tornado"


if __name__ == "__main__":
    runModule()
