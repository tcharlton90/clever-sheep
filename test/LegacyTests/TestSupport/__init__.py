#!/usr/bin/env python
"""<+One-liner+>

<+Detailed multiline documentation+>
"""

import os
import sys


# We need the directory above to be in out path so that we can import the
# modules under test.
sys.path[0:0] = [os.path.normpath(os.path.join(os.getcwd(), "../.."))]


def importForTest(name):
    """Import a module that we wish to test.

    <+multiline details+>
    """
    exec("import %s as _temp" % name)
    return _temp


