#! /usr/bin/env python
"""Unit tests for the Files module."""

import os
import stat
import time
import shutil

import CheckEnv

from CleverSheep.Prog.Curry import Curry

from CleverSheep.Test.Tester import *
from TestSupport.Files import FileBasedTests, ensureNoFile
from CleverSheep.Test.Tester import Suites

from CleverSheep.Prog import Files

control = Suites.control()


def getStat(path):
    st = os.stat(path)
    return stat.S_IMODE(st.st_mode)


def failUnlessStatEqual(expect, actual):
    def rep():
        return "File mode did not match, expected=%03o actual=%03o" % (
                expect, actual)
    failUnlessEqual(expect, actual, makeMessage=rep)


class Test_File(FileBasedTests):
    """Tester for the File class and hence Stat mixin.

    It should be possible to exercise most of the Stat mixin via the
    File class. Anything that cannot be tested this way may well be dead
    code.
    """
    def suiteSetUp(self):
        if os.path.exists("A"):
            shutil.rmtree("A")

    def setUp (self):
        # TODO: There is some commonality in various setup and teardown methods.
        FileBasedTests.setUp(self)
        self.addDirs(["TestSources"])
        os.chdir("TestSources")

        cwd = os.getcwd()
        self.saveRoot = Files.projectRoot
        Files.projectRoot = cwd

    def tearDown(self):
        if self.saveRoot is not None:
            Files.projectRoot = self.saveRoot
        FileBasedTests.tearDown(self)

    @test
    def basic(self):
        """Basic file operations."""
        f = Files.File("a.c")
        ensureNoFile("a.c")
        failIf(os.path.exists("a.c"))
        failUnlessEqual(f.path, os.path.abspath("a.c"))
        failUnlessEqual(f.getmtime(), 0)
        failIf(f.exists())
        failIf(f.isfile())
        failIf(f.islink())

        self.addFile("a.c")
        failUnless(f.exists())
        mtime = os.path.getmtime("a.c")
        failIfEqual(f.getmtime(), 0)
        failUnlessEqual(f.getmtime(), mtime)

        failUnless(f.isfile())
        failIf(f.isdir())
        failIf(f.islink())

        os.chmod("a.c", 0o751)
        failUnlessEqual(0o751, f.mode())

    @test
    def mode_for_symlink(self):
        """Verify we can get the mode for a symbolic link.

        We create a file, set it mode and then create a link to it.
        The ``mode`` method should give the underlying file's mode, but
        the ``lmode`` method should give 0777.

        Also ``lmode`` for the plain file should work OK, giving the same
        result as for ``mode``.
        """
        self.addFile("a.c")
        os.chmod("a.c", 0o751)
        self.addSymLink(name="a-link.c", target="a.c")

        f = Files.File("a.c")
        s = Files.File("a-link.c")
        failUnlessStatEqual(0o751, f.mode())
        failUnlessStatEqual(0o751, s.mode())
        failUnlessStatEqual(0o777, s.lmode())
        failUnlessStatEqual(0o751, f.lmode())

    @test
    def relative_names(self):
        """Extracting relative path names, also test __str__."""
        self.addDir("A/B-1/CCCCC")
        self.addDir("B")
        self.addFile("A/B-1/CCCCC/fred")

        f = Files.File("A/B-1/CCCCC/fred")
        fAbs = os.path.abspath("A/B-1/CCCCC/fred")
        failUnlessEqual("A/B-1/CCCCC/fred", f.relName())
        failUnlessEqual("A/B-1/CCCCC/fred", str(f))
        failUnlessEqual("B-1/CCCCC/fred", f.relName(cwd=os.path.abspath("A")))

        here = os.getcwd()
        os.chdir("A/B-1")
        failUnlessEqual("CCCCC/fred", f.relName())
        failUnlessEqual("CCCCC/fred", str(f))
        os.chdir("CCCCC")
        failUnlessEqual("fred", f.relName())
        failUnlessEqual("fred", str(f))
        os.chdir(here)

        os.chdir("B")
        failUnlessEqual("../A/B-1/CCCCC/fred", f.relName())
        failUnlessEqual("../A/B-1/CCCCC/fred", str(f))
        os.chdir(here)

        os.chdir("A")
        Files.projectRoot = os.getcwd()
        os.chdir("../B")
        failUnlessEqual(fAbs, f.relName())
        failUnlessEqual(fAbs, str(f))
        os.chdir(here)

        failUnlessEqual("B-1/CCCCC/fred", f.projName())

        f = Files.File("A/B-1/CCCCC")
        failUnlessEqual(".", f.relName(cwd=os.path.abspath("A/B-1/CCCCC")))
        f = Files.File("A/B-1/CCCCC")

    @test
    def directories(self):
        """Tester handling of directories."""
        f = Files.File("A/B-1/CCCCC")
        failIf(f.isdir())

        self.addDir("A/B-1/CCCCC")
        failUnless(f.isdir())

    @test
    def symlinks(self):
        """Tester handling of symbolic links."""
        ensureNoFile("x")
        self.addFiles(["a", "b"])
        self.addSymLink("c", "b")
        self.addSymLink("d", "x")

        a = Files.File("a")
        b = Files.File("b")
        c = Files.File("c")
        d = Files.File("d")

        failUnless(a.exists())
        failUnless(b.exists())
        failUnless(c.exists())
        failIf(d.exists())

        failUnless(c.islink())
        failUnless(d.islink())

    @test
    def symlinkModTime(self):
        """Verify we can get the modification time of a symbolic link."""
        self.addFile("real-file")
        a = Files.File("real-file")
        while int(a.mtime()) >= int(time.time()):
            time.sleep(0.1)
        time.sleep(0.1) # Just to be sure (floats are tricky)
        self.addSymLink(name="link-file", target="real-file")
        b = Files.File("link-file")
        failUnlessEqual(a.mtime(), b.mtime())
        failIfEqual(a.mtime(), b.getlmtime())
        failIfEqual(a.getlmtime(), b.getlmtime())
        failUnlessEqual(a.getlmtime(), b.mtime())
        failIfEqual(0, a.getlmtime())

    @test
    def isBelowDir(self):
        """Check the isBelowDir method."""
        # The same directory is not below.
        failIf(Files.File(".").isBelowDir("."))
        failIf(Files.File("A").isBelowDir("A"))
        failIf(Files.File("A/").isBelowDir("A"))

        # True sub-directories."""
        a = Files.File("A/B")
        failUnless(Files.File("A/B").isBelowDir("A"))
        failUnless(Files.File("A/B/").isBelowDir("A"))
        failUnless(Files.File("A/B/C").isBelowDir("A"))
        failUnless(Files.File("A/B/C/").isBelowDir("A"))


    @test
    def parts_of_names(self):
        """Tester the methods that give dirname extension, etc."""
        self.addDir("A/B-1/CCCCC")
        self.addDir("B")
        self.addFile("A/B-1/CCCCC/fred.obj", "Hello")
        fAbs = os.path.abspath("A/B-1/CCCCC/fred.obj")

        f = Files.File("A/B-1/CCCCC/fred.obj")
        failUnlessEqual(f.ext(), ".obj")
        failUnlessEqual(f.head(), os.path.splitext(fAbs)[0])
        failUnlessEqual(f.dir(),  os.path.dirname(fAbs))
        failUnlessEqual(f.base(), "fred.obj")

        failUnlessEqual(f.relDir(), "A/B-1/CCCCC")
        failUnlessEqual(f.relHead(), "A/B-1/CCCCC/fred")

    @test
    def information(self):
        """Tester the methods that get file information."""
        self.addDir("A/B-1/CCCCC")
        self.addDir("B")
        self.addFile("A/B-1/CCCCC/fred.obj", "Hello\n")
        fAbs = os.path.abspath("A/B-1/CCCCC/fred.obj")

        f = Files.File("A/B-1/CCCCC/fred.obj")
        failUnlessEqual(f.getsize(), 6)


class Chmod(FileBasedTests):
    """Tester for the chmod funciton"""
    @test
    def add_permissions(self):
        """Verify adding permissions."""
        p = "temp.txt"
        self.addFile(p)
        os.chmod(p, 0o000)
        failUnlessEqual(0o000, getStat(p))

        Files.chmod(p, "u+r")
        failUnlessEqual(0o400, getStat(p))
        Files.chmod(p, "u+w")
        failUnlessEqual(0o600, getStat(p))
        Files.chmod(p, "u+x")
        failUnlessEqual(0o700, getStat(p))

        Files.chmod(p, "g+rx")
        failUnlessEqual(0o750, getStat(p))
        Files.chmod(p, "o+xrw")
        failUnlessEqual(0o757, getStat(p))

    @test
    def del_permissions(self):
        """Verify removing permissions."""
        p = "temp.txt"
        self.addFile(p)
        os.chmod(p, 0o777)
        failUnlessEqual(0o777, getStat(p))

        Files.chmod(p, "u-r")
        failUnlessEqual(0o377, getStat(p))
        Files.chmod(p, "u-w")
        failUnlessEqual(0o177, getStat(p))
        Files.chmod(p, "u-x")
        failUnlessEqual(0o077, getStat(p))

        Files.chmod(p, "g-rx")
        failUnlessEqual(0o027, getStat(p))
        Files.chmod(p, "o-xrw")
        failUnlessEqual(0o020, getStat(p))

    @test
    def set_permissions(self):
        """Verify setting permission bits explicitly."""
        p = "temp.txt"
        self.addFile(p)
        os.chmod(p, 0o070)
        failUnlessEqual(0o070, getStat(p))

        Files.chmod(p, "u=rw")
        failUnlessEqual(0o670, getStat(p))
        Files.chmod(p, "g=w")
        failUnlessEqual(0o620, getStat(p))
        Files.chmod(p, "o=xrw")
        failUnlessEqual(0o627, getStat(p))

    @test
    def set_permissions_using_int(self):
        """Verify setting permission bits explicitly, using an integer value."""
        p = "temp.txt"
        self.addFile(p)
        os.chmod(p, 0o070)
        failUnlessEqual(0o070, getStat(p))

        Files.chmod(p, 0o531)

    @test
    def multiple_groups_and_methods(self):
        """Use a mixture of setting methods, as in u=rw,g-x,o+rw."""
        p = "temp.txt"
        self.addFile(p)
        os.chmod(p, 0o071)
        failUnlessEqual(0o071, getStat(p))

        Files.chmod(p, "u=rw,g-x,o+rw")
        failUnlessEqual(0o667, getStat(p))

    @test
    def cannot_get_orig_mode(self):
        """If the original mode cannot be read, then should return (None, None)"""
        p = "no-such-file.txt"
        ret = Files.chmod(p, "u=rw,g-x,o+rw")
        failUnlessEqual((None, None), ret)

    @test
    def cannot_set_new_mode(self):
        """If the new mode cannot be set, then should return (mode, -1)"""
        p = "temp.txt"
        self.addFile(p)
        os.chmod(p, 0o071)

        def _chmod(p, v):
            raise OSError("Just testing")
        origChmod, os.chmod = os.chmod, _chmod
        try:
            ret = Files.chmod(p, "u=rw,g-x,o+rw")
        finally:
            os.chmod = origChmod
        failUnlessEqual((0o071, -1), ret)

    @test
    def invalid_mode_string(self):
        """If the mode string is invalid, then should return (mode, -2)"""
        p = "temp.txt"
        self.addFile(p)
        os.chmod(p, 0o051)
        ret = Files.chmod(p, "U=rw,g-x,o+rw")
        failUnlessEqual((0o051, -2), ret)


class MiscFuncs(FileBasedTests):
    """Tester for a set of simple functions in the File module."""
    def setUp (self):
        # TODO: There is some commonality in various setup and teardown methods.
        FileBasedTests.setUp(self)
        self.addDirs(["TestSources"])
        os.chdir("TestSources")

        cwd = os.getcwd()
        self.saveRoot = Files.projectRoot
        Files.projectRoot = cwd

    def tearDown(self):
        if self.saveRoot is not None:
            Files.projectRoot = self.saveRoot
        FileBasedTests.tearDown(self)

    @test
    def findInPathsets(self):
        """Tester the findInPathSets function."""
        self.addDir("A/B-1/CCCCC")
        self.addDir("A/B-2/CCCCC")
        self.addDir("A/B-3/CCCCC")
        self.addDir("A/B-4/CCCCC")
        self.addDir("B")
        self.addFile("A/B-1/CCCCC/fred")

        f = Files.File("A/B-1/CCCCC/fred")
        fAbs = os.path.abspath("A/B-1/CCCCC/fred")

        fullPath, dirName, setIdx = Files.findInPathSets("fred", ("A/B-1/CCCCC",))
        failUnlessEqual(fullPath, fAbs)
        failUnlessEqual(dirName, f.relDir())
        failUnlessEqual(setIdx, 0)

        fullPath, dirName, setIdx = Files.findInPathSets(fAbs, ("A/B-1/CCCCC",))
        failUnless(fullPath is None)

        fullPath, dirName, setIdx = Files.findInPathSets("fred", ("B",))
        failUnless(fullPath is None)

        fullPath, dirName, setIdx = Files.findInPathSets("fred",
                ("A/B-3/CCCCC", "A/B-4/CCCCC"),
                ("A/B-2/CCCCC", "A/B-1/CCCCC"),
                ("A/B-2/CCCCC", "A/B-1/CCCCC"),
                )
        failUnlessEqual(fullPath, fAbs)
        failUnlessEqual(dirName, f.relDir())
        failUnlessEqual(setIdx, 1)

    @test
    def normPath(self):
        """Tester the normPath function."""
        fAbs = os.path.abspath("A/B-1/CCCCC/fred")

        failUnlessEqual(Files.normPath(
            "A/B-1/CCCCC/fred", os.getcwd()), fAbs)
        failUnlessEqual(Files.normPath(
            "A/../A/B-1/CCCCC/fred", os.getcwd()), fAbs)
        failUnlessEqual(Files.normPath(
            "B-1/CCCCC/fred", os.path.join(os.getcwd(),"A")), fAbs)

    @test
    def rmFile(self):
        """The rmFile function."""
        p = "temp.txt"
        # It is OK if the file does not exist.
        failIf(os.path.exists(p))
        Files.rmFile(p)
        failIf(os.path.exists(p))

        # File is removed if it does exist.
        self.addFile(p)
        failUnless(os.path.exists(p))
        Files.rmFile(p)
        failIf(os.path.exists(p))

        # Directory can be separately provided
        p = "A/temp.txt"
        self.addDir("A")
        self.addFile(p)
        failUnless(os.path.exists(p))
        Files.rmFile("temp.txt", parentDir="A")
        failIf(os.path.exists(p))

    @test
    def rmEmptyDir(self):
        """The rmEmptyDir function."""
        p = "A/temp.txt"
        self.addDir("A")
        self.addFile(p)
        failUnless(os.path.exists(p))

        # Not removed if the directory is not empty
        ret = Files.rmEmptyDir("A")
        failUnless(os.path.exists("A"))
        failIf(ret)

        # Is removed if the directory is empty
        Files.rmFile(p)
        ret = Files.rmEmptyDir("A")
        failIf(os.path.exists("A"))
        failUnless(ret)

    @test
    def parentOrThisDir(self):
        """The parentOrThisDir function."""
        self.addDir("A")
        failUnlessEqual("A", Files.parentOrThisDir("A"))
        failUnlessEqual("A/", Files.parentOrThisDir("A/"))
        failUnlessEqual("A", Files.parentOrThisDir("A/c.py"))

    @test
    def filesAndDirs(self):
        """The filesAndDirs function."""
        self.addDirs(["A", "A/X", "A/Y"])
        self.addFiles(["A/a", "A/x.py", "A/X/x"])

        files, dirs = Files.filesAndDirs("A")
        failUnlessEqual(["a", "x.py"], sorted(files))
        failUnlessEqual(["X", "Y"], sorted(dirs))

        files, dirs = Files.filesAndDirs("A", fileRegExp=".*[.]py")
        failUnlessEqual(["x.py"], sorted(files))
        failUnlessEqual(["X", "Y"], sorted(dirs))


class MkDir(FileBasedTests):
    """Tests for the mkDir functions."""
    @test
    def mkDir_ok(self):
        """The mkDir function."""
        self.addDir("A")
        p = "A/B"
        if os.path.exists(p):
            shutil.rmtree(p)
        failIf(os.path.exists(p))
        Files.mkDir(p)
        failUnless(os.path.exists(p))

        # A second attempt does not cause any error.
        Files.mkDir(p)
        failUnless(os.path.exists(p))

    @test
    def mkDir_race(self):
        """A race condition does not raise OSError."""
        self.addDir("A")
        def _makedirs(p):
            origMakedirs(p)
            raise OSError("Just testing")
        p = "A/C"
        failIf(os.path.exists(p))
        origMakedirs, os.makedirs = os.makedirs, _makedirs
        try:
            Files.mkDir(p)
        finally:
            os.makedirs = origMakedirs
        failUnless(os.path.exists(p))

    @test
    def mkDir_nonrace(self):
        """A nonrace condition does raise OSError."""
        self.addDir("A")
        def _makedirs(p):
            raise OSError("Just testing")
        p = "A/C"
        failIf(os.path.exists(p))
        origMakedirs, os.makedirs = os.makedirs, _makedirs
        try:
            failUnlessRaises(OSError, Files.mkDir, p)
        finally:
            os.makedirs = origMakedirs
        failIf(os.path.exists(p))

    @test
    def mkParentDir(self):
        """The mkParentDir function."""
        p = "A/B/C"
        failIf(os.path.isdir("A/B"))
        Files.mkParentDir(p)
        failUnless(os.path.isdir("A/B"))
        failIf(os.path.exists("A/B/C"))


class IterGlobTree(FileBasedTests):
    """Test for the iterGlobTree function."""
    def setUp (self):
        FileBasedTests.setUp(self)
        self.expect = {}
        self.addDirs(["TestSources"])
        os.chdir("TestSources")

        self.addDirs(["A", "B", "C"])
        self.addFiles(["A/a.c", "A/b.c"])
        self.addFiles(["B/a.c", "B/b.c", "B/b.h"])
        self.addFiles(["C/a.f", "C/b.c"])

    def addFiles(self, files, contents=None):
        FileBasedTests.addFiles(self, files, contents=contents)
        for p in files:
            self.expect["./" + p] = None

    @test
    def find_all(self):
        """The default behaviour of finding all files."""
        def report():
            return "Function iterGlobTree yielded unexpected path\n  %r" % p

        for p in Files.iterGlobTree("."):
            failUnless(p in self.expect, makeMessage=report)
            del self.expect[p]
        failUnlessEqual(0, len(self.expect))

    @test
    def find_using_pattern(self):
        """The matches can be restricted using patterns."""
        def report():
            return "Function iterGlobTree yielded unexpected path\n  %r" % p

        for p in ["C/a.f"]:
            del self.expect["./" + p]
        for p in Files.iterGlobTree(".", inclFiles=["*.c", "*.h"]):
            failUnless(p in self.expect, makeMessage=report)
            del self.expect[p]
        failUnlessEqual(0, len(self.expect))

    @test
    def find_in_selected_dirs(self):
        """The matches can be restricted to specific directories."""
        def report():
            return "Function iterGlobTree yielded unexpected path\n  %r" % p

        self.expect = dict(((k, v) for k, v in self.expect.iteritems()
            if k[2] in "AC"))
        for p in Files.iterGlobTree(".", inclDirs=["A", "C"]):
            failUnless(p in self.expect, makeMessage=report)
            del self.expect[p]
        failUnlessEqual(0, len(self.expect))

    @test
    def find_with_excluded_dirs(self):
        """The matches can be restricted by excluding directories."""
        def report():
            return "Function iterGlobTree yielded unexpected path\n  %r" % p

        self.expect = dict(((k, v) for k, v in self.expect.iteritems()
            if k[2] not in "AC"))
        for p in Files.iterGlobTree(".", exclDirs=["A", "C"]):
            failUnless(p in self.expect, makeMessage=report)
            del self.expect[p]
        failUnlessEqual(0, len(self.expect))

    @test
    def find_using_simple_string_pattern(self):
        """A simple string can be used for the pattern."""
        def report():
            return "Function iterGlobTree yielded unexpected path\n  %r" % p

        for p in ["C/a.f", "B/b.h"]:
            del self.expect["./" + p]
        for p in Files.iterGlobTree(".", inclFiles="*.c"):
            failUnless(p in self.expect, makeMessage=report)
            del self.expect[p]
        failUnlessEqual(0, len(self.expect))

    @test
    def string_for_incl_dirs(self):
        """The inclDirs argument can just be a string."""
        def report():
            return "Function iterGlobTree yielded unexpected path\n  %r" % p

        self.expect = dict(((k, v) for k, v in self.expect.iteritems()
            if k[2] in "C"))
        for p in Files.iterGlobTree(".", inclDirs="C"):
            failUnless(p in self.expect, makeMessage=report)
            del self.expect[p]
        failUnlessEqual(0, len(self.expect))

    @test
    def string_for_excl_dirs(self):
        """The exclDirs argument can just be a string."""
        def report():
            return "Function iterGlobTree yielded unexpected path\n  %r" % p

        self.expect = dict(((k, v) for k, v in self.expect.iteritems()
            if k[2] not in "A"))
        for p in Files.iterGlobTree(".", exclDirs="A"):
            failUnless(p in self.expect, makeMessage=report)
            del self.expect[p]
        failUnlessEqual(0, len(self.expect))

    @test
    def strip_leading_dirs(self):
        """Some leading directories can be removed."""
        def report():
            return "Function iterGlobTree yielded unexpected path\n  %r" % p

        savedExpect = self.expect.copy()
        self.expect = dict(((k[2:], v) for k, v in self.expect.iteritems()))
        for p in Files.iterGlobTree(".", stripCount=1):
            failUnless(p in self.expect, makeMessage=report)
            del self.expect[p]
        failUnlessEqual(0, len(self.expect))

        self.expect = dict(((k[4:], v) for k, v in savedExpect.iteritems()))
        for p in Files.iterGlobTree(".", stripCount=2):
            failUnless(p in self.expect, makeMessage=report)

    @test
    def glob_tree(self):
        """Check calling via the globTree (non-generator) version."""
        def report():
            return "Function globTree yielded unexpected path\n  %r" % p

        for p in Files.globTree("."):
            failUnless(p in self.expect, makeMessage=report)
            del self.expect[p]
        failUnlessEqual(0, len(self.expect))


class FindDir(FileBasedTests):
    """Test for the findDir function."""
    @test
    def find_above(self):
        """Find a directory that *is* above the current directory."""
        self.addDirs(["A", "A/B", "A/B/C"])
        here = os.getcwd()
        try:
            os.chdir("A/B/C")
            d1 = Files.findDirAbove("A")
            d2 = Files.findDirAbove("B")
            os.chdir("..")
            d3 = Files.findDirAbove("A")
        finally:
            os.chdir(here)
        failUnlessEqual(os.path.abspath("A"), d1)
        failUnlessEqual(os.path.abspath("A/B"), d2)
        failUnlessEqual(os.path.abspath("A"), d3)

    @test
    def find_immediate(self):
        """Find a directory that is the immediate directory"""
        self.addDirs(["A", "A/B"])
        here = os.getcwd()
        try:
            os.chdir("A/B")
            d1 = Files.findDirAbove("B")
        finally:
            os.chdir(here)
        failUnlessEqual(os.path.abspath("A/B"), d1)

    @test
    def find_fail(self):
        """Check failure is handled."""
        self.addDirs(["A", "A/B"])
        here = os.getcwd()
        try:
            os.chdir("A/B")
            d1 = Files.findDirAbove("NO-DIR")
        finally:
            os.chdir(here)
        failUnlessEqual(None, d1)


class PathComponents(FileBasedTests):
    """Test for the pathComponents function."""
    @test
    def no_components(self):
        """A path with no components."""
        failUnlessEqual([], Files.pathComponents(""))

    @test
    def single_component(self):
        """A path with just one component"""
        failUnlessEqual(["abc"], Files.pathComponents("./abc"))
        failUnlessEqual(["abc"], Files.pathComponents("abc"))
        failUnlessEqual(["abc"], Files.pathComponents("abc/"))

    @test
    def several_component(self):
        """A path with just one component"""
        failUnlessEqual(["abc", "xxx"], Files.pathComponents("./abc/xxx"))
        failUnlessEqual(["abc", "xxx"], Files.pathComponents("abc/xxx"))
        failUnlessEqual(["abc", "xxx"], Files.pathComponents("abc/xxx/"))


class IsSubDir(FileBasedTests):
    """Test for the isSubDir function."""
    @test
    def the_same_directory(self):
        """The same directory is not a sub-directory."""
        failIf(Files.isSubDir(".", "."))
        failIf(Files.isSubDir("A", "A"))
        failIf(Files.isSubDir("A", "A/"))

    @test
    def a_true_subdirectory(self):
        """True sub-directories."""
        failUnless(Files.isSubDir("A", "A/B"))
        failUnless(Files.isSubDir("A", "A/B/"))
        failUnless(Files.isSubDir("A", "A/B/C"))
        failUnless(Files.isSubDir("A", "A/B/C/"))


if __name__ == "__main__":
    runModule()
