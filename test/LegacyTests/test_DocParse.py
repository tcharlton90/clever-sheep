#!/usr/bin/env python
"""Unit tests for TestTool.DocParse."""

import sys

from CleverSheep.Prog import Files
from CleverSheep.Test.Tester import *
from CleverSheep.Test import ImpUtils
from CleverSheep.Test import DataMaker


# The module under test.
module_under_test = "../CleverSheep/TextTools/DocParse.py"
from CleverSheep.TextTools import DocParse


class TrimLines(Suite):
    """The trimLines function"""
    @test
    def trim_leading_blanks(self):
        """Check leading blanks lines get removed"""
        lines = DataMaker.literalText2Lines("""
          |
          |
          | The only real line.
        """)
        trimmed = list(DocParse.trimLines(lines))
        failUnlessEqual(1, len(trimmed))
        failUnlessEqual("The only real line.", trimmed[0])

    @test
    def trim_trailing_blanks(self):
        """Check trailing blanks lines get removed"""
        lines = DataMaker.literalText2Lines("""
          | The only real line.
          |
          |
        """)
        trimmed = list(DocParse.trimLines(lines))
        failUnlessEqual(1, len(trimmed))
        failUnlessEqual("The only real line.", trimmed[0])

    @test
    def trim_both(self):
        """Check trailing and leading blanks lines get removed"""
        lines = DataMaker.literalText2Lines("""
          |
          |
          | The only real line.
          |
          |
        """)
        trimmed = list(DocParse.trimLines(lines))
        failUnlessEqual(1, len(trimmed))
        failUnlessEqual("The only real line.", trimmed[0])

    @test
    def keep_embedded(self):
        """Check embedded blanks lines *do not* get removed"""
        lines = DataMaker.literalText2Lines("""
          |
          |
          | The first line.
          | 
          | The third line.
          |
          |
        """)
        trimmed = list(DocParse.trimLines(lines))
        failUnlessEqual(3, len(trimmed))
        failUnlessEqual("The first line.", trimmed[0])
        failUnlessEqual("", trimmed[1])
        failUnlessEqual("The third line.", trimmed[2])


class Dedenting(Suite):
    """The dedentLines function"""
    @test
    def equal_indent(self):
        """Check we handle when all lines have the same indent"""
        lines = DataMaker.literalText2Lines("""
          | Line 1
          | Line 2
        """)
        dedented = list(DocParse.dedentLines(lines))
        failUnlessEqual(2, len(dedented))
        failUnlessEqual("Line 1", dedented[0])
        failUnlessEqual("Line 2", dedented[1])

    @test
    def nothing_to_do(self):
        """Check unindented text is unmodified."""
        lines = DataMaker.literalText2Lines("""
          | Line 1
          |   Line 2
        """)
        dedented = list(DocParse.dedentLines(lines))
        failUnlessEqual(2, len(dedented))
        failUnlessEqual("Line 1", dedented[0])
        failUnlessEqual("  Line 2", dedented[1])

    @test
    def first_line_provides_indent(self):
        """Check the first line can define the indent."""
        lines = DataMaker.literalText2Lines("""
          |   Line 1
          |     Line 2
        """)
        dedented = list(DocParse.dedentLines(lines))
        failUnlessEqual(2, len(dedented))
        failUnlessEqual("Line 1", dedented[0])
        failUnlessEqual("  Line 2", dedented[1])

    @test
    def last_line_provides_indent(self):
        """Check the last line can define the indent."""
        lines = DataMaker.literalText2Lines("""
          |     Line 1
          |   Line 2
        """)
        dedented = list(DocParse.dedentLines(lines))
        failUnlessEqual(2, len(dedented))
        failUnlessEqual("  Line 1", dedented[0])
        failUnlessEqual("Line 2", dedented[1])

    @test
    def mid_line_provides_indent(self):
        """Check that a middle line can define the indent."""
        lines = DataMaker.literalText2Lines("""
          |     Line 1
          |   Line 2
          |     Line 3
        """)
        dedented = list(DocParse.dedentLines(lines))
        failUnlessEqual(3, len(dedented))
        failUnlessEqual("  Line 1", dedented[0])
        failUnlessEqual("Line 2", dedented[1])
        failUnlessEqual("  Line 3", dedented[2])

    @test
    def blanks_lines_are_ignored(self):
        """Blanks lines do not affect the dedenting process."""
        lines = DataMaker.literalText2Lines("""
          |     Line 1
          |
          |   Line 2
          |     Line 3
        """)
        dedented = list(DocParse.dedentLines(lines))
        failUnlessEqual(4, len(dedented))
        failUnlessEqual("  Line 1", dedented[0])
        failUnlessEqual("Line 2", dedented[2])
        failUnlessEqual("  Line 3", dedented[3])

    @test
    def no_lines_are_handled(self):
        """A list of all empty lines is handled."""
        lines = DataMaker.literalText2Lines("""
          |
          |
        """)
        dedented = list(DocParse.dedentLines(lines))
        failUnlessEqual(2, len(dedented))
        failUnlessEqual("", dedented[0])
        failUnlessEqual("", dedented[1])


class DocStringTidying(Suite):
    """The tidyDocstringLines function"""
    @test
    def single_line(self):
        """Check we can handle a single line docstring"""
        lines = DataMaker.literalText2Lines("""
          | The summary
        """)
        summary, body = list(DocParse.tidyDocstringLines(lines))
        failUnlessEqual(1, len(summary))
        failUnlessEqual(0, len(body))
        failUnlessEqual("The summary", summary[0])

    @test
    def multiline_summary_no_body(self):
        """Check we can handle a doc string with a multi-line summary, but no body."""
        lines = DataMaker.literalText2Lines("""
          | The summary
          | on two lines.
        """)
        summary, body = list(DocParse.tidyDocstringLines(lines))
        failUnlessEqual(2, len(summary))
        failUnlessEqual(0, len(body))
        failUnlessEqual("The summary", summary[0])
        failUnlessEqual("on two lines.", summary[1])

        #: Slight variant, where there is a trailing blank line.
        lines = DataMaker.literalText2Lines("""
          | The summary
          | on two lines.
          |
        """)
        summary, body = list(DocParse.tidyDocstringLines(lines))
        failUnlessEqual(2, len(summary))
        failUnlessEqual(0, len(body))
        failUnlessEqual("The summary", summary[0])
        failUnlessEqual("on two lines.", summary[1])

    @test
    def single_line_summary_and_body(self):
        """Check we can handle a summary and body - single line summary"""
        lines = DataMaker.literalText2Lines("""
          | The summary
          |
          | The body.
          | More body.
        """)
        summary, body = list(DocParse.tidyDocstringLines(lines))
        failUnlessEqual(1, len(summary))
        failUnlessEqual(2, len(body))
        failUnlessEqual("The summary", summary[0])
        failUnlessEqual("The body.", body[0])
        failUnlessEqual("More body.", body[1])

    @test
    def multi_line_summary_and_body(self):
        """Check we can handle a summary and body - multi line summary"""
        lines = DataMaker.literalText2Lines("""
          | The summary
          | on two lines.
          |
          | The body.
          | More body.
        """)
        summary, body = list(DocParse.tidyDocstringLines(lines))
        failUnlessEqual(2, len(summary))
        failUnlessEqual(2, len(body))
        failUnlessEqual("The summary", summary[0])
        failUnlessEqual("on two lines.", summary[1])
        failUnlessEqual("The body.", body[0])
        failUnlessEqual("More body.", body[1])

    @test
    def multi_para_body(self):
        """Check that a body can contain paragraphs."""
        lines = DataMaker.literalText2Lines("""
          | The summary
          |
          | The body.
          |
          | More body.
        """)
        summary, body = list(DocParse.tidyDocstringLines(lines))
        failUnlessEqual(1, len(summary))
        failUnlessEqual(3, len(body))
        failUnlessEqual("The summary", summary[0])
        failUnlessEqual("The body.", body[0])
        failUnlessEqual("", body[1])
        failUnlessEqual("More body.", body[2])

    @test
    def leading_trailing_blanks(self):
        """Check that leading and trailing blank lines are removed."""
        lines = DataMaker.literalText2Lines("""
          |
          |
          | The summary
          |
          |
          | The body.
          |
          | More body.
          |
          |
        """)
        summary, body = list(DocParse.tidyDocstringLines(lines))
        failUnlessEqual(1, len(summary))
        failUnlessEqual(3, len(body))
        failUnlessEqual("The summary", summary[0])
        failUnlessEqual("The body.", body[0])
        failUnlessEqual("", body[1])
        failUnlessEqual("More body.", body[2])

    @test
    def indepenent_dedenting(self):
        """Check that the dedent for the body and summary is independent."""
        lines = DataMaker.literalText2Lines("""
          |   The summary
          |
          |      The body.
          |      More body.
        """)
        summary, body = list(DocParse.tidyDocstringLines(lines))
        failUnlessEqual(1, len(summary))
        failUnlessEqual(2, len(body))
        failUnlessEqual("The summary", summary[0])
        failUnlessEqual("The body.", body[0])
        failUnlessEqual("More body.", body[1])


class PrevCurrNext(Suite):
    """The prevCurrNext generator."""
    @test
    def xxx(self):
        """Basic look-ahead, look-behind. This function yields a tuple of::

            (prev, curr, next)

        for an iterable. For an iterable that provides N values (``x(1), x(2),
        ... x(N)``), we expect the sequence::

            (None, x(0), x(1))
            (x(0), x(1), x(2))
            (x(1), x(2), x(3))
            ...
            (x(N-2), x(N-1), None)
            (x(N-1), None, None)
        """
        lines = DataMaker.literalText2Lines("""
          | 1
          | 2
          | 3
          | 4
          | 5
        """)
        for i, (prev, curr, next) in enumerate(DocParse.prevCurrNext(lines)):
            N = i + 1
            if N == 1:
                failUnlessEqual(None, prev)
            else:
                failUnlessEqual("%s" % (N - 1) , prev)
            if N >= 5:
                failUnlessEqual(None, next)
            else:
                failUnlessEqual("%s" % (N + 1) , next)
            if N == 6:
                failUnlessEqual(None, curr)
            else:
                failUnlessEqual("%s" % (N) , curr)


class Paragraphing(Suite):
    """Splitting text into paragraphs."""
    @test
    def single_paragraph_isolation(self):
        """We should be able to yield a single paragraph, which may have leading and
        trailing blank lines. """
        lines = DataMaker.literalText2Lines("""
          | A single
          | paragraph.
        """)
        for i, para in enumerate(DocParse.paragraphs(lines)):
            failUnlessEqual(2, len(para))
            failUnlessEqual("A single", para[0])
            failUnlessEqual("paragraph.", para[1])
        failUnlessEqual(0, i)

    @test
    def two_paragraphs(self):
        """Two paragraphs"""
        lines = DataMaker.literalText2Lines("""
          | Para 1, line 1.
          | Para 1, line 2.
          |
          | Para 2, line 1.
          | Para 2, line 2.
          |
        """)
        for i, para in enumerate(DocParse.paragraphs(lines)):
            failUnlessEqual(2, len(para))
            failUnlessEqual("Para %d, line 1." % (i + 1), para[0])
            failUnlessEqual("Para %d, line 2." % (i + 1), para[1])
        failUnlessEqual(1, i)


    @test
    def two_paragraphs_widely_spaced(self):
        """Two paragraphs, multiple separating lines and trailing blank line."""
        lines = DataMaker.literalText2Lines("""
          | Para 1, line 1.
          | Para 1, line 2.
          |
          |
          | Para 2, line 1.
          | Para 2, line 2.
          |
        """)
        for i, para in enumerate(DocParse.paragraphs(lines)):
            failUnlessEqual(2, len(para))
            failUnlessEqual("Para %d, line 1." % (i + 1), para[0])
            failUnlessEqual("Para %d, line 2." % (i + 1), para[1])
        failUnlessEqual(1, i)


class DocStringDisector(Suite):
    """The full DocString disector."""
    @test
    def simple_params(self):
        """Parsing of DocStrings with simple parameters."""
        lines = DataMaker.literalText2Lines("""
          | The summary.
          |
          | Some description.
          |
          | A second paragraph.
          |
          |
          | :Param p1:
          |    This is param 1.
          |    It holds a value.
          |
          | :Param p2:
          |    This is param 2.
        """)
        ds = DocParse.DocString(lines)
        failUnlessEqual(1, len(ds.summary))
        failUnlessEqual("The summary.", ds.summary[0])
        failUnlessEqual(3, len(ds.body))
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | Some description.
        |
        | A second paragraph.
        """), "\n".join(ds.body))
        failUnlessEqual(2, len(ds.paramDescriptions))

        params = ds.paramDescriptions
        failUnlessEqual(["p1"], params[0].names)
        failUnlessEqual(2, len(params[0].body))
        failUnlessEqual(["p2"], params[1].names)
        failUnlessEqual(1, len(params[1].body))


if __name__ == "__main__":
    runModule()

