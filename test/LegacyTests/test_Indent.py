#!/usr/bin/env python
"""CleverSheep.TTY_Utils.Indent unit tests"""

from cStringIO import StringIO

import CheckEnv

from CleverSheep.Test.Tester import *

# The module under test.
from CleverSheep.TTY_Utils import Indent


class IndentStream(Suite):
    """Tests for the IndentStream class."""
    @test
    def noIndentation(self):
        """Verify that IndentStream can simply pass through."""
        f = StringIO()
        s = Indent.IndentStream(f)

        s.write("Hello\nPaul\n")
        failUnlessEqual("Hello\nPaul\n", f.getvalue())

    @test
    def addIndentationFromZero(self):
        """Verify that unindented IndentStream can indent more."""
        f = StringIO()
        s = Indent.IndentStream(f)

        s.indent(2)
        s.write("Hello\nPaul\n")
        failUnlessEqual("  Hello\n  Paul\n", f.getvalue())

    @test
    def addIndentation(self):
        """Verify that IndentStream's indent level can be increased."""
        f = StringIO()
        s = Indent.IndentStream(f, 2)
        s.write("Hello\n")
        failUnlessEqual("  Hello\n", f.getvalue())

        s.indent(2)
        s.write("Paul\n")
        failUnlessEqual("  Hello\n    Paul\n", f.getvalue())

    @test
    def subIndentation(self):
        """Verify that IndentStream indentation can be reduced."""
        f = StringIO()
        s = Indent.IndentStream(f, 4)
        s.write("Hello\n")
        failUnlessEqual("    Hello\n", f.getvalue())

        s.indent(-2)
        s.write("Paul\n")
        failUnlessEqual("    Hello\n  Paul\n", f.getvalue())

        s.indent(-2)
        s.write("Done\n")
        failUnlessEqual("    Hello\n  Paul\nDone\n", f.getvalue())

    @test
    def excessSubIndentation(self):
        """Verify that indentation cannot be reduced below zero."""
        f = StringIO()
        s = Indent.IndentStream(f, 2)
        s.write("Hello\n")
        failUnlessEqual("  Hello\n", f.getvalue())

        s.indent(-2)
        s.write("Paul\n")
        failUnlessEqual("  Hello\nPaul\n", f.getvalue())

        s.indent(-2)
        s.write("Done\n")
        failUnlessEqual("  Hello\nPaul\nDone\n", f.getvalue())

    @test
    def partialLines(self):
        """Verify that partial lines get handed correctly.
        
        If the last line written has not EOL, then the next write should
        continue on the same line.
        """
        f = StringIO()
        s = Indent.IndentStream(f, 2)
        s.write("Hello\nPaul")
        failUnlessEqual("  Hello\n  Paul", f.getvalue())

        s.indent(2)
        s.write(", my friend\n")
        failUnlessEqual("  Hello\n  Paul, my friend\n", f.getvalue())

    @test
    def getvalue(self):
        """Check getvalue works if the underlying stream supports it.

        If, for example, the IndentStream's underlying file is a StringIO
        then we can invoke ``getvalue`` on the IndentStream.
        """
        f = StringIO()
        s = Indent.IndentStream(f, 2)
        s.write("Hello\nPaul")
        
        # Note we use **s** not **f** the call ``getvalue``.
        failUnlessEqual("  Hello\n  Paul", s.getvalue())

    @test
    def indentIndentStream(self):
        """Verify that IndentStream can be constructed from another IndentStream.
        
        The original IndentStream's indent is inherited as a base level of
        indentation.
        """
        f = StringIO()
        a = Indent.IndentStream(f, 2)
        s = Indent.IndentStream(a, 2)

        s.write("Hello\nPaul\n")
        failUnlessEqual("    Hello\n    Paul\n", f.getvalue())


if __name__ == "__main__":
    runModule()
