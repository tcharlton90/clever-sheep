#!/usr/bin/env python
"""CleverSheep.Test.LogTracker unit tests"""

import sys
import os

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import ImpUtils
from CleverSheep.Test import PollManager

# The module under test.
from CleverSheep.Test import LogTracker


class Base(Suite):
    """Base class for tests."""
    def setUp(self):
        if os.path.exists("XXX"):
            os.unlink("XXX")

    def tearDown(self):
        if os.path.exists("XXX"):
            os.unlink("XXX")


class Test_LogTracker(Suite):
    """Tests for the LogTracker class."""

    class Tracker(LogTracker.LogTracker):
        def __init__(self, *args, **kwargs):
            super(Test_LogTracker.Tracker, self).__init__(*args, **kwargs)
            self.data_count = 0
            self.restart_count = 0
            self.orig_data_count = 0
            self.no_file_count = 0

        def onRead(self, code):
            if code == LogTracker.NEW_DATA:
                self.data_count += 1
                if self.data_count % 2 == 0:
                    self.buf = ""
                else:
                    self.buf = self.buf[5:]
            elif code == LogTracker.RESTART:
                self.restart_count += 1
            elif code == LogTracker.ORIG_DATA:
                self.orig_data_count += 1
            elif code == LogTracker.NO_FILE:
                self.no_file_count += 1

    @test
    def missing_file_is_handled(self):
        """A missing file should cause no callbacks, even when created."""
        def seq():
            f = open("XXX", "w")
            yield False
            yield None

        def stepper():
            v = next(steps)
            if v is None:
                self.control.poll.quit()

        tracker = self.Tracker(self.control.poll, "XXX")
        steps = seq()
        self.control.poll.addRepeatingTimeout(0.1, stepper)
        self.control.delay(0.2)
        failUnlessEqual(0, tracker.data_count)

    @test
    def write_to_new_file(self):
        """Writing to a new file causes callbacks."""
        def seq():
            f = open("XXX", "w")
            yield False
            for n in range(5):
                f.write("Line %d\n" % n)
                f.flush()
                yield False
            yield None

        def stepper():
            v = next(steps)
            if v is None:
                self.control.poll.quit()

        tracker = self.Tracker(self.control.poll, "XXX")
        steps = seq()
        self.control.poll.addRepeatingTimeout(0.1, stepper)
        self.control.delay(1.0)
        failUnlessEqual(5, tracker.data_count)

    @test
    def reading_all_content(self):
        """It is possible to start by reading existing content."""
        def seq():
            yield False
            for n in range(5):
                f.write("Line %d\n" % n)
                f.flush()
                yield False
            yield None

        def stepper():
            v = next(steps)
            if v is None:
                self.control.poll.quit()

        f = open("XXX", "w")
        f.write("First line\n")
        f.flush()
        tracker = self.Tracker(self.control.poll, "XXX", readAll=True)
        steps = seq()
        self.control.poll.addRepeatingTimeout(0.1, stepper)
        self.control.delay(1.0)
        failUnlessEqual(5, tracker.data_count)
        failUnlessEqual(1, tracker.orig_data_count)

    @test
    def restart_is_spotted(self):
        """Restart of a file is spotted and notified."""
        def seq():
            f = open("XXX", "w")
            yield False
            for n in range(2):
                f.write("Line %d\n" % n)
                f.flush()
                yield False
            f.close()

            f = open("XXX", "w")
            yield False
            for n in range(2):
                f.write("Line %d\n" % n)
                f.flush()
                yield False

            yield None

        def stepper():
            v = next(steps)
            if v is None:
                self.control.poll.quit()

        tracker = self.Tracker(self.control.poll, "XXX")
        steps = seq()
        self.control.poll.addRepeatingTimeout(0.1, stepper)
        self.control.delay(1.0)
        failUnlessEqual(4, tracker.data_count)
        failUnlessInRange(1, 2, tracker.restart_count)


class Test_LineLogTracker(Suite):
    """Tests for the LineLogTracker class."""

    class Tracker(LogTracker.LineLogTracker):
        def __init__(self, *args, **kwargs):
            super(Test_LineLogTracker.Tracker, self).__init__(*args, **kwargs)
            self.data_count = 0
            self.restart_count = 0
            self.orig_data_count = 0
            self.no_file_count = 0

        def onRead(self, code):
            if code == LogTracker.NEW_DATA:
                self.data_count += 1
                if self.data_count % 2 == 0:
                    self.buf = ""
                else:
                    self.buf = self.buf[5:]
            elif code == LogTracker.RESTART:
                self.restart_count += 1
            elif code == LogTracker.ORIG_DATA:
                self.orig_data_count += 1
            elif code == LogTracker.NO_FILE:
                self.no_file_count += 1

    def setUp(self):
        if os.path.exists("XXX"):
            os.unlink("XXX")

    def tearDown(self):
        if os.path.exists("XXX"):
            os.unlink("XXX")

    @test
    def write_to_new_file(self):
        """Writing to a new file causes callbacks."""
        def seq():
            f = open("XXX", "w")
            yield False
            for n in range(5):
                f.write("Line %d\n" % n)
                f.flush()
                yield False
            yield None

        def stepper():
            v = next(steps)
            if v is None:
                self.control.poll.quit()

        tracker = self.Tracker(self.control.poll, "XXX")
        steps = seq()
        self.control.poll.addRepeatingTimeout(0.1, stepper)
        self.control.delay(1.0)
        failUnlessEqual(5, tracker.data_count)

    @test
    def partial_lines(self):
        """Partial lines are correctly handled.

        When a partial line is written to the file, the tracker should
        buffer the partial line until a new  line is seen.

        """
        def seq():
            f = open("XXX", "w")
            yield False
            for n in range(5):
                if n:
                    f.write("\n")
                f.write("Line %d" % n)
                f.flush()
                yield False

            f.write("\n")
            f.flush()
            yield None

        def stepper():
            v = next(steps)
            if v is None:
                self.control.poll.quit()

        tracker = self.Tracker(self.control.poll, "XXX")
        steps = seq()
        self.control.poll.addRepeatingTimeout(0.2, stepper)
        self.control.delay(1.4)
        failUnlessEqual(5, tracker.data_count)


if __name__ == "__main__":
    runModule()
