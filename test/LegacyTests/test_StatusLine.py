#!/usr/bin/env python
"""StatusLine unit tests"""

import sys
import os

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import TermEmu
from CleverSheep.TTY_Utils import RichTerm

# The module under test.
from CleverSheep.TTY_Utils import StatusLine


class Status(Suite):
    """Tests for the Status class.

    Currently these just mop up some coverage, but should be extended to
    do some proper tests.

    """

    def suiteTearDown(self):
        """
        Tidy up a tmp file created during these tests
        """
        os.remove("/tmp/emu.log")
        super(Status, self).suiteTearDown()

    @test
    def adding_a_spinner(self):
        """Add a spinner to a status line."""
        #> Create a status instance and add a spinner.
        st = StatusLine.Status()
        st.addSpinner()

        #> Build a line to force the spinner to start rotating.
        line = st.buildLine()
        failUnlessEqual("\\ ", line)

    @test
    def left_hand_fields(self):
        """We can define fields to appear on the left side of the line."""
        #> Create a status instance and add two fields on the left.
        st = StatusLine.Status()
        st.addLeftField("title", 5)
        st.addLeftField("name", 10)

        #> Set the two field values and then build the status line.
        st.setField("name", "Harold")
        st.setField("title", "Sir")
        line = st.buildLine()

        failUnlessEqual("Sir  |Harold    ", line)

    @test
    def right_hand_fields(self):
        """We can define fields to appear on the right side of the line."""
        #> Create a status instance and add two fields on the right. Also
        #> reduce the width to keep output cleaner.
        st = StatusLine.Status()
        st.addRightField("title", 5)
        st.addRightField("name", 10)
        st.w = 40

        #> Set the two field values and then build the status line.
        st.setField("name", "Harold")
        st.setField("title", "Sir")
        line = st.buildLine()

        expect = "                      Harold    |Sir  "
        failUnlessEqual(expect, line)

    @test
    def mixed_fields(self):
        """We can define fields to appear on both side of the line."""
        #> Create a status instance and add two fields on the right and left.
        #> Also reduce the width to keep output cleaner.
        st = StatusLine.Status()
        st.addLeftField("animal", 10)
        st.addLeftField("state", 8)
        st.addRightField("title", 5)
        st.addRightField("name", 10)
        st.w = 40

        #> Set the field values and then build the status line.
        st.setField("name", "Harold")
        st.setField("title", "Sir")
        st.setField("animal", "Parrot")
        st.setField("state", "dead!")

        #> Build a line and verify the fields have the correct values.
        line = st.buildLine()
        expect = "Parrot    |dead!     |Harold    |Sir  "
        failUnlessEqual(expect, line)

    @test
    def line_truncation(self):
        """When things are too long the line is truncated.

        Both sides are truncated from the middle.

        """
        #> Create a status instance and add two fields on the right and left.
        #> Also reduce the width to be slighly too small.
        st = StatusLine.Status()
        st.addLeftField("animal", 10)
        st.addLeftField("state", 8)
        st.addRightField("title", 5)
        st.addRightField("name", 10)
        st.w = 30

        #> Set the field values and then build the status line.
        st.setField("name", "Harold")
        st.setField("title", "Sir")
        st.setField("animal", "Parrot")
        st.setField("state", "dead!")

        #> Build a line and verify the fields have the correct values.
        line = st.buildLine()
        expect = "Parrot    |dea|Harold    |Sir"
        failUnlessEqual(expect, line)

    @test
    def severe_truncation(self):
        """Severe truncation occurs in a sensible manner.

        This is a situation where the left part is too big on its own.

        """
        #> Create a status instance and add two fields on the right and left.
        #> Also reduce the width to be seriously too small.
        st = StatusLine.Status()
        st.addLeftField("animal", 10)
        st.addLeftField("state", 8)
        st.addRightField("title", 5)
        st.addRightField("name", 10)
        st.w = 17

        #> Set the field values and then build the status line.
        st.setField("name", "Harold")
        st.setField("title", "Sir")
        st.setField("animal", "Parrot")
        st.setField("state", "dead!")

        #> Build a line and verify the fields have the correct values.
        line = st.buildLine()
        expect = "Parrot |   |Sir"
        failUnlessEqual(expect, line)

    @test
    def very_severe_truncation(self):
        """Very severe truncation occurs in a sensible manner.

        If the right side is too long to fit on its own then the left side
        is completely zapped.

        """
        #> Create a status instance and add two fields on the right and left.
        #> Also reduce the width to be seriously too small.
        st = StatusLine.Status()
        st.addLeftField("animal", 10)
        st.addLeftField("state", 8)
        st.addRightField("title", 5)
        st.addRightField("name", 10)
        st.w = 13

        #> Set the field values and then build the status line.
        st.setField("name", "Harold")
        st.setField("title", "Sir")
        st.setField("animal", "Parrot")
        st.setField("state", "dead!")

        #> Build a line and verify the fields have the correct values.
        line = st.buildLine()
        expect = "Parro| |Sir"
        failUnlessEqual(expect, line)

    @test
    def updating(self):
        """Update method writes to the terminal.

        If the right side is too long to fit on its own then the left side
        is completely zapped.

        """
        #> Create a status instance, but change its terminal to the test
        #> emulator. Also set the width to 80 characters.
        st = StatusLine.Status()
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        st.term = f
        st.w = 40

        #> Add two fields on the right and left. Also reduce the width to keep
        #> output cleaner.
        st.addLeftField("animal", 10)
        st.addLeftField("state", 8)
        st.addRightField("title", 5)
        st.addRightField("name", 10)

        #> Set the field values.
        st.setField("name", "Harold")
        st.setField("title", "Sir")
        st.setField("animal", "Parrot")
        st.setField("state", "dead!")

        #> Invoke the update method, this should write a single status
        #> line at the top of the terminal.
        line = st.update(force=True)
        print repr(str(tty))
        lines = [(i, l) for (i, l) in enumerate(str(tty).splitlines())
                    if l.strip()]
        expect = "Parrot    |dead!     |Harold    |Sir  "
        failUnlessEqual(1, len(lines))
        failUnlessEqual(0, lines[0][0])
        failUnlessEqual(expect, lines[0][1])

        #> Change a field and update again. The terminal's first line
        #> should update.
        st.setField("state", "alive!")
        line = st.update(force=True)
        lines = [(i, l) for (i, l) in enumerate(str(tty).splitlines())
                    if l.strip()]
        expect = "Parrot    |alive!    |Harold    |Sir  "
        failUnlessEqual(1, len(lines))
        failUnlessEqual(0, lines[0][0])
        failUnlessEqual(expect, lines[0][1])

    @test
    def update_interval(self):
        """Update normally does nothing until update interval expires.

        Norally the update method does nothing in order to avoid excessive
        terminal updates. The default update interval is 0.1 seconds, so this
        test does a quick update then one just after 0.1 seconds.

        """
        #> Create a status instance, but change its terminal to the test
        #> emulator. Also set the width to 80 characters.
        st = StatusLine.Status()
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        st.term = f
        st.w = 40

        #> Add two fields on the right and left. Also reduce the width to keep
        #> output cleaner.
        st.addLeftField("animal", 10)
        st.addLeftField("state", 8)
        st.addRightField("title", 5)
        st.addRightField("name", 10)

        #> Set the field values.
        st.setField("name", "Harold")
        st.setField("title", "Sir")
        st.setField("animal", "Parrot")
        st.setField("state", "dead!")

        #> Invoke the update method, nothing should happen (unless this is
        #> running on a ludicrously overloaded computer).
        line = st.update()
        lines = [(i, l) for (i, l) in enumerate(str(tty).splitlines())
                    if l.strip()]
        failUnlessEqual(0, len(lines))

        #> Wait 0.1 seconds and update again. The terminal's first line
        #> should now get written.
        self.control.delay(0.1)
        line = st.update()
        lines = [(i, l) for (i, l) in enumerate(str(tty).splitlines())
                    if l.strip()]
        expect = "Parrot    |dead!     |Harold    |Sir  "
        failUnlessEqual(1, len(lines))
        failUnlessEqual(0, lines[0][0])
        failUnlessEqual(expect, lines[0][1])

    @test
    def non_terminals_are_ignored(self):
        """When redirected to a non-terminal, the status line is inactive.

        """
        class F(object):
            def __init__(self):
                self.count = 0

            def isatty(self):
                return False

            def write(self, s):
                self.count += 1

            def flush(self):
                self.count += 1

        #> Create a status instance, but change its terminal to a dummy
        #> non-tty file like object.
        st = StatusLine.Status()
        f = F()
        st.term = f
        st.w = 40

        #> Add some fields and set values.
        st.addLeftField("animal", 10)
        st.addLeftField("state", 8)
        st.setField("animal", "Parrot")
        st.setField("state", "dead!")

        #> Force an update, which should write nothing to the output.
        #> line at the top of the terminal.
        line = st.update(force=True)
        failUnlessEqual(0, f.count)

        #> Do a kill, this should also do nothing.
        line = st.kill()
        failUnlessEqual(0, f.count)

    @test
    def line_kill(self):
        """The kill method erases the status line (using spaces).

        """
        #> Create a status instance, but change its terminal to the test
        #> emulator. Also set the width to 80 characters.
        st = StatusLine.Status()
        tty = TermEmu.TTY()
        f = RichTerm.RichTerminal(tty)
        st.term = f
        st.w = 40

        #> Add two fields on the right and left. Also reduce the width to keep
        #> output cleaner.
        st.addLeftField("animal", 10)
        st.addLeftField("state", 8)
        st.addRightField("title", 5)
        st.addRightField("name", 10)

        #> Set the field values.
        st.setField("name", "Harold")
        st.setField("title", "Sir")
        st.setField("animal", "Parrot")
        st.setField("state", "dead!")

        #> Invoke the update method with force, this should write a single
        #> status line at the top of the terminal.
        line = st.update(force=True)
        lines = [(i, l) for (i, l) in enumerate(str(tty).splitlines())
                    if l.strip()]
        expect = "Parrot    |dead!     |Harold    |Sir  "
        failUnlessEqual(1, len(lines))
        failUnlessEqual(0, lines[0][0])
        failUnlessEqual(expect, lines[0][1])

        #> Kill the status, this should overwtie with spaces and leave the
        #> cursor in column zero.
        st.kill()
        lines = [(i, l) for (i, l) in enumerate(str(tty).splitlines())
                    if l]
        expect = "                                       "
        failUnlessEqual(1, len(lines))
        failUnlessEqual(0, lines[0][0])
        failUnlessEqual(expect, lines[0][1])
        failUnlessEqual(0, tty.col)

        #> Write some text to the terminal and try killing again. Nothing
        #> should happen.
        tty.write("Hello Harold!")
        st.kill()
        lines = [(i, l) for (i, l) in enumerate(str(tty).splitlines())
                    if l]
        expect = "Hello Harold!                          "
        failUnlessEqual(1, len(lines))
        failUnlessEqual(0, lines[0][0])
        failUnlessEqual(expect, lines[0][1])


if __name__ == "__main__":
    runModule()
