#!/usr/bin/env python
"""Unit test for IOUtils.Redir"""

import sys

import CheckEnv

from CleverSheep.Prog import Files
from CleverSheep.Test.Tester import *
from CleverSheep.Test import ImpUtils

# The module under test.
from CleverSheep.IOUtils import Redir


class Test_BitSink(Suite):
    """Test for the BitSink class."""
    @test
    def main_operations(self):
        """Verify we can write, flush, close, etc.

        This is an odd test because we have few assertions. Nearly all the
        methods do nothing and return nothing.
        """
        f = Redir.BitSink()
        f.write("This")
        f.writelines(["will all", "disappear"])
        f.flush()
        failIf(f.isatty())
        f.close()


if __name__ == "__main__":
    runModule()
