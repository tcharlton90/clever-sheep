#!/usr/bin/env python
"""Check that the test environment is correct.

When we run the CleverSheep test, we need to be using the local CleverSheep
library, not any parts of an installed version. This is because we
otherwise end up not testing the local code or with duplicate modules
loaded, which causes test to fail in strange ways. (For example,
the ``reload`` built-in fails in the RichTerm tests.

"""


import os
import sys

projDir = os.path.abspath(os.path.join(os.path.dirname(__file__), "../.."))
cleverSheepDir=os.path.join(projDir, 'src')

if sys.path[0] != cleverSheepDir:
    sys.path[0:0] = [cleverSheepDir]


thisDir = os.path.abspath(os.path.dirname(__file__))

if thisDir not in sys.path:
    sys.path[0:0] = [thisDir]
