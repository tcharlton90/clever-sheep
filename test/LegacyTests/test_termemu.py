#!/usr/bin/env python
"""Terminal emulation units tests.

"""

import os
import sys

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test.DataMaker import literalText2Text
from CleverSheep.Test import TermEmu


class Base(Suite):
    """Base for all emulator tests.

    """
    def setUp(self):
        self.emu = TermEmu.TTY(width=15, height=5)

    def suiteTearDown(self):
        """
        Tidy up a tmp file created during these tests
        """
        os.remove("/tmp/emu.log")
        super(Base, self).suiteTearDown()

    def checkTerminalContent(self, row, col, litText, augmentMessage=None):
        if litText is not None:
            failUnlessEqualStrings(literalText2Text(litText), str(self.emu),
                                   augmentMessage=augmentMessage)
        failUnlessEqual(col, self.emu.col,
                        augmentMessage=augmentMessage)
        failUnlessEqual(row, self.emu.row,
                        augmentMessage=augmentMessage)


class PlainText(Base):
    """Plain test tests for the emulator.

    """

    @test
    def emulator_claims_to_be_a_tty(self):
        """The emulator always claims to be a TTY.

        """
        failUnlessEqual(True, self.emu.isatty())

    @test
    def emulator_supports_flush(self):
        """The flush as no specific behaviour, it is just required to support
        the file protocol.

        """
        self.emu.flush()

    @test
    def cursor_starts_at_0_0(self):
        """Initially the cursor position is (r=0, c=0).

        """
        failUnlessEqual(0, self.emu.col)
        failUnlessEqual(0, self.emu.row)

    @test
    def simple_line_of_text(self):
        """A simple line of text appears on the first line of the terminal.

        There is no trailing newline.

        """
        #> Write the line of text.
        self.emu.write("Hello!")

        #> The line should be all that appears when we dump as a string.
        self.checkTerminalContent(0, 6, "| Hello!")

    @test
    def default_colour_is_black_qhite(self):
        """The default colour of text is black on white background.

        """
        #> Write the line of text.
        self.emu.write("Hello!")

        #> The line should be all that appears when we dump as a string.
        block = self.emu.blocks().next()
        print "PAO", block
        failUnlessEqual(("Hello!", ("black", "white", "")), block)

    @test
    def two_simple_lines_of_text(self):
        """Two simple line of text appears on the first two lines.

        """
        #> Write the line of text.
        self.emu.write("Hello!")
        self.emu.write("\nHello!")

        #> The line should be all that appears when we dump as a string.
        self.checkTerminalContent(1, 6, """
            | Hello!
            | Hello!
        """)

    @test
    def long_lines_wrap(self):
        """A long line of text will wrap onto the next line.

        """
        #> Write a long line of text.
        self.emu.write("Hello brave world!")

        #> The line should be all that appears when we dump as a string.
        self.checkTerminalContent(1, 3, """
            | Hello brave wor
            | ld!
        """)

    @test
    def a_line_can_be_filled(self):
        """An entire line can be filled.

        But the cursor remains at the end of the line.

        """
        #> Write enough characters to almost fill the first row.
        self.emu.write("x" * 14)

        #> The line should be all that appears.
        self.checkTerminalContent(0, 14, """
            | xxxxxxxxxxxxxx
        """)

        #> Write another character. The line should be updated, but the cursor
        #> should not move.
        self.emu.write("x")
        self.checkTerminalContent(0, 14, """
            | xxxxxxxxxxxxxxx
        """)

        #> Write another character. It should appear at the beginning of the
        #> next line.
        self.emu.write("x")
        self.checkTerminalContent(1, 1, """
            | xxxxxxxxxxxxxxx
            | x
        """)

    @test
    def terminal_can_be_filled(self):
        """The entire terminal can be filled.

        So writing a character to the bottom-right cursor position does not
        scroll.

        """
        #> Write enough characters to just fill the entire screen.
        self.emu.write("x" * (5 * 15))

        #> The terminal should be filled with characters.
        self.checkTerminalContent(4, 14, """
            | xxxxxxxxxxxxxxx
            | xxxxxxxxxxxxxxx
            | xxxxxxxxxxxxxxx
            | xxxxxxxxxxxxxxx
            | xxxxxxxxxxxxxxx
        """)

    @test
    def terminal_should_scroll(self):
        """The top lines scroll with newlines.

        """
        #> Write enough lines to force one line to scroll off. Do not put a
        #> trailing newline on tha last line.
        for n in range(6):
            self.emu.write("Line %d" % n)
            if n < 5:
                self.emu.write("\n")

        #> Every line should contain text, but the 'Line 0' should have
        #> scrolled off.
        self.checkTerminalContent(4, 6, """
            | Line 1
            | Line 2
            | Line 3
            | Line 4
            | Line 5
        """)


class SimpleNonColourEscapeSequences(Base):
    """Basic handling of the non-colour escape sequences.

    """
    def setUp(self):
        super(SimpleNonColourEscapeSequences, self).setUp()
        self.resetTerm()
        self.expectedContent = """
            | Line 0
            | Line 1
            | Line 2
            | Line 3
        """

    def resetTerm(self):
        self.emu = TermEmu.TTY(width=15, height=5)
        for n in range(4):
            if n == 3:
                self.emu.write("Line %d" % n)
            else:
                self.emu.write("Line %d\n" % n)

    @test
    def handles_clearscreen(self):
        """The clear screen escape sequence is handled.

        """
        self.emu.write("\x1b[2J")
        self.checkTerminalContent(0, 0, "")

    @test
    def handles_home(self):
        """The cursor home sequence should be handled.

        """
        self.emu.write("\x1b[H")
        self.checkTerminalContent(0, 0, self.expectedContent)

    @test
    def handles_cursor_position_to_home(self):
        """The cursor positioning sequence for 1, 1 should be handled.

        """
        self.emu.write("\x1b[1;1H")
        self.checkTerminalContent(0, 0, self.expectedContent)

    @test
    def ignore_partial_position_sequences(self):
        """A partial cursor position sequence should be ignored.

        """
        def err(msg):
            return "%s\nDid not ignore sequence %r" % (msg, seq)

        for part in ("1;", ";1", ";", "1", "99999"):
            seq = chr(27) + "[%sH" % part
            self.emu.write(seq)
            self.checkTerminalContent(3, 6, self.expectedContent,
                                      augmentMessage=err)

    @test
    def ignore_invalid_position_args(self):
        """An invalid argument for a cursor position sequence should be
        ignored.

        Invalid arguments are illegal regardless of the terminal's dimensions.

        """
        def err(msg):
            return "%s\nDid not ignore sequence %r" % (msg, seq)

        for part in ("0;1", "1;0"):
            seq = chr(27) + "[%sH" % part
            self.resetTerm()
            self.emu.write(seq)
            self.checkTerminalContent(3, 6, None, augmentMessage=err)
        for part, offset in (("-1;1", 5), ("1;-1", 3), ("x;1", 4),
                             ("1;x", 2)):
            seq = chr(27) + "[%sH" % part
            self.resetTerm()
            self.emu.write(seq)
            self.checkTerminalContent(3, 6 + offset, None, augmentMessage=err)

    @test
    def handles_cleareol(self):
        """The clear end of line escape sequence is handled.

        """
        self.emu.write("\x1b[1;3H")
        self.emu.write("\x1b[0K")
        self.checkTerminalContent(0, 2, """
            | Li
            | Line 1
            | Line 2
            | Line 3
        """)

    @test
    def handles_cleareol_short_form(self):
        """The short form clear end of line escape sequence is handled.

        """
        self.emu.write("\x1b[1;3H")
        self.emu.write("\x1b[K")
        self.checkTerminalContent(0, 2, """
            | Li
            | Line 1
            | Line 2
            | Line 3
        """)

    @test
    def handles_clearsol_part_left(self):
        """The clear to start of line escape sequence is handled.

        """
        self.emu.write("\x1b[1;3H")
        self.emu.write("\x1b[1K")
        self.checkTerminalContent(0, 2, """
            |    e 0
            | Line 1
            | Line 2
            | Line 3
        """)

    @test
    def handles_clearsol_none_left(self):
        """The clear to start of line empties cells if no text is left.

        """
        self.emu.write("\x1b[1;6H")
        self.emu.write("\x1b[1K")
        self.checkTerminalContent(0, 5, """
            |
            | Line 1
            | Line 2
            | Line 3
        """)

    @test
    def handles_clearline(self):
        """The clear line escape sequence is handled.

        """
        self.emu.write("\x1b[1;3H")
        self.emu.write("\x1b[2K")
        self.checkTerminalContent(0, 2, """
            |
            | Line 1
            | Line 2
            | Line 3
        """)

    @test
    def handles_cleareos(self):
        """The clear end of screen sequence is handled.

        """
        self.emu.write("\x1b[2;3H")
        self.emu.write("\x1b[0J")
        self.checkTerminalContent(1, 2, """
            | Line 0
            | Li
        """)

    @test
    def handles_cleareos_short_form(self):
        """The short form clear end of line screen sequence is handled.

        """
        self.emu.write("\x1b[2;3H")
        self.emu.write("\x1b[J")
        self.checkTerminalContent(1, 2, """
            | Line 0
            | Li
        """)

    @test
    def handles_clearsos(self):
        """The clear start of line screen sequence is handled.

        """
        self.emu.write("\x1b[2;3H")
        self.emu.write("\x1b[1J")
        self.checkTerminalContent(1, 2, """
            |
            |    e 1
            | Line 2
            | Line 3
        """)

    @test
    def ignore_designated_hard_char_set(self):
        """Each designed hard character set sequence is ignored.

        Also any sequence like the DHCS sequence is ignore. The DHCS sequence
        has the form ``ESC [+()*] {final}``, where the ``{final}`` character is
        one of a defined set. In practice any character is accepted for
        ``{final}``.

        """
        def err(msg):
            return "%s\nDid not ignore sequence %r" % (msg, seq)

        for c in "()*+":
            for f in [chr(x) for x in range(127)]:
                seq = "\x1b%s%s" % (c, f)
                self.emu.write(seq)
                self.checkTerminalContent(3, 6, self.expectedContent,
                                          augmentMessage=err)


class NonColourEscapeSequences(Base):
    """Handling of the non-colour escape sequences.

    """

    @test
    def can_position_past_eol(self):
        """We can position past the end a line and write text.

        The skipped cells are filled with spaces.

        """
        self.emu.write("\x1b[1;6H")
        self.emu.write("hello")
        self.checkTerminalContent(0, 10, "|      hello")

    @test
    def can_position_past_last_line(self):
        """We can position past the last line with content.

        """
        self.emu.write("\x1b[3;1H")
        self.emu.write("hello")
        self.checkTerminalContent(2, 5, """
            |
            |
            | hello
        """)


class UsingCellAttributes(Base):
    """Basic handling of the cell attribute escape sequences.

    These allow the foreground and background colours to be set and also things
    like bold and underline.

    """
    @test
    def set_foreground_colour(self):
        """Set foreground colour using escape sequences.

        """
        self.emu.write("\x1b[31m")
        self.emu.write("Hello")
        self.emu.write("\x1b[32m")
        self.emu.write(" World!")

        block = list(self.emu.blocks())
        failUnlessEqual(("Hello", ("red", "white", "")), block[0])
        failUnlessEqual((" World!", ("green", "white", "")), block[1])

    @test
    def set_background_colour(self):
        """Set background colour using escape sequences.

        """
        self.emu.write("\x1b[41m")
        self.emu.write("Hello")
        self.emu.write("\x1b[42m")
        self.emu.write(" World!")

        block = list(self.emu.blocks())
        failUnlessEqual(("Hello", ("black", "red", "")), block[0])
        failUnlessEqual((" World!", ("black", "green", "")), block[1])

    @test
    def set_bold(self):
        """Set the bold attribute.

        """
        self.emu.write("\x1b[1m")
        self.emu.write("Hello")

        block = list(self.emu.blocks())
        failUnlessEqual(("Hello", ("black", "white", "b")), block[0])

    @test
    def set_underline(self):
        """Set the underline attribute.

        """
        self.emu.write("\x1b[4m")
        self.emu.write("Hello")

        block = list(self.emu.blocks())
        failUnlessEqual(("Hello", ("black", "white", "u")), block[0])

    @test
    def set_blink(self):
        """Set the blinking attribute.

        """
        self.emu.write("\x1b[5m")
        self.emu.write("Hello")

        block = list(self.emu.blocks())
        failUnlessEqual(("Hello", ("black", "white", "B")), block[0])

    @test
    def set_reverse(self):
        """Set the reverse attribute.

        """
        self.emu.write("\x1b[7m")
        self.emu.write("Hello")

        block = list(self.emu.blocks())
        failUnlessEqual(("Hello", ("black", "white", "r")), block[0])

    @test
    def reset_attributes(self):
        """All attributes can be cleared.

        """
        self.emu.write("\x1b[7m")
        self.emu.write("Hello")
        self.emu.write("\x1b[0m")
        self.emu.write("Hello")

        block = list(self.emu.blocks())
        failUnlessEqual(("Hello", ("black", "white", "r")), block[0])
        failUnlessEqual(("Hello", ("black", "white", "")), block[1])

    @test
    def reset_attributes_short_form(self):
        """All attributes can be cleared, using short form sequence.

        """
        self.emu.write("\x1b[7m")
        self.emu.write("Hello")
        self.emu.write("\x1b[m")
        self.emu.write("Hello")

        block = list(self.emu.blocks())
        failUnlessEqual(("Hello", ("black", "white", "r")), block[0])
        failUnlessEqual(("Hello", ("black", "white", "")), block[1])


class SimpleControlCodes(Base):
    """Handling of control codes.

    """
    @test
    def cr_moves_to_start_of_line(self):
        """The CR code moves to the start of the line.

        """
        self.emu.write("Hello\n")
        self.emu.write("Hello\r")
        self.checkTerminalContent(1, 0, """
            | Hello
            | Hello
        """)


class ComplexSequence(Base):
    """Handling of complex sequences such as scrolling regions.

    """
    def setUp(self):
        super(ComplexSequence, self).setUp()
        self.resetTerm()
        self.expectedContent = """
            | Line 0
            | Line 1
            | Line 2
            | Line 3
        """

    def resetTerm(self):
        self.emu = TermEmu.TTY(width=15, height=5)
        for n in range(4):
            if n == 3:
                self.emu.write("Line %d" % n)
            else:
                self.emu.write("Line %d\n" % n)

    @test
    def scrolling_region(self):
        """A scrolling region can be established.

        """
        #> Set scrolling to lines 2 and 3. Move to line 3 and add a line.
        self.emu.write("\x1b[2;3r")
        self.emu.write("\x1b[3;1H")

        #> Write a new line.
        self.emu.write("\nA new line")
        self.checkTerminalContent(2, 10, """
            | Line 0
            | Line 2
            | A new line
            | Line 3
        """)

    @test
    def bad_scrolling_region_is_ignored(self):
        """A malformed scrolling region is ignored.

        """
        # TODO: Not a comprehensive test.
        #> Try to set a bad scrolling region.
        self.emu.write("\x1b[2;r")
        self.emu.write("\x1b[3;1H")

        #> Write a new line.
        self.emu.write("\nA new line")
        self.checkTerminalContent(3, 10, """
            | Line 0
            | Line 1
            | Line 2
            | A new line
        """)


if __name__ == "__main__":
    runModule()

