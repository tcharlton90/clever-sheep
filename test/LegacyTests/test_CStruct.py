#!/usr/bin/env python
"""Struct unit tests"""

import CheckEnv

from CleverSheep.Sys import Platform
if Platform.platformType == "windows":
    from CleverSheep.Test.Tester import Collection
    raise Collection.Unsupported

import sys
import struct

from CleverSheep.Test import DataMaker
from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester
from array import array
try:
    import ctypes
    #del ctypes
    newStyle = True
except ImportError:
    newStyle = False


# The module under test.
try:
    from CleverSheep.Prog import CStruct as _Struct
except ImportError:
    raise Tester.Unsupported

Struct = _Struct.Struct
Type = _Struct.Type
Format = _Struct.Format
Union = _Struct.Union

# The ctypes module maps effectively provides aliases some types. So, for
# example, 'c_int' might effectively be an alias for 'c_long'. For testing
# purposes we need to know how such things get alised, which is what the
# following code does.
sizemap = {}
for t in ("long", "int"):
    a = "c_%s" % t
    exec("inst = ctypes.c_%s()" % t)
    mType = inst.__class__.__name__
    sizemap[a] = mType
    sizemap["p_%s" % t] = " " * (6 - len(mType))


# We also need some tests to be conditional depending on whether this is a 32
# or 64 bit platform.
if struct.calcsize("l") == 8:
    x64 = True
else:
    x64 = False


class Test_Struct(Suite):
    """Tests for the Struct module"""
    @test
    def simple_padded_struct(self):
        """Test a simple struct, with padding."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        a = A()
        failUnlessEqual(array("B", [0]*4), a._meta.bytes)
        a.a = 1
        a.b = 0x1234
        failUnlessEqual(array("B", [0x1, 0, 0x34, 0x12]), a._meta.bytes)
        failUnlessEqual(["01", "00", "34", "12"], a._meta.hex)
        failUnlessEqual(1, a.a)
        failUnlessEqual(0x1234, a.b)

    @test
    def simple_padded_struct_be(self):
        """Test with a simple struct, with padding, BigEndian."""
        class A(Struct):
            _byteOrder = Format.BigEndian
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        a = A()
        failUnlessEqual(a._meta.bytes, array("B", [0]*4))
        a.a = 1
        a.b = 0x1234
        failUnlessEqual(a._meta.bytes, array("B", [0x1, 0, 0x12, 0x34]))
        failUnlessEqual(a._meta.hex, ["01", "00", "12", "34"])
        failUnlessEqual(a.a, 1)
        failUnlessEqual(a.b, 0x1234)

        class S(Struct):
            a = Type.UnsignedByte[5]

        a = S()
        for i, c in enumerate("Hello"):
            a.a[i] = ord(c)
        failUnlessEqual(a._meta.s, "Hello")

    @test
    def padded_3_byte_array_struct(self):
        """Test a simple struct, with padding."""
        class A(Struct):
            a = Type.UnsignedByte[3]
            b = Type.UnsignedShort

        a = A()
        failUnlessEqual(array("B", [0]*6), a._meta.bytes)

    @test
    def init_using_array(self):
        """Test initialising/setting using string or array."""
        class S(Struct):
            a = Type.UnsignedByte[5]

        a = S("Hello")
        failUnlessEqual("Hello", a._meta.s)

        v = array('B', "Hello")
        a = S(v)
        failUnlessEqual("Hello", a._meta.s)

        a = S([ord(c) for c in "Hello"])
        failUnlessEqual("Hello", a._meta.s)

        a = S(tuple([ord(c) for c in "Hello"]))
        failUnlessEqual("Hello", a._meta.s)

        a = S("Hell")
        if newStyle:
            failUnlessEqual("Hell\0", a._meta.s)
        else:
            failUnlessEqual("Hell", a._meta.s)

        a._meta.setBytes("Hi", trim=1)
        if newStyle:
            failUnlessEqual("Hi\0\0\0", a._meta.s)
        else:
            failUnlessEqual("Hi", a._meta.s)

        a._meta.setBytes("Boffo")
        failUnlessEqual("Boffo", a._meta.s)

        a._meta.setBytes("Hell")
        failUnlessEqual("Hello", a._meta.s)


    @test
    def overlong_raw_data(self):
        """Test code catches over-long raw data."""
        class S(Struct):
            a = Type.UnsignedByte[5]

        failUnlessRaises(ValueError, lambda: S("Hello!"))


    @test
    def init_using_kwargs(self):
        """Test initialising using kwargs"""
        class S(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedByte
            c = Type.UnsignedByte
            d = Type.UnsignedByte
            e = Type.UnsignedByte

        a = S(a=ord("H"), b=ord("e"), e=ord("o"), c=ord("l"), d=ord("l"))
        failUnlessEqual(a._meta.s, "Hello")

        a = S("He", e=ord("o"), c=ord("l"), d=ord("l"))
        failUnlessEqual(a._meta.s, "Hello")

    @test
    def simple_nested_struct(self):
        """A simple nested struct."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        a = X()
        failUnlessEqual(a._meta.bytes, array("B", [0]*8))

        a.a.a = 1
        a.a.b = 0x1234
        a.b.a = 9
        a.b.b = 0x5678
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x1, 0, 0x34, 0x12, 0x9, 0, 0x78, 0x56]))

        # failUnlessEqual(str(a.a), "Proxy:0")
        # failUnlessEqual(str(a.b), "Proxy:4")

    @test
    def direct_access_to_children(self):
        """Children should be accessible using _meta.children."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        a = X()
        failUnlessEqual(a._meta.bytes, array("B", [0]*8))

        a.a.a = 1
        a.a.b = 0x1234
        a.b.a = 9
        a.b.b = 0x5678
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x1, 0, 0x34, 0x12, 0x9, 0, 0x78, 0x56]))

        # failUnlessEqual(str(a.a), "Proxy:0")
        # failUnlessEqual(str(a.b), "Proxy:4")

        name, obj = a._meta.children[0]
        failUnlessEqual("a", name)
        # failUnlessEqual(str(obj), "Proxy:0")

        name, obj = a._meta.children[1]
        failUnlessEqual("b", name)
        # failUnlessEqual(str(obj), "Proxy:4")

    @test
    def core_meta_information(self):
        """Check the core _meta information is correctly reported."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        x = X()
        failUnlessEqual("X", x._meta.name)

    @test
    def getting_memory_address(self):
        """Check we can get the underlying byte array's address.
        
        We cannot actually verify that the address is valid because
        we are relying on the underlying ``array`` module's behavour.
        So this does little more than exercise the code.
        """
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        a = A()
        addr = a._meta.addr()
        failIfEqual(0, addr)

    @test
    def description(self):
        """Check that description of structs works."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A[2]
            c = Type.Int[:3]
            d = Type.Int[:5]
            f = Type.UnsignedShort

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | 0    : struct X
        | 0    :   struct A             a
        | 0    :     c_ubyte              a
        | 2    :     c_ushort             b
        | 4    :   struct A[2]          b
        | 4    :     struct A
        | 4    :       c_ubyte              a
        | 6    :       c_ushort             b
        | 12   :   %(c_int)s;0:3           %(p_int)sc
        | 12   :   %(c_int)s;3:5           %(p_int)sd
        | 16   :   c_ushort             f
        |
        """ % sizemap), X._cmeta.describe())

        x = X()
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | 0    : struct X
        | 0    :   struct A             a
        | 0    :     c_ubyte              a
        | 2    :     c_ushort             b
        | 4    :   struct A[2]          b
        | 4    :     struct A
        | 4    :       c_ubyte              a
        | 6    :       c_ushort             b
        | 12   :   %(c_int)s;0:3           %(p_int)sc
        | 12   :   %(c_int)s;3:5           %(p_int)sd
        | 16   :   c_ushort             f
        |
        """ % sizemap), x._meta.describe())

        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort[5]

        a = A()
        failUnlessEqualStrings(DataMaker.literalText2Text("""
       | 0    : struct A
       | 0    :   c_ubyte              a
       | 2    :   c_ushort[5]          b
       | 2    :     int
       |
        """), a._meta.describe())

    @test
    def dumpingStringsArrays(self):
        """Check that dumping of string arrays works OK"""
        class A(Struct):
            a = Type.String[1]
            b = Type.String[2][10]

        a = A()
        a.a = "1"
        a.b[0] = "hello"
        a.b[1] = "1234567890"
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | 0    : struct A
        | 0    :   c_char[1]            a = '1'\n"
        | 1    :   c_char[2, 10]        b
        | 1    :     [0] 'hello'\n"
        | 11   :     [1] '1234567890'\n"
        |
        """), a._meta.dump())

    @test
    def dumpingUnionArrays(self):
        """Check that dumping of arrays of unions works OK"""
        class U(Union):
            x = Type.Int
            y = Type.Int
        class A(Struct):
            a = Type.U[2]

        a = A()
        a.a[0].x = 1
        a.a[0].x = 2
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | 0    : struct A
        | 0    :   union U[2]           a
        | 0    :     [0]
        | 0    :       %(c_int)s               %(p_int)sx
        | 0    :       %(c_int)s               %(p_int)sy
        | 4    :     [1]
        | 4    :       %(c_int)s               %(p_int)sx
        | 4    :       %(c_int)s               %(p_int)sy
        |
        """ % sizemap), a._meta.dump())

    @test
    def dumping(self):
        """Check that dumping an object's detailed content works."""
        class A(Struct):
            a = Type.String[1]
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A[2]
            c = Type.Int[:3]
            d = Type.Int[:5]
            e = Type.Int[3]

        x = X()
        x.a.a = "a"
        x.a.b = 2
        x.b.a = "b"
        x.b.b = 0xffff
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | 0    : struct X
        | 0    :   struct A             a
        | 0    :     c_char[1]            a = 'a'\n"
        | 2    :     c_ushort             b = 2
        | 4    :   struct A[2]          b
        | 4    :     [0]
        | 4    :       c_char[1]            a = ''\n"
        | 6    :       c_ushort             b = 0
        | 8    :     [1]
        | 6    :       c_char[1]            a = ''\n"
        | 8    :       c_ushort             b = 0
        | 12   :   %(c_int)s;0:3           %(p_int)sc = 0
        | 12   :   %(c_int)s;3:5           %(p_int)sd = 0
        | 16   :   %(c_int)s[3]            %(p_int)se
        | 16   :     [0] = 0
        | 20   :     [1] = 0
        | 24   :     [2] = 0
        |
        """ % sizemap), x._meta.dump())

        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort[5]

        a = A()
        a.a = 1
        a.b[0] = 2
        a.b[1] = 0x80
        a.b[2] = 0x8000
        a.b[3] = 0x8001
        a.b[4] = 0xffff
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | 0    : struct A
        | 0    :   c_ubyte              a = 1
        | 2    :   c_ushort[5]          b
        | 2    :     [0] = 2
        | 4    :     [1] = 128
        | 6    :     [2] = 32768
        | 8    :     [3] = 32769
        | 10   :     [4] = 65535
        |
        """), a._meta.dump())

        class U(Union):
            a = Type.UnsignedLong
            b = Type.UnsignedShort[5]
            c = Type.UnsignedLong[:3]
            d = Type.UnsignedLong[:5]
            e = Type.UnsignedShort

        class A(Struct):
            hdr = Type.Int
            body = Type.U

        m = A()
        a = m.body
        a.a = 0xffffffff
        a.b[2] = 3
        a.b[3] = 4
        a.b[4] = 5

        A._selectors_ = {"body": lambda o: "a"}
        if x64:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct A
            | 0    :   c_int                hdr = 0
            | 8    :   union U              body
            | 8    :     c_ulong              a = 1125917086711807L
            |
            """), m._meta.dump())
        else:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct A
            | 0    :   c_long               hdr = 0
            | 4    :   union U              body
            | 4    :     c_ulong              a = 4294967295L
            |
            """), m._meta.dump())

        A._selectors_ = {"body": lambda o: "b"}
        if x64:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct A
            | 0    :   c_int                hdr = 0
            | 8    :   union U              body
            | 8    :     c_ushort[5]          b
            | 8    :       [0] = 65535
            | 10   :       [1] = 65535
            | 12   :       [2] = 3
            | 14   :       [3] = 4
            | 16   :       [4] = 5
            |
            """), m._meta.dump())
        else:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct A
            | 0    :   c_long               hdr = 0
            | 4    :   union U              body
            | 4    :     c_ushort[5]          b
            | 4    :       [0] = 65535
            | 6    :       [1] = 65535
            | 8    :       [2] = 3
            | 10   :       [3] = 4
            | 12   :       [4] = 5
            |
            """), m._meta.dump())

        A._selectors_ = {"body": lambda o: "c"}
        if x64:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct A
            | 0    :   c_int                hdr = 0
            | 8    :   union U              body
            | 8    :     c_ulong;0:3          c = 7L
            |
            """), m._meta.dump())
        else:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct A
            | 0    :   c_long               hdr = 0
            | 4    :   union U              body
            | 4    :     c_ulong;0:3          c = 7L
            |
            """), m._meta.dump())

        if 0:
            # There appears to be a problem with ctypes. Bitfields
            # within unions do not behave as expected.
            A._selectors_ = {"body": lambda o: "d"}
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct A
            | 0    :   %(c_long)s               %(p_long)shdr = 0
            | 4    :   union U              body
            | 4    :     c_ulong;3:5          d = 7L
            |
            """ % sizemap), m._meta.dump())

            A._selectors_ = {"body": lambda o: "e"}
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct A
            | 0    :   %(c_long)s               %(p_long)shdr = 0
            | 4    :   union U              body
            | 4    :     c_ushort             e = 65535
            |
            """ % sizemap), m._meta.dump())


    @test
    def meta_sizes(self):
        """Check that _meta sizes are correctly reported."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        a = A()
        a.a = 1
        a.b = 2
        x = X(a._meta.s)
        failUnlessEqual(8, x._meta.size)
        
        failUnlessEqual(1, x.a.a)
        failUnlessEqual(2, x.a.b)
        failUnlessEqual(0, x.b.a)
        failUnlessEqual(0, x.b.b)

    # Disabled until I figure out exactly how this feature should work.
    #@test
    def finding_objects(self):
        """Check that the find-by-name feature works."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        x = X()
        x.a.a = 1
        x.a.b = 2
        x.b.a = 0x80
        x.b.b = 0xffff
        l = x._meta.find("a")
        failUnlessEqual(3, len(l))
        failUnlessEqual("Proxy:0", str(l[0]))
        failUnlessEqual(1, l[1])
        failUnlessEqual(128, l[2])

        # Find for simple type returns None
        failUnless(x.a.a._meta.find("b") is None)

        # Proxy arrays are handled OK by find (i.e. ignored).
        class A(Struct):
            a = Type.UnsignedByte[5]
            b = Type.UnsignedShort

        a= A()
        l = a._meta.find("b")
        failUnlessEqual(1, len(l))

        # Proxy arrays found handled OK by find.
        class A(Struct):
            b = Type.UnsignedShort
            a = Type.UnsignedByte[5]

        a= A()
        l = a._meta.find("a")
        failUnlessEqual(1, len(l))

    @test
    def access_undefined_members(self):
        """A access to undefined members."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        class C(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class D(Struct):
            a = Type.C


        a = X()
        failUnlessRaises(AttributeError, lambda: a.c)
        failUnlessRaises(AttributeError, lambda: a.b.fred)

        def doSet():
            a.b.q = 3
        # TODO: Hmm. This is odd. The ctypes structures allow me to do:
        #     a.b.q = 9999
        # but there is no effect. So if I then try:
        #     x = a.b.q
        # I get attribute error. So, for now the CStruct module adds behaviour to
        # make the assignment raise an attribute error.
        failUnlessRaises(AttributeError, doSet)

        def doSet():
            a.b = 3
        failUnlessRaises(TypeError, doSet)

        def doSet():
            c = C()
            a.b = c
        failUnlessRaises(TypeError, doSet)

        def doSet():
            d = D()
            a.b = d.a
        failUnlessRaises(TypeError, doSet)


    # TODO: It seems that ctypes does not support mixed endienness within a
    #       struct.
    #@test
    def mixed_endian_nested_struct(self):
        """A nested struct, mixed endian"""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class B(Struct):
            _byteOrder = Format.BigEndian
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class M(Struct):
            _byteOrder = Format.BigEndian
            a = Type.A
            b = Type.B
            c = Type.UnsignedLong

        a = M()
        failUnlessEqual(a._meta.bytes, array("B", [0]*12))
        a.a.a = 1
        a.a.b = 0x1234
        a.b.a = 9
        a.b.b = 0x5678
        a.c = 0x12345678

        failUnlessEqual(a._meta.bytes, array("B",
                    [0x01, 0x00, 0x34, 0x12, 0x09, 0x00, 0x56, 0x78,
                     0x12, 0x34, 0x56, 0x78]))

        class M(Struct):
            _byteOrder = Format.LittleEndian
            a = Type.A
            b = Type.B
            c = Type.UnsignedLong

        a = M()
        a.a.a = 1
        a.a.b = 0x1234
        a.b.a = 9
        a.b.b = 0x5678
        a.c = 0x12345678

        failUnlessEqual(a._meta.bytes, array("B",
                    [0x01, 0x00, 0x34, 0x12, 0x09, 0x00, 0x56, 0x78,
                     0x78, 0x56, 0x34, 0x12]))

    @test
    def access_invalid_meta_data(self):
        """Try accessing non-existant meta-data."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class B(Struct):
            x = Type.A

        a = B()
        failUnlessRaises(AttributeError, lambda: a._meta.xxx )
        failUnlessRaises(AttributeError, lambda: a.x._meta.xxx )

    @test
    def doubley_nested_struct(self):
        """A doubly nested struct."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class M(Struct):
            a = Type.A
            b = Type.A

        class X(Struct):
            a = Type.M
            b = Type.M

        a = X()
        failUnlessEqual(a._meta.bytes, array("B", [0]*16))

        a.a.a.a = 1
        a.a.a.b = 0x1234
        a.a.b.a = 2
        a.a.b.b = 0x5678
        a.b.a.a = 3
        a.b.a.b = 0x9abc
        a.b.b.a = 4
        a.b.b.b = 0xdef0
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x01, 0, 0x34, 0x12, 0x02, 0, 0x78, 0x56,
                            0x03, 0, 0xbc, 0x9a, 0x04, 0, 0xf0, 0xde]))

        failUnlessEqual(a.a.a.a, 1)
        failUnlessEqual(a.a.a.b, 0x1234)
        failUnlessEqual(a.a.b.a, 2)
        failUnlessEqual(a.a.b.b, 0x5678)
        failUnlessEqual(a.b.a.a, 3)
        failUnlessEqual(a.b.a.b, 0x9abc)
        failUnlessEqual(a.b.b.a, 4)
        failUnlessEqual(a.b.b.b, 0xdef0)


    @test
    def correct_proxy_behaviour(self):
        """Check proxies behave correctly."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class M(Struct):
            a = Type.A
            b = Type.A

        class X(Struct):
            a = Type.M
            b = Type.M

        a = X()
        failUnlessEqual(a._meta.bytes, array("B", [0]*16))

        temp = a.a               # Temp is a proxy
        temp.a.a = 1
        temp.a.b = 0x1234
        temp.b.a = 2
        temp.b.b = 0x5678
        a.a = temp

        temp = a.b               # Temp is a proxy
        t2 = A()
        t2.a = 3
        t2.b = 0x9abc
        temp.a = t2
        t2.a = 4
        t2.b = 0xdef0
        temp.b = t2
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x01, 0, 0x34, 0x12, 0x02, 0, 0x78, 0x56,
                            0x03, 0, 0xbc, 0x9a, 0x04, 0, 0xf0, 0xde]))

        temp = a.a
        failUnlessEqual(temp.a.a, 1)
        failUnlessEqual(temp.a.b, 0x1234)
        failUnlessEqual(temp.b.a, 2)
        failUnlessEqual(temp.b.b, 0x5678)

        temp = M()
        temp.a.a = 5
        temp.a.b = 0x1122
        temp.b.a = 6
        temp.b.b = 0x3344
        a.a = temp
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x05, 0, 0x22, 0x11, 0x06, 0, 0x44, 0x33,
                            0x03, 0, 0xbc, 0x9a, 0x04, 0, 0xf0, 0xde]))

        t1 = a.a.a
        t2 = a.b
        t2.a = t1
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x05, 0, 0x22, 0x11, 0x06, 0, 0x44, 0x33,
                            0x05, 0, 0x22, 0x11, 0x04, 0, 0xf0, 0xde]))


    @test
    def arrays_within_structs(self):
        """Check arrays within structs."""
        class A(Struct):
            a = Type.UnsignedByte[3]
            b = Type.UnsignedShort

        class M(Struct):
            a = Type.A[4]

        a = A()
        failUnlessEqual(a._meta.bytes, array("B", [0, 0, 0, 0, 0, 0]))
        a.a[0] = 1
        a.a[1] = 2
        a.a[2] = 3
        failUnlessEqual(a._meta.bytes, array("B", [1, 2, 3, 0, 0, 0]))
        failUnlessEqual(a.a[0], 1)
        failUnlessEqual(a.a[1], 2)
        failUnlessEqual(a.a[2], 3)

        a.a[-3] = 4
        a.a[-2] = 5
        a.a[-1] = 6
        failUnlessEqual(a._meta.bytes, array("B", [4, 5, 6, 0, 0, 0]))
        failUnlessEqual(a.a[-3], 4)
        failUnlessEqual(a.a[-2], 5)
        failUnlessEqual(a.a[-1], 6)

        m = M()
        failUnlessEqual(m._meta.bytes, array("B", [
                0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0,
                ]))

        for i in range(4):
            m.a[i].b = 0x1234 + i
            m.a[i].a[0] = 1 + i * 4
            m.a[i].a[1] = 2 + i * 4
            m.a[i].a[2] = 3 + i * 4 
        failUnlessEqual(m._meta.bytes, array("B", [
                1, 2, 3, 0, 0x34, 0x12,
                5, 6, 7, 0, 0x35, 0x12,
                9, 10, 11, 0, 0x36, 0x12,
                13, 14, 15, 0, 0x37, 0x12,
                ]))


    @test
    def array_indices_get_checked(self):
        """Check that array indices are checked."""
        class A(Struct):
            a = Type.UnsignedByte[3]
            b = Type.UnsignedShort

        class M(Struct):
            a = Type.A[4]

        a = A()
        def doIndex(i):
            return a.a[i]

        a.a[0] = 1
        a.a[1] = 2
        a.a[2] = 3
        failUnlessEqual(a.a[0], 1)
        failUnlessEqual(a.a[-3], 1)
        failUnlessEqual(a.a[1], 2)
        failUnlessEqual(a.a[-2], 2)
        failUnlessEqual(a.a[2], 3)
        failUnlessEqual(a.a[-1], 3)
        failUnlessRaises(IndexError, doIndex, 3)
        failUnlessRaises(IndexError, doIndex, -4)


    @test
    def basic_types_not_redifinable(self):
        """Check that you cannot redefine basic types."""
        def f():
            class Int(Struct):
                a = 3
            return Int

        failUnlessRaises(TypeError, f)

    @test
    def multi_dim_arrays(self):
        """Check multi-dimensional arrays."""
        class A(Struct):
            a = Type.UnsignedByte[2][3][4]
            b = Type.UnsignedShort

        a = A()
        a.b = 0xeeee
        seq = list(enumerate([(i, j, k)
            for i in range(2) for j in range(3) for k in range(4)]))

        for n, (i, j, k) in seq:
            a.a[i][j][k] = n

        for n, (i, j, k) in seq:
            failUnlessEqual(a.a[i][j][k], n)

        failUnlessEqual(a._meta.bytes, array('B', range(len(seq)) + [0xee, 0xee]))

    @test
    def unions(self):
        """Test unions."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedByte
            c = Type.UnsignedByte
            d = Type.UnsignedByte

        class B(Struct):
            a = Type.UnsignedShort
            b = Type.UnsignedShort

        class U(Union):
            a = Type.A
            b = Type.B
            c = Type.UnsignedLong

        u = U()
        u.a.a = 1;
        u.a.b = 2;
        u.a.c = 3;
        u.a.d = 4;
        if x64:
            failUnlessEqual(u._meta.bytes, array("B",
                [0x01, 0x02, 0x03, 0x04, 0, 0, 0, 0]))
            failUnlessEqual(u._meta.hex,
                ["01", "02", "03", "04", "00", "00", "00", "00"])
        else:
            failUnlessEqual(u._meta.bytes, array("B", [0x01, 0x02, 0x03, 0x04]))
            failUnlessEqual(u._meta.hex, ["01", "02", "03", "04"])
        failUnlessEqual(u.b.a, 0x201)
        failUnlessEqual(u.b.b, 0x403)
        failUnlessEqual(u.c,   0x4030201)

        u.c = 0x01020304
        failUnlessEqual(u.c,   0x01020304)
        failUnlessEqual(u.b.a, 0x304)
        failUnlessEqual(u.b.b, 0x102)
        if x64:
            failUnlessEqual(u._meta.bytes,
                    array("B", [0x04, 0x03, 0x02, 0x01, 0, 0, 0, 0]))
        else:
            failUnlessEqual(u._meta.bytes, array("B", [0x04, 0x03, 0x02, 0x01]))

        class A5(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedByte
            c = Type.UnsignedByte
            d = Type.UnsignedByte
            e = Type.UnsignedByte

        class A(Union):
            a = Type.UnsignedByte
            b = Type.UnsignedShort
            c = Type.UnsignedInt
            d = Type.A5

        failUnlessEqual(A._cmeta.size, 8)


    @test
    def bit_fields(self):
        """Test for bit fields."""
        class A(Struct):
            a = Type.UnsignedByte[:4]
            b = Type.UnsignedByte[:4]

        a = A()
        a.a = 1;
        a.b = 2;
        failUnlessEqual(a._meta.bytes, array("B", [0x21]))
        failUnlessEqual(a.a, 1)
        failUnlessEqual(a.b, 2)

        class A(Struct):
            a = Type.UnsignedByte[:5]
            b = Type.UnsignedByte[:4]

        a = A()
        a.a = 1;
        a.b = 2;
        failUnlessEqual(a._meta.bytes, array("B", [0x1, 0x2]))
        failUnlessEqual(a.a, 1)
        failUnlessEqual(a.b, 2)

        class A(Struct):
            a = Type.UnsignedByte[:5]
            b = Type.UnsignedByte[:3]

        a = A()
        a.a = 1;
        a.b = 2;
        failUnlessEqual(a._meta.bytes, array("B", [0x41]))
        failUnlessEqual(a.a, 1)
        failUnlessEqual(a.b, 2)


    @test
    def bit_field_alignment(self):
        """Test various alignment issues for bit-fields"""
        class A(Struct):
            a = Type.UnsignedByte[:4]
            b = Type.UnsignedByte[:4]
        failUnlessEqual(A._cmeta.size, 1)

        class A(Struct):
            a = Type.UnsignedByte[:4]
            b = Type.UnsignedByte[:5]
        failUnlessEqual(A._cmeta.size, 2)

        class A(Struct):
            x = Type.UnsignedByte
            a = Type.UnsignedByte[:2]
            b = Type.UnsignedByte[:2]
            c = Type.UnsignedByte[:5]
        failUnlessEqual(A._cmeta.size, 3)

        class A(Struct):
            a = Type.UnsignedShort[:2]
            b = Type.UnsignedByte[:2]
            c = Type.UnsignedByte[:6]
        failUnlessEqual(A._cmeta.size, 2)

        class A(Struct):
            a = Type.UnsignedShort[:2]
            b = Type.UnsignedByte[:2]
            c = Type.UnsignedByte[:5]
            d = Type.UnsignedByte[:2]
        failUnlessEqual(A._cmeta.size, 2)


    @test
    def padding_and_sizes(self):
        """Check that padding and sizes are correctly worked out."""
        class A(Struct):
            a = Type.Byte
            b = Type.Byte
            c = Type.Byte
        failUnlessEqual(A._cmeta.size, 3)

        class B(Struct):
            a = Type.Byte    # Plus pad byte for alignment
            b = Type.Short
        failUnlessEqual(B._cmeta.size, 4)

        class C(Struct):
            a = Type.Short
            b = Type.Byte    # Plus pad byte for array alignment
        failUnlessEqual(C._cmeta.size, 4)

        class AA(Struct):
            a = Type.A
            b = Type.A
        failUnlessEqual(AA._cmeta.size, 6)

        class BB(Struct):
            a = Type.B
            b = Type.B
        failUnlessEqual(BB._cmeta.size, 8)

        class CC(Struct):
            a = Type.A
            b = Type.B
        failUnlessEqual(CC._cmeta.size, 8)


    @test
    def large_data_types(self):
        """Check the larger data types"""
        class A(Struct):
            _byteOrder = Format.BigEndian
            a = Type.UnsignedLongLong

        a = A()
        failUnlessEqual(a._meta.size, 8)
        failUnlessEqual(a._meta.bytes, array('B', [0] * 8))

        a.a = 0x123456789abcdef0
        failUnlessEqual(a._meta.bytes,
                array('B', [0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0]))
        return

        # TODO: The ctypes structure do not treat the following as bitfields
        #       in a single long long. I need to figure out whether this is
        #       a bug or not. However, I should probably go with what ctypes
        #       does is how things are defined to work.
        class A(Struct):
            _byteOrder = Format.BigEndian
            a = Type.UnsignedLongLong[:32]
            b = Type.UnsignedLongLong[:32]
        a = A()
        a.a = 0x9abcdef0
        a.b = 0x12345678
        failUnlessEqual(
                array('B', [0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0]),
                a._meta.bytes)

        a.b = 0x9abcdef0
        a.a = 0x12345678
        failUnlessEqual(a._meta.bytes,
                array('B', [0x9a, 0xbc, 0xde, 0xf0, 0x12, 0x34, 0x56, 0x78]))


    @test
    def string_member(self):
        """Test the String member type"""
        class X(Struct):
            c = Type.String[3][4]              # Array of 4 byte strings.
        a = X()

        class A(Struct):
            a = Type.String                    # Length of 1
            b = Type.String[7]                 # 7 char string
            c = Type.String[3][4]              # Array of 4 byte strings.
            d = Type.UnsignedShort             # Sentinel

        a = A()
        a.d = 0xeeee
        seq = list(enumerate([(i, j) for i in range(3) for j in range(4)]))

        a.a = chr(1)
        a.b = ''.join([chr(v) for v in range(2, 9)])
        for i in range(3):
            x = 9 + 4 * i
            a.c[i] = ''.join([chr(v) for v in range(x, x + 4)])
        
        failUnlessEqual(a._meta.bytes, array('B', range(1, 21) + [0xee, 0xee]))

        failUnlessEqual(a.a, chr(1))
        failUnlessEqual(a.b, ''.join([chr(v) for v in range(2, 9)]))
        for i in range(3):
            x = 9 + 4 * i
            failUnlessEqual(a.c[i], ''.join([chr(v) for v in range(x, x + 4)]))
        
    @test("xbroken")
    def struct_description(self):
        """Test structure description."""
        class A(Struct):
            a = Type.Byte
            b = Type.Byte
            c = Type.Byte
            d = Type.Byte[3]
            f1 = Type.Byte[:4]
            f2 = Type.Byte[:4]

        class Hdr(Struct):
            length = Type.Long
            msgType = Type.Long

        class B(Struct):
            x = Type.A
            y = Type.Short

        class C(Union):
            x = Type.A
            y = Type.Short
            z = Type.A[3]

        class D(Struct):
            hdr = Type.Hdr
            body = Type.C

        def sel(s):
            t = s.hdr.msgType
            if t == 0:
                return "x"
            if t == 1:
                return "y"
            if t == 2:
                return "z"
        D._selectors_ = {
                "body": sel,
            }

        a = D()
        if x64:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
             | 0    : struct D\n'
             | 0    :   struct Hdr           hdr\n'
             | 0    :     c_long               length\n'
             | 8    :     c_long               msgType\n'
             | 16   :   union C              body\n'
             | 16   :     struct A             x\n'
             | 16   :     c_short              y\n'
             | 16   :     struct A[3]          z\n'
             |
            """), a._meta.describe())
        else:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
             | 0    : struct D\n'
             | 0    :   struct Hdr           hdr\n'
             | 0    :     c_long               length\n'
             | 4    :     c_long               msgType\n'
             | 8    :   union C              body\n'
             | 8    :     struct A             x\n'
             | 8    :     c_short              y\n'
             | 8    :     struct A[3]          z\n'
             |
            """), a._meta.describe())

        if x64:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct D
            | 0    :   struct Hdr           hdr
            | 0    :     c_long               length = 0
            | 8    :     c_long               msgType = 0
            | 16   :   union C              body
            | 16   :     struct A             x
            | 16   :       c_byte               a = 0
            | 17   :       c_byte               b = 0
            | 18   :       c_byte               c = 0
            | 19   :       c_byte[3]            d
            | 19   :         [0] = 0
            | 20   :         [1] = 0
            | 21   :         [2] = 0
            | 22   :       c_byte;0:4           f1 = 0
            | 22   :       c_byte;4:4           f2 = 0
            |
            """ % sizemap), a._meta.dump())
        else:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct D
            | 0    :   struct Hdr           hdr
            | 0    :     c_long               length = 0
            | 4    :     c_long               msgType = 0
            | 8    :   union C              body
            | 8    :     struct A             x
            | 8    :       c_byte               a = 0
            | 9    :       c_byte               b = 0
            | 10   :       c_byte               c = 0
            | 11   :       c_byte[3]            d
            | 11   :         [0] = 0
            | 12   :         [1] = 0
            | 13   :         [2] = 0
            | 14   :       c_byte;0:4           f1 = 0
            | 14   :       c_byte;4:4           f2 = 0
            |
            """ % sizemap), a._meta.dump())

        c = C()
        failUnlessEqualStrings(DataMaker.literalText2Text("""
         | 0    : union C
         | 0    :   struct A             x
         | 0    :   c_short              y
         | 0    :   struct A[3]          z
         |
        """), c._meta.describe())

    @test
    def test_160(self):
        """Test structure dump."""
        class A(Struct):
            a = Type.Byte
            b = Type.Byte
            c = Type.String
            d = Type.Byte[2][3][4]
            f1 = Type.Byte[:4]
            f2 = Type.Byte[:4]

        class Q(Struct):
            addr = Type.Long
            port = Type.Short

        class B(Struct):
            x = Type.A
            y = Type.Short
            z = Type.Q[3]

        # Set all the fields.
        a = B()
        seq = list(enumerate([(i, j, k) 
            for i in range(2) for j in range(3) for k in range(4)]))

        for n, (i, j, k) in seq:
            a.x.d[i][j][k] = n

        a.x.a = 0x11
        a.x.b = 0x22
        a.x.c = chr(0x33)
        a.x.f1 = 0x4
        a.x.f2 = 0x5
        a.y = 999

        for i in range(3):
            a.z[i].addr = 60 + i * 2
            a.z[i].port = 60 + i * 2 + 1

        # Dump struct and check output.
        if x64:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct B
            | 0    :   struct A             x
            | 0    :     c_byte               a = 17
            | 1    :     c_byte               b = 34
            | 2    :     c_char               c = '3'\n"
            | 3    :     c_byte[2, 3, 4]      d
            | 3    :       [0]
            | 3    :         [0]
            | 3    :           [0] = 0
            | 4    :           [1] = 1
            | 5    :           [2] = 2
            | 6    :           [3] = 3
            | 7    :         [1]
            | 7    :           [0] = 4
            | 8    :           [1] = 5
            | 9    :           [2] = 6
            | 10   :           [3] = 7
            | 11   :         [2]
            | 11   :           [0] = 8
            | 12   :           [1] = 9
            | 13   :           [2] = 10
            | 14   :           [3] = 11
            | 15   :       [1]
            | 15   :         [0]
            | 15   :           [0] = 12
            | 16   :           [1] = 13
            | 17   :           [2] = 14
            | 18   :           [3] = 15
            | 19   :         [1]
            | 19   :           [0] = 16
            | 20   :           [1] = 17
            | 21   :           [2] = 18
            | 22   :           [3] = 19
            | 23   :         [2]
            | 23   :           [0] = 20
            | 24   :           [1] = 21
            | 25   :           [2] = 22
            | 26   :           [3] = 23
            | 27   :     c_byte;0:4           f1 = 4
            | 27   :     c_byte;4:4           f2 = 5
            | 28   :   c_short              y = 999
            | 32   :   struct Q[3]          z
            | 32   :     [0]
            | 32   :       c_long               addr = 60
            | 40   :       c_short              port = 61
            | 48   :     [1]
            | 40   :       c_long               addr = 62
            | 48   :       c_short              port = 63
            | 64   :     [2]
            | 48   :       c_long               addr = 64
            | 56   :       c_short              port = 65
            |
            """), a._meta.dump())
        else:
            failUnlessEqualStrings(DataMaker.literalText2Text("""
            | 0    : struct B
            | 0    :   struct A             x
            | 0    :     c_byte               a = 17
            | 1    :     c_byte               b = 34
            | 2    :     c_char               c = '3'\n"
            | 3    :     c_byte[2, 3, 4]      d
            | 3    :       [0]
            | 3    :         [0]
            | 3    :           [0] = 0
            | 4    :           [1] = 1
            | 5    :           [2] = 2
            | 6    :           [3] = 3
            | 7    :         [1]
            | 7    :           [0] = 4
            | 8    :           [1] = 5
            | 9    :           [2] = 6
            | 10   :           [3] = 7
            | 11   :         [2]
            | 11   :           [0] = 8
            | 12   :           [1] = 9
            | 13   :           [2] = 10
            | 14   :           [3] = 11
            | 15   :       [1]
            | 15   :         [0]
            | 15   :           [0] = 12
            | 16   :           [1] = 13
            | 17   :           [2] = 14
            | 18   :           [3] = 15
            | 19   :         [1]
            | 19   :           [0] = 16
            | 20   :           [1] = 17
            | 21   :           [2] = 18
            | 22   :           [3] = 19
            | 23   :         [2]
            | 23   :           [0] = 20
            | 24   :           [1] = 21
            | 25   :           [2] = 22
            | 26   :           [3] = 23
            | 27   :     c_byte;0:4           f1 = 4
            | 27   :     c_byte;4:4           f2 = 5
            | 28   :   c_short              y = 999
            | 32   :   struct Q[3]          z
            | 32   :     [0]
            | 32   :       c_long               addr = 60
            | 36   :       c_short              port = 61
            | 40   :     [1]
            | 36   :       c_long               addr = 62
            | 40   :       c_short              port = 63
            | 48   :     [2]
            | 40   :       c_long               addr = 64
            | 44   :       c_short              port = 65
            |
            """), a._meta.dump())

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | 0    : struct A\n'
        | 0    :   c_byte               a = 17\n'
        | 1    :   c_byte               b = 34\n'
        | 2    :   c_char               c = '3'\n"
        | 3    :   c_byte[2, 3, 4]      d\n'
        | 3    :     [0]\n'
        | 3    :       [0]\n'
        | 3    :         [0] = 0\n'
        | 4    :         [1] = 1\n'
        | 5    :         [2] = 2\n'
        | 6    :         [3] = 3\n'
        | 7    :       [1]\n'
        | 7    :         [0] = 4\n'
        | 8    :         [1] = 5\n'
        | 9    :         [2] = 6\n'
        | 10   :         [3] = 7\n'
        | 11   :       [2]\n'
        | 11   :         [0] = 8\n'
        | 12   :         [1] = 9\n'
        | 13   :         [2] = 10\n'
        | 14   :         [3] = 11\n'
        | 15   :     [1]\n'
        | 15   :       [0]\n'
        | 15   :         [0] = 12\n'
        | 16   :         [1] = 13\n'
        | 17   :         [2] = 14\n'
        | 18   :         [3] = 15\n'
        | 19   :       [1]\n'
        | 19   :         [0] = 16\n'
        | 20   :         [1] = 17\n'
        | 21   :         [2] = 18\n'
        | 22   :         [3] = 19\n'
        | 23   :       [2]\n'
        | 23   :         [0] = 20\n'
        | 24   :         [1] = 21\n'
        | 25   :         [2] = 22\n'
        | 26   :         [3] = 23\n'
        | 27   :   c_byte;0:4           f1 = 4\n'
        | 27   :   c_byte;4:4           f2 = 5\n'
        |
        """), a.x._meta.dump())

    @test
    def test_170(self):
        """Test structure querying."""
        class A(Struct):
            a = Type.Byte
            b = Type.Byte
            c = Type.String
            d = Type.Byte[2][3][4]
            f1 = Type.Byte[:4]
            f2 = Type.Byte[:4]

        class Q(Struct):
            addr = Type.Long
            port = Type.Short

        class B(Struct):
            x = Type.A
            y = Type.Short
            z = Type.Q[3]

    @test
    def undefined_type(self):
        """An undefined type should cause a TypeError."""
        try:
            class A(Struct):
                a = Type.Byte
                b = Type.Byte
                c = Type.Wibble
        except TypeError as exc:
            failUnlessEqual("Type.Wibble is not defined", exc.args[0])
        else:
            fail("A TypeError exception should have been raised.")


if __name__ == "__main__":
    runModule()
