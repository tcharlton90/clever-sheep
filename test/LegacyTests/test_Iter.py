#!/usr/bin/env python
"""CleverSheep.Prog.Iter unit tests"""

import CheckEnv

from cStringIO import StringIO

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import ImpUtils

# The module under test.
from CleverSheep.Prog import Iter


class PushBackIter(Suite):
    """Tests for the PushBackIterator class"""
    @test
    def main(self):
        """Test the PushBackIterator class."""
        s = [1, 2, 3, 4, 5]
        pb = Iter.PushBackIterator(s)

        got = []
        for i in range(2):
            got.append(next(pb))

        a = next(pb)
        b = next(pb)
        pb.unget(b)
        pb.unget(a)

        for n in pb:
            got.append(n)

        failUnlessEqual(s, got)

class FileReader(Suite):
    """Tests for the FileReader class."""
    @test
    def fileReader(self):
        """Test the file reader class."""
        f = StringIO("Line 1\nLine 2\nLine 3\n")

        f = Iter.FileReader(f)
        l = next(f)
        failUnlessEqual((0, "Line 1\n"), l)
        l = next(f)
        failUnlessEqual((1, "Line 2\n"), l)

        f.unget(l)
        l = next(f)
        failUnlessEqual((1, "Line 2\n"), l)

        l = next(f)
        failUnlessEqual((2, "Line 3\n"), l)


class PairsAndGroups(Suite):
    """Tests for the pairs and groups generators."""
    @test
    def groups(self):
        """Test the groups generator."""
        s = [1, 2, 3, 4, 5]

        b = list(Iter.groups(s, 3))
        c = list(Iter.groups(s, partial=0))

        failUnlessEqual([[1, 2, 3], [4, 5]], b)
        failUnlessEqual([[1, 2], [3, 4]], c)

    def pairs(self):
        """Test the pairs generator."""
        s = [1, 2, 3, 4, 5]

        a = list(Iter.pairs(s))

        failUnlessEqual([[1, 2], [3, 4], [5]], a)


if __name__ == "__main__":
    runModule()
