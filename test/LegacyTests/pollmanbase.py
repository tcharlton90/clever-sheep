#!/usr/bin/env python
"""CleverSheep.Test.PollManager unit tests"""
import CheckEnv

from CleverSheep.Sys import Platform
if Platform.platformType == "windows":
    from CleverSheep.Test.Tester import Collection
    raise Collection.Unsupported

import time
import os
import errno
import fcntl
import threading
import functools

from CleverSheep.Test.Tester import *


# The module under test.
from CleverSheep.Test import PollManager


class PollManTests(Suite):
    def canNest(self, test):
        return getattr(self, "implementsNesting", False)

    def hasWorkFunction(self, test):
        return getattr(self, "implementsWorkFunction", False)

    def returnsErrCodes(self, test):
        ret= getattr(self, "implementsErrCodes", False)
        return ret

    def canCatchUp(self, test):
        return getattr(self, "implementsTimerCatchUp", False)



class PollManager_Timeouts(PollManTests):
    """Tests for the PollManager class - timeout processing"""

    def setUp(self):
        self.count = 0

    @test
    def basic_creation(self):
        """We can create a PollManager instance."""
        p = PollManager.PollManager(self.impl)

    @test
    def timeout_with_no_args(self):
        """A timeout with no arguments for the callback.

        The important thing is that the lower test delays for at least the
        specified 0.1 seconds. We check for an upper limit of 0.2, but that may
        need increasing if the test is unreliable.
        """
        def ping():
            times[1] = time.time()
            p.quit()

        p = PollManager.PollManager(self.impl)
        times = [time.time(), None]
        p.addTimeout(0.10, ping)
        p.run()
        failUnlessInRange(0.10, 0.20, times[1] - times[0])

    @test
    def timeout_with_args(self):
        """A timeout with arguments for the callback."""
        def ping(*args, **kwargs):
            times[1] = time.time()
            p.quit()
            self.usedArgs = args
            self.usedKwargs = kwargs

        self.usedArgs = None
        self.usedKwargs = None
        p = PollManager.PollManager(self.impl)
        times = [time.time(), None]
        p.addTimeout(0.02, ping, 1, 2, x=3, y=4)
        p.run()
        failUnlessEqual((1, 2), self.usedArgs)
        failUnlessEqual({"x":3, "y":4}, self.usedKwargs)
        failUnlessInRange(0.02, 0.10, times[1] - times[0])

    @test
    def timeout_removal(self):
        """Remove a timeout whilst in a timeout callback."""
        def ping1():
            self.count += 1
            p.removeTimeout(uid2)
        def ping2():
            self.count += 1
        def lastPing():
            self.count += 1
            p.quit()

        p = PollManager.PollManager(self.impl)
        uid1 = p.addTimeout(0.02, ping1)
        uid2 = p.addTimeout(0.04, ping2)
        uid3 = p.addTimeout(0.06, lastPing)
        failIfEqual(uid1, uid2)
        failIfEqual(uid1, uid3)
        failIfEqual(uid2, uid3)
        p.run()
        failUnlessEqual(2, self.count)

    @test
    def timeout_remove_self(self):
        """A timeout callback should be able to remove itself."""
        def ping1():
            self.count += 1
            p.removeTimeout(uid1)
        def ping2():
            self.count += 1
        def lastPing():
            self.count += 1
            p.quit()

        p = PollManager.PollManager(self.impl)
        uid1 = p.addRepeatingTimeout(0.02, ping1)
        uid2 = p.addTimeout(0.04, ping2)
        uid3 = p.addTimeout(0.07, lastPing)
        failIfEqual(uid1, uid2)
        failIfEqual(uid1, uid3)
        failIfEqual(uid2, uid3)
        p.run()
        failUnlessEqual(3, self.count)

    @test
    def timeout_remove_done(self):
        """There should be no problem removing a timeout after it has fired.

        The twisted, for example, reactor will raise an exception if this is tried.
        This test verifies that such exceptions are suppressed.

        """
        exceptions = []
        def ping1():
            pass
        def ping2():
            try:
                p.removeTimeout(uid1)
            except Exception, exc:
                exceptions.append(exc)
            p.quit()

        #> Add a one shot timeout and let it fire. Then remove the, now fired
        #> timeout.
        p = PollManager.PollManager(self.impl)
        uid1 = p.addTimeout(0.01, ping1)
        uid2 = p.addTimeout(0.02, ping2)
        p.run()

        #> The test passes if no exception occurs.
        def err():
            return "Timeout removal CC raised an unhandled exception\n   %r" % (
                        exceptions[0],)
        failIf(exceptions, makeMessage=err)

    @test
    def repeating_timeout(self):
        """Verify repeating timeouts work."""
        def ping1():
            self.count += 1
        def lastPing():
            p.quit()

        p = PollManager.PollManager(self.impl)
        uid1 = p.addRepeatingTimeout(0.02, ping1)
        uid3 = p.addTimeout(0.07, lastPing)
        p.run()
        failUnlessInRange(3, 4, self.count)

    @test(cs_is_applicable=PollManTests.canCatchUp)
    def timeout_catchup(self):
        """Verify that a delayed timer fires immediately."""
        def lastPing():
            p.quit()

        times = [time.time()]
        p = PollManager.PollManager()
        p.addTimeout(0.05, lastPing)
        time.sleep(0.1)
        a = time.time()
        p.run()
        b = time.time()
        failUnlessInRange(0.0, 0.04, b - a)

    @test
    def timeout_survives_loop_restart(self):
        """A timeout set up during one ioloop run survives and triggers in a
        subsequence run.

        """
        def ping1():
            p.quit()
        def ping2():
            self.count += 1
            p.quit()
        def ping3():
            p.quit()

        p = PollManager.PollManager(self.impl)
        uid1 = p.addTimeout(0.01, ping1)
        uid2 = p.addTimeout(0.02, ping2)
        p.run()
        uid2 = p.addTimeout(0.02, ping3)
        p.run()
        failUnlessEqual(1, self.count)

    @test(cs_is_applicable=PollManTests.hasWorkFunction)
    def work_functions(self):
        """Verify the work function feature."""
        def ping1():
            self.count += 1
        def lastPing():
            p.quit()
        def work1():
            self.count += 1
        def work2():
            self.count += 1

        p = PollManager.PollManager(self.impl)
        uid1 = p.addRepeatingTimeout(0.02, ping1)
        uid3 = p.addTimeout(0.07, lastPing)
        p.addWorkFunction(work1)
        p.addWorkFunction(work2)
        p.run()

        # We expect both work functions to be invoked at least 2 times,
        # but not more than 3 times. This gives a total count of 9-11.
        # We allow for a slightly larger windows.
        failUnless(8 <= self.count <= 12)

    def work1(self):
        self.count += 1

    @test(cs_is_applicable=PollManTests.hasWorkFunction)
    def remove_work_functions(self):
        """Verify that work functions can be removed.

        Arrange for a count to be incremeented by both a regular 0.2s ping and
        two work functions. The second work function removes the first work function,
        so the first work function should only increment the counter once.
        After 0.7s everything is shut down.
        ::

          T(0.1s)     0   10   20   30   40   50   60
          WF 1                 1         1         1
          WF 2                 1         2         3
          Ping count           1         2         3
          Total                3         5         7


        """
        def ping1():
            self.count += 1
        def lastPing():
            p.quit()
            p.removeWorkFunction(work2)
        def work2():
            self.count += 1
            p.removeWorkFunction(self.work1)

        p = PollManager.PollManager(self.impl)
        self.base = time.time()
        p.addRepeatingTimeout(0.02, ping1)
        p.addTimeout(0.07, lastPing)
        p.addWorkFunction(self.work1)
        p.addWorkFunction(work2)
        p.run()
        failUnlessInRange(7, 8, self.count)

    @test
    def remove_callback_object(self):
        """Verify that removal of object providing callback method means the
        callback stops being invoked."""
        class X(object):
            def __init__(self, suicide_pill):
                self.suicide_pill = suicide_pill

            def ping2(self):
                counts.append(1)
                if len(counts) >= 3:
                    self.suicide_pill()

        def ping1():
            pass
        def lastPing():
            p.quit()
        def kill_x():
            del x[0]

        counts = []
        p = PollManager.PollManager(self.impl)
        uid1 = p.addRepeatingTimeout(0.02, ping1)
        uid3 = p.addTimeout(0.11, lastPing)
        x = [X(kill_x)]
        p.addRepeatingTimeout(0.02, x[0].ping2)

        p.run()

        # Expect the X.ping2 method to be invoked only 3 times.
        failUnlessEqual(3, len(counts))

    @test
    def reliable_repeat_timer_counts(self):
        """Verify that repeating timers average reliably.

        A repeating timer, should on average, fire every interval seconds.
        An earlier version of the PollManager did not try to do this. In that
        version, the firing time drifted about 0.003 seconds per firing
        interval. Using an interval of 0.02 seconds and 20 polls, the older
        code would take around ``0.023 * 20 = 0.46`` seconds. For the current
        code we should expect pretty close to ``0.40`` seconds.

        So we set the limit of ``0.40 <= T <= 0.42``.

        """
        def ping1():
            self.count += 1
            if self.count >= 20:
                p.quit()

        p = PollManager.PollManager(self.impl)
        base = time.time()
        tid = p.addRepeatingTimeout(0.02, ping1)
        p.run()
        delta = time.time() - base
        failUnlessInRange(0.40, 0.45, delta)

    @test(cs_is_applicable=PollManTests.canCatchUp)
    def timer_catchup_skips_misses(self):
        """Verify that repeating timers skip missed appointments.

        The PollManage tries to catch for repeating timers. However, missed
        appointments are skipped to try to make performance degrade gracefully.

        This test is like `reliable_repeat_timer_counts`, but the callback
        sleeps for 0.03 seconds thus making every second appointment be missed.
        We arrange for 10 polls, which should take close to 0.4 seconds.

        """
        def ping1():
            self.count += 1
            if self.count >= 10:
                p.quit()
            time.sleep(0.03)

        p = PollManager.PollManager(self.impl)
        base = time.time()
        tid = p.addRepeatingTimeout(0.02, ping1)
        p.run()
        delta = time.time() - base
        failUnlessInRange(0.40, 0.65, delta,
                msg="Run time %.3f, outside expected range" % delta)


class PollManager_SimpleCallback(PollManTests):
    """Tests for the PollManager class - using simple callbacks.

    """
    def setUp(self):
        self.count = 0

    @test
    def simple_callback(self):
        """Simple callbacks should be invoked during the next poll loop.

        """
        def ping1():
            self.count += 1
            data.append(self.count)
            if self.count == 1:
                p.addCallback(cb, 1, 2, x=3, y=4)
            p.addCallback(cb, 1, 2, x=3, y=4)
        def lastPing():
            p.quit()
        def cb(*args, **kwargs):
            data.append((args, kwargs))

        p = PollManager.PollManager(self.impl)
        data = []
        uid1 = p.addRepeatingTimeout(0.02, ping1)
        uid3 = p.addTimeout(0.05, lastPing)
        p.run()
        failUnlessInRange(2, 3, self.count)

        #> Each callback should be invoked immediately after the timer that
        #> added it.
        for i in range(5):
            if i in (0, 3):
                failUnless(isinstance(data.pop(0), int))
            else:
                failUnlessEqual(((1, 2), {'y': 4, 'x': 3}), data.pop(0))

    @test
    def simple_callback_threading(self):
        """Simple callbacks can be added from another thread.

        """
        class T(threading.Thread):
            def run(self):
                self.allTrue = []
                self.allFalse = []
                for i in range(100, 110):
                    p.addCallback(self.cb, i)
                    time.sleep(0.01)
                    self.allFalse.append(threading.current_thread() is pollThread)

            def cb(self, n):
                data.append(n)
                self.allTrue.append(threading.current_thread() is pollThread)

        def ping1():
            self.count += 1
            data.append(self.count)
        def lastPing():
            p.quit()

        p = PollManager.PollManager(self.impl)
        t = T()
        data = []
        uid1 = p.addRepeatingTimeout(0.02, ping1)
        uid3 = p.addTimeout(0.05, lastPing)

        pollThread = threading.current_thread()
        t.start()
        p.run()
        t.join()
        failUnlessInRange(2, 3, self.count)

        #> The mainloop should have terminated before all the thread's
        #> callbacks we executed.
        failUnless(len(t.allTrue) < 10,
                   msg="Test not working as intended.")

        #> Running a second main loop, should invoke the remaining thread
        #> callbacks.
        uid3 = p.addTimeout(0.05, lastPing)
        p.run()
        failUnlessEqual(10, len(t.allTrue))

        #> Verify that the callback occurred in the poll manager's thread.
        for ent in t.allTrue:
            failUnless(ent is True)
        for ent in t.allFalse:
            failUnless(ent is False)

    @test
    def simple_callback_cleanup(self):
        """All pending callbacks can be discarded.

        """
        def lastPing():
            p.quit()
        def cb(*args, **kwargs):
            data.append(None)

        #> Verify that callbacks are normally invoked on the next run.
        data = []
        p = PollManager.PollManager(self.impl)
        p.addCallback(cb)
        p.addCallback(cb)
        uid3 = p.addTimeout(0.05, lastPing)
        p.run()
        failUnlessEqual(2, len(data))

        #> Verify that clearCallbacks cleans up.
        data = []
        p = PollManager.PollManager(self.impl)
        p.addCallback(cb)
        p.addCallback(cb)
        p.clearCallbacks()
        p.addCallback(cb)
        uid3 = p.addTimeout(0.05, lastPing)
        p.run()
        failUnlessEqual(1, len(data))


class PollManager_FileCallbacks(PollManTests):
    """Tests for the PollManager class - file callback processing"""
    def setUp(self):
        if os.path.exists("XXX"):
            os.unlink("XXX")

    def tearDown(self):
        if os.path.exists("XXX"):
            os.unlink("XXX")

    @test
    def follow_file(self):
        """We should be able to follow input from a pipe."""
        def lastPing():
            p.quit()
        def ping():
            s = "Line %02d" % len(lines)
            w.write(s.encode("ascii"))
        def rd(fd, ev):
            if ev == PollManager.POLLIN:
                lines.append(r.read(7))

        lines = []
        r, w = os.pipe()
        r = os.fdopen(r, "rb", 0)
        w = os.fdopen(w, "wb", 0)
        p = PollManager.PollManager(self.impl)
        p.addRepeatingTimeout(0.05, ping)
        p.addTimeout(0.29, lastPing)
        p.addFileCallback(r, PollManager.POLLIN, rd)
        p.run()
        failUnlessInRange(2, 5, len(lines))
        for i, line in enumerate(lines):
            failUnlessEqual("Line %02d" % i, line.decode("ascii"))

    @test
    def file_callback_with_args(self):
        """Verify that callback closure args are passed correctly."""
        def lastPing():
            p.quit()
        def ping():
            w.write("Hello".encode("ascii"))
        def rd(fd, ev, *args, **kwargs):
            self.usedArgs = args
            self.usedKwargs = kwargs
            p.removeFileCallback(fd)
            return args

        self.usedArgs = None
        self.usedKwargs = None
        r, w = os.pipe()
        r = os.fdopen(r, "rb", 0)
        w = os.fdopen(w, "wb", 0)
        p = PollManager.PollManager(self.impl)
        p.addRepeatingTimeout(0.05, ping)
        p.addTimeout(0.15, lastPing)
        uid = p.addFileCallback(r, PollManager.POLLIN, rd, 1, 2, x=3, y=4)
        p.run()
        failUnlessEqual((1, 2), self.usedArgs)
        failUnlessEqual({"x":3, "y":4}, self.usedKwargs)

    @test
    def unregister(self):
        """Unregistering a file callback."""
        def lastPing():
            p.quit()
        def ping():
            s = "Line %02d" % len(lines)
            w.write(s.encode("ascii"))
        def rd(fd, ev):
            lines.append(r.read(7))
            p.removeFileCallback(fd)

        lines = []
        r, w = os.pipe()
        r = os.fdopen(r, "rb", 0)
        w = os.fdopen(w, "wb", 0)
        p = PollManager.PollManager(self.impl)
        p.addRepeatingTimeout(0.05, ping)
        p.addTimeout(0.29, lastPing)
        p.addFileCallback(r, PollManager.POLLIN, rd)
        p.run()
        failUnlessEqual(1, len(lines))

    @test
    def unregister_input(self):
        """Unregistering a file callback, using removeInputCallback"""
        def lastPing():
            p.quit()
        def ping():
            s = "Line %02d" % len(lines)
            w.write(s.encode("ascii"))
        def rd(fd, ev):
            lines.append(r.read(7))
            p.removeInputCallback(fd)

        lines = []
        r, w = os.pipe()
        r = os.fdopen(r, "rb", 0)
        w = os.fdopen(w, "wb", 0)
        p = PollManager.PollManager(self.impl)
        p.addRepeatingTimeout(0.05, ping)
        p.addTimeout(0.29, lastPing)
        p.addFileCallback(r, PollManager.POLLIN, rd)
        p.run()
        failUnlessEqual(1, len(lines))

    @test
    def fileCallback_for_write(self):
        """We should be able use file callbacks for write."""
        def lastPing():
            p.quit()
        def ping(*args):
            if self.haveFileCallback:
                p.removeOutputCallback(w)
                self.haveFileCallback = False
            wouldBlock = False
            while not wouldBlock:
                try:
                    s = "x" * 256
                    ret = w.write(s.encode("ascii"))
                    if ret is None:
                        wouldBlock = True
                except IOError as exc:
                    if exc.args[0] == errno.EAGAIN:
                        wouldBlock = True
                    else:
                        raise
                if wouldBlock:
                    p.addOutputCallback(w, ping)
                    self.haveFileCallback = True
                    self.waitWrite += 1
                    break

        def rd(fd, ev):
            wouldBlock = False
            while not wouldBlock:
                try:
                    s = r.read(256)
                    if s is None:
                        wouldBlock = True
                except IOError as exc:
                    if exc.args[0] == errno.EAGAIN:
                        wouldBlock = True

        self.haveFileCallback = False
        self.waitWrite = 0
        r, w = os.pipe()
        fcntl.fcntl(r, fcntl.F_SETFL, os.O_NONBLOCK)
        fcntl.fcntl(w, fcntl.F_SETFL, os.O_NONBLOCK)
        r = os.fdopen(r, "rb", 0)
        w = os.fdopen(w, "wb", 0)
        p = PollManager.PollManager(self.impl)
        p.addRepeatingTimeout(0.05, ping)
        p.addTimeout(0.29, lastPing)
        p.addInputCallback(r, rd)
        p.run()
        failIfEqual(0, self.waitWrite)

    @test
    def no_timers(self):
        """We should get file activity, without timers."""
        def rd(fd, ev):
            if ev == PollManager.POLLIN:
                lines.append(r.read(7))
            p.quit()

        lines = []
        r, w = os.pipe()
        r = os.fdopen(r, "rb", 0)
        w = os.fdopen(w, "wb", 0)
        s = "Line %02d" % len(lines)
        w.write(s.encode("ascii"))
        p = PollManager.PollManager(self.impl)
        p.addFileCallback(r, PollManager.POLLIN, rd)
        p.run()
        failUnlessEqual(1, len(lines))

    @test
    def file_callback_survives_restart(self):
        """A file callback should remain active when a ioloop is restarted.

        """
        def lastPing():
            p.quit()
        def ping():
            s = "Line %02d" % len(lines)
            w.write(s.encode("ascii"))
        def rd(fd, ev):
            if ev == PollManager.POLLIN:
                lines.append(r.read(7))

        lines = []
        r, w = os.pipe()
        r = os.fdopen(r, "rb", 0)
        w = os.fdopen(w, "wb", 0)
        p = PollManager.PollManager(self.impl)
        p.addRepeatingTimeout(0.05, ping)
        p.addTimeout(0.29, lastPing)
        p.addTimeout(0.1, lastPing)
        p.addFileCallback(r, PollManager.POLLIN, rd)
        a = time.time()
        p.run()
        p.run()
        failUnlessEqual(5, len(lines))
        for i, line in enumerate(lines):
            failUnlessEqual("Line %02d" % i, line.decode("ascii"))

    @test(cs_is_applicable=PollManTests.returnsErrCodes)
    def unregister_bad_fd2(self):
        """Unregistering an unregistered file descriptor returns None."""
        p = PollManager.PollManager(self.impl)
        failUnless(p.removeFileCallback(10) is None)

    @test
    def file_closes(self):
        """If a file closes, the PollManager should cope OK.

        Note this was an attempt to force the code through a particular path
        (``poll.poll()`` raising an exception). However I think under Linux
        the path cannot happen, but I have left the test in.

        """
        def lastPing():
            p.quit()
        def ping():
            if len(lines) == 0:
                s = "Line %02d" % len(lines)
                w.write(s.encode("ascii"))
        def rd(fd, ev):
            if ev == PollManager.POLLIN:
                lines.append(r.read(7))
            r.close()

        lines = []
        r, w = os.pipe()
        r = os.fdopen(r, "rb", 0)
        w = os.fdopen(w, "wb", 0)
        p = PollManager.PollManager(self.impl)
        tid1 = p.addRepeatingTimeout(0.05, ping)
        p.addTimeout(0.29, lastPing)
        p.addFileCallback(r, PollManager.POLLIN, rd)
        p.run()
        failUnlessEqual(1, len(lines))

    #@test
    def xerrorConditionIsHandled(self):
        """The POLLERR/PULLHUP condition on a file is passed to the
        reader/writer and handler removed.

        When POLLERR/POLLHUP occurs for a file descriptor, the event should be
        passed to the read or write handler and the handler(s) then removed.
        """
        def lastPing():
            p.quit()
        def ping():
            if count[0] == 0:
                s = "Line %02d" % len(events)
                w.write(s.encode("ascii"))
            elif count[0] > 0:
                w.close()
            count[0] += 1
        def rd(fd, ev):
            if ev == PollManager.POLLIN:
                r.read(7)
            events.append(ev)

        count = [0]
        events = []
        r, w = os.pipe()
        r = os.fdopen(r, "rb", 0)
        w = os.fdopen(w, "wb", 0)
        p = PollManager.PollManager(self.impl)
        p.addRepeatingTimeout(0.05, ping)
        p.addTimeout(0.29, lastPing)
        p.addFileCallback(r, PollManager.POLLIN, rd)
        p.run()

        # There is no guarantee that the read event will occur (for example
        # when using Twisted). So we can only check that the error condition
        # is processed.
        #failUnlessEqual(2, len(events))
        failUnlessEqual(PollManager.POLLIN, events[0])
        failUnless(events[-1] in (
                            PollManager.POLLERR, PollManager.POLLHUP))

    @test
    def errorConditionIsHandled(self):
        """A POLLERR/PULLHUP condition on a file causes the handle to be removed.

        When POLLERR/POLLHUP occurs for a file descriptor, the event should be
        passed to the read or write handler and the handler(s) then removed.
        """
        def lastPing():
            p.quit()
        def ping():
            if count[0] == 0:
                s = "Line %02d" % len(events)
                w.write(s.encode("ascii"))
            elif count[0] > 0:
                w.close()
            count[0] += 1
        def rd(fd, ev):
            if ev == PollManager.POLLIN:
                r.read(7)
            events.append(ev)

        count = [0]
        events = []
        r, w = os.pipe()
        r = os.fdopen(r, "rb", 0)
        w = os.fdopen(w, "wb", 0)
        p = PollManager.PollManager(self.impl)
        p.addRepeatingTimeout(0.05, ping)
        p.addTimeout(0.29, lastPing)
        p.addFileCallback(r, PollManager.POLLIN, rd)
        p.run()

        # There is no guarantee that the read event will occur (for example
        # when using Twisted). So we can only check that the error condition
        # is processed.
        #failUnlessEqual(2, len(events))
        failUnlessEqual(PollManager.POLLIN, events[0])


class CallbackRef(PollManTests):
    """Tests for the CallbackRef class.

    These only target code that the other tests do not exercise.

    """
    @test
    def none_func(self):
        """Check None is handled as the function."""
        c = PollManager.CallbackRef(None)
        failUnlessEqual((False, None), c())

    @test(cs_is_applicable=PollManTests.returnsErrCodes)
    def internal_function_matching(self):
        """Force an unlikely path for function comparison."""
        def work1():
            pass

        #> Create a poll manager and add a bound method as a work function.
        p = PollManager.PollManager(self.impl)
        p.addWorkFunction(self.work2)

        #> Now try to remove a non-bound method. This will fail, but also
        #> go through an unlikely code path.
        p.removeWorkFunction(work1)

    def work2(self):
        pass


class Nesting(PollManTests):
    """It should be possible to nest PollManager.run invocations.

    The `active` flag indicates if the PollManager is already executing its
    `run` method, but it is nicer to be able to simply invoke::

        pollman.run()

    and assume that on return the sub-run has finished.

    Note: While these tests can pass they also periodically fail due to
    timings but I don't think it is worth the time to look at it.
    """
    def setUp(self):
        self.count = 0

    def doInner(self, delay, allLevels=False):
        def ping():
            self.times.append(time.time() - self.start)
            p.quit(allLevels=allLevels)

        p = self.p
        p.prepareToNest()
        p.addTimeout(delay, ping)
        p.run()

    def timeIsWithinTolerance(self, expect, actual):
        actual = round(actual, 3)
        def err():
            return "Value %.3f was not within the range %.3f to %.3f" % (
                    actual, a, b)
        a = round(expect - 0.005, 3)
        b = round(expect + 0.005, 3)
        failUnless(a <= actual <= b, makeMessage=err)

    @test("bug", cs_is_applicable=PollManTests.canNest)
    def inner_loop_finishes_first(self):
        """If the inner loop finishes first then the outer loop resumes.

        """
        def ping():
            self.times.append(time.time() - self.start)
            self.doInner(0.01)

        def ping2():
            self.times.append(time.time() - self.start)
            p.quit()

        self.start = time.time()
        p = self.p = PollManager.PollManager(self.impl)
        self.times = []
        p.addTimeout(0.01, ping)
        p.addTimeout(0.03, ping2)
        p.run()
        #print ["%.3f" % t for t in self.times]
        for i, t in enumerate([0.01, 0.02, 0.03]):
            self.timeIsWithinTolerance(t, self.times[i])

    @test("bug", cs_is_applicable=PollManTests.canNest)
    def outer_loop_finishes_first(self):
        """If the outer loop finishes first then the inner loop continues.

        """
        def ping():
            self.times.append(time.time() - self.start)
            self.doInner(0.02)

        def ping2():
            self.times.append(time.time() - self.start)
            p.quit()

        self.start = time.time()
        p = self.p = PollManager.PollManager(self.impl)
        self.times = []
        p.addTimeout(0.01, ping)
        p.addTimeout(0.02, ping2)
        p.run()
        #print ["%.3f" % t for t in self.times]
        for i, t in enumerate([0.01, 0.02, 0.03]):
            self.timeIsWithinTolerance(t, self.times[i])

    def doInnerDouble(self, delay, delay2):
        def ping():
            self.times.append(time.time() - self.start)
            p.addTimeout(delay2, ping2)
            p.quit()
        def ping2():
            self.times.append(time.time() - self.start)
            p.quit()

        p = self.p
        p.prepareToNest()
        p.addTimeout(delay, ping)
        p.run()

    @test("bug", cs_is_applicable=PollManTests.canNest)
    def inner_loop_timers_get_cancelled(self):
        """When the inner loop finishes timers added within the loop are
        cancelled.

        """
        def ping():
            self.times.append(time.time() - self.start)
            self.doInnerDouble(0.01, 0.01)

        def ping2():
            self.times.append(time.time() - self.start)
            p.quit()

        self.start = time.time()
        p = self.p = PollManager.PollManager(self.impl)
        self.times = []
        p.addTimeout(0.01, ping)
        p.addTimeout(0.04, ping2)
        p.run()
        #print ["%.3f" % t for t in self.times]
        for i, t in enumerate([0.01, 0.02, 0.04]):
            self.timeIsWithinTolerance(t, self.times[i])

    def doInnerWithCallback(self, delay):
        def ping():
            # This should not get invoked.
            self.times.append(time.time() - self.start)
            p.quit()
        def pong():
            p.quit()

        p = self.p
        p.prepareToNest()
        p.addTimeout(delay, ping)
        p.addCallback(pong)
        p.run()

    @test("bug", cs_is_applicable=PollManTests.canNest)
    def callbackAlwaysExecutesInInnerLoop(self):
        """A simple callback always executes in the context of the innermost
        loop.

        """
        def ping():
            self.times.append(time.time() - self.start)
            self.doInnerWithCallback(0.01)

        def ping2():
            self.times.append(time.time() - self.start)
            p.quit()

        self.start = time.time()
        p = self.p = PollManager.PollManager(self.impl)
        self.times = []
        p.addTimeout(0.01, ping)
        p.addTimeout(0.03, ping2)
        p.run()
        #print ["%.3f" % t for t in self.times]
        for i, t in enumerate([0.01, 0.03]):
            self.timeIsWithinTolerance(t, self.times[i])
        failUnlessEqual(2, len(self.times))

    @test("bug", cs_is_applicable=PollManTests.canNest)
    def inner_loop_can_cancel_outers(self):
        """If the inner loop can force a complete quit, including outer loops.

        """
        def ping():
            self.times.append(time.time() - self.start)
            self.doInner(0.01, allLevels=True)

        def ping2():
            self.times.append(time.time() - self.start)
            p.quit()

        self.start = time.time()
        p = self.p = PollManager.PollManager(self.impl)
        self.times = []
        p.addTimeout(0.01, ping)
        p.addTimeout(0.03, ping2)
        p.run()
        #print ["%.3f" % t for t in self.times]
        for i, t in enumerate([0.01, 0.02]):
            self.timeIsWithinTolerance(t, self.times[i])
        failUnlessEqual(2, len(self.times))
