#!/usr/bin/env python
"""CleverSheep.Prog.SortedQ unit tests"""

import sys
import random

from CleverSheep.Test.Tester import *
from CleverSheep.Test import ImpUtils

# The module under test.
from CleverSheep.Prog.SortedQ import SortedQ


def makeQueue(size=10):
    random.seed(0)
    q = SortedQ()
    lll = {}

    for i in range(10):
        t = random.random()
        uid = q.add(t, str(t))
        lll[uid] = t

    return q, lll


class Test_SortedQ(Suite):
    """Tests for the SortedQ"""

    @test
    def basic(self):
        """Basic operation.

        We add 10 random values to the queue, then verify that we can iterate over
        the queue and that the popped values are in the correct order.
        """
        # Use queue with 10 entries.
        q, lll = makeQueue()
        expect = sorted(lll.values())

        # Check ordering using iteration.
        for i, (v, uid, data) in enumerate(q):
            failUnlessEqual(expect[i], v)

        # Check ordering by popping until the queue is empty.
        for i, exp in enumerate(expect):
            v, uid, data = q.pop()
            failUnlessEqual(exp, v)

        # Check queue really was emptied.
        failUnlessEqual(0, len(q))
        failUnlessEqual(q.head(), (None, None, None))

        # Verify bool() behaviour
        failIf(q)
        q.add(10, 10)
        failUnless(q)


    @test
    def removal(self):
        """Verify that we can delete various numbers of items.

        We create a Q of ten items and remove a number of items, then
        verify that the resulting Q has the corect size and is in order.
        We repeat this process, removeing first 1 item, then 2 items; upto all 10
        items.
        """
        for count in range(1, 11):
            # Make the queue then remove a number of items.
            q, lll = makeQueue()
            for i in range(count):
                uid, v = lll.popitem()
                q.remove(uid)

            failUnlessEqual(10 - count, len(q))
            expect = sorted(lll.values())

            # Check ordering using iteration.
            for i, (v, uid, data) in enumerate(q):
                failUnlessEqual(expect[i], v)

            # Check ordering by popping until the queue is empty.
            for i, exp in enumerate(expect):
                v, uid, data = q.pop()
                failUnlessEqual(exp, v)

            failUnlessEqual(q.head(), (None, None, None))


if __name__ == "__main__":
    Tester.runModule()
