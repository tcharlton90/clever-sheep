#!/usr/bin/env python
"""Struct unit tests"""

import sys
import struct
import cPickle as pickle

import CheckEnv

from CleverSheep.Test import DataMaker
from CleverSheep.Test.Tester import *
from array import array

# The module under test.
from CleverSheep.Test import Struct as _Struct
Struct = _Struct.Struct
Type = _Struct.Type
Format = _Struct.Format
Union = _Struct.Union

# We need some tests to be conditional depending on whether this is a 32 or 64
# bit platform.
if struct.calcsize("l") == 8:
    x64 = True
else:
    x64 = False


class Test_Struct(Suite):
    """Tests for the Struct module"""
    @test
    def simple_padded_struct(self):
        """Test a simple struct, with padding."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        a = A()
        failUnlessEqual(array("B", [0]*4), a._meta.bytes)
        a.a = 1
        a.b = 0x1234
        failUnlessEqual(array("B", [0x1, 0, 0x34, 0x12]), a._meta.bytes)
        failUnlessEqual(["01", "00", "34", "12"], a._meta.hex)
        failUnlessEqual(1, a.a)
        failUnlessEqual(0x1234, a.b)

    @test
    def simple_padded_struct_be(self):
        """Test with a simple struct, with padding, BigEndian."""
        class A(Struct):
            _byteOrder = Format.BigEndian
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        a = A()
        failUnlessEqual(a._meta.bytes, array("B", [0]*4))
        a.a = 1
        a.b = 0x1234
        failUnlessEqual(a._meta.bytes, array("B", [0x1, 0, 0x12, 0x34]))
        failUnlessEqual(a._meta.hex, ["01", "00", "12", "34"])
        failUnlessEqual(a.a, 1)
        failUnlessEqual(a.b, 0x1234)

        class S(Struct):
            a = Type.UnsignedByte[5]

        a = S()
        for i, c in enumerate("Hello"):
            a.a[i] = ord(c)
        failUnlessEqual("Hello", a._meta.s.decode("ascii"))


    @test
    def padded_3_byte_array_struct(self):
        """Test a simple struct, with padding."""
        class A(Struct):
            a = Type.UnsignedByte[3]
            b = Type.UnsignedShort

        a = A()
        failUnlessEqual(array("B", [0]*6), a._meta.bytes)

    @test
    def init_using_array(self):
        """Test initialising/setting using string or array."""
        class S(Struct):
            a = Type.UnsignedByte[5]

        a = S("Hello")
        failUnlessEqual("Hello", a._meta.s.decode("ascii"))

        v = array('B', "Hello".encode("ascii"))
        a = S(v)
        failUnlessEqual("Hello", a._meta.s.decode("ascii"))

        a = S([ord(c) for c in "Hello"])
        failUnlessEqual("Hello", a._meta.s.decode("ascii"))

        a = S(tuple([ord(c) for c in "Hello"]))
        failUnlessEqual("Hello", a._meta.s.decode("ascii"))

        a = S("Hell")
        failUnlessEqual("Hell", a._meta.s.decode("ascii"))

        # a = S("Hell", trim=1)
        a._meta.setBytes("Hi", trim=1)
        failUnlessEqual("Hi", a._meta.s.decode("ascii"))

        a._meta.setBytes("Boffo")
        failUnlessEqual("Boffo", a._meta.s.decode("ascii"))

        a._meta.setBytes("Hell")
        failUnlessEqual("Hello", a._meta.s.decode("ascii"))


    @test
    def overlong_raw_data(self):
        """Test code catches over-long raw data."""
        class S(Struct):
            a = Type.UnsignedByte[5]

        failUnlessRaises(ValueError, lambda: S("Hello!"))


    @test
    def init_using_kwargs(self):
        """Test initialising using kwargs"""
        class S(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedByte
            c = Type.UnsignedByte
            d = Type.UnsignedByte
            e = Type.UnsignedByte

        a = S(a=ord("H"), b=ord("e"), e=ord("o"), c=ord("l"), d=ord("l"))
        failUnlessEqual("Hello", a._meta.s.decode("ascii"))

        a = S("He", e=ord("o"), c=ord("l"), d=ord("l"))
        failUnlessEqual("Hello",  a._meta.s.decode("ascii"))

    @test
    def simple_nested_struct(self):
        """A simple nested struct."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        a = X()
        failUnlessEqual(a._meta.bytes, array("B", [0]*8))

        a.a.a = 1
        a.a.b = 0x1234
        a.b.a = 9
        a.b.b = 0x5678
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x1, 0, 0x34, 0x12, 0x9, 0, 0x78, 0x56]))

        failUnlessEqual(str(a.a), "Proxy:0")
        failUnlessEqual(str(a.b), "Proxy:4")

    @test
    def direct_access_to_children(self):
        """Children should be accessible using _meta.children."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        a = X()
        failUnlessEqual(a._meta.bytes, array("B", [0]*8))

        a.a.a = 1
        a.a.b = 0x1234
        a.b.a = 9
        a.b.b = 0x5678
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x1, 0, 0x34, 0x12, 0x9, 0, 0x78, 0x56]))

        failUnlessEqual(str(a.a), "Proxy:0")
        failUnlessEqual(str(a.b), "Proxy:4")

        name, obj = a._meta.children[0]
        failUnlessEqual("a", name)
        failUnlessEqual(str(obj), "Proxy:0")

        name, obj = a._meta.children[1]
        failUnlessEqual("b", name)
        failUnlessEqual(str(obj), "Proxy:4")

    @test
    def core_meta_information(self):
        """Check the core _meta information is correctly reported."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        x = X()
        failUnlessEqual("X", x._meta.name)

    @test
    def getting_memory_address(self):
        """Check we can get the underlying byte array's address.
        
        We cannot actually verify that the address is valid because
        we are relying on the underlying ``array`` module's behavour.
        So this does little more than exercise the code.
        """
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        a = A()
        addr = a._meta.addr()
        failIfEqual(0, addr)

    @test
    def description(self):
        """Check that description of structs works."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A[2]
            c = Type.Int[:3]
            d = Type.Int[:5]

        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | struct X
        |   a                                        : struct A, offset=0
        |     a                                      : UnsignedByte, offset=0
        |     b                                      : UnsignedShort, offset=2
        |   b                                        : struct A[2], offset=4
        |       a                                    : UnsignedByte, offset=4
        |       b                                    : UnsignedShort, offset=6
        |   c                                        : Int:3, offset=12
        |   d                                        : Int:5, offset=12
        |
        """), X._meta.describe())

        x = X()
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | struct X
        |   a                                        : struct A, offset=0
        |     a                                      : UnsignedByte, offset=0
        |     b                                      : UnsignedShort, offset=2
        |   b                                        : struct A[2], offset=4
        |       a                                    : UnsignedByte, offset=4
        |       b                                    : UnsignedShort, offset=6
        |   c                                        : Int:3, offset=12
        |   d                                        : Int:5, offset=12
        |
        """), x._meta.describe())

        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort[5]

        a = A()
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | struct A
        |   a                                        : UnsignedByte, offset=0
        |   b                                        : UnsignedShort[5], offset=1
        |
        """), a._meta.describe())

    @test
    def dumping(self):
        """Check that dumping an object's detailed content works."""
        class A(Struct):
            a = Type.String[1]
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A[2]
            c = Type.Int[:3]
            d = Type.Int[:5]

        x = X()
        x.a.a = "a"
        x.a.b = 2
        x.b.a = "b"
        x.b.b = 0xffff
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | struct X
        |   a : struct A\n'
        |     a = 'a'/ 0x61\n'
        |     b = 2/ 0x2\n'
        |   b : Array[2]
        |   [0] : struct A
        |       a = ''
        |       b = 0/ 0x0
        |   [1] : struct A
        |       a = ''
        |       b = 0/ 0x0
        |   c = 0/ 0x0
        |   d = 0/ 0x0
        |
        """), x._meta.dump())

        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort[5]

        a = A()
        a.a = 1
        a.b[0] = 2
        a.b[1] = 0x80
        a.b[2] = 0x8000
        a.b[3] = 0x8001
        a.b[4] = 0xffff
        failUnlessEqualStrings(DataMaker.literalText2Text("""
        | struct A
        |   a = 1/ 0x1\n'
        |   b : Array[5]\n'
        |   [0] = 2\n'
        |   [1] = 128\n'
        |   [2] = 32768\n'
        |   [3] = 32769\n'
        |   [4] = 65535\n'
        |
        """), a._meta.dump())

    @test
    def meta_sizes(self):
        """Check that _meta sizes are correctly reported."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        a = A()
        a.a = 1
        a.b = 2
        x = X(a._meta.s)
        failUnlessEqual(4, x._meta.dataSize)
        failUnlessEqual(8, x._meta.size)
        
        failUnlessEqual(1, x.a.a)
        failUnlessEqual(2, x.a.b)
        failUnlessEqual(0, x.b.a)
        failUnlessEqual(0, x.b.b)
        failUnlessEqual(4, x._meta.dataSize)

    @test
    def finding_objects(self):
        """Check that the find-by-name feature works."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        x = X()
        x.a.a = 1
        x.a.b = 2
        x.b.a = 0x80
        x.b.b = 0xffff
        l = x._meta.find("a")
        failUnlessEqual(3, len(l))
        failUnlessEqual("Proxy:0", str(l[0]))
        failUnlessEqual(1, l[1])
        failUnlessEqual(128, l[2])

        # Find for simple type returns None
        failUnless(x.a.a._meta.find("b") is None)

        # Proxy arrays are handled OK by find (i.e. ignored).
        class A(Struct):
            a = Type.UnsignedByte[5]
            b = Type.UnsignedShort

        a= A()
        l = a._meta.find("b")
        failUnlessEqual(1, len(l))

        # Proxy arrays found handled OK by find.
        class A(Struct):
            b = Type.UnsignedShort
            a = Type.UnsignedByte[5]

        a= A()
        l = a._meta.find("a")
        failUnlessEqual(1, len(l))


    @test
    def access_undefined_members(self):
        """A access to undefined members."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class X(Struct):
            a = Type.A
            b = Type.A

        class C(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class D(Struct):
            a = Type.C


        a = X()
        failUnlessRaises(AttributeError, lambda: a.c)
        failUnlessRaises(AttributeError, lambda: a.b.fred)

        def doSet():
            a.b.q = 3
        failUnlessRaises(AttributeError, doSet)

        def doSet():
            a.b = 3
        failUnlessRaises(TypeError, doSet)

        def doSet():
            c = C()
            a.b = c
        failUnlessRaises(TypeError, doSet)

        def doSet():
            d = D()
            a.b = d.a
        failUnlessRaises(TypeError, doSet)


    @test
    def mixed_endian_nested_struct(self):
        """A nested struct, mixed endian"""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class B(Struct):
            _byteOrder = Format.BigEndian
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class M(Struct):
            _byteOrder = Format.BigEndian
            a = Type.A
            b = Type.B
            c = Type.UnsignedLong

        a = M()
        if x64:
            failUnlessEqual(a._meta.bytes, array("B", [0]*16))
        else:
            failUnlessEqual(a._meta.bytes, array("B", [0]*12))
        a.a.a = 1
        a.a.b = 0x1234
        a.b.a = 9
        a.b.b = 0x5678
        a.c = 0x12345678

        failUnlessEqual(a._meta.bytes, array("B", 
                    [0x01, 0x00, 0x34, 0x12, 0x09, 0x00, 0x56, 0x78,
                     0x12, 0x34, 0x56, 0x78]))

        class M(Struct):
            _byteOrder = Format.LittleEndian
            a = Type.A
            b = Type.B
            c = Type.UnsignedLong

        a = M()
        a.a.a = 1
        a.a.b = 0x1234
        a.b.a = 9
        a.b.b = 0x5678
        a.c = 0x12345678

        failUnlessEqual(a._meta.bytes, array("B", 
                    [0x01, 0x00, 0x34, 0x12, 0x09, 0x00, 0x56, 0x78,
                     0x78, 0x56, 0x34, 0x12]))


    @test
    def access_invalid_meta_data(self):
        """Try accessing non-existant meta-data."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class B(Struct):
            x = Type.A

        a = B()
        failUnlessRaises(AttributeError, lambda: a._meta.xxx )
        failUnlessRaises(AttributeError, lambda: a.x._meta.xxx )

    @test
    def doubley_nested_struct(self):
        """A doubly nested struct."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class M(Struct):
            a = Type.A
            b = Type.A

        class X(Struct):
            a = Type.M
            b = Type.M

        a = X()
        failUnlessEqual(a._meta.bytes, array("B", [0]*16))

        a.a.a.a = 1
        a.a.a.b = 0x1234
        a.a.b.a = 2
        a.a.b.b = 0x5678
        a.b.a.a = 3
        a.b.a.b = 0x9abc
        a.b.b.a = 4
        a.b.b.b = 0xdef0
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x01, 0, 0x34, 0x12, 0x02, 0, 0x78, 0x56,
                            0x03, 0, 0xbc, 0x9a, 0x04, 0, 0xf0, 0xde]))

        failUnlessEqual(a.a.a.a, 1)
        failUnlessEqual(a.a.a.b, 0x1234)
        failUnlessEqual(a.a.b.a, 2)
        failUnlessEqual(a.a.b.b, 0x5678)
        failUnlessEqual(a.b.a.a, 3)
        failUnlessEqual(a.b.a.b, 0x9abc)
        failUnlessEqual(a.b.b.a, 4)
        failUnlessEqual(a.b.b.b, 0xdef0)


    @test
    def correct_proxy_behaviour(self):
        """Check proxies behave correctly."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedShort

        class M(Struct):
            a = Type.A
            b = Type.A

        class X(Struct):
            a = Type.M
            b = Type.M

        a = X()
        failUnlessEqual(a._meta.bytes, array("B", [0]*16))

        temp = a.a               # Temp is a proxy
        temp.a.a = 1
        temp.a.b = 0x1234
        temp.b.a = 2
        temp.b.b = 0x5678
        a.a = temp

        temp = a.b               # Temp is a proxy
        t2 = A()
        t2.a = 3
        t2.b = 0x9abc
        temp.a = t2
        t2.a = 4
        t2.b = 0xdef0
        temp.b = t2
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x01, 0, 0x34, 0x12, 0x02, 0, 0x78, 0x56,
                            0x03, 0, 0xbc, 0x9a, 0x04, 0, 0xf0, 0xde]))

        temp = a.a
        failUnlessEqual(temp.a.a, 1)
        failUnlessEqual(temp.a.b, 0x1234)
        failUnlessEqual(temp.b.a, 2)
        failUnlessEqual(temp.b.b, 0x5678)

        temp = M()
        temp.a.a = 5
        temp.a.b = 0x1122
        temp.b.a = 6
        temp.b.b = 0x3344
        a.a = temp
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x05, 0, 0x22, 0x11, 0x06, 0, 0x44, 0x33,
                            0x03, 0, 0xbc, 0x9a, 0x04, 0, 0xf0, 0xde]))

        t1 = a.a.a
        t2 = a.b
        t2.a = t1
        failUnlessEqual(a._meta.bytes, 
                array("B", [0x05, 0, 0x22, 0x11, 0x06, 0, 0x44, 0x33,
                            0x05, 0, 0x22, 0x11, 0x04, 0, 0xf0, 0xde]))


    @test
    def arrays_within_structs(self):
        """Check arrays within structs."""
        class A(Struct):
            a = Type.UnsignedByte[3]
            b = Type.UnsignedShort

        class M(Struct):
            a = Type.A[4]

        a = A()
        failUnlessEqual(a._meta.bytes, array("B", [0, 0, 0, 0, 0, 0]))
        a.a[0] = 1
        a.a[1] = 2
        a.a[2] = 3
        failUnlessEqual(a._meta.bytes, array("B", [1, 2, 3, 0, 0, 0]))
        failUnlessEqual(a.a[0], 1)
        failUnlessEqual(a.a[1], 2)
        failUnlessEqual(a.a[2], 3)

        failUnlessEqual(str(a.a), "Array[3] at offset=0")

        a.a[-3] = 4
        a.a[-2] = 5
        a.a[-1] = 6
        failUnlessEqual(a._meta.bytes, array("B", [4, 5, 6, 0, 0, 0]))
        failUnlessEqual(a.a[-3], 4)
        failUnlessEqual(a.a[-2], 5)
        failUnlessEqual(a.a[-1], 6)

        m = M()
        failUnlessEqual(m._meta.bytes, array("B", [
                0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0,
                ]))

        for i in range(4):
            m.a[i].b = 0x1234 + i
            m.a[i].a[0] = 1 + i * 4
            m.a[i].a[1] = 2 + i * 4
            m.a[i].a[2] = 3 + i * 4 
        failUnlessEqual(m._meta.bytes, array("B", [
                1, 2, 3, 0, 0x34, 0x12,
                5, 6, 7, 0, 0x35, 0x12,
                9, 10, 11, 0, 0x36, 0x12,
                13, 14, 15, 0, 0x37, 0x12,
                ]))


    @test
    def array_indices_get_checked(self):
        """Check that array indices are checked."""
        class A(Struct):
            a = Type.UnsignedByte[3]
            b = Type.UnsignedShort

        class M(Struct):
            a = Type.A[4]

        a = A()
        def doIndex(i):
            return a.a[i]

        a.a[0] = 1
        a.a[1] = 2
        a.a[2] = 3
        failUnlessEqual(a.a[0], 1)
        failUnlessEqual(a.a[-3], 1)
        failUnlessEqual(a.a[1], 2)
        failUnlessEqual(a.a[-2], 2)
        failUnlessEqual(a.a[2], 3)
        failUnlessEqual(a.a[-1], 3)
        failUnlessRaises(IndexError, doIndex, 3)
        failUnlessRaises(IndexError, doIndex, -4)


    @test
    def basic_types_not_redifinable(self):
        """Check that you cannot redefine basic types."""
        def f():
            class Int(Struct):
                a = 3

        failUnlessRaises(TypeError, f)
             

    @test
    def multi_dim_arrays(self):
        """Check multi-dimensional arrays."""
        class A(Struct):
            a = Type.UnsignedByte[2][3][4]
            b = Type.UnsignedShort

        a = A()
        a.b = 0xeeee
        seq = list(enumerate([(i, j, k) 
            for i in range(2) for j in range(3) for k in range(4)]))

        for n, (i, j, k) in seq:
            a.a[i][j][k] = n

        for n, (i, j, k) in seq:
            failUnlessEqual(a.a[i][j][k], n)

        failUnlessEqual(a._meta.bytes,
                array('B', list(range(len(seq))) + [0xee, 0xee]))


    @test
    def unions(self):
        """Test unions."""
        class A(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedByte
            c = Type.UnsignedByte
            d = Type.UnsignedByte

        class B(Struct):
            a = Type.UnsignedShort
            b = Type.UnsignedShort

        class U(Union):
            a = Type.A
            b = Type.B
            c = Type.UnsignedLong

        u = U()
        u.a.a = 1;
        u.a.b = 2;
        u.a.c = 3;
        u.a.d = 4;
        if x64:
            failUnlessEqual(u._meta.bytes,
                    array("B", [0x01, 0x02, 0x03, 0x04, 0, 0, 0, 0]))
            failUnlessEqual(u._meta.hex,
                    ["01", "02", "03", "04", "00", "00", "00", "00"])
        else:
            failUnlessEqual(u._meta.bytes, array("B", [0x01, 0x02, 0x03, 0x04]))
            failUnlessEqual(u._meta.hex, ["01", "02", "03", "04"])
        failUnlessEqual(u.b.a, 0x201)
        failUnlessEqual(u.b.b, 0x403)
        failUnlessEqual(u.c,   0x4030201)

        u.c = 0x01020304
        failUnlessEqual(u.c,   0x01020304)
        failUnlessEqual(u.b.a, 0x304)
        failUnlessEqual(u.b.b, 0x102)
        if x64:
            failUnlessEqual(u._meta.bytes,
                    array("B", [0x04, 0x03, 0x02, 0x01, 0, 0, 0, 0]))
        else:
            failUnlessEqual(u._meta.bytes, array("B", [0x04, 0x03, 0x02, 0x01]))

        class A5(Struct):
            a = Type.UnsignedByte
            b = Type.UnsignedByte
            c = Type.UnsignedByte
            d = Type.UnsignedByte
            e = Type.UnsignedByte

        class A(Union):
            a = Type.UnsignedByte
            b = Type.UnsignedShort
            c = Type.UnsignedInt
            d = Type.A5

        failUnlessEqual(A._meta.size, 8)


    @test
    def bit_fields(self):
        """Test for bit fields."""
        class A(Struct):
            a = Type.UnsignedByte[:4]
            b = Type.UnsignedByte[:4]

        a = A()
        a.a = 1;
        a.b = 2;
        failUnlessEqual(a._meta.bytes, array("B", [0x21]))
        failUnlessEqual(a.a, 1)
        failUnlessEqual(a.b, 2)

        class A(Struct):
            a = Type.UnsignedByte[:5]
            b = Type.UnsignedByte[:4]

        a = A()
        a.a = 1;
        a.b = 2;
        failUnlessEqual(a._meta.bytes, array("B", [0x1, 0x2]))
        failUnlessEqual(a.a, 1)
        failUnlessEqual(a.b, 2)

        class A(Struct):
            a = Type.UnsignedByte[:5]
            b = Type.UnsignedByte[:3]

        a = A()
        a.a = 1;
        a.b = 2;
        failUnlessEqual(a._meta.bytes, array("B", [0x41]))
        failUnlessEqual(a.a, 1)
        failUnlessEqual(a.b, 2)


    @test
    def bit_field_alignment(self):
        """Test various alignment issues for bit-fields"""
        class A(Struct):
            a = Type.UnsignedByte[:4]
            b = Type.UnsignedByte[:4]
        failUnlessEqual(A._meta.size, 1)

        class A(Struct):
            a = Type.UnsignedByte[:4]
            b = Type.UnsignedByte[:5]
        failUnlessEqual(A._meta.size, 2)

        class A(Struct):
            x = Type.UnsignedByte
            a = Type.UnsignedByte[:2]
            b = Type.UnsignedByte[:2]
            c = Type.UnsignedByte[:5]
        failUnlessEqual(A._meta.size, 3)

        class A(Struct):
            a = Type.UnsignedShort[:2]
            b = Type.UnsignedByte[:2]
            c = Type.UnsignedByte[:6]
        failUnlessEqual(A._meta.size, 4)

        class A(Struct):
            a = Type.UnsignedShort[:2]
            b = Type.UnsignedByte[:2]
            c = Type.UnsignedByte[:5]
            d = Type.UnsignedByte[:2]
        failUnlessEqual(A._meta.size, 4)


    @test
    def padding_and_sizes(self):
        """Check that padding and sizes are correctly worked out."""
        class A(Struct):
            a = Type.Byte
            b = Type.Byte
            c = Type.Byte
        failUnlessEqual(A._meta.size, 3)

        class B(Struct):
            a = Type.Byte    # Plus pad byte for alignment
            b = Type.Short
        failUnlessEqual(B._meta.size, 4)

        class C(Struct):
            a = Type.Short
            b = Type.Byte    # Plus pad byte for array alignment
        failUnlessEqual(C._meta.size, 4)

        class AA(Struct):
            a = Type.A
            b = Type.A
        failUnlessEqual(AA._meta.size, 6)

        class BB(Struct):
            a = Type.B
            b = Type.B
        failUnlessEqual(BB._meta.size, 8)

        class CC(Struct):
            a = Type.A
            b = Type.B
        failUnlessEqual(CC._meta.size, 8)


    @test
    def large_data_types(self):
        """Check the larger data types"""
        class A(Struct):
            _byteOrder = Format.BigEndian
            a = Type.UnsignedLongLong

        a = A()
        failUnlessEqual(a._meta.size, 8)
        failUnlessEqual(a._meta.bytes, array('B', [0] * 8))

        a.a = 0x123456789abcdef0
        failUnlessEqual(a._meta.bytes, 
                array('B', [0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0]))

        class A(Struct):
            _byteOrder = Format.BigEndian
            a = Type.UnsignedLongLong[:32]
            b = Type.UnsignedLongLong[:32]

        a = A()
        a.a = 0x9abcdef0
        a.b = 0x12345678
        failUnlessEqual(a._meta.bytes, 
                array('B', [0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0]))

        a.b = 0x9abcdef0
        a.a = 0x12345678
        failUnlessEqual(a._meta.bytes, 
                array('B', [0x9a, 0xbc, 0xde, 0xf0, 0x12, 0x34, 0x56, 0x78]))

    @test
    def string_member(self):
        """Test the String member type"""
        class A(Struct):
            a = Type.String                    # Length of 1
            b = Type.String[7]                 # 7 char string
            c = Type.String[3][4]              # Array of 4 byte strings.
            d = Type.UnsignedShort             # Sentinel

        a = A()
        a.d = 0xeeee
        seq = list(enumerate([(i, j) for i in range(3) for j in range(4)]))

        a.a = chr(1)
        a.b = ''.join([chr(v) for v in range(2, 9)])
        for i in range(3):
            x = 9 + 4 * i
            a.c[i] = ''.join([chr(v) for v in range(x, x + 4)])
        
        failUnlessEqual(a._meta.bytes,
                array('B', list(range(1, 21)) + [0xee, 0xee]))

        failUnlessEqual(a.a, chr(1))
        failUnlessEqual(a.b, ''.join([chr(v) for v in range(2, 9)]))
        for i in range(3):
            x = 9 + 4 * i
            failUnlessEqual(a.c[i], ''.join([chr(v) for v in range(x, x + 4)]))
        
    @test("broken")
    def pickling(self):
        """Verify we can pickle and unpickle."""
        class A(Struct):
            a = Type.String                    # Length of 1
            b = Type.String[7]                 # 7 char string
            c = Type.String[3][4]              # Array of 4 byte strings.
            d = Type.UnsignedShort             # Sentinel

        a = A()
        a.a = "1"
        a.b = "123456"
        a.c[0] = "one"
        a.c[1] = "two"
        a.c[2] = "tree"
        a.d = 0xeeee
        
        b = pickle.loads(pickle.dumps(a))
        failUnlessEqual(a._meta.bytes, b._meta.bytes)

    @test("broken")
    def struct_description(self):
        """Test structure description."""
        class A(Struct):
            a = Type.Byte
            b = Type.Byte
            c = Type.Byte
            d = Type.Byte[3]
            f1 = Type.Byte[:4]
            f2 = Type.Byte[:4]

        class Hdr(Struct):
            length = Type.Long
            msgType = Type.Long

        class B(Struct):
            x = Type.A
            y = Type.Short

        class C(Union):
            x = Type.A
            y = Type.Short
            z = Type.A[3]

        class D(Struct):
            hdr = Type.Hdr
            body = Type.C

        a = D()
        from cStringIO import StringIO
        s = a._meta.describe()

        failUnlessEqualStrings(Test_Struct.expect1, s)

        c = C()
        s = c._meta.describe()
        failUnlessEqualStrings(Test_Struct.expect2, s)

    expect1 = """struct D
      hdr                                      : struct Hdr, offset=0
        length                                 : Long, offset=0
        msgType                                : Long, offset=4
      body                                     : union C, offset=8
        x                                      : struct A, offset=8
          a                                    : Byte, offset=8
          b                                    : Byte, offset=9
          c                                    : Byte, offset=10
          d                                    : Byte[3], offset=11
          f1                                   : Byte:4, offset=14
          f2                                   : Byte:4, offset=14
        y                                      : Short, offset=8
        z                                      : struct A[3], offset=8
            a                                  : Byte, offset=8
            b                                  : Byte, offset=9
            c                                  : Byte, offset=10
            d                                  : Byte[3], offset=11
            f1                                 : Byte:4, offset=14
            f2                                 : Byte:4, offset=14
    """

    expect2 = """union C
      x                                        : struct A, offset=0
        a                                      : Byte, offset=0
        b                                      : Byte, offset=1
        c                                      : Byte, offset=2
        d                                      : Byte[3], offset=3
        f1                                     : Byte:4, offset=6
        f2                                     : Byte:4, offset=6
      y                                        : Short, offset=0
      z                                        : struct A[3], offset=0
          a                                    : Byte, offset=0
          b                                    : Byte, offset=1
          c                                    : Byte, offset=2
          d                                    : Byte[3], offset=3
          f1                                   : Byte:4, offset=6
          f2                                   : Byte:4, offset=6
    """

    @test("broken")
    def test_160(self):
        """Test structure dump."""
        class A(Struct):
            a = Type.Byte
            b = Type.Byte
            c = Type.String
            d = Type.Byte[2][3][4]
            f1 = Type.Byte[:4]
            f2 = Type.Byte[:4]

        class Q(Struct):
            addr = Type.Long
            port = Type.Short

        class B(Struct):
            x = Type.A
            y = Type.Short
            z = Type.Q[3]

        # Set all the fields.
        a = B()
        seq = list(enumerate([(i, j, k) 
            for i in range(2) for j in range(3) for k in range(4)]))

        for n, (i, j, k) in seq:
            a.x.d[i][j][k] = n

        a.x.a = 0x11
        a.x.b = 0x22
        a.x.c = chr(0x33)
        a.x.f1 = 0x4
        a.x.f2 = 0x5
        a.y = 999

        for i in range(3):
            a.z[i].addr = 60 + i * 2
            a.z[i].port = 60 + i * 2 + 1

        # Dump struct and check output.
        s = a._meta.dump()
        failUnlessEqualStrings(Test_Struct.expect3, s)
        
        s = a.x._meta.dump()
        failUnlessEqualStrings(Test_Struct.expect4, s)


    @test
    def test_170(self):
        """Test structure querying."""
        class A(Struct):
            a = Type.Byte
            b = Type.Byte
            c = Type.String
            d = Type.Byte[2][3][4]
            f1 = Type.Byte[:4]
            f2 = Type.Byte[:4]

        class Q(Struct):
            addr = Type.Long
            port = Type.Short

        class B(Struct):
            x = Type.A
            y = Type.Short
            z = Type.Q[3]


    expect3 = '''struct B
      x : struct A
        a = 17/ 0x11
        b = 34/ 0x22
        c = '3'/ 0x33
        d : Array[2]
        [0] : Array[3]
          [0] : Array[4]
            [0] = 0
            [1] = 1
            [2] = 2
            [3] = 3
          [1] : Array[4]
            [0] = 4
            [1] = 5
            [2] = 6
            [3] = 7
          [2] : Array[4]
            [0] = 8
            [1] = 9
            [2] = 10
            [3] = 11
        [1] : Array[3]
          [0] : Array[4]
            [0] = 12
            [1] = 13
            [2] = 14
            [3] = 15
          [1] : Array[4]
            [0] = 16
            [1] = 17
            [2] = 18
            [3] = 19
          [2] : Array[4]
            [0] = 20
            [1] = 21
            [2] = 22
            [3] = 23
        f1 = 4L/ 0x4L
        f2 = 5L/ 0x5L
      y = 999/ 0x3e7
      z : Array[3]
      [0] : struct Q
          addr = 60/ 0x3c
          port = 61/ 0x3d
      [1] : struct Q
          addr = 62/ 0x3e
          port = 63/ 0x3f
      [2] : struct Q
          addr = 64/ 0x40
          port = 65/ 0x41
    '''

    expect4 = '''  a = 17/ 0x11
      b = 34/ 0x22
      c = '3'/ 0x33
      d : Array[2]
      [0] : Array[3]
        [0] : Array[4]
          [0] = 0
          [1] = 1
          [2] = 2
          [3] = 3
        [1] : Array[4]
          [0] = 4
          [1] = 5
          [2] = 6
          [3] = 7
        [2] : Array[4]
          [0] = 8
          [1] = 9
          [2] = 10
          [3] = 11
      [1] : Array[3]
        [0] : Array[4]
          [0] = 12
          [1] = 13
          [2] = 14
          [3] = 15
        [1] : Array[4]
          [0] = 16
          [1] = 17
          [2] = 18
          [3] = 19
        [2] : Array[4]
          [0] = 20
          [1] = 21
          [2] = 22
          [3] = 23
      f1 = 4L/ 0x4L
      f2 = 5L/ 0x5L
    '''


if __name__ == "__main__":
    runModule()

