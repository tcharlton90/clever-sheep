#!/usr/bin/env python
"""Unit test for Test.DataMaker

"""

import sys

import CheckEnv

from CleverSheep.Test.Tester import *

# The module under test.
from CleverSheep.Test import DataMaker


class Test_LiteralTest2Lines(Suite):
    """The literalText2Lines function."""
    @test
    def simple_text(self):
        """Simple text should be converted as naturally expected.

        """
        lines = DataMaker.literalText2Lines("""
                    | Line 1
                    | Line 2
                """)
        failUnlessEqual(["Line 1", "Line 2"], list(lines))

    @test
    def trailing_space_is_discarded(self):
        """Trailing whitespace is normally discarded.

        """
        inputText = """
            | Line 1
            | Line 2"""
        inputText += "    "
        lines = DataMaker.literalText2Lines(inputText)
        failUnlessEqual(["Line 1", "Line 2"], list(lines))

    @test
    def trailing_bar_preserves_spaces(self):
        """A trailing vertical bar preserves trailing whitespace.

        """
        inputText = """
            | Line 1
            | Line 2  |"""
        inputText += "    "
        lines = DataMaker.literalText2Lines(inputText)
        failUnlessEqual(["Line 1", "Line 2  "], list(lines))

    @test
    def leading_whitspace_is_preserved(self):
        """All spaces (after the first) are preserved.

        So we can easily define indented text.

        """
        inputText = """
            |    Line 1
            | Line 2"""
        inputText += "    "
        lines = DataMaker.literalText2Lines(inputText)
        failUnlessEqual(["   Line 1", "Line 2"], list(lines))

    @test
    def test_with_no_leading_bar_is_discarded(self):
        """Any line that does not start with a vertical bar is discarded.

        This provides a comment mechanism for test code.

        """
        inputText = """
            The first line is he, well, first line.
            | Line 1
            Following lines are additional lines.
            | Line 2
            And that is that
        """
        inputText += "    "
        lines = DataMaker.literalText2Lines(inputText)
        failUnlessEqual(["Line 1", "Line 2"], list(lines))


class Test_LiteralText2Text(Suite):
    """The literalText2Text function.
    
    This is like the `literalText2Lines` function, but the returned value
    is a single text string. This suite uses the knowledge that this function
    is implemented using `literalText2Lines` and therfore just has a single
    basic test.

    """
    @test
    def simple_text(self):
        """Simple text should be converted as naturally expected.

        """
        text = DataMaker.literalText2Text("""
                    | Line 1
                    | Line 2
                """)
        lines = text.splitlines()
        failUnlessEqual(["Line 1", "Line 2"], list(lines))


if __name__ == "__main__":
    runModule()
