#!/usr/bin/env python
"""CleverSheep.Log.FileTracker unit tests"""

import sys
import os
import __builtin__

import CheckEnv

from CleverSheep.Test.Tester import *
from CleverSheep.Test import ImpUtils

# The module under test.
from CleverSheep.Log import FileTracker


class _File(object):
    _action = None
    def __init__(self, path, *args):
        self.path = path

    def read(self, l):
        if self._action == 1:
            raise IOError("Oops")
        return "abc"

    def close(self):
        pass

    def seek(self, *args):
        pass


_x_open = __builtin__.open
def _open(path, *args):
    if path == "mock":
        return _File(path)
    return _x_open(path, *args)


class Test_FileTracker(Suite):
    """Tests for the FileTracker module."""
    def setUp(self):
        if os.path.exists("XXX"):
            os.unlink("XXX")
        if os.path.exists("mock"):
            os.unlink("mock")
        __builtin__.open = _open

    def tearDown(self):
        __builtin__.open = _x_open
        if os.path.exists("XXX"):
            os.unlink("XXX")
        if os.path.exists("mock"):
            os.unlink("mock")

    @test
    def Basic_Follow(self):
        """Test that an initially missing file gets followed OK."""
        tracker = FileTracker.FileTracker("XXX")
        failUnlessEqual((FileTracker.NO_FILE, None), tracker.read())

        f = open("XXX", "w")
        failUnlessEqual((FileTracker.NEW_DATA, None), tracker.read())

        for n in range(10):
            f.write("Line %d\n" % n)
            f.flush()
            failUnlessEqual((FileTracker.NEW_DATA, "Line %d\n" % n), tracker.read())

    @test
    def reading_all_content(self):
        """Test that we can read all the content of an existing file."""
        f = open("XXX", "w")
        f.write("First line\n")
        f.flush()
        tracker = FileTracker.FileTracker("XXX", readAll=True)
        failUnlessEqual(
                (FileTracker.ORIG_DATA, 'First line\n'), tracker.read())

        for n in range(10):
            f.write("Line %d\n" % n)
            f.flush()
            failUnlessEqual((FileTracker.NEW_DATA, "Line %d\n" % n), tracker.read())

    @test
    def follow_existing_file(self):
        """Test that an initially existing file gets followed OK.
        
        We start a tracker with an existing file. We do this with both
        an empty file and a non-empty file.
        """
        f = open("XXX", "w")
        tracker = FileTracker.FileTracker("XXX")
        failUnlessEqual((FileTracker.NEW_DATA, None), tracker.read())

        for n in range(10):
            f.write("Line %d\n" % n)
            f.flush()
            failUnlessEqual((FileTracker.NEW_DATA, "Line %d\n" % n), tracker.read())

        tracker = FileTracker.FileTracker("XXX")
        failUnlessEqual((FileTracker.NEW_DATA, None), tracker.read())

        for n in range(10, 20):
            f.write("Line %d\n" % n)
            f.flush()
            code = FileTracker.NEW_DATA
            if n == 10:
                code = FileTracker.ORIG_DATA
            failUnlessEqual((code, "Line %d\n" % n), tracker.read())

    @test
    def file_truncation_is_spotted(self):
        """Test that when a file is truncated, it is spotted.
        
        We start a tracker with an existing file, which is not-empty. 
        Then we add some lines before re-opening the file and writing some lines.
        """
        # Use follow_existing_file to do the first phase.
        self.follow_existing_file()

        f = open("XXX", "a+")
        tracker = FileTracker.FileTracker("XXX")
        failUnlessEqual((FileTracker.NEW_DATA, None), tracker.read())

        for n in range(20, 30):
            f.write("Line %d\n" % n)
            f.flush()
            code = FileTracker.NEW_DATA
            if n == 20:
                code = FileTracker.ORIG_DATA
            failUnlessEqual((code, "Line %d\n" % n), tracker.read())
        f.close()

        # Use follow_existing_file to replace the file with a truncated version.
        self.follow_existing_file()
        expect = "".join(["Line %d\n" % n for n in range(20)])
        code, text = tracker.read()
        failUnlessEqual(FileTracker.RESTART, code)
        failUnlessEqualStrings(expect, text)

    @test
    def file_removal_is_spotted(self):
        """Test that when a file is removed, it is spotted.
        
        We start a tracker with an existing file, which is not-empty.
        Then we remove it.
        """
        # Use follow_existing_file to do the first phase.
        self.follow_existing_file()

        tracker = FileTracker.FileTracker("XXX")
        os.unlink("XXX")
        failUnlessEqual((FileTracker.NO_FILE, None), tracker.read())

        f = open("XXX", "w")
        for n in range(20, 30):
            f.write("Line %d\n" % n)
            f.flush()
            code = FileTracker.NEW_DATA
            if n == 20:
                code = FileTracker.RESTART
            failUnlessEqual((code, "Line %d\n" % n), tracker.read())
        f.close()

    @test
    def io_error_handling(self):
        """I/O Errors should always be handled.
        
        """
        f = file("mock", "w")
        f.write("a")
        f.flush()
        tracker = FileTracker.FileTracker("mock")
        f.write("a")
        f.flush()
        _File._action = 1
        failUnlessEqual((FileTracker.NO_FILE, None), tracker.read())


if __name__ == "__main__":
    runModule()
