#!/usr/bin/env python
"""CleverSheep.Log.LogSupport unit tests"""

import os
from cStringIO import StringIO

import CheckEnv

from CleverSheep.Test.Tester import *

# The module under test.
from CleverSheep.Log import LogSupport


def cleanUp():
    if os.path.exists("XXX"):
        os.unlink("XXX")

class F(object):
    def flush(self):
        self.flushed = True


class Test_HangStream(Suite):
    """Tests for the HandStream class."""
    @test
    def write(self):
        """Test the write method."""
        f = StringIO()
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.write("A single line\n")
        failUnlessEqual("PREFIX:A single line\n", f.getvalue())

        f = StringIO()
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.write("Several\nlines\n")
        failUnlessEqual("PREFIX:Several\n       lines\n", f.getvalue())

    @test
    def writelines(self):
        """Test the writelines method."""
        f = StringIO()
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.writelines(["Several\n", "lines\n"])
        failUnlessEqual("PREFIX:Several\n       lines\n", f.getvalue())

    @test
    def write_no_lines(self):
        """The writelines method gracefully handles no lines."""
        f = StringIO()
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.writelines([])
        failUnlessEqual("", f.getvalue())

    @test
    def write_partial_lines(self):
        """The writelines tracks when newline is required."""
        f = StringIO()
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.writelines(["Several\n", "li"])
        h.writelines(["nes\n"])
        failUnlessEqual("PREFIX:Several\n       lines\n", f.getvalue())

    @test
    def empty_prefix(self):
        """An empty prefix is OK."""
        f = StringIO()
        h = LogSupport.HangStream(f)
        h.writelines(["Several\n", "lines\n"])
        failUnlessEqual("Several\nlines\n", f.getvalue())

    @test
    def flushinc(self):
        """Test that flush is passed to the underlying file."""
        f = F()
        f.flushed = False
        h = LogSupport.HangStream(f, getPrefix=lambda: "PREFIX:")
        h.flush()
        failUnless(f.flushed)


if __name__ == "__main__":
    runModule()
