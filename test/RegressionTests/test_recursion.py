#!/usr/bin/env python
"""Regression test a recursive issue that was present in 0.5.10 and fixed in
 0.6.0"""

from CleverSheep.Test.Tester import Suite
from CleverSheep.Test.Tester import test
from CleverSheep.Test.Tester import runModule


class RecursiveSuite(Suite):
    """Recursive Bug Regression Suite"""

    def setUp(self):
        super(RecursiveSuite, self).setUp()
        self.prepared = set()

    def prepare_devices(self, device_id):
        self._prepare_devices(device_id)

    def _prepare_devices(self, device_id):
        if device_id not in self.prepared:
            print 'Preparing', device_id
            self.prepared.add(device_id)
            if device_id == 2:
                self.prepare_devices(1)

    @test(testID='recursive-test')
    def recursive_test(self):
        """Recursive Test
        
        Previously attempting to run this test would have caused a
        recursion exception
        """
        self.prepare_devices(2)


if __name__ == "__main__":
    runModule()