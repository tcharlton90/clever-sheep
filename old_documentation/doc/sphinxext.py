#!/usr/bin/env python
"""A small extension module to support EpySpinx generated documentation.

This adds a new directive called ``currentclass``, which is similar to the
``currentmodule`` directive.
"""


import re

from docutils import nodes
from docutils.parsers.rst.directives import images

import sphinx

def currentclass_directive(name, arguments, options, content, lineno,
                       content_offset, block_text, state, state_machine):
    # This directive is just to tell people that we're documenting stuff in
    # class foo, but links to class foo won't (necessarily) lead here.
    env = state.document.settings.env
    clsname = arguments[0].strip()
    if clsname == 'None':
        env.temp_data['py:class'] = None
    else:
        env.temp_data['py:class'] = clsname
    return []


def setup(app):
    app.add_directive('currentclass', currentclass_directive, 1, (1, 0, 1))
