===================
CleverSheep Testing
===================

.. highlight:: python
   :linenothreshold: 5

CleverSheep is a Python package largely focussed on high-level automated
testing. It started life around 2007 and has been professionally used since
then, albeit by a small number of system developers.

It is also perfectly usable for lower level testing of Python code. CleverSheep
is (of course) used to implement its own tests, a large subset of which are at
the unit level.

  +--------------------+-----------------------------------------------------+
  | Get it from here.  | `CleverSheep releases`_                             |
  +--------------------+-----------------------------------------------------+

To install:

1. Download the file (latest release recommended).

2. Untar and change to the created directory.

3. Use setup.py to build then install the code.

For example:
::

    tar xf CleverSheep-0.5.10.tar.gz
    cd CleverSheep-0.5.10
    python setup.py build
    sudo python setup.py install


More information
================

.. toctree::
   :maxdepth: 1

   tester-manual/index.rst
   design/index.rst
   glossary.rst
   api.rst

------------------------------------------------------------------------------

Credits
=======

CleverSheep contains some code that I have reused or simply copied. I think
this list it is complete, but if you think there is a credit missing then let
me know.

  +----------------+-----------------------------------------------------+
  | Module         | Information                                         |
  +================+=====================================================+
  | ultraTB.py     | A neat module written by Nathan Gray.               |
  +----------------+-----------------------------------------------------+
  | decorator.py   | Signature preserving decorating, written by         |
  |                | Michele Simionato.                                  |
  |                |                                                     |
  |                | This is used pretty much unmodified.                |
  +----------------+-----------------------------------------------------+
  | Struct.py      | Inspired by an ASPN recipe by Brian McErlean.       |
  |                |                                                     |
  |                | It is possible that some snippits of his code have  |
  |                | made it into mine.                                  |
  +----------------+-----------------------------------------------------+

.. _CleverSheep releases: https://sourceforge.net/projects/cleversheep/files/

.. --------
.. vim: sw=2 spell
