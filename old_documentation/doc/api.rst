===============
CleverSheep API
===============

.. toctree::
   :maxdepth: 1

   api-rst/index.rst

.. note::

   This is currently not very complete or helpful. In particular there is
   no way to discriminate what is reaaly intended to be part of the puclic API.


*CleverSheep* provides a single nested package called, well, ``CleverSheep``.
So to import any *CleverSheep* module, you need to prefix the module name with
``CleverSheep.``. For example, the :mod:`App` module is typically imported
using one of the following forms:

.. sourcecode:: python

  from CleverSheep import App
  import CleverSheep.App
  import CleverSheep.App as App
