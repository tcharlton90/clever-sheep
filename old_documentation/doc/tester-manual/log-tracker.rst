.. _sec_log_tracker:

==================
Tracking Log Files
==================


Sometimes it is necessary to examine the output of log files in order
to verify whether the :term:`SUT` has behaved correctly. A simple approach
is to run the :term:`SUT`, poke it as necessary and then read and process
the log file after everything has finished. However this approach can have
certain disadvantages.

- This may force you to starts and stop the :term:`SUT` for each test in order to,
  for example, start with a clean log file each time.

- An early failure of the :term:`SUT` cannot be detected until all the test
  steps have taken place. So you waste time waiting for the test to finish
  when you could have been starting to diagnose and fix the bug.

The *CleverSheep* toolkit provides a module that helps track the
contents of a file as it is written. Within the test framework, this module
is used to implement the :mod:`~Test.LogTracker`, which is designed to work
well within test scripts. This allows you to write tests that use log file
output, but fail as quickly as possible.


Creating a tracker
------------------

To create a tracker, you need to crate a class that inherits from one of the
classes in the :mod:`~Test.LogTracker` module. For example:

.. literalinclude:: Examples/log_tracking/test_log.py
   :linenos:
   :lines: 8-18

This example uses the :class:`~Test.LogTracker.LineLogTracker`, which ensures
that you can always process whole lines. The ``onRead`` method is the only
thing you need to over ride. It is automatically called when new lines become
available. In this example, the tracker looks for informational messages and
stores them in the :attr:`~Test.TestEventStore.eventStore`, which was
introduced in :ref:`sec_event_store`.

Note that the ``self.lines`` list is emptied on line 11. The
:class:`~Test.LogTracker.LineLogTracker` only appends to this list [#]_.


Using your tracker
------------------

Once you have the tracker, you create it with a
:class:`~Test.PollManager.PollManager` instance and the name of a file to
track. This is often done in your set-up method:

.. literalinclude:: Examples/log_tracking/test_log.py
   :linenos:
   :lines: 23,25-26

It is also common practice to write a method to wait for messages,
as described in :ref:`sec_event_store`.

.. literalinclude:: Examples/log_tracking/test_log.py
   :linenos:
   :lines: 41-50

It is the call to ``self.control.expect`` that allows the tracker to actively
watch the log file. Then you can write your test, which expects to see
certain messages appear in the log.

.. literalinclude:: Examples/log_tracking/test_log.py
   :linenos:
   :lines: 33-39


.. [#] This is to allow, for example, you to treat multiple lines as a single
       message/event without needing to implement your own buffering.
