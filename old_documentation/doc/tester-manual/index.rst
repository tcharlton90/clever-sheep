==================
The Tester Package
==================

.. highlight:: python
   :linenothreshold: 5


Introduction
============

The :mod:`Tester<Test.Tester>` sub-package provides a basic framework to
support the development of automated tests at a number of levels.

It is perfectly usable for writing unit tests, but its development was driven
by the need to automate tests at higher (such as subsystem and system) levels, 

The test framework is the largest part of the CleverSheep package. It has been
actively used and developed for a few years and is fairly well proven software.

The manual provides a high level guide on how to use the framework to write
tests, run tests, etc. This should be a useful introduction to the framework
as a whole.

The reference is the starting point for getting detailed API information.

.. toctree::
   :maxdepth: 1

   manual.rst
   tester-reference.rst

