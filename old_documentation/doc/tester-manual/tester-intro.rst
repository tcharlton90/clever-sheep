.. _sec_basic_tests:

=======================
Basic CleverSheep Tests
=======================

.. _ref_test_intro:

The :mod:`Tester<Test.Tester>` sub-package provides a basic framework to allow
you to, well, write tests. The ``Tester`` package is part of the ``Test``
package, which is designed to support testing at various levels (from unit to
system).

This section provides a basic tutorial on writing unit based tests.
This is enough to cover the basic concepts.


A Simple Unit Test
==================

The following shows a unit test which illustrates the basic anatomy of a test
script [#]_.

.. literalinclude:: Examples/test_anatomy.py
   :linenos:

The example illustrates a lot of points, which we now cover in some detail.

Line 1
    The one provides the standard 'shebang', common on UNIX like platforms.
    This is not necessary, but it is good style to make each script runnable
    standalone. See also lines 36-37.

Lines 2 to 4
    The module docstring provides test documentation for the script as a whole.
    Test script docstrings are important because the framework uses them to
    generate output when the tests are run and to generate test documentation.

    In general the CleverSheep framework will refuse to execute if you fail to
    provide docstrings for modules, suites and tests.

    All docstrings should follow the PEP-008 convention of a short summary
    (typically a single sentence) followed by an optional, longer explanation.

Line 7
    This (obviously) imports the test framework module. Using the `from ... \*`
    style is recommended, **but** only for actual test script; i.e. code that
    has actual test methods (see line 17). In practice, it is common to
    write common support modules for a set of test scripts, which also need
    to import ``CleverSheep.Test.Tester``. However, such support scripts
    should stick to ``from CleverSheep.Test import Tester``.

Line 8
    This line simply imports the module under test.

Lines 10 and 23
    This defines a test suite called ``Addition``. A test suite is any class
    than ultimately inherits from the :obj:`Suite` class. Note that this class
    has a docstring on line 11. This is important, see the above discussion
    about lines 2-4.

Lines 12 to 13
    These define the suite set-up and tear-down methods. You do not have to
    define these, they are shown here for the purposes of illustration and, in
    our example, do nothing.

    If defined the ``suiteSetUp`` method will be executed immediately before
    the first test in the suite is run. Similarly the ``suiteTearDown`` method
    will be executed once the suites test have completed.

    More importantly, the ``suiteTearDown`` method is always executed, even if the
    run of tests in the suite failed in any way.

Lines 14 to 15
    These define the per-test set-up and tear-down methods. These are like the
    ``suiteSetUp`` and ``suiteTearDown`` methods, except these are executed
    before and after each test. In our example they do nothing.

    Together with the suite set-up and tear-down methods on lines 12-13, these
    form the test :term:`test fixture` for all the tests in the suite.
    
Lines 17, 23 and 30
    These line decorates the following method to mark it as a test. The
    ``test`` decorator is provided by ``CleverSheep.Test.Tester`` and is the
    only supported way to define a test method [#]_.

Lines 18 to 21 and 24 to 26
    The two test methods for the ``Addition`` suite. As for the module and test
    suite class these must have docstrings.

Lines 21, 26 and 34
    This is an example of an assertion function. These are used to check
    results. If such an assertion fails, the test immediately fails.

Line 28
    This starts a second test suite, called ``Subtraction``. This has no
    set-up or tear-down methods.

Lines 36 to 37
    Standard code used to make this module runnable as a stand alone test
    script. You should always put this at then end of each test script so that
    it can be executed on its own.

    On operating systems that support it, common practice is to make the script
    executable as well.

------------------------------------------------------------------------------

One thing the framework tries to do is provide good information when tests
fail. So assume that the ``sums`` module contains a bug so that when asked to
add ``1`` and ``1``, it produces ``3``. When you run this test script, you will
see:

.. literalinclude:: Examples/run_test_anatomy.eg
   :language: cstest
   :linenos:

The above example shows the diagnostics you get by default. The different pars
of the output are:

Lines 6-8.
  A default message for a failed test assertion. In this case the default is good
  enough; it is telling you that ``2`` (the expected value) did not match ``3``
  the actual value.

  The framework follows the convention that, whenever expected values are
  compared against actual values, the expected value always appears first -
  both in calls to assertion functions and in the default failure reports.

Lines 9 to 16.
  These provide a Python stack trace back, which allows you to see the sequence
  of calls leading up to the point where the test was detected as failing.

  Line 13 shows the assertion call that detected the error. Line 9 shows that
  this is in the test script itself (``test_anatomy.py``) so you know that the
  first place to look is in this file around line 21, to see what the test was
  trying to do/check (although it is pretty obvious in this example).

Lines 19 to 22
  This simply provides a summary of the failing tests. This summary is only
  written to the console if there are failures. Otherwise all you get is a nice
  list of passes.

The output is coloured where possible (i.e. when the OS and terminal support
it). The framework (naturally) uses red to indicate failure and always writes a
failure summary if any failure occurred. This means you can run a long set of
tests and, when you return to your screen, if there was a failure there will be
red text.

Also notice that the test run stopped after the failure; the second and third
tests were not executed. This is the default behaviour for the framework
because, during development, you normally want to stop and fix problems as soon
as possible. However, you can over ride this behaviour using the
:option:`--keep-going` command line option, as in
::

    $ ./my_test --keep-going

------------------------------------------------------------------------------

As well as the console output, the framework also generates a log file for the
test run. This is normally called ``test.log``, but can be changed using the
:option:`--log-file` command line option. A log for this example is shown below.

.. literalinclude:: Examples/log_test_anatomy.eg
   :language: text
   :linenos:

The log can be used to provide a record of a formal test run, but its main
intended purpose is to provide all the extra details that are typically
required to diagnose test failures. Generally it provides a superset of
the information printed to the terminal.

Line 1
  Records when the tests were run.

Line 5
  The is extra output generated by executing the test. This line is printed
  by line 20 in the test script itself.

  In general any output to ``sys.stdout``/``sys.stderr`` is redirected to the
  log file. This means you can litter your test script with ``print``
  statements/functions in order to provide additional information the test
  log.

Lines 20 to 26
  This section provides a report of every test, whether run or not. Notice that
  tests 2 and 3 are shown as not-run. This is because they were not run
  following the failure if test 1.

Lines 28 to 31
  If any test failed then the final section is a summary of the failing tests.
  This section is only written if at least one test failed.

------------------------------------------------------------------------------

.. [#] Many of these examples deviate from good style (as, for example, defined
       in PEP-008) in order to reduce the number of lines taken up by the examples.
       This a pragmatic decision to reduce the number of lines required by example
       code to a readable minimum.

.. [#] Older versions of *CleverSheep* did support test identification based
       on method name (viz ``test_...``). However that approach is now
       deprecated and definitely not supported.


Building up Test Hierarchies
============================

Using multiple test scripts
---------------------------

In any non-trivial project, you will likely need to have more than one test
script. Moreover, these scripts may be organised into some form of directory
hierarchy. The *CleverSheep* framework provides support for executing all
or part of such hierarchies.

.. todo::
   Need to reword the following - it is clumsy and not really correct.

The framework takes note of test script, file and directory structure when
determining the hierarchy of a set of tests. At the lowest level is the suite,
as in:

.. literalinclude:: Examples/test_suite_1.py
   :linenos:

This is the basic building block for test suites. You can run this script,
asking for a summary of the tests.

.. literalinclude:: Examples/sum_test_suite_1.eg
   :language: cstest
   :linenos:

The following example illustrates when you have more then one suite
within the script:

.. literalinclude:: Examples/test_suite_2.py
   :linenos:

Then you get a different hierarchy. We now have a suite of
:term:`test suites<test suite>`.

.. literalinclude:: Examples/sum_test_suite_2.eg
   :language: cstest
   :linenos:

The next level involves having more than one test script in a directory.
So we, could arrange the previous example to be split as two files:

.. literalinclude:: Examples/suite_3/test_add.py

.. literalinclude:: Examples/suite_3/test_sub.py

Then we also need an ``all_tests.py`` script:

.. literalinclude:: Examples/suite_3/all_tests.py

.. _ref_all_tests_example:


When we use the ``all_tests.py`` script to get a summary we see:


.. literalinclude:: Examples/suite_3/sum_all_tests.eg
   :language: cstest

.. _ref_summary_example:

Notice that the top-level description comes from the ``all_tests.py``
docstring.

It is good practice to provide an ``all_tests.py`` script, even if you only
have a single test script.


Using a directory tree
----------------------

In the previous section we introduced a script called ``all_tests.py``. This is
a special name within the *CleverSheep* test framework. Specifically, the last
line of ``all_tests.py`` (``runTree()``) tells the framework to look for all
test scripts in the current and all child directories, building up a single
:term:`test hierarchy`. The framework specifically looks for a file called
``all_tests.py`` in each directory it searches and uses this to load the tests
in that directory and below.

This means that you can construct a directory tree of tests, each containing an
``all_tests.py`` script. Within any directory, you can run ``all_tests.py`` in
order to execute all the tests at or below that level, or choose to just run a
single test script within the directory.
For example, if you have a directory tree like this::

  tree/
  |-- all_tests.py
  |-- complex
  |   |-- all_tests.py
  |   |-- test_add.py
  |   |-- test_sub.py
  `-- matrix
      |-- all_tests.py
      |-- test_a.py
      `-- test_b.py

Then running ``all_tests.py`` in the ``tree`` directory will result in all
the tests in the entire tree being executed as a nested hierarchy of test
suites. But you can still switch to, say, the ``complex`` sub-directory and the
run just the tests in that part of the tree.

The output produced when you run such a hierarchy will look something like:

.. literalinclude:: Examples/tree/run_all_tests.eg
   :language: cstest

There is no practical limit to how nested such test hierarchies can become.


Ways to run your test script
============================

The test framework has a number of built in features to allow you to:

- Run selected tests.
- List tests, with various amount of information.
- Continue from the point where a previous test failed.
- Other features, yet to be described.

Any test script has built-in command line option and argument parsing. So,
for example:

.. literalinclude:: Examples/help_test_anatomy.eg
   :language: cstest


Listing information about tests
-------------------------------

The ``--summary`` option simply lists the titles of tests, producing
output like that of the :ref:`tree example<ref_summary_example>` above.
The ``--details`` option provides lots of information.

.. literalinclude:: Examples/det_test_anatomy.eg
   :language: cstest


.. _ref_test_selection:

Selecting tests
---------------

You can select which tests to run (or query) using the test numbers, for
example:

.. literalinclude:: Examples/tree/sel_num_all_tests.eg
   :language: cstest

You can also use simple string matching to select tests by title.

.. literalinclude:: Examples/tree/sel_name_all_tests.eg
   :language: cstest

Test selection also works when, for example, you want to see the details.

.. literalinclude:: Examples/tree/sel_det_all_tests.eg
   :language: cstest

Rather than simple string matching, you can use regular expressions.

.. literalinclude:: Examples/tree/sel_pat_all_tests.eg
   :language: cstest

.. todo::
   Need to provide a reference chapter on selecting tests.


.. _ref_resume_example:

Resuming a test run
-------------------

The test framework saves test execution status information in two files
``test.failures`` and ``test.states``. This allows you to resume from the
failing test. For example, suppose you are regression testing you get a
failure:

.. literalinclude:: Examples/resume/fail_test_a.eg
   :language: cstest

You fix the bug that caused test number 2 to fail and then resume the test run.

.. literalinclude:: Examples/resume/pass_test_a.eg
   :language: cstest


Continuing after failure
------------------------

The default behaviour is to stop when a test fails. When, for example,
running tests overnight you will typically want to continue after a test
failure. Use the :option:`--keep-going` option for this.

.. literalinclude:: Examples/resume/keep_going_broken.eg
   :language: cstest

The :option:`--fork-suites` and :option:`--timeout` options can also be useful
when you want to run tests under continuous integration. An example of using
a timeout is shown below:

.. _ref_timeout_example:

.. literalinclude:: Examples/timeout/timeout_timeout.eg
   :language: cstest


.. -------------------
.. vim: sw=2 spell
