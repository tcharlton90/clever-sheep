.. _sec_event_store:

========================
Using the TestEventStore
========================

The *CleverSheep* :class:`~Test.TestEventStore.EventStore` provides
support for tracking and querying the events that occur whilst exercising the
:term:`SUT`. In many ways it is little more than a list of Python objects,
but it provides a very useful abstraction when writing high-level tests.


Background
==========

The problem
-----------

Testing essentially involves poking the :term:`SUT`, recording the resultant
events and verifying that the expected events occured (and somethimes no
unexpected events occurred). However checking events can be hard because:

- Events of interest can be a number of things; log files being created,
  messages written to log files, TCP connections being made or broken,
  messages being received, processes starting or stopping, etc.

  Hence there might be a lot of different types of events that a test script
  must observe and check.

- The order in which events can be expected to occur is often not fully
  deterministic. For example, it may be perfectly OK for events A and B to
  occur in the order A then B or B then A, provide that they both occur.

- Events can be generated from more than one source.


The solution
------------

The :attr:`Test.TestEventStore.eventStore` provides a single place
where a number of mocks, log watchers, etc. can deposit events as they
occur. The event driven features allow events to be added to the store
in a way that does not interfere with the main flow of a test script.
All the test script has to do is query the store to verify that expected
events have occurred.

The event store is a time ordere FIFO. Events older that a configurable
age are discarded and the length of the FIFO can also be configured.

You can put any Python object into the store. However, it is often easiest to
use the :class:`~Test.TestEventStore.TestEvent` object, which is designed to
store information about an event and make querying easier.


Using the store
===============

Importing
---------

The typical way is to import is:

.. code-block:: python

    from CleverSheep.Test.TestEventStore import eventStore, TestEvent

The :attr:`~Test.TestEventStore.eventStore` object is a default instance of
:class:`~Test.TestEventStore.TestEventStore`, which is typically all you
need.

The :attr:`Test.TestEventStore.eventStore` provides a single store for all
events generated or detected during a test run.


Adding events
-------------

To add an event, simply use the
:meth:`~Test.TestEventStore.EventStore.addEvent` method. For most
situations using the :class:`~Test.TestEventStore.TestEvent` (or a derived
class) will be appropriate. For example:

.. code-block:: python

    eventStore.addEvent(TestEvent(message="Hello", type="greeting",
        index=msgIndex))

The keyword arguments passed to the :class:`~Test.TestEventStore.TestEvent`
constructor are stored in the event's
:attr:`~Test.TestEventStore.TestEvent.info` dictionary. You can store any
arbitrary combination of information in the event. The added event is
time stamped and appended to the end of the store's FIFO.


Finding and removing events
---------------------------

The store has a :class:`~Test.TestEventStore.EventStore.find` function, to
allow you to search the store. This provides a number of ways of checking
events in the store for a match. One way is to use a
:class:`~Test.TestEventStore.TestEvent` instance with the required
information to match. For example, to find an event matching the one stored
in the previous example you could do:

.. code-block:: python

    eventStore.find(evt=TestEvent(message="Hello", type="greeting",
        index=msgIndex))

In this case we pass it a :class:`~Test.TestEventStore.TestEvent` instance,
which has the same values as those we wish to match. The
:class:`~Test.TestEventStore.TestEvent`
class has an equality operator that does the hard work. See the
:class:`~Test.TestEventStore.EventStore.find` for more details of how to
match events.

The :meth:`~Test.TestEventStore.EventStore.find` method searches
sequentially, starting with the oldest event. So if more than one
matching event is in the store, the oldest one will be found.

Often, having found an event you will want to remove it and all earlier
events. There are two ways to do this. Firstly you can request old
events are chopped when you do the find.

.. code-block:: python

    eventStore.find(evt=TestEvent(message="Hello", type="greeting",
        index=msgIndex), chop=True)

Alternatively, you can use the :meth:`~Test.TestEventStore.EventStore.chop`
method.

.. code-block:: python

    evt = eventStore.find(evt=TestEvent(message="Hello", type="greeting",
        index=msgIndex))
    eventStore.chop(evt)

You can also remove just a single event, having found it:

.. code-block:: python

    evt = eventStore.find(evt=TestEvent(message="Hello", type="greeting",
        index=msgIndex))
    eventStore.remove(evt)


Managing the store
------------------

There are a number of things you can do to manage the the vent store.

- You can empty the store by simply invoking the
  :meth:`~Test.TestEventStore.EventStore.clear` method.

- You can change the maximum length (how many events is holds before
  old events are discarded) by setting the
  :attr:`~Test.TestEventStore.EventStore.maxLength` attribute. If you set
  this to ``None`` then the size of the store is not constrained.
  
- You can change the maximum age (how old an event may become before
  it is discarded) by setting the
  :attr:`~Test.TestEventStore.EventStore.maxAge` attribute. If you set
  this to ``None`` then the is no limit to the age of events.

- You can reset the store to its originally created state by using the
  :meth:`~Test.Table.Counters.reset` method. It is often a good idea to
  to do this in a :meth:`~Test.Tester.Suites.Suite.setUp` method.

.. warning::

   You should always try to keep the number of events in the store to
   a minimum. Searches of the store are performed linearly, starting
   with the oldest event. If you do not remove events from the store once
   they are no longer needed then:

   - You will slow down searching unnecessarily.

   - You risk false test passes because an old event might incorrectly
     match an expected event.

   Normally, the best way to keep the store tidy is by using the
   :meth:`~Test.TestEventStore.EventStore.chop` or ``chop`` parameter
   of the :meth:`~Test.TestEventStore.EventStore.find` method.


Using with control.expect
=========================

In the section :ref:`sec_using_expect`, the introduced use of the
:meth:`~Test.Tester.Suites.Controller.expect` method to wait for things to
happen. The example in that section involves waiting for a subprocess to
terminate. However, nearly any test that uses the event store needs to wait for
one or more events to occur. Once again, using
:meth:`~Test.Tester.Suites.Controller.expect` provides a clean approach.


An example system
-----------------

For this example, we will assume that the :term:`SUT` writes to a log file
(sut.log). We use a log file tracker to follow the log during a test run.
When the tracker see important entries in the log, it converts them
to events and puts them into the
:attr:`~Test.TestEventStore.EventStore.eventStore`. (Log tracking is
covered in TBD.)

There are four messages that we are interested in, which the tracker converts
to the following events::

    TestEvent(name="Startup complete")
    TestEvent(name="Normally first")
    TestEvent(name="Normally second")
    TestEvent(name="Shutdown complete")


Looking for in-order events
---------------------------

The following test script will run the :term:`SUT` and verify that the
startup and shutdown appear in sequence.

.. literalinclude:: Examples/event_store/test_using_expect.py
   :linenos:

The lines 17-18 start the log tracker and line 19 starts the :term:`SUT`
(the underlying details are not relevant for this example). Lines 21 and 22
wait for the two events to occur *in order*, allowing upto 1.0 seconds for
each event. All of the interesting work is done by the ``wait_for_event``
method.

The ``wait_for_event`` method hands over to
:meth:`~Test.Tester.Suites.Controller.expect` at line 36. This allows other
activity to occur (such as the tracker to poll the log file and add events
to the :attr:`~Test.TestEventStore.EventStore.eventStore`). The nested function
``check`` detects the arrival of the event using the
:meth:`~Test.TestEventStore.EventStore.find` method. The ``chop`` argument
to ``wait_for_event`` defaults to ``True``, which is why lines 21 and 22
check for the events in a specific order. You can easily verify this by
swapping lines 21 and 22. So we have:

.. literalinclude:: Examples/event_store/test_using_expect_1.py
   :lines: 21,22

The result is:

.. literalinclude:: Examples/event_store/run_test_using_expect_1.eg
   :language: cstest

Notice that the test fails to see the "Startup complete" event. This is because
it first waited for "Shutdown complete", which in this example did occur within
1.0 seconds. However, by this time the "Startup complete" has already occurred
and is in the event store. The call to
:meth:`~Test.TestEventStore.EventStore.find` on line 26 has ``chop`` set so it
removes the "Startup complete" event. Hence the wait on the second line times
out waiting for an event that has already been discarded.


Handling out-of-order events
----------------------------

Suppose that the two events::

    TestEvent(name="Normally first")
    TestEvent(name="Normally second")

normally occur in this order, but they tend to occur close together and can
sometimes occur in the reverse order; but always within 1 second. So, if we
want to extend the test to check for those events as well, we need to handle
this slight non-determinism. So we modify the test class to:

.. literalinclude:: Examples/event_store/test_using_expect_2.py
   :linenos:
   :lines: 12-40

On line 12, we wait for the "Normally first" event, but with ``chop=False``.
This means that when the event is detected older events are not removed.
However, the event that was found is removed; by line 21. On line 12 we wait
for the second event and this will succeed even if the event occurred first.
Notice that we only have a short timeout, because we expect these two events to
be very close together; there is not point waiting a whole second to detect a
failure.

The following is the log output for this test showing out-or-order events,
but the test still passing.

.. literalinclude:: Examples/event_store/log_test_using_expect_2.eg
