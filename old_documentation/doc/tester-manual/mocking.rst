.. _sec_mocking:

============
Mock Support
============


The *CleverSheep* framework also contains some support for mocking high level
components; i.e. other processes and external entities than communicate using
sockets. This part of the framework requires the `~Test.PollManager`.


What do mocks do?
=================

Mocks can serve a number of purposes.

1. Just keeping the :term:`SUT` happy, by (for example) making/accepting
   connections, responding normaly to messages and sending keep-alives. Thus
   the test script itself need only deal with the details of the functions
   being tested.

2. Drive the :term:`SUT` in specific ways, as required by certain tests.
   This is quite typical and tends to be a layer of functionality on top
   of purppose (1) above.

3. Provide ways of extracting information about the :term:`SUT`, by logging
   messages, responses or otherwise communicating with the :term:`SUT`.

4. Allow you to force the :term:`SUT` to experience scenarios that are
   difficult or impossible to cause using real components.

One thing that mocks in a *CleverSheep* test often do is put events into the
:attr:`~Test.TestEventStore.eventStore` so that the test script can check that
expected events occur in a timely manner.


CleverSheep Mocks
=================

The :mod:`~Test.Mock.Component` module provides the specific support. To create
a mock, you create a class that inherits from
:class:`~Test.Mock.Component.Component` and over-ride certain methods,
depending on your needs.

.. rubric:: Mocking a server

Your would typically over-ride ``onIncomingConnect``, ``onReceive`` and,
optionally, ``onClose`` and ``onError``.

.. rubric:: Mocking a client

Your would typically over-ride ``onOutgoingConnect`` and, optionally,
``onClose`` and ``onError``.


Keeping the SUT happy
=====================

The most basic thing a mock does is keep the :term:`SUT` by pretending to be
the the component in question.

It is difficult to find simple enough real examples for this king of thing.
So here as really daft contrived one.

We have an application called ``wc.py``, which takes a number of command line
arguments and prints out the total number of words in all the arguments.
However the way it is implemented os that it concatenates the arguments and
then sends the resulting string to a ``wc_server`` program, using a TCP socket.
The server counts the words and send the number back as a string. (I said it
was daft.)

We wish to test the ``wc.py`` (client) application.


The mock component
------------------

So we need to mock the ``wc_server``. The following will do this:

.. literalinclude:: Examples/mock/wc_server_mock.py
   :linenos:

Obviously this example is unrealistically trivial, but it should give some
indication of the relative ease with which mocks can be constructed. The mock
is created by inheriting from :class:`~Test.Mock.Component.Component` and we arrange for the
component to start listening for connections immediately (line 6) upon
construction. The name "Client" on line 6 is a symbolic name for the peer that
is expected to connect; the ``wc.py`` client in this case.

The :meth:`~Test.Mock.Component.Component.onIncomingConnect` method is
automatically called when a new connection is received (and accepted). We
over-ride it on line 8, just to log the connection, but otherwise need not have
bothered.

The :meth:`~Test.Mock.Component.Component.onReceive` method is automatcally
called when there is data to read from a connected socket. This mocks up the
expected behaviour; counts words and sends back the result.


The test script
---------------

Now lets look at a test script which uses this mock and does some testing of
the client.

.. literalinclude:: Examples/mock/test_wc.py
   :linenos:

We use the ``setUp`` to create an instance of the server mock. In the
``tearDown`` we make sure that the ``wc.py`` client process is stopped,
in case it fails to stop correctly during any test [#]_.

Notice that on line 15, the object ``self.control.poll`` is passed to the mock.
It is this that ensures that the mock is properly managed by the test framework

Obviously we need to run the ``wc.py`` program in order to test it. It might
seem that the obvious approach would be to use the standard 
``commands.getoutput`` function. As follows:

.. code-block:: python

   v = commands.getoutput("./wc.py %s" % sentence)
   failUnlessEqual("1", v)

This is simpler that the actual test code. However this approach has some
problems:

1. There is no guarantee that the call to ``commands.getoutput`` will complete
   meaning the test will not complete [#]_.

2. It hands control to the :term:`SUT`, which is not a good idea. The test
   script should retain control.
   
3. It will not work. The test framework uses the :mod:`~Test.PollManager`,
   running in single thread. While the script is in the ``commands.getoutput``
   call the thread cannot do anything else; i.e the mock cannot accept the
   connection from the client.

Hence we run the client as a subprocess (line 28) and then loop (lines 29, 30)
waiting for the client process to finish. This does work because the call to
``self.control.delay`` hands control to the ``self.control.poll`` object (which
is a :class:` ~Test.PollManager.PollManger` instance) for 0.1 seconds. During
this time the ``poll`` object ensures that the mock server can respond to the
connection request and subsequent message.

The ``self.control`` member is key to writing tests that require this
kind of high level mocking. Essentially the script should hand over control
to ``self.control`` whenever the script needs to wait for something to happen.
Typically, in this kind of testing, most of the elapsed time occurs under the
management of ``self.control``.

.. [#] Simplified code. In reality we should allow a grace period for the
       client to die, verify it has terminated and terminate with extreme
       prejudice if necessary.

.. _sec_using_expect:

Doing things properly
---------------------

The above test script does work, but is it a bit simplistic. One major problem
that you may have spotted is that the loop on lines 29 and 30 will not
terminate if the ``wc.py`` client fails to stop. This can, of course be
fairly easily fixed by looping a fixed number of times:

.. code-block:: python

    for i in range(10):
        if self.client.poll() is not None:
            break
        self.control.delay(0.1)
    else:
        # We looped 10 times, the client did not terminate.
        fail("Client failed to terminate after about 1 second")

This can be a suitable approach in some instances, but the framework provides
a more elegant approach. For this we add some helper methods and change the
test method.

.. code-block:: python
   :linenos:

    def client_has_terminated(self):
        return self.client.poll() is not None

    @test
    def single_word(self):
        """Single sentence, single word."""
        def describe_timeout_failure():
            return "Client failed to terminate after about %.1f seconds" % timeout

        sentence = "Hello"
        f = open("client.log", "w")
        self.client = subprocess.Popen(["/usr/bin/python", "./wc.py", sentence], stdout=f)
        timeout = 0.1
        self.control.expect(timeout, self.client_has_terminated,
                describe_timeout_failure)
        f.close()

        v = open("client.log").read().strip()
        failUnlessEqual("1", v)

This may look more complicated (in some ways it is), but is is arguably cleaner
and more elegant. For example, we no longer have a loop so the script reads
much more like a sequence of steps.

At line 14 the script hands control over to ``self.control`` by calling the
``expect`` method. The first argument ``1.0`` tells the method that we expect
something to happen within ``1.0`` seconds. The second argument provides a
function that can be used to check if the event has happened. The final
argument provides a function that will return a suitable error message if the
event fails to happen. The ``client_has_terminated`` method will get called
occasionally (about 50 times a second) and it *must* return a ``True`` value
if it determines that the event has occurred.

The ``self.control.expect`` method will return as soon as the
``client_has_terminated`` returns a ``True`` value, it will only wait the full
second if the event does not occur, in which case it will make the test fail.

Notice that the ``describe_timeout_failure`` function is nested inside the test
method. This is quite common in test methods because it allows (as in this
example) the generated message to include details of the failure context; the
``timeout`` period in this case.


.. [#] Actually the framework has support for timing out tests, but this is
       not the sort of thing it is intended for.
