============================
The Tester Package Reference
============================

Introduction
============

This provides the top-level reference manual for the :mod:`Tester<Test.Tester>`
package. Of course you can just look at the :mod:`API<Test.Tester>`, but not
all of the automatically documented API is really public. Hopefully, starting
from here will better guide you towards the "real" API.


.. toctree::
   :maxdepth: 1

   test-anatomy.rst


The Main Attributes
===================

The tester package is designed to be typically imported as:

.. code-block:: python

    from CleverSheep.Test.Tester import *

The ``import *`` on line 2 is (rightly) generally considered poor style in
Python modules, but within the context of test scripts, I think it keeps things
less clunky and all the examples assume this approach.

Only a subset of the Tester's names are imported by the ``import *``, so for
some features you also need to import ``Tester`` itself.

.. code-block:: python

    from CleverSheep.Test import Tester


Exported Names
---------------

The names imported by the ``import *`` form are:

:obj:`Suite <Test.Tester.Suite>`
    The main test suite class, which your are encouraged to use to collect
    test functions (as instance methods).

:obj:`test <Test.Tester.test>`
    The decorator, which is used to identify methods or (if you wish) functions
    [#]_ be be executed as automated tests.

:obj:`log <Test.Tester.log>`
    Provides access to the functions that write to the test log. You need these
    to write anything other than debug (for which you can use ``sys.stdout``
    and errors (sys.stderr)).

:obj:`console <Test.Tester.console>`
    Allows you to force output to appear on the console/terminal (since
    ``sys.stdout``/``sys.stderr`` are redirected to the log. This can be useful
    when debugging tests, but generally messes up the output.

    This is a ``file`` like object and it is recommended that you use its ``write``
    method rather than ``print >>console, ...``.

fail*
    All the built-in ``fail...`` assertions functions, such as
    :obj:`failUnlessEqual <Test.Tester.failUnlessEqual>`.

:obj:`importModuleUnderTest <Test.Tester.importModuleUnderTest>`
    Used to import *the* Python module targeted by a test script, in
    order to ensure complete coverage reports.

:obj:`runModule <Test.Tester.runModule>`
    The function to put in the ``__main__`` section to run the tests in
    a module. As in:

    .. code-block:: python

      if __name__ == "__main__":
            runModule()

    See TBD for more details.

:obj:`runTree <Test.Tester.runTree>`
    Performs a similar role to `runModule <Test.Tester.runModule>`, but this
    is used in an ``all_tests.py`` module.

    See :ref:`an example of all_tests.py<ref_discovery>`.

:obj:`currentTestHasFailed() <Test.Tester.currentTestHasFailed>`
    
    Use this in a ``tearDown`` method to determine if the current test has just
    failed.


Other Global Names
------------------

The :obj:`Tester <Test.Tester>` package also makes the following objects
available as module attributes.

:obj:`~Test.Tester.currentTestInfo`
    Provides access to information about the currently executing test.
    TBD - Add details.

:obj:`~Test.Tester.Execution.addTestFilter`
    Registers a function to be used to select which defined tests are actually OK
    to run.

:obj:`~Test.Tester.Discovery.addModulesUnderTest`
    Primarily intended for internal use. This adds Python modules to the set of
    modules that contain test suites.

:obj:`~Test.Tester.Fixture.add_testStateChangeCallback`
    Registers a function to be called whenever the state of a test changes,
    for example from not-run to passed.

:obj:`~Test.Tester.Execution.setDefaultTimeout`
    Used to define a period within which the run of all tests must complete.
    After this period the test run is stopped, failing the executing test.

:obj:`~Test.Tester.Execution.executeTest`
    Provides direct access for executing a single test.

    This is only for specialised use.

:obj:`~Test.Tester.Execution.add_option`
    Use to add command line options in addition to the default set provided
    by the test framework.

:obj:`~Test.Tester.Execution.add_testRunEndCallback`
    Registers a function to be called when no more tests can be run.

Test state codes
    These are: ``NOT_RUN``, ``PASS``, ``FAIL``, ``ERROR``, ``BAD_SETUP``,
    ``BAD_TEARDOWN``, ``CHILD_FAIL``, ``INCOMPLETE``, ``SKIPPED``,
    ``DISABLED``, ``BROKEN``, ``BAD_SUITE_SETUP``, ``BAD_SUITE_TEARDOWN``,
    ``SUITE_RUNNING``, ``SUITE_CRASH``.

    Most of the names are fairly self explanatory.
    

.. _ref_discovery:

Test Discovery
==============

The test framework provides automated test discovery. For any non-trivial
project, you will likely make use of this. To do this you simply need a script
in each directory which should be called ``all_tests.py`` [#]_ and contains
code like the following example:

.. code-block:: python

    #!/usr/bin/env python
    """All of my tests"""

    from CleverSheep.Test.Tester import *

    if __name__ == "__main__":
        Tester.runTree()

The docstring at the top is important. It is used as the title of the
collection of tests within the directory and below.

When you run this the framework will find all the test modules in a directory
tree, starting in the current directory, using the following basic rules:

1. Any python modules in the directory with a name of the form ``test_<some-
   name>.py`` will be loaded and all test suites added to the test tree.

2. For each sub-directory, if there is a file called ``all_tests.py`` then
   import it. Then recursively perform step 1, (above).

Thus an entire directory tree of tests is loaded. Then (by default) either all
tests or those selected on the command line,  executed until a test failure is
encountered.

The reason for loading any ``all_tests.py`` module in step 2 is because that
script can modify the test discovery behaviour, for example by excluding some
sub-directories from the search. It is also a good idea to have an
``all_tests.py`` in each directory, just for the convenience of being able to
run the sub-test tree from any level, without needing to identify the name of a
test script.


Controlling Test Order
----------------------

The test discovery mechanism provides a natural default ordering for the
execution of a test tree.

1. Higher level directories get loaded and run before lower level directories.

2. Sub-directories are loaded in lexicographical order.

3. The ``test_<some-name>.py`` files within a directory are loaded and executed
   in lexical order.

4. Within a module the ordering is set by the ordering of Suites and methods
   within the file [#]_.

You can, if required, partially over-ride the ordering created by rule 3,
by setting the ``_run_after`` variable in test modules.

This is quite simple. For example is you wish to ensure that
``test_aardvark.py`` gets run after ``test_the_larch.py`` then simply put:

.. code-block:: python

  _run_after = ["test_the_larch.py"]

in your ``test_aardvark.py`` module. And the framework will try to arrange
that your preferred ordering is used. The value can be a list of modules that
should preferably be run before the module in question. The names are relative
to the parent directory of the test module. You should note the following:

- If you manage to set up a circular dependence (run A after B after C after A)
  then which module runs first is undefined; so you may get ABC or BCA or CAB .
  The selected running order may not even be the same each time you run the
  tests.

- You **should not** use this to set up a suite of tests where one test depends
  on the side effects of an earlier test. This feature is intended to support
  the situation where some tests exercise low level functions so it makes sense
  to run them before those that target higher level functions. That way early
  failures tend to lead to swifter diagnosis of the true problem.

  As a general rule, it should be possible to run any single test in isolation.
  [#]_.


The Test Framework Phases
=========================

This is partly background, but might be useful.

When you execute an ``all_tests.py`` script this is what happens (to a first
approximation). There are three main phases:

1. Test discovery and suite building.

2. Execution of the entire suite hierarchy.

3. Final reporting.


Phase 1
-------

Phase 1 is mostly covered in :ref:`ref_discovery`. Each
:class:`~Test.Tester.Suites.Suite` found in a test script is instantiated and
then the test methods are identified along with the
:meth:`~Test.Tester.Suites.Suite.setUp`
:meth:`~Test.Tester.Suites.Suite.tearDown`
:meth:`~Test.Tester.Suites.Suite.suiteSetUp`
:meth:`~Test.Tester.Suites.Suite.suiteTearDown` methods. All of these are
combined into a :class:`~Test.Tester.Fixture.TestSuite`, which effectively
provides the :term:`fixture` for all that group or tests.

Since the
:class:`~Test.Tester.Suites.Suite` is instantiated, all tests within the suite
have the same ``self`` which, for example makes it easy for the
:meth:`~Test.Tester.Suites.suiteSetUp` method to create data to be used by all
tests in a suite. Each instantiated test method is stored in a
:class:`~Test.Tester.Fixture.Test` instance. A :class:`~Test.Tester.Fixture.Test`
stores the test method, plus its set-up and tear-down, this providing the
:term:`fixture` for an individual test.

If there is only a single :class:`~Test.Tester.Suites.Suite` in a module then
the created suite is the suite for the module. Otherwise all
:class:`~Test.Tester.Suites.Suite` instances in the module are combined into
another :class:`~Test.Tester.Suites.Suite`; i.e. the module forms a suite of
suites.

All the suites built from individual modules in a directory are combined
into a :class:`~Test.Tester.Suites.Suite`. If there is an ``all_test.py`` in
the directory then its docstring provides the stuite's title, otherwise a
(thoroughly unsatisfactory) title based on the directory name is generated.

Within a directory, all suites created from sub-directories are combined
into suites; and so on.

Once the entire hierarchy has been created, a second stage of processing walks
the entire tree to select the actual tests to be executed depending on the
command line arguments and registered test selection functions.


Phase 2
-------

This, unsurprisingly, basically consists of walking the entire hierarchy,
executing each suite that contains at least one selected test. For each executed
suite there are three sub-phases:

2.1 Run the suite's :meth:`~Test.Tester.Suites.Suite.suiteSetUp`

2.2 Run the suite's tests.

2.3 Run the suite's :meth:`~Test.Tester.Suites.Suite.suiteTearDown`

If phase 2.1 fails for any reason then phase 2.2 is not executed and the entire
suite of tests is considered to have fails, but phase 2.3 is executed. If phase
2.3 fails then the framework will issue dire warnings about the fact that later
suites may fail as a consequence.


Phase 2.2
---------

This executes each individual test within a suite. For each test there are
three sub-phases:

2.2.1 Run the suite's :meth:`~Test.Tester.Suites.Suite.setUp`

2.2.2 Run the test method.

2.2.3 Run the suite's :meth:`~Test.Tester.Suites.Suite.tearDown`

If phase 2.2.1 fails for any reason then phase 2.2.2 is not executed and the
entire suite of tests is considered to have fails, but phase 2.2.3 is executed.
If phase 2.2.3 fails then the framework will issue dire warnings about the fact
that later tests may fail as a consequence.


Phase 3
-------

As each test is run, information about the result are stored in
:class:`~Test.Tester.Execution.Result` instance. This information is then
stored in a :class:`~Test.Tester.Execution.ResultDetails` object, which also
stores the result of the :meth:`~Test.Tester.Suites.Suite.setUp` and
:meth:`~Test.Tester.Suites.Suite.tearDown` phases. This information is
attached to each :class:`~Test.Tester.Fixture.Test` instance.

The information thus tracked is used to provide a test failure summary on the
console and a detailed test result in the ``test.log``.

-------------------------------------

.. [#] Author's note. When I first created this framework, I wanted to support
       using top-level functions to define tests - so that it was not necessary
       to wrap your tests up into a class.

       However, my experience of writing a lot of tests within and without
       Suite classes has led me to the conclusion that not using the Suite
       classes gains almost nothing. Even for a single test, it is a trivial
       amount of effort to wrap it into a Suite. So most of the documentation
       assumes the use of Suites as the default approach.

.. [#] Actually, the ``all_tests.py`` script is not strictly necessary, the
       framework will recursively find ``test_...py`` scripts. However, I
       believe that this starts to stray into the realm of 'too much magic' and
       is best avoided. Also it is generally good practice to provide an
       ``all_tests.py`` script in each directory. This means that at every
       point in your test hierarchy, you can always simply do:
       ::

         ./all_tests.py

       to run part of your test tree.

.. [#] The framework used to support discovery of tests within a module
       using a naming convention of ``Test_...`` for a suite and ``test_...``
       for a test method/function. Suite and test ordering using this method
       is lexical.
       
       This approach is now deprecated in favour of using the ``@test(...)``
       decorator and basing suites on the on the
       :obj:`Suite <Suite<Test.Tester.Suite>` class. The running order
       when mixing the two mechanisms is undefined.

.. [#] A planned enhancement is to support running tests in random order.
       This is a good way to check you have not accidentally introduced
       any order dependencies.

.. -------------------
.. vim: sw=2 spell
