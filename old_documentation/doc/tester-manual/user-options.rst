.. _sec_user_options:

================================
Adding user command line options
================================

The test framework handles command line option and argument parsing. However,
it is often useful to have custom command line options, so the framework
provides for this with what are called "user options".

The framework's command line option parsing uses the standard ``optarse``
module. This means you can do quite powerful option parsing, but it also means
that you might want to read the Python ``optparse`` documentation as well. This
section assumes som familiarity with ``optparse``.


Defining user options
=====================

This is pretty much like defining options using ``optparse``. The framework
provides an ``add_option`` function. For example:

.. literalinclude:: Examples/options/test_options.py
   :lines: 7-10

The options are available via the :attr:`~Test.Tester.userOptions` object.
This can be used in a test, method or the set up and tear down methods.

.. literalinclude:: Examples/options/test_options.py
   :lines: 20-26

The help includes the user-options:

.. literalinclude:: Examples/options/help_test_options.eg

And you can get just the user options:

.. literalinclude:: Examples/options/help_z_test_options.eg

In general, you can mix standard and user options on the command line. Where
ther is a conflict, the standard option is assumed. So, for example, if you put
``--keep`` on the command line, the framework will treat it as ``--keep-going``
*not* ``--keep-temp-files``. If you want to be sure that an option is
interpreted as a user option then use the ``-z`` flag to terminate the standard
options. So ``-z --keep`` will be treated as ``--keep-temp-files``. It is
recommended that you always use the ``-z`` flag for any formal execution, for
example when running tests using a continuous integration tool.

.. note::

    You can define user options in any test script in a hierarchy. This can
    be convenient; i.e. if just one test needs an option, you can define
    it in that test's script. When you run all tests at a given point in a test
    hierarchy, all options defined at or below that point will be available.

    Within a hierarchy you cannot define the same option more than once.


Uses of user options
====================

Obviously you can use them for any purpose, but some common uses are:

- As shown above, to control behaviour like whether to save temporary files,
  which can be useful to analyse failing tests.

- To set the level of verbosity for the messages written to the test log.

- To control which tests are selected for execution. See :ref:`sec_test_marking`
  for how to do this.
