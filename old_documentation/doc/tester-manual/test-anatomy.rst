=====================
Detailed test anatomy
=====================

The :ref:`tester introduction<sec_basic_tests>` provides information on the
basics of writing tests, test suite and test hierarchies. This part of the
documentation focuses on the formal and more advanced details of writing tests.


Building Blocks
===============

Scripts and test suites
-----------------------

The most basic building block is the test script. All scripts should have file
names of the form ``test_*.py``. They should be executable and they should have the
following basic outline:

.. code-block:: python

    #!/usr/local/bin python
    """<Script title>

    <Script details>

    """
    __docformat__ == "restructuredtext"

    from CleverSheep.Test.Tester import *


    class suite_name_1(Suite):
        """<Suite title>

        <Suite details>

        """

    class suite_name_2(Suite):
        """<Suite title> ... """


    if __name__ == "__main__":
        runModule()

The script itself forms a suite of test suites. Each class derived from the
:obj:`Suite<CleverSheep.Test.Tester.Suite>` class forms a suite of tests.
The call to ``runModule`` at the end of the script is what makes this script
one that will be executed under the contol of *CleverSheep*.

Notice that each script and suite should be given docstrings. The title
is mandatory because that gets displayed when the test is run. The details
are optional, but are recommended for non-trivial tests.

.. sidebar:: Proper docs

   As personal note. My view is that you should be able to read a *CleverSheep*
   test script and be able to understand what it tests, why and how. The same
   goes for each suite within the script and each test method within the
   script. And that is what the docstrings should help you to do.

   Along with comments within each test method, the script should be able to
   provide the same level of detail (or better) as a test specification. (A
   long term aim for *CleverSheep* is to make such scripts the starting point
   for writing specifications, by providing document generation from scripts.)

The names of the classes should be meaningful, but the title is most important.

You can have other top-level functions and non-Suite based classes in the
suite; in order to provide support for your tests.

The line that reads ``__docformat__ == "restructuredtext"`` should be included,
but it currently has no effect. If you care about documentation you will
include it.

The form ``from CleverSheep.Test.Tester import *`` is recommended for test
script, but not any helper modules; for those use ``from CleverSheep.Test
import Tester``. The distinction is that a test script can exepct to operate in
the test framework environment, but helper scripts *use* the ``Tester`` module.


Test and fixture methods
------------------------

The main building block within a test suite is a test method. Each test suite
class may have one or more such methods [#]_.

.. code-block:: python

    class suite_name_1(Suite):
        """<Suite title>

        <Suite details>

        """
        @test
        def test_1_name(self):
            """<Test title>

            <Test details>

            """

        @test
        def test_2_name(self):
            """ ... """

A test method is identified by the ``@test`` decorator. Only methods marked in
this way are treated as tests. The same rules/advice applie to the naming and
docstrings or test methods as for suite classes above.

The other main building blocks within a test suite class are those providing fixture
support. These are the (optional) methods:

setUp
  This invoked before each test in the suite is executed.

tearDown
  This invoked after each test in the suite is executed, even if the test fails or
  crashes.

suiteSetUp
  This invoked before the first test selected to run in the suite is executed.

suiteTearDown
  This invoked when all tests within the suite have executed or been skipped.

Each method takes no arguments.

Some important things to note about the fixture methods.

1. Unexpected errors are handled by the framework. So, for example, if the
   suiteSetUp raises an exception, the suiteTearDown will still be executed
   (although in this case, no tests within the suite will be run).

2. You should endevour to make tearDown and suiteTearDown methods very robust.
   If these fail unexpectedly, the framework will continue to run tests, but
   a bad tear down can easily cause subsequent tests to run incorrectly.

3. In case it is not obvious, the tear down methods are primarily there for you
   to clean up so that any subsequent test cannot be affected by whichever test
   preceded it. However, they can also be useful for doing things like storing
   log files for later analysis (in the event of failure).

4. The suiteSetUp and suiteTearDown methods are intended for more specialist use.
   In general, if you can just use setUp and tearDown then do so.

   In unit testing, if you find yourself thinking that you need a suiteSetUp then
   the chances are you are doing somthing wrong.

All other methods in a test suite class are ignored by the framework. It is
very common to have a number of helper methods within a suite.


The all_tests script
--------------------

For any non-trivial set of tests [#]_ it is a good idea to put an
``all_tests.py`` script in each directory containing ``test.*.py`` scrripts,
even if there is only one. This script looks like:

.. code-block:: python

    #!/usr/bin/env python
    """<Title for all tests in this directory>
    
    <High level description of all tests in this directory>
    
    """
    __docformat__ == "restructuredtext"

    from CleverSheep.Test.Tester import *

    if __name__ == "__main__":
        runTree()

Within any directory of a tree of test scripts, executing this script will
discover all tests within the directory, any child directories, grandchild
directories, etc.
        

Test assertions
===============

Obviously, any useful test has to be able to verify that the code or system
under test has operated correctly. The framework provides a set of assertion
functions for this purpose. These are made available by the ``from
CleverSheep.Test.Tester import *`` statement.

The assertion functions all tend to follow a similar pattern.

- They all start with ``fail``.

- They tend to take a common set of keyword arguments, when it makes sense.

- Any expected value arguments preced actual value arguments.

This will become clear below.

The simplest assertion function is simply ``fail``. This is invoked as:

.. code-block:: python

    fail("The test has failed because of quantum")

This is typically used in support functions and methods, rather that test
methods. Invoking this causes the current test to unconditionally fail. The
argument is a (``message``) string describing the reason for the failure. You
can make this as detatiled as you want, including newlines for formatting. The
``message`` is printed by the framework - on the terminal and in the test log.

All other assertion functions take arguments that are examined to determine
whether or not the test has failed. Example showing the two common forms are:

.. code-block:: python

    failUnless(some_condition)
    failUnlessEqual(expect, actual)

It is easy to figure out what these do; ie. the first fails if the
``condition`` evaluates as ``False`` and the second if ``expect`` does not
compare as equal to ``actual``.

All the standard assertion functions are documentes under
:obj:`assertions<Test.Tester.Assertions>`, see those descriptions for details.
What follows describes the general features and how to use them.

Where possible, assertion functions provide a standard failure message. For
example::

    failUnlessEqual: 13 != 14

The failure is always followed by a traceback, allowing you to pinpoint the
test method and line where the failure was detected.

Often the default message may not provide enough information to easily
determine the failure condition.

.. sidebar:: Proper failure messages

   One of the issues *CleverSheep* tries to address is that nobody realy cares
   when a test passes. It is when a test fails, particularly when regression
   testing, that you discover a simple 'failed' is not very helpful.

   That is why *CleverSheep* provides facilities to generate really useful
   failure messages. Use them: your peers/colleagues will respect you in the
   morning.

   BTW: If you do proper TDD then you will find providing good failure
   messages becomes much easier, because every test you write will fail at
   least once. If you do not do proper TDD, but understand why it is a good idea
   then at least contrive to make every one of your tests fail at least once.

All except the more specialised assertion functions take additional keyword
arguments to support better message generation. These are:

message, msg
  This is a simple string, describing the failure. This is like the single
  paramter to ``fail`` above. In the future the ``msg`` argument may be
  deprecated, so please use ``message``.

makeMessage
  This is a function, taking no parameters, which **must** return a string
  describing the reason for failure.

The ``message`` parameter is generally easier to use. An example where it
enhances the error reporting might be:

.. code-block:: python

    for i, res in enumerate(results):
        failUnlessEqual(expected[i], res,
            message="Index = %d, Expected = %r, actual = %r" % (
                i, expected[i], res))

Which will tell you which of the result values was incorrect.

However the above example formats the ``message`` string for every call to the
``failUnlessEqual`` function. Sometimes this can drastically slow down a test.
In such circumstances, if is more efficient to use the ``makeMessage``
argument. In this case the above test code would look like:

.. code-block:: python

    @test
    def to_a_test(self):
        """..."""
        def err():
            return "Index = %d, Expected = %r, actual = %r" % (
                i, expected[i], res)

        for i, res in enumerate(results):
            failUnlessEqual(expected[i], res, makeMessage=err)

In this  case, the ``failUnlessEqual`` will only invoke the ``err`` function in
the event of test failure. Sometimes this can make a huge difference to how
long tests take to run.

Note that the function ``err`` in the above example, is a nested function
(within the test method). This a common pattern for functions passed as the
``makeMessage`` argument.


The control member
==================

This is only an introduction; see TBD for more details.

In any test suite, you always have a member called ``control``. This provides
access to an underlying test :meth:`~Test.Tester.Suites.Suite.control`
property. This object knows how to manage other mocks, timers, etc. yet makes
it realtively easy for you to write test methods that appear to (naturally)
follow a linear sequence of actions and checks.

Apart from the setup and teardown methods, the ``control`` property is the only
other special name within a test suite. In other words, you are free to create
any attributes, methods or propeties in your test suite, without unduly
worrying that things might break in a newer version of *CleverSheep*.

The ``control`` member provides a number of methods, each of which takes
temporary control of the test script's execution. For any non-trivial test,
it is common to invoke ``control`` methods directly or indirectly.

The most simple example is:

.. code-block:: python

    @test
    def to_a_test(self):
        """..."""
        self.control.delay(5)

This will pause the flow of the executing test for 5 seconds. However during
those 5 seconds:

- Any mocks will be able to interact with the :term:`SUT`.
  
- Any registered timouts will be handled.

- It is possible to detect a failure condition and abort the test.

Note that this is nothing like simply invoking ``time.sleep(5)``.


.. [#] A class with no test methods is not treated as a test suite by the
       framework; it is effectively ignored.

.. [#] Meaning pretty much **any** set of tests.
