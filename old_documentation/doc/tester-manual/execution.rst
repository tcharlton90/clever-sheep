.. _sec_running_tests:

=============
Running tests
=============


Selecting tests
---------------

On the command line you can select only certain tests to be run, using test
numbers; or strings or regular expressions to match against test
descriptions/summaries.

You can provide a number of tests and include ranges. For example::

        3-6 10 1 20-

Will execute tests 1, 3, 4, 5, 6 and 10 in that order, followed by all the tests
from 20 onwards.

Simple strings are searched for in the summary of each test. So, for example
::

    ./all_tests.py complex imag

Will find all tests that contain 'complex' or 'imaj' in the summary. If you want
to search the entire description (including the summary) then prefix the string
with ``d:``, as in::

    ./all_tests.py d:complex d:imag

You can also use regular expressions, by starting the string with a tilde
(``~``) character. As in::

    ./all_tests.py 'd:~complex.*imag'

For completeness, you can also prefix a string with "s:" to indicate that you
only wish to search the summary.

You can, of course, mix numbers, strings and regular expressions on the command
line.

Some examples of test selection can be found :ref:`here<ref_test_selection>`.


.. rubric:: Limitations

Currently there is no good support, for example, finding strings that start with
"d:".


Using command line options
--------------------------

Any *CleverSheep* test script provides standard command line option and
argument processing. You can get a quick summary of the options by running
any script with the :option:`--help` option.

.. literalinclude:: Examples/help_test_anatomy.eg
   :language: cstest

This shows quite a few options. At the time of writing not all of these are
officially supported (or even working). So, for now, please ignore the
``--gui``, ``--list-tested``, ``-e, --erase`` ``-r, --report`` ``-a,
--annotate`` ``-c, --cover`` ``--c-cover`` ``--c-erase`` ``--c-all`` options
[#]_.

Note that you can abbreviate long options, provided that the option can
be uniquely identified.

Here are the detailed explanations of each supported option.

.. option:: --help, -h

   Displays a summay of the command syntax and options.
   
.. option:: --summary

   This will display a summary of the tests in your hierarchy. The title
   of each suite is printed and the title of each test, preceded by a number
   is displayed. The output is nested so you can easily see the hierarchy.

   If you select only some tests on the command line then then the summary will
   show only the matching tests.

.. option:: --ids-summary

   Similar to --summary, but it also displays a unique identification string
   for each test. These strings can be used in a test.disabled file to
   temporarily prevent some tests from being executed, see TBD for more
   details.

.. option:: --details

   Prints a detailed display of the test hierarchy. The full docstring of
   each test and suite is displayed. Also, the path, class and method name
   is shown for each test.

   This can be useful when you are trying to remember where a specific test
   is in a big hierarchy.

.. option:: -i, --ignore-case:

   Ignore case when selecting tests using text string.

.. option:: --resume

   Start you test run from the test that failed on the previous run.
   See :ref:`resuming tests<ref_resume_example>` for some examples.

.. option:: --fork-suites
   
   Runs each suite containing test methods in a separately forked process.
   This means that, if any test in the suite causes an uncatchable error then
   the following suites will still run.

   In practice this is only really needed if there is the possibility of a test
   crashing the Python interpreter. All other conditions should be gracefully
   handled by the framework.

.. option:: --timeout=T

   This will force the test run to fail if it does not complete within T seconds.
   The time must be a whole number of seconds.

   See :ref:`timing out a test run<ref_timeout_example>` for an example.
   
.. option:: --no-forking

   Prevent the framework from forking a new process to run test suites.

   It is possible to mark specific suites as forkable (see TBD for details).
   Where possible, the framework will execute all tests in such suites in
   forked sub-processes. This option will force all suites to be run without
   forking.

.. option:: --no-disabled

   The forces any tests identified in a local ``test.disabled`` file to be run.

.. option:: --keep-going

   This will ensure that all tests get executed, i.e. the test run keeps going
   after any test afilures.

.. option:: --log-file=PATH

   This sets the name of the test log file to ``PATH``. The default is otherwise
   simply ``test.log``.

.. option:: --columns=COL

   Normally the framework will try to figure out the width of the terminal and
   format its output to fit. If the terminal width cannot be determined then 80
   characters is assumed. You can use this option to set the width you want.

.. option:: -n, --skip-run
   
   Do not actually run any tests. This is actually only of use with the
   coverage support options, which are currently not supported nor documented
   here.

.. option:: -z

   This option does not have a direct effect. Its job it to separate user
   (or custom) options from the above standard test framework options. Any
   option after this will only be compared to user options.

   See :ref:`sec_user_options` for details on user options.


.. [#] Apart from ``--gui`` these do serve useful purposes, but they should be
       treated as experimental. They are also likely to change in the future.

