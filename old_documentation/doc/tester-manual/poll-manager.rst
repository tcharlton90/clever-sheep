.. _sec_poll_manager:

======================
The Tester PollManager
======================

The *CleverSheep* framework contains a relatively general purpose component
called the :mod:`~Test.PollManager`. It is part of the test framework for
historical reasons [#]_.

This component provides an event driven (reactor) framework for programs that
need to interact with other asynchonous entities; typically communicating via
sockets or pipes. It basically uses the standard ``select.poll`` interface to
provide a higher level event driven framework.

Note that, when you need to write tests that must to run an event driven way,
there is no need to specifically use the PollManager; the framework
automatically uses it as necessary. You just need to write your tests in a
suitable manner. This is covered in more details in :ref:`sec_mocking`.

.. [#] Because the test framework needed it. It will probably move to a more
       sensible default place in a future version.


Basic use
=========

The simplest way to use the PollManager is for timeouts, as shown in the
following trivial program:

.. literalinclude:: Examples/poll/timeouts_1.py
   :linenos:

It should be fairly ovious what this will do. The ``tick`` callback function is
called repeatedly every 0.3 seconds. The ``finish`` callback function is called
after 1.0 seconds and it tells the ``manager`` to quit; i.e. return from the
call to ``run`` on line 14. So we expect to see the work 'tick' 3 times.

.. literalinclude:: Examples/poll/run_timeouts_1.eg
   :linenos:

More usefully, you can also also ask the PollManager to monitor open files for
activity and invoke callback functions in response. The following, toy program
will accept a TCP connection and then echo anything anything it receives.

.. literalinclude:: Examples/poll/echo_server.py
   :linenos:

You can have many sockets and timeout active at any time. The PollManager does
all the housekeeping and invokes callbacks as appropriate.


General rules
=============

There is obviously much information on event driven programming already written.
What follows is simply tailored to the this framework.

Event driven programming can be simpler than using threads because there is no
need to implement any resource locking and so no risk of deadlock, livelock,
*etc.* [#]_. However, there are some rules you need to follow in order to keep
event driven programs responsive.

1. Callback functions (ones passed to ``addInputCallback``,
   ``addInputCallback``, etc.) should execute quickly. If a callback takes,
   say, several seconds then during those seconds then the program willl be
   unresponsive to any other inputs (appearing to temporarily hang).

   In general callbacks should take a smallish fraction of a second at most.

2. Callbacks that handle file input, should normally try to process all pending
   input, but (of course) without unnecessarily violating the previous rule.
   Often this is as simple as reading from a socket until there is no more
   data left to read. However, if the input data rate can occasionally be heavy
   then you might perhaps do (say) 100 reads at a time, thus allowing other
   events and I/O to get serviced. A more general approach is to track
   executions time and stop processing the input after (say) 100 ms.
   
   See `More about sockets and pipes`_ for more details of the how to handle
   input correctly.

3. Use ``addOutputCallback``/``POLLOUT`` with care. This means when your socket
   or pipe's buffers are full.

   If you, for example, use ``addOutputCallback`` on a socket that can accept
   data for writing then your callback function will get called repeatedly,
   effectively making you program busy-wait on the socket [#]_.

   Generally, you should only set callbacks for output when you get an
   ``EAGAIN`` error. And the callback should remove the POLLOUT callback -
   unless it also gets ``EAGAIN``.

   See `More about sockets and pipes`_ for more details of the how to handle
   input correctly.

4. Set sockets and pipes to non-blocking mode. Otherwise a code may block on a
   socket operation then no other processing can occur until the block clears.

.. [#] On the down-side most program logic occurs within callback functions,
       which can lead to difficult to follow code.

.. [#] Although, admittedly whilst still processing other sockets and handling
       timeouts. In other words, you can get away with it, but your program
       will use lots of CPU, scale poorly and warm the planet.


More about sockets and pipes
============================

As mentioned earlier, sockets should be non-blocking as should open pipes [#]_.
To make a socket non-blocking use:

    .. code-block:: python

        my_socket.setblocking(0)

Note: it is neither necessary nor desirable to do this for a listening socket. 

For pipes, *TBD - copy from example code I have lying around*.

Non-blocking I/O needs some careful handling (a small price for avoiding
threads and locking mechanisms). For reading a socket you should have logic
something like this in your input callback.:

   .. code-block:: python

      # WARNING: Unproven code. 

      def handle_intput(fd, event, sock):
          while True:
              try:
                  data = sock.recv(1024)
              except IOError, exc:
                  if exc.errno == errno.EAGAIN:
                      # No more data to read, so stop trying
                      return
                  # Something bad has happened - handle it!

              if len(data) == 0:
                  # The socket has closed.
                  manager.removeInputCallback(fd)
                  return

              # Something was read, process it.

For socket output you should have (somewhat more complex) logic something like:

   .. code-block:: python

      # WARNING: Unproven code. 

      def write(self, sock, data):
          output_buf.append(data)
          if self.out_fid:
              return
          self.send_all()

      def send_all(self):
          while output_buf:
              try:
                  sock.send(output_buf[0])
                  output_buf.pop(0)
              except IOError, exc:
                  if exc.errno == errno.EAGAIN:
                      # Output buffer is full.
                      self.out_fid = self.manager.addOutputCallback(
                                                        sock, onOutputReady)
                      return

                  # Something bad has happened - handle it!

          if self.out_fid:
              self.manager.removeOutputCallback(self.out_fid)
              self.out_fid = None

        def onOutputReady(self, sock, data):
            self.manager.removeOutputCallback(self.out_fid)
            self.out_fid = None
            self.writesock, data)

The :mod:`~Test.Mock.Component` module provides a much higher level interface,
which handles most of these complexities. :ref:`sec_mocking` provides information
on how to create mocks.

.. [#] Not an option on Windows. Currently CleverSheep does not provide anything
       in the way of support for the Window's way of handling non-socket based
       communication file.

Using other frameworks
======================

The built-in `PollManager` implementation is fairly functional, but there are a
number of alternative frameworks, which provide very similar features. The
`PollManager` can be made to use some of these alternatives rather than its
built-in implementation. Currenty the supported frameworks are:

- Tornado
- Twisted

A common reason for doing this would be when using CleverSheep to test code
that is designed to use one of these alternatives.
::

     _____ ___  ____   ___  
    |_   _/ _ \|  _ \ / _ \ 
      | || | | | | | | | | |
      | || |_| | |_| | |_| |
      |_| \___/|____/ \___/ 

    I have no proof-of-concept code that runs tests of Tornado/Twisted code
    under CleverSheep.
