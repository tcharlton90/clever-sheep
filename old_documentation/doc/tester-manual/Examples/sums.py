"""My sums module"""

def add(a, b):
    """Add two numbers in a dumb and broken way."""
    r = a
    for i in range(b):
        r += 1
    return r + 1

def sub(a, b):
    """Subtract two numbers in the obvious way."""
    return a - b
