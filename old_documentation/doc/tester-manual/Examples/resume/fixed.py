#!/usr/bin/env python
"""Core functional tests for the matrix library."""

from CleverSheep.Test.Tester import *


class Basic(Suite):
    """Basic tests."""
    @test
    def test_1(self):
        """Creation of a matrix."""

    @test
    def test_2(self):
        """Creations of several matrices"""


class BinaryOps(Suite):
    """Binary matrix operations."""
    @test
    def test_1(self):
        """Addition of two matrices."""

    @test
    def test_2(self):
        """Subtraction of two martices."""


if __name__ == "__main__":
    runModule()
