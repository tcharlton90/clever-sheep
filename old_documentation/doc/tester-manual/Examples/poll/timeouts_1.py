from CleverSheep.Test import PollManager

def tick():
    print("tick")

def finish():
    manager.quit()

manager = PollManager.PollManager()
manager.addRepeatingTimeout(0.3, tick)
manager.addTimeout(1.0, finish)

# Invoke the manager's run method to start event driven execution.
manager.run()
print("The end")
