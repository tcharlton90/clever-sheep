#! /usr/bin/env python
"""Tests for word count client (wc.py)."""

import os
import subprocess

from CleverSheep.Test.Tester import *

import wc_server_mock        # The mock component.


class WcClient(Suite):
    """Noddy tests for word counting client."""
    def setUp(self):
        self.server = wc_server_mock.Server(self.control.poll)
        self.client = None

    def tearDown(self):
        if self.client:
            if self.client.poll() is None:
                os.kill(self.client.pid, 15)

    def client_has_terminated(self):
        return self.client.poll() is not None

    @test
    def single_word(self):
        """Single sentence, single word."""
        def describe_timeout_failure():
            return "Client failed to terminate after about %.1f seconds" % (
                    timeout)

        sentence = "Hello"
        f = open("client.log", "w")
        self.client = subprocess.Popen(["/usr/bin/python", "./wc.py", sentence], stdout=f)
        timeout = 1.0
        self.control.expect(timeout, self.client_has_terminated,
                describe_timeout_failure)
        f.close()

        v = open("client.log").read().strip()
        failUnlessEqual("1", v)


if __name__ == "__main__":
    runModule()
