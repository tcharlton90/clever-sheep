#! /usr/bin/env python
"""Tests for word count client (wc.py)."""

import os
import subprocess

from CleverSheep.Test.Tester import *

import wc_server_mock        # The mock component.


class WcClient(Suite):
    """Noddy tests for word counting client."""
    def setUp(self):
        self.server = wc_server_mock.Server(self.control.poll)
        self.client = None

    def tearDown(self):
        if self.client:
            if self.client.poll() is None:
                os.kill(self.client.pid, 15)

    @test
    def single_word(self):
        """Single sentence, single word."""
        sentence = "Hello"
        f = open("client.log", "w")
        self.client = subprocess.Popen(["/usr/bin/python", "./wc.py", sentence], stdout=f)
        while self.client.poll() is None:
            self.control.delay(0.1)
        f.close()

        v = open("client.log").read().strip()
        failUnlessEqual("1", v)

    # More tests here...


if __name__ == "__main__":
    runModule()
