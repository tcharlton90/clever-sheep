from CleverSheep.Test.Mock import Component
from CleverSheep.Test.PollManager import PollManager

class Server(Component.Component):
    def onIncomingConnect(self, s, peerName):
        print("Connect from", peerName)

    def onReceive(self, conn):
        data = conn.read()
        print("From %s: %r`" % (conn.peerName, data))
        n = len(data.split())
        self.sendTo(conn.peerName, "%d" % n)


p = PollManager()
server = Server(p)
server.listenFor("CLIENT", ("localhost", 7899))

p.run()
