from CleverSheep.Test.Mock import Component

class Server(Component.Component):
    def __init__(self, *args, **kwargs):
        super(Server, self).__init__(*args, **kwargs)
        self.listenFor("Client", ("localhost", 7899))

    def onIncomingConnect(self, s, peerName):
        print("Connect from", peerName)

    def onReceive(self, conn):
        data = conn.read()
        print("From %s: %r`" % (conn.peerName, data))
        n = len(data.split())
        self.sendTo(conn.peerName, "%d" % n)
