#!/usr/bin/env python
"""Example simpler server done the hard way.

This uses the PollManager to create a server.

"""


import socket

from CleverSheep.Test.PollManager import PollManager

def onConnectAttempt(fd, event, s):
    global connection
    connection, addr = s.accept()
    p.addInputCallback(connection, onReceive, connection)


def onReceive(fd, event, connection):
    data = connection.recv(1024)
    print("Recv %r`" % data)
    connection.send("%s back" % data)


p = PollManager()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(("localhost", 7899))
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

s.listen(1)

p.addInputCallback(s, onConnectAttempt, s)
p.run()

