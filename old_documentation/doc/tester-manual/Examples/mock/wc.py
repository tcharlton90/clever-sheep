#!/usr/bin/env python
import sys

from CleverSheep.Test.Mock import Component
from CleverSheep.Test.PollManager import PollManager

class Client(Component.Component):
    def write(self):
        if "SERVER" in self.connections:
            self.sendTo("SERVER", cmdline_phrase)
        sys.stdout.flush()

    def onReceive(self, conn):
        data = conn.read()
        print(data)
        sys.exit()


p = PollManager()
client = Client(p)
client.connectTo("SERVER", ("localhost", 7899))
p.addRepeatingTimeout(0.1, client.write)

cmdline_phrase = " ".join(sys.argv[1:])
p.run()

