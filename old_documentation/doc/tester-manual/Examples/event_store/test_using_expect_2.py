#!/usr/bin/env python
"""Test example, using expect to wait for event store events."""


from CleverSheep.Test.Tester import *
from CleverSheep.Test.TestEventStore import eventStore, TestEvent

import tracker
import sut_control


class EventEg(Suite):
    """Another example."""
    @test
    def wait_for_events(self):
        """Expect start up and then shutdown."""
        self.tracker = tracker.Tracker(self.control.poll, "sut.log")
        self.tracker.start()
        sut_control.start_sut()

        self.wait_for_event(1.0, name="Startup complete")

        self.wait_for_event(1.0, name="Normally first", chop=False)
        self.wait_for_event(0.1, name="Normally second")

        self.wait_for_event(1.0, name="Shutdown complete")

    def wait_for_event(self, maxWait, chop=True, **kwargs):
        def check():
            found = eventStore.find(expect, chop=chop)
            if found and not chop:
                eventStore.remove(found)
            return found

        def err():
            return ("Event matching %r did not occur within %.2f seconds"
                    % (kwargs, maxWait))

        expect = TestEvent(**kwargs)
        self.control.expect(maxWait, check, err)


if __name__ == "__main__":
    runModule()
