from CleverSheep.Test.TestEventStore import eventStore, TestEvent

events = [

    TestEvent(name="Startup complete"),
    TestEvent(name="Normally second"),
    TestEvent(name="Normally first"),
    TestEvent(name="Shutdown complete"),
]

class Tracker(object):
    """Spoof of a log file tracker."""
    def __init__(self, poll, *args, **kwargs):
        self.poll = poll

    def start(self):
        self.tid = self.poll.addRepeatingTimeout(0.2, self.writeEvent)

    def writeEvent(self):
        ev = events.pop(0)
        eventStore.addEvent(ev)
        print("Event %r occurred" % ev.name)
        if len(events) == 2:
            ev = events.pop(0)
            eventStore.addEvent(ev)
            print("Event %r occurred" % ev.name)
        if not events:
            self.poll.removeTimeout(self.tid)
