#! /usr/bin/env python
"""Example of using user options. """

import os

from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester

Tester.add_option("--keep-temp-files", action="store_true",
    help="Do no delete temporary file when tests complete.")

class Example(Suite):
    """Selectively runnable tests."""
    def add_temp_file(self, path):
        self.temp_files.append(path)

    def setUp(self):
        self.temp_files = []

    def tearDown(self):
        if not Tester.userOptions.keep_temp_files:
            for path in self.temp_files:
                try:
                    os.unlink(path)
                except OSError:
                    print("Could not delete", path)

    @test
    def test_1(self):
        """A test."""
        t_path = "/tmp/abc.txt"
        self.add_temp_file(t_path)
        f = open(t_path, "w")

if __name__ == "__main__":
    runModule()
