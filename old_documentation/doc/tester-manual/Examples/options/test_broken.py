#! /usr/bin/env python
"""Example of using user options. """

import time

from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester

def select_by_time(func, info):
    if None in (Tester.userOptions.max_time, info.approx_exec_time):
        return True
    if info.approx_exec_time > Tester.userOptions.max_time:
        return False
    return True


Tester.add_option("--max_time", action="store", type="int", metavar="MAX",
    help="Limit test selection to tests that take no more than MAX seconds.")

Tester.addTestFilter(select_by_time)

class Example(Suite):
    """Selectively runnable tests."""
    @test("broken")
    def test_1(self):
        """A test."""
        time.sleep(5)

    @test
    def test_2(self):
        """Another test."""
        time.sleep(1)

if __name__ == "__main__":
    time.sleep = lambda x: None
    runModule()
