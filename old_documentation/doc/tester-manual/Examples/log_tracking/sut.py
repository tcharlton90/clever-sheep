#!/usr/bin/env python
"""A script to play the part of the system under tests.

This SUT writes a log files as it executes. It is the contents
of the log file that we are interested in.

"""



import time


def main():
    log = open("sut.log", "w", 1)
    time.sleep(0.2)
    log.write("Info: Initiated connection\n")
    time.sleep(0.2)
    log.write("Info: Connection established\n")
    time.sleep(0.3)
    log.write("Info: Sending request\n")
    log.write("Debug: awaiting response\n")
    log.write("Debug: awaiting response\n")
    time.sleep(0.1)
    log.write("Info: Request rejected\n")


main()
