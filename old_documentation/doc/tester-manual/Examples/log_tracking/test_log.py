#! /usr/bin/env python
"""Tests for fictional word count client."""

import os
import subprocess

from CleverSheep.Test.Tester import *
from CleverSheep.Test import LogTracker
from CleverSheep.Test.TestEventStore import eventStore, TestEvent

class SUTLogTracker(LogTracker.LineLogTracker):
    def onRead(self, code):
        for line in self.lines:
            if line.startswith("Info: "):
                ev = TestEvent(msg=line)
                eventStore.addEvent(ev)
                print(line)
        self.lines[:] = []


class WcClient(Suite):
    """Noddy tests for word counting client."""
    def setUp(self):
        self.client = None
        self.tracker = SUTLogTracker(self.control.poll, "sut.log")
        self.client = subprocess.Popen(["./sut.py"])

    def tearDown(self):
        if self.client:
            if self.client.poll() is None:
                os.kill(self.client.pid, 15)

    @test
    def request_failure(self):
        """Verify a request failure is correctly handled."""
        self.wait_for_log_message(1.0, "Info: Initiated connection")
        self.wait_for_log_message(1.0, "Info: Connection established")
        self.wait_for_log_message(1.0, "Info: Sending request")
        self.wait_for_log_message(1.0, "Info: Request rejected")

    def wait_for_log_message(self, maxWait, msg):
        def check():
            return eventStore.find(expect, chop=True)

        def err():
            return ("SUT did not log expected message within %.2f seconds"
                    "\n  Expected: %r" % (maxWait, msg))

        expect = TestEvent(msg=msg)
        self.control.expect(maxWait, check, err)


if __name__ == "__main__":
    runModule()
