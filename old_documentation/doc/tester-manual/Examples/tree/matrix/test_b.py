#!/usr/bin/env python
"""Set B of tests for the matrix library."""

from CleverSheep.Test import Tester
from CleverSheep.Test.Tester import *


class Eigen(Suite):
    """Eigenvalues and vectors.
    
    As last used at Uni.
    """
    @test
    def test_1(self):
        """Calculate eigenvalue."""

    @test("sys:wibble", q=5)
    def test_2(self):
        """Calculate eigenvector.
        
        I used to know what this was!
        """

if __name__ == "__main__":
    runModule()
