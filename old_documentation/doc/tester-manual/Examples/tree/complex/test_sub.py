#!/usr/bin/env python
"""A very simple and unrealistic test module.

This tests a thoroughly mythical and pointless ``sums`` module.
"""

from CleverSheep.Test.Tester import *


class Subtraction(Suite):
    """Test subtraction"""
    @test
    def subtract_2_numbers(self):
        """Verify correct subtraction for some simple values."""

    @test
    def subtract_2_other_numbers(self):
        """Verify correct subtraction for other simple values."""


if __name__ == "__main__":
    runModule()
