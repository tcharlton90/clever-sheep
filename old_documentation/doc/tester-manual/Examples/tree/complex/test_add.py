#!/usr/bin/env python
"""A very simple and unrealistic test module.

This tests a thoroughly mythical and pointless ``sums`` module.
"""

from CleverSheep.Test.Tester import *


class Addition(Suite):
    """Test subtraction"""
    @test
    def add_2_numbers(self):
        """Verify correct addition for some simple values."""


if __name__ == "__main__":
    runModule()
