#!/usr/bin/env python
"""A very simple and unrealistic test module.

This tests a thoroughly mythical and pointless ``sums`` module.
"""

from CleverSheep.Test.Tester import *
import sums


class Subtraction(Suite):
    """Test subtraction"""
    @test
    def subtract_2_numbers(self):
        """Verify correct subtraction for some simple values."""
        failUnlessEqual(-5, sums.sub(4, 9))

    @test
    def subtract_2_other_numbers(self):
        """Verify correct subtraction for other simple values."""
        failUnlessEqual(1, sums.sub(10, 9))


if __name__ == "__main__":
    runModule()
