#!/usr/bin/env python
"""A very simple and unrealistic test module.

This tests a thoroughly mythical and pointless ``sums`` module.
"""

from CleverSheep.Test.Tester import *
import sums

class Addition(Suite):
    """Test addition"""
    def suiteSetUp(self): pass
    def suiteTearDown(self): pass
    def setUp(self): pass
    def tearDown(self): pass

    @test
    def add_2_numbers(self):
        """Verify correct addition for some simple values."""
        print("Testing addition")
        failUnlessEqual(2, sums.add(1, 1))

    @test
    def add_2_more_numbers(self):
        """Verify correct addition for more values."""
        failUnlessEqual(0, sums.add(-1, 1))

class Subtraction(Suite):
    """Test subtraction"""
    @test
    def subtract_2_numbers(self):
        """Verify correct subtraction for some simple values."""
        print("Testing subtraction")
        failUnlessEqual(-5, sums.sub(4, 9))

if __name__ == "__main__":
    runModule()
