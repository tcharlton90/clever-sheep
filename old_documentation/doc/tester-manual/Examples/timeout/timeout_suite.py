#!/usr/bin/env python
"""A timeout set for a suite."""

import time

from CleverSheep.Test.Tester import *


class Subtraction(Suite):
    """Test subtraction"""
    Timeout = 2

    @test
    def sleep(self):
        """Sleep for 2 seconds."""
        time.sleep(3)


if __name__ == "__main__":
    runModule()
