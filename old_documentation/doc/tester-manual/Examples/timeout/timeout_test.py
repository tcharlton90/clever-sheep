#!/usr/bin/env python
"""A timeout set for a suite."""

import time

from CleverSheep.Test.Tester import *
from CleverSheep.Test import Tester


class Subtraction(Suite):
    """Test subtraction"""

    @test("timeout:2")
    def sleep_1(self):
        """This takes 1 second."""
        time.sleep(1)

    @test("timeout:2")
    def sleep_2(self):
        """This should take no more than 2 seconds."""
        time.sleep(3)


if __name__ == "__main__":
    runModule()
