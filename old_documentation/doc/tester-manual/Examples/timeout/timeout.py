#!/usr/bin/env python
"""A test that takes too long to complete."""

import time

from CleverSheep.Test.Tester import *


class Subtraction(Suite):
    """Test subtraction"""
    @test
    def sleep(self):
        """Sleep for 2 seconds."""
        time.sleep(3)


if __name__ == "__main__":
    runModule()
