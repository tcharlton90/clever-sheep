#! /usr/bin/env python
"""A very simple and unrealistic test module.

This tests a thouroughly mythical and pointless ``sums`` module.
"""

from CleverSheep.Test.Tester import *

import sums

class Example(Suite):
    """Some example tests."""
    def suiteSetUp(self): pass
    def suiteTearDown(self): pass
    def setUp(self): pass
    def tearDown(self): pass

    @test
    def add_2_numbers(self):
        """Verify correct addition for some simple values."""
        failUnlessEqual(2, sums.add(1, 1))

# Strandard boilerplate that arranges for all tests to run.
if __name__ == "__main__":
    runModule()
