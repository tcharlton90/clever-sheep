=================
All about Testing
=================

Introduction
============

This is intended to be a lightweight paper on testing, heavily biased towards
the features in CleverSheep that help support testing in an automated fashion.


Testing Theory
==============

This is not in any way formal. The theory is basically, you poke the system
under test (SUT), see what happens and verify that it is what should have
happened. The type of things that happen can include:

- Actual outputs; messages, files (including log files) created/appended to.

- State changes; a database being updated, the internal state of the SUT
  being modified in some well defined way.

For the purposes of this discussion, all things that can happen are referred
to as events. For poking, read providing stimuli.

.. .. ---------------------------------------------------------------------------
.. .. sidebar:: White Box/Black Box
..    :subtitle: Unit, Module, System -- Who Cares?
.. 
..    As far as this discussion and the *CleverSheep* is concerned these
..    categories of testing are not important. The important idea is the
..    you stimulate the SUT and check the events. In white box testing, for
..    example, an event may be some state change within the SUT, but it is still
..    a kind of event.
.. .. ---------------------------------------------------------------------------


A 'Test Rig'
------------

So a general test rig, with system under test, might look something like::

   ,--------.            ,=======.      ,----------.
   |        |            |       |      |          |'
   | Driver |--Stimuli-->|  SUT  |<---->|  Player  || 
   |        |            |       |      |          ||
   `--------'            `======='      `----------'|
        |                    ^           `----|-----'
        |                    :             ^  |
        |                    :             |  |
        |------------------] : [-----------'  |
        |                    :                |
        |                    :                v
        |                    :           ---------------
        |------------------] : [-------->  Event Store
        |                    :           ---------------
        |                    :                ^    ^
        |                ,-------.            |    :
        |                |       |            |    :
        |--------------->| Probe |------------'    :
        |                |       |                 :
        |                `-------'                 :
        v                                          :
   ,----------.                                    :
   |          |                                    :
   | Checker  |- - - - - - - - - - - - - - - - - - '
   |          |
   `----------'
         |                   
         v
       Test
       Results

   - Solid lines (----) indicate information sent in the direction of the arrow.
   - Dotted lines (- - - ) indicate queries sent in the direction of the
     arrow; for example the Checker queries the Event Store.

Apart from the SUT, the other components are:

  The Driver
    This controls the execution of the test. for some types of test it will
    directly provide stimuli to the SUT.

    A test driver can be, and generally is, a complex beast that provides the
    necessary fixture (set-up and tear-down) for each of a suit of test cases.

  The Players
    There can be zero or more of these. They take the role of things that the
    SUT would normally expect to interact with. In general, they need to be
    able to interact with the SUT in a largely autonomous manner. For example
    providing acknowledgements, responding to requests, etc.

    Players, store interesting information in the event store.

    Players are the other source of stimuli to the SUT.

  The Event Store
    We need some where to store information about the events that have
    occurred during a test. This may seem to be an unnecessary abstraction,
    but hopefully its purpose will become clear later.

    The store is a FIFO, that provides a window on recent events. The window
    may be time limited, number of events limited or both.

  The Probe
    You need one of these for any form of white box testing. It is able to
    query (peek inside) the SUT and store state changes as events in the event
    store.

  The Checker
    This performs the verification part of a test, which essentially involves
    querying the event store to check that the expected events have occurred,
    when the should have occurred.

The Driver, sends information or instructions to most of the other components.

  Players
    Information about which events are currently interesting, so the Players
    can copy useful information into the Event Store. Also instructions on
    how to interact with the SUT, for example, 'do not acknowledge the next
    XXX request'.

  Event Store
    Usefule events can include actions taken by the driver. In general
    anything (except the SUT) should be able to append to the Event Store.

  Probe
    To tell the probe what it should currently be probing for.

  Checker
    So the checker knows what to expect in the event store, in order to
    determine whether a test has passed/failed.


Why so Complex?
---------------

The above 'test rig' is complex so that it can, in theory, to handle the
general case of an SUT that interacts asynchronously with a number of other
components. The Players act as other copmonents, providing the necessary
asynchronous behaviour that a complex SUT requires.

For example, in a system level test, the SUT might a computer that communicates
with  a number of other computers, acting as client to some and server to
others. In such an scenario, the Player would act as the other computers, the
Probe might not be used or perhaps query the SUT using some back-door provided
for test purposes.

The separate Event Store provides a clean abstraction and decouples stimuli
from events. Since it is a FIFO, we can handle the general (and common)
situation where expected events may not always occur in the same order. It
provides a short term history that can be queried with or without regard to
precise ordering of events.

Of course, some tests do not require such complexity, but the 'rig' is easily
simplified. For example, a unit test:

  - Does not need Players, the Driver provides all the stmuli by invoking
    the units functions.
  - The Event Store is also redundant, events are the returns from the invoked
    functions.
  - The Checker becomes little more than a toolkit of checking functions.
  - The Probe may not exist or otherwise is another set of functions that
    peek into the SUT.


Enter Clever Sheep
==================

The CleverSheep test modules and packages are intended to help you create
the various parts of the general 'test rig'. There is not a precise mapping
but to a first approximation we have:

  +-----------------+-----------------------------------------------------+
  | The Driver      | The :mod:`Tester<Test.Tester>` sub-package.         |
  +-----------------+-----------------------------------------------------+
  | The Players     | The :mod:`Mock` sub-package. This is not currently  |
  |                 | well defined, but may become so in the near future. |
  +-----------------+-----------------------------------------------------+
  | The Event Store | The :mod:`TestEventStore` module, in the            |
  |                 | :mod:`Test` package.                                |
  +-----------------+-----------------------------------------------------+
  | The Probe       | The :mod:`FileTracker` module in the :mod:`Log`     |
  |                 | package.                                            |
  |                 | Otherwise, you are on your own, but essentially if  |
  |                 | you can figure out how to track what happens then   |
  |                 | you can probe things and store the details as       |
  |                 | events.                                             |
  +-----------------+-----------------------------------------------------+
  | The Checker     | The assertion functions in the                      |
  |                 | :mod:`Tester<Test.Tester>` sub-package.             |
  |                 | You can, of course, add your own checks then simply |
  |                 | use the low level ``fail`` assertion.               |
  +-----------------+-----------------------------------------------------+


Unit Testing
------------

For this, the :mod:`Tester<Test.Tester>` sub-project provides the required framework. It
provides the necessary infrastructure required for a test Driver; such as
providing suitable fixtures (setup and tear-down). It also has a rich set of
assertion functions that formalise the cheking of results, as well as helping
to provide detailed and useful desciptions of failure conditions.


Component and System Testing
----------------------------

Basically, anything in which the SUT is a process, set of processes, etc.

For this situation, the :mod:`Tester<Test.Tester>` package provides the PollTester module,
which in turn provides the :mod:`PollTester` class. This is layered on on top
of the core Tester functionality and uses the :mod:`PollManager` to manage the
various components in the test harness, including each test as it runs.

Most tests at this level also need to provide Players as stubs/mock
componnents. Such Players, more oftent than not, will interact communicate with
the SUT using sockets. The :mod:`Mock` sub-package provides a toolkit to help
write such players. The :mod:`Component` and :mod:`Comms` modules contained in
this sub-package are designed to integrate well with test that are run using
the :mod:`PollTester`. When the messages to/from the SUT are in binary form,
based on C structures, the :mod:`Struct` module makes it easy to describe,
build, decode and query such messages.

This is also the area where an Event Store is really applicable, to provide a
short term window on recent messages, for example. The :mod:`TestEventStore`
module provides a generic event store, which can be constrained by time and/or
number of events.

Probe functionality tends to be a bit more test specific, but the
:mod:`FileTracker` module can be useful here. You can use it to monitor log
files produced by the SUT and store significant entries in the Event Store.


Testing Strategy
================

This section makes some suggestions on useful approaches to testing.
Some things may seem blindingly obvious; as aways your mileage may vary.


Unit Testing C/C++ Code
-----------------------

CleverSheep is written in Python, which means that you obviously cannot unit
test C [#]_ code without some effort. Essentially, you need to wrap the C code
to make it callable from Python. Fortunately this can largely be automated and
I would recommend SWIG for wrapping code to be unit tested.

Even with the automated code wrapping, the extra effort is significant: so why
bother? There are a number of possible reasons:

  - Obviously you can user :mod:`Tester<Test.Tester>`, which provides a ready made,
    consistent framework. Compared to pure hand-crafted tests or (my
    understanding of) common C/C++ unit testing frameworks, :mod:`Tester<Test.Tester>`
    provides a number of useful advantages, including:

      - Very easy to build up a big hierarchy of automated tests.

      - Test selection from the command line.

      - Ability to provide detailed information of test failures.

      - Easy to re-run just failing test and/or temporarily disable some tests.

  - Python is a much more efficient language to write in. So it is possible to
    write tests with less effort. Of course, this depends on the type of code
    being tested, but the benefits can be huge.

  - You can write and run tests without the need to compile and link every
    time.

  - Tests written in Python will tend to be easier to read, maintain and are
    more self-documenting, especially when using the :mod:`Tester<Test.Tester>` package.

Of course, at the end of the day, there is a trade off to be made, so testing C
code from Python may not be the best choice.

.. [#] Assume C means C/C++ in this section.


Modelling
---------

In order to test something you have to be able to predict what it will do.
Sometimes this can be most easily achieved by modelling the SUT within your
test harness. Verification then becomes a simple matter of comparing the SUT;s
state with your models state.

The approach obviously depends on a reasonable amount of independence between
the SUT and the model of the SUT. This is a situation where testing C code
from Python works well. It is often very much easier to model a something in
Python than it is to write the equivalent thing in C. Also the very fact that
the model is in a different language greatly reduces the chance that the model
will contain the same errors as the SUT; even if written by the same person
[#]_.

This really is a viable and useful approach. As a simple example, the
following is a snippet of real test code [#]_, used to test a linked list
implementation,

.. literalinclude:: Examples/modelling-example.py


In this case the model is trivially easy, it is the list called
``expectTuples``. Since this uses the built in Python list type, we can be very
sure of correct behaviour. The ``removeAtIdx`` local function is used to
perform each operation on both the SUT and the ``expectTuples`` list. After
each operation the two are compared (element by element) by the ``check``
function.

As a more extreme example, again taken from experience. There was project that
involved the implementation of a virtual machine (VM). The real (C version)
involved several man-months of design and coding effort and required a high-
level of testing. The model based approach was used, involving a Python
implementation of the VM. The VM took about 1 man-week to implement fully and
about 2 days to have be good enough for initial testing. Test verification was
as simple as comparing the memory of the model to the memory of the real VM. It
is difficult to imagine how the same level of testing could otherwise have been
achieved.


.. [#] Obviously some care is required, but if you want maintainable and
       extensible tests, you *will* write code to generate expected results.
       All automated testing needs some effort to validate the tests.

.. [#] The test is real, but in reality the linked list implementation was
       written as a demonstration.
