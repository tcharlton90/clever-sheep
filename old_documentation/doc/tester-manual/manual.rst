.. _s_manual:

==========
The Manual
==========

This introduces the features of the framework and provides examples of
how to use most of them. It can be read from start to end.

The section :ref:`sec_basic_tests` provides an introduction to the basics
of test writing and creating test hierarchies. :ref:`sec_running_tests`
provides information the different ways you can run tests scripts, such as
selecting to execute only some tests. :ref:`sec_model` describes the
informal testing model that is behind the structure of the framework.

:ref:`sec_poll_manager` introduces the :mod:`~Test.PollManager` module, which
is central to the way the framework operates for higher level testing.
:ref:`sec_mocking` describes how to use the framework to write semi-autonomous
mock components, which rely on the the :mod:`~Test.PollManager` described in
the preceding section. :ref:`sec_event_store` describes how to
to track and check that expected events occur; allowing for
natural variation in event order. The section :ref:`sec_log_tracker` shows how
to use the event store and log tracking support to actively verify the contents
of a log files, whilst a test is executing. This is the last of the main
framework building blocks.

:ref:`sec_user_options` introduces one of the more advanced features; adding
you own options to control how tests are run from the command line.
:ref:`sec_test_marking` show how you can tag and flag individual tests with
meta-information to do things like set test timeouts.

.. toctree:: :maxdepth: 1

   tester-intro.rst
   execution.rst
   tester-model.rst
   poll-manager.rst
   mocking.rst
   event-store.rst
   log-tracker.rst
   user-options.rst
   test-marking.rst
