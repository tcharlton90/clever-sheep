.. _sec_test_marking:

==================
Using test marking
==================

You will already be aware that a test method is identified by putting the
:prop:`Test.Tester.test` decorator on the line above the ``def ...``. As in:

.. code-block:: python

    @test
    def string_concatenation(self):
        """Yada ..."""

However, the :prop:`Test.Tester.test` decorator also supports marking
a test with meta-data. Tests can be marked with flags and tags. Flags are
understood and used by the framework (see :ref:`sec_flags`) and tags are
arbitrary attributes that can be used as the test writer see fit.

An example of tagging a test:

.. literalinclude:: Examples/options/test_sel.py
   :lines: 24-26

The recommended way to tag tests is using the keyword form, as in the above
example. Tag names should always begin with a lower case letter, upper case
letters are reserved for framework use; as are names beginning "cs\_".

The framework does not place any meaning on tags added in this way. The meaning
of such tags is left up to the test script writer. The rest of this section
illustrates some common uses.


Controlling which tests get run
===============================

One way that this can be useful is to provide special ways to select which
tests are run. For example, suppose you have a large set of tests which you
tend to run very often [#]_, but some tests take an inconvenient length of time
to run to completion.

So, as in the earlier example,  let's tag slow tests with information about how
long slow tests take to run. Then we can can add a test selection filter to
ignore tests that slow us down. The following is from a working (albeit
pointless) example (expanded later):

.. literalinclude:: Examples/options/test_sel.py
   :linenos:
   :lines: 1-34,36-

In this example, ``test_1`` is slow, so it is tagged as taking about 5 seconds,
on line 25. We need a way of setting a maximum time on the command line, so we
add a user option on lines 18-19. Finally, we need to use the option to select
tests, so we write a filter function (lines 10-15) and register it (line 20).

Now we can run all the tests:

.. literalinclude:: Examples/options/all_test_sel.eg

Or just the quicker ones:

.. literalinclude:: Examples/options/sel_test_sel.eg

The test filter function ``select_by_time`` has to take two parameters. The
first (``func``) is the (bound) test method and the second (``info``) is
meta-data about the test method. The ``func`` parameter can be useful for
advanced usage, but is typically ignored. The ``info`` parameter carries the
tagging information applied using the :prop:`Test.Tester.test` decorator.
For convenience (and to be consistent with user options) any keyword argument
suppplied to the :prop:`Test.Tester.test` decorator can be accessed as an
attribute of the ``info``. Unknown attributes yield the ``None`` value, so for
``test_2``, ``info.approx_exec_time`` is ``None`` at line 10.

.. Note::

   You should not mark tests with names the begin ``cs_``. Names of this
   form are reserved for future use by the ``info`` object.
   

Built-in test filtering
-----------------------

The framework has some built-in filtering that is applied at test collection
time.

cs_is_applicable
    If supplied, this must be a function that takes two parameters ``instance,
    test``. The instance is the test suite class instance and the test is a
    :class:`Test.Tester.Core.Test` that wraps the test function.

    If this function does not return a true value then the test is considered
    inapplicable and is not added to the test collection.


Information about the current test
==================================

The test meta-data for the currently executing test can also be obtained using
the :func:`Test.Tester.currentTestInfo` function. This can be used during test
setup, the test method itself, test tear down and suite tear down. It is most
useful in the set-up and tear-down methods, **but I cannot remember why!**


.. _sec_flags:

Test flags
==========

Test flags are intended to allow you to signal information about tests to the
framework. Currently there are two defined flags:

- broken

- timeout


Flag: broken
------------

The ``broken`` flag allows you mark a test as (currently broken) for some
reason. For example:

    During a development stage some large change might temporarily cause some
    tests to  fail, but you expect them to start passing once the the
    development has finished.

    You employ a continuous integration system, which therefore starts to
    report test failures. You use the ``broken`` flag to mark test you expect
    to fail, thus ensuring that and test failure reports only identify
    unexpected failures.

The ``broken`` flag is set by simply pass the string "broken" as a non-keyword
argument to the :prop:`Test.Tester.test` decorator.

.. literalinclude:: Examples/options/test_broken.py
   :lines: 24-26

Broken tests are not executed (there would be no point).

.. literalinclude:: Examples/options/run_test_broken.eg
   :language: cstest

But, as shown above they are not completely ignored, in that they are noted and
counted. They are also listed in the ``test.log`` summary [#]_:

.. literalinclude:: Examples/options/log_test_broken.eg

The effect of providing any other non-keyword arguments to the
:prop:`~Test.Tester.test`  decorator is undefined.


Flag: timeout
-------------

The ``timeout`` flag allows you to impose a timeout on any individual test.

.. literalinclude:: Examples/timeout/timeout_test.py
   :lines: 18-20

The value is a whole number of seconds. If a test does not finish before the
timeout then it fails.

.. literalinclude:: Examples/timeout/run_timeout_test.eg
   :language: cstest


.. [#] Doing TDD for example.

.. [#] The log file is designed to provide an 'official' record of a test
       run. Hence it identifies tests that were not run, for any reason.
