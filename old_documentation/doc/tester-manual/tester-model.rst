.. _sec_model:

==========================
The CleverSheep Test Model
==========================

Introduction
============

The *CleverSheep* test framework was originally developed to support testing
above the unit level. It has been used in the following scenarios:

- Tesing of single programs.

- Testing of systems running on a single computer.

- Testing of systems running on multiple computers; some forming the system
  under test and others being part of the test harness.

The framework is based upon the common pattern used in unit testing frameworks,
but in order to accommodate higher level testing scenarios and the problems
they raise, *CleverSheep* has evolved to include various additional features.
There is nothing particularly special or formal, but understanding the model
can help understand the way the test framework works.

What follows is a sanitised and idealised version of how the *CleverSheep*
tester model emerged/evolved. The 'true'/'correct'/'actual' version would
be, both, even less interesting and impossible to accurately reproduce.


The Model
=========

What is testing?
----------------

Whilst it can be reasonably argued that testing is difficult [#]_, it is also
fundamentally quite simple. Any test bench consists of two things:

- The system under test (SUT).
- The thing that tests the SUT.

And every test essentially follows the same pattern, which can be (trivially)
represented by the following example::

  Poke the SUT with event A
  Expect event B.
  Expect event C.
  Poke the SUT with event D
  Expect event E.
  ... and so on

Poking the SUT is simply a matter of sending it messages, calling functions,
*etc*. Expecting events is simply a matter of watching what the SUT does. The
difficulty (or complexity) arises from a number of factors, including.

Firstly any, some or all of:

    - The SUT.
    - Event A, Event B, *etc*.
    - 'Poke' is a non-trivial operation.

    Can be non-trivial to describe or implement.

Secondly.

    It might be perfectly acceptable for ``event C`` to happen *before*
    ``event B``.

Thirdly.
    
    Managing to do all of this automatically is not trivial.

Furthermore.

- Any complex SUT may need to interact with a number of other systems and will
  only behave as predicted provided those other subsystems do the right thing
  for the test scenario. In practice this means the harness must typically play the
  part of those other systems, in order to drive the system through all possible
  execution paths.

- The interaction with the other systems can be (essentially) asynchronous.

- Things do not necessarily happen in the same order every time. Sometimes
  event C may (validly) occur before event B. Both sequences "B then C" and "C then B"
  must be interpreted as correct behaviour.

There are some other common, non-trivial to handle features, such as when the
output events are actually information written to log files.

.. [#] Because it is!

The Full Model
--------------

So, given the above factors, the *CleverSheep* test framework is designed to
support something more like this:

.. aafig::
   :aspect: 50
   :proportional:
   :textual:
   :foreground: #0022aa

   /--------\            +=======+        /---------\
   |        |            |       |        |         |
   |        |  Stimuli   |       +<-------+         |
   | Driver +----------->+  SUT  |        |   Mock  | 
   |        |            |       +>------#+         |
   |        |            |       |        |         |
   \----+---/            +===+===+        \--+--+---/
        |                    v               ^  | 
        |                    |               |  |
        |     "Commands"     |               |  |
        +--------------------|---------------/  |
        |                    |                  |
        |                    |                  o
        |                    |             -----+-----
        +--------------------|-----------o Event Store
        |                    |             ---+----+--
        |                    #                o   v
        |                /---+---\            |   |
        |    "Commands"  |       |            |   |
        +--------------->+ Probe +------------/   |
        |                |       |                |
        |                \-------/                |
        v                                         |
   /----+-----\                                   |
   |          |     "Check expected events"       |
   | Checker  +#----------------------------------/
   |          |
   \-----+----/
         |                ----->  "Send command / event"
         o                -----o  "Store"
   "Test Results"         >----#  "Query"

Apart from the SUT, the other components are:

  The Driver
    This controls the execution of the test. for some types of test it will
    directly provide stimuli to the SUT.

  The :term:`Mocks<mock>`
    There can be zero or more of these. They take the role of things that the
    SUT would normally expect to interact with. In general, they need to be
    able to interact with the SUT in a largely autonomous manner. For example
    providing acknowledgements, responding to requests, etc.

    :term:`Mocks<mock>`, store interesting information in the event store.

    :term:`Mocks<mock>` are the other source of stimuli to the SUT. They can do
    so autonomously but also in response to 'commands' from the ``Driver``.

  The Event Store
    Somewhere to store information about interesting events until they can be
    compared to expected events.

    The store is a FIFO, that provides a window on recent events. The window
    may be time limited, number of events limited or both.

  The Probe
    A probe extracts information from the SUT. The details of how it does
    this depends, of course on the SUT. They can include:

    - Watching log files for interesting events.
    
    - Capturing and watching the console output of a process, looking for
      interesting events.
    
    - Using some form of diagnostic interface to query the SUT.

    Probes store interesting events in the event store.

  The Checker
    This performs the verification part of a test, which essentially involves
    querying the event store to check that the expected events have occurred,
    when they should have occurred.

The Driver, sends information or instructions to most of the other components.

  :term:`Mocks<mock>`
    Information about which events are currently interesting, so the
    :term:`Mocks<mock>` can copy useful information into the Event Store. Also
    instructions on how to interact with the SUT, for example, 'do not
    acknowledge the next XXX request'.

  Event Store
    The operations include:

    - Removing single events.
    - Removing old events.
    - Changing the capacity (maximum number or age) of the event store.
    - And, of course, searching for events.

  Probe
    To tell a probe what it should currently be probing for.

  Checker
    So the checker knows what to expect in the event store, in order to
    determine whether a test has passed/failed.

Of course, not all test scenarios require all parts of this model.
For example, typically, a unit test:

  - May not need *CleverSheep* :term:`Mocks<mock>` [#]_, the Driver can often
    provides all the stmuli required by invoking the unit's functions.

  - The Event Store is also typically redundant, events are the returns from
    the invoked functions.

  - The Checker becomes little more than a toolkit of checking functions.

  - The Probe may not exist or otherwise is another set of functions that
    peek into the SUT.

.. [#] But, or course, a unit test specific mocking library will be useful.
       Have a look at `mocking <http://www.voidspace.org.uk/python/mock/>`_.


How CleverSheep Supports the Model
==================================

The CleverSheep test modules and packages are intended to help you create the
various parts of the model. There is not a precise mapping but to a first
approximation we have:

  +-----------------+------------------------------------------------------+
  | The Driver      | The :mod:`~Test.Tester` sub-package. More            |
  |                 | specifically, an individual test method provides     |
  |                 | the driving script, along with supporting modules,   |
  |                 | functions, etc.                                      |
  +-----------------+------------------------------------------------------+
  | The Mocks       | The :mod:`~Test.Mock` sub-package. This is not       |
  |                 | currently well defined, but may become so in the     |
  |                 | near future.                                         |
  +-----------------+------------------------------------------------------+
  | The Event Store | The :mod:`~Test.TestEventStore` module, in the       |
  |                 | :mod:`Test` package.                                 |
  +-----------------+------------------------------------------------------+
  | The Probe       | The :mod:`Test.LogTracker` module in the :mod:`Test` |
  |                 | package. Otherwise, you are on your own, but         |
  |                 | essentially if you can figure out how to track what  |
  |                 | happens then you can probe things and store the      |
  |                 | details as events.                                   |
  +-----------------+------------------------------------------------------+
  | The Checker     | The assertion functions in the                       |
  |                 | :mod:`~Test.Tester` sub-package. You can, of         |
  |                 | course, add your own checks then simply use the low  |
  |                 | level `~Test.Tester.Assertions.fail`` assertion.     |
  +-----------------+------------------------------------------------------+

The *CleverSheep* framework does not attempt to impose this model, it merely 
provides modules to support it.


.. rubric:: Unit Testing

For this, the :mod:`~Test.Tester` sub-project provides the required
framework. It provides the necessary infrastructure required for a test Driver;
such as providing suitable :term:`fixtures<fixture>` (set-up and tear-down). It
also has a rich set of assertion functions that formalise the checking of
results, as well as helping to provide detailed and useful desciptions of
failure conditions.


.. rubric:: Component and System Testing

Basically, anything in which the SUT is a process, set of processes, etc.

For this situation, the :mod:`~Test.Tester` package provides the
:obj:`~Test.Tester.Suites.Suite.control` property. This supports
asynchronous behaviour using a simple event driven framework (provided
by the :obj:`~Test.PollManager`). This makes it possible to write test methods
that can interact cleanly with asynchronously executing external programs.

Most tests at this level also need to provide *CleverSheep* :term:`mocks<mock>`
as stubs/mock components. When your players communicate with the SUT using
sockets, the :mod:`~Test.Mock` sub- package may help simplify development of
those players. The :mod:`~Test.Mock` sub- package provides a toolkit to help
write such players. The :mod:`~Test.Mock.Component` and :mod:`~Test.Mock.Comms`
modules contained in this sub- package are designed to integrate well with
tests that use the :obj:`~Test.Tester.Suites.Suite.control` property.

.. When the messages to/from the SUT are in binary form, based on C
   structures, the :mod:`Struct` module makes it easy to describe, build, decode
   and query such messages.

This is also the area where an Event Store is really applicable, to provide a
short term window on recent messages, for example. The
:mod:`~Test.TestEventStore` module provides a generic event store, which can be
constrained by time and/or number of events.

Probe functionality tends to be a bit more test specific, but the
:mod:`~Test.LogTracker` module can be useful here. You can use it to monitor log
files produced by the SUT and store significant entries in the Event Store.
