"""Pygments lexer for CleverSheep's Tester output.

The test framework in CleverSheep produces nice coloured output. This allows us
to reproduce a reasonable likeness in the Sphinx generated output.

Basically, this provides a Pygments lexer for the CleverSheep. It does not
plug-in using the defined mechanism for two reaons:

1. That would require packaging this up separately as an installable Python
   module/package, which introduces an extra dependency just to create the
   CleverSheep/Mym documentation.

2. The plug-in mechanism uses ``setuptools``, which is an evil that must be
   stopped.

"""


import os

from pygments import lexers
from pygments.lexer import RegexLexer, bygroups, combined, include
from pygments.token import *
from pygments.styles import friendly
from pygments.style import Style
from pygments import styles


class CsTestLexer(RegexLexer):
    """For `CsTestfiles <http://www.cleversheep.org>`_"""

    name = 'cstest'
    aliases = ['cstest',]
    filenames = ['*.cstest']
    mimetypes = ['text/x-cstest', 'application/x-cstest']

    tokens = {
        'root': [
            (r'\n', Keyword),
            (r'^Summary of the failures\s*$', Generic.Strong),
            (r'^\s*Test case failed\s*$',     Generic.Error),
            (r'^\s*fail[A-Z].*$',             Generic.Assertion),
            (r'^\s*==>\d+ ?:.*$',             Generic.MarkedLine),
            (r'^\s*\d+ ?:.*$',                Generic.Context),
            (r'^\s*(.*\.py)(: )(.*)$',
                    bygroups(Generic.File, Text, Generic.Func)),
            (r'(\.        )(FAIL)',           bygroups(Text, Generic.Error)),
            (r'.', Text)
            #(r'(?i).+$', Keyword),
        ],
    }

    def analyse_text(text):
        return shebang_matches(text, r'pythonw?(2\.\d)?')


def _fixStyles(styles):
    col2rgb = (
        ("Brown", "#bc8f8f"),
        ("Magenta", "#ff00ff"),
        ("DarkCyan", "#008b8b"),
        ("Cyan", "#00ffff"),
        ("Blue", "#0000ff"),
    )
    for style, styleSpec in styles.iteritems():
        for colName, colHex in col2rgb:
            if colName in styleSpec:
                styleSpec = styleSpec.replace(colName, colHex)
        styles[style] = styleSpec

    return styles


class CleverSheepStyle(Style):
    background_color = "#f8f8f8"
    default_style = ""

    styles = friendly.FriendlyStyle.styles #.copy()
    styles = _fixStyles({
        Whitespace:                "#bbbbbb",
        Comment:                   "italic Blue",
        Comment.Preproc:           "noitalic #007020",
        Comment.Special:           "noitalic bg:#fff0f0",

        Keyword:                   "bold Brown",
        Keyword.Constant:          "bold #ffa0a0",
        Keyword.Pseudo:            "nobold",
        Keyword.Type:              "",

        Literal:                   "Magenta",

        Operator:                  "Brown",
        Operator.Word:             "bold #007020",

        Name.Builtin:              "#007020",
        Name.Builtin.Pseudo:       "italic #007020",
        Name.Function:             "DarkCyan",
        Name.Class:                "bold #0e84b5",
        Name.Namespace:            "bold #0e84b5",
        Name.Exception:            "#007020",
        Name.Variable:             "#bb60d5",
        Name.Constant:             "#60add5",
        Name.Label:                "bold #002070",
        Name.Entity:               "bold #d55537",
        Name.Attribute:            "#4070a0",
        Name.Tag:                  "bold #062873",
        Name.Decorator:            "bold #555555",
        Name.Other:                "italic #ff0000",

        String:                    "Magenta",
        String.Doc:                "italic Magenta",
        String.Interpol:           "italic #70a0d0",
        String.Escape:             "bold #4070a0",
        String.Regex:              "#235388",
        String.Symbol:             "#517918",
        String.Other:              "#c65d09",
        Number:                    "Magenta",

        Generic.Heading:           "bold #000000",
        Generic.Assertion:         "#0000ff",
        Generic.Context:           "Magenta",
        Generic.MarkedLine:        "bold Magenta",
        Generic.File:              "bold Blue",
        Generic.Func:              "Magenta",

        Generic.Subheading:        "bold #800080",
        Generic.Deleted:           "#A00000",
        Generic.Inserted:          "#00A000",
        Generic.Error:             "#FF0000",
        Generic.Emph:              "italic",
        Generic.Strong:            "bold",
        Generic.Prompt:            "bold #c65d09",
        Generic.Output:            "#888",
        Generic.Traceback:         "#04D",

        Error:                     "border:#FF0000"
    })


# We do not want to have to install this separately just plug into
# Pygments, hence this slight monkey patching approach.
lexers.LEXERS['CsTestLexer'] = (
        'cstest_lex', 'cstest',
            ('cstest', 'cstest'),
            ('*.cstest', ),
            ('text/x-cstest', 'application/x-cstest'))

styles.STYLE_MAP['harold'] = 'harold::CleverSheepStyle'
import sys
sys.modules["pygments.styles.harold"] = sys.modules[__name__]


def setup(*args):
    pass


__all__ = ["CsTestLexer"]
