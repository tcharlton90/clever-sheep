====================
CleverSheep Glossary
====================

.. glossary::
   :sorted:

   Fixture
     See :term:`test fixture`.

   Event
     See :term:`test event`.

   Stub
     In testing, a relatively simple obkect that stands in for some other
     piece of functionality. Stubs tend not to process the inputs they
     receive and respond with fixed, or a small set of, outputs.

   Mock
     In testing, a fairly complex object that stands in for some other piece
     of functionality, (at least partially) emulating its behaviour. Mocks
     will process some of the inputs they receive and respond to those
     imputs. However, the way they respond can often be 'programmed' in order
     to support specific test cases.

   Test Event
     Some interesting occurence that might be germain to an executing test.
     There are two kinds of interesting events:

     - Things that are expected to occur.
     - Things that are **not** expected to occur.
       
   Test Case
     In general terms, a description that says:

       1. What you are testing.
       2. The purpose of the test.
       3. The required inputs events.
       4. The expected output events.

     I reality, items 3 and 4 are often a sequence of inputs and expected
     outputs.

     A test case tends to be focussed on testing a single requirement,
     feature, bug, use case, etc. It is the 'atom' of testing; something that
     can pass or fail, but cannot half pass.

     A test case is self contained, but not normally enough to actually
     execute a test. You need some form of context, which is (at least
     partly) provided by a :term:`Test Fixture`.

   Test fixture
     The specific environment that one or more tests need to be succesfully
     executed. A test fixture can include :term:`stubs<stub>` and
     :term:`mocks<mock>`.

   Test Script
     The set of instructions the describe how to carry out a test. Basically
     this will consist of:

     1. The required test fixture and how to achieve it.

        This is referred to as the set up phase.

     2. The required test case and the details of how to perform the steps it
        contains.

     3. Optionally, instructions on how to reset the system under test
        (:term:`SUT`) back to a known state.

        The is referred to as the tear down phase.

   Test Environment
     Well, the environment in which a set of tests will be executed. The
     environment is the static arena, for example a well defined
     configuration of computers, loaded with specific programs and configured
     in a particular way.

   Test Suite
     A collection of tests, which are logically grouped. Reasons for grouping
     tests into suites include:

     - All the tests require the same :term:`test fixture`.

     - The tests logically belong together. For example, as a group they test
       some feature.

     Within *CleverSheep*, a suite can contain other test suites, creating
     a :term:`test hierarchy`.

   Test Specification
     Formally a document which pulls together a bunch of the above items into
     a complete description of how the system will be tested. So it can
     describe:

     1. A number of :term:`test environments<test environment>`.

     2. For each environment, a number of :term:`test suites<test suite>`
        and or :term:`test scripts<test script>`.

     It will also typically contain additional information; such as how to
     build the :term:`mocks<mock>`, term:`SUT`, initially configure it, etc.

   Test Hierarchy
     A suite of :term:`test suites<test suite>`, each of which may be
     composed of :term:`test suites<test suite>`, etc.; hence a hierarchy.

   SUT
     System under test.

.. -------------------
.. vim: sw=2 spell

