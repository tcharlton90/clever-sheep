def test_03_remove_entries(self):
    """Removal of entries.

    We use test_01_append to create a populated list. Then we empty it by
    removing entries from various positions, including the head and tail of
    the list.
    """
    def removeAtIdx(idx):
        ent = self.ll.head
        i = 0
        while i != idx:
            ent = ent.next
            i += 1
        cLists.removeEntryFromPairList(ent)
        del expectTuples[idx]

    self.test_01_append()
    expectTuples = list(self.expectTuples)

    n = len(expectTuples)
    i = 0
    while expectTuples:
        if i % 3 == 0:
            # Remove the first entry
            removeAtIdx(0)
        elif i % 3 == 1:
            # Remove the last entry
            removeAtIdx(n - 1)
        else:
            # Remove an entry in the middle
            removeAtIdx(n // 2)
        self.check(expectTuples, self.ll)

        i += 1
        n -= 1
        failUnlessEqual(n, self.ll.length)
