.. Links for the CleverSheep API.

.. _Comms: ../API/html/CleverSheep.Test.Mock.Comms-module.html
.. _Component: ../API/html/CleverSheep.Test.Mock.Component-module.html
.. _FileTracker: ../API/html/CleverSheep.Log.FileTracker-module.html
.. _Log: ../API/html/CleverSheep.Log-module.html
.. _Mock: ../API/html/CleverSheep.Test.Mock-module.html
.. _PollManager: ../API/html/CleverSheep.Test.PollManager-module.html
.. _PollTester: ../API/html/CleverSheep.Test.Tester.PollTester-module.html
.. _Struct: ../API/html/CleverSheep.Test.Struct-module.html
.. _Test: ../API/html/CleverSheep.Test-module.html
.. _Tester: ../API/html/CleverSheep.Test.Tester-module.html
.. _TestEventStore: ../API/html/CleverSheep.Test.TestEventStore-module.html
