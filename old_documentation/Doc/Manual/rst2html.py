#!/usr/bin/env python

"""
A minimal front end to the Docutils Publisher, producing HTML.
"""

# $Id: rst2html.py 4564 2006-05-21 20:44:42Z felixwiemann $
# Author: David Goodger <goodger@python.org>
# Copyright: This module has been placed in the public domain.

import os

try:
    import locale
    locale.setlocale(locale.LC_ALL, '')
except:
    pass

# Add pygments support.
from docutils import nodes
from docutils.parsers.rst import directives
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter

formatter = HtmlFormatter(linenos="inline")

extMap = {
    ".py": "python", 
}

def pygments_directive(name, arguments, options, content, lineno,
                      content_offset, block_text, state, state_machine):
    arguments = arguments[0].split()
    sourceLang = arguments[0]
    source = None
    if len(arguments) == 1:
        root, ext = os.path.splitext(arguments[0])
        if ext:
            sourceLang = extMap.get(ext, "")
            source = arguments[0]
    try:
        lexer = get_lexer_by_name(sourceLang)
    except ValueError:
        # no lexer found
        lexer = get_lexer_by_name('text')
    if len(arguments) > 1:
        source = arguments[1]
    if source is not None:
        f = open(source)
        content = [l.rstrip() for l in f.readlines()]
        f.close()
        parsed = highlight(u'\n'.join(content), lexer, formatter)
    else:
        parsed = highlight(u'\n'.join(content), lexer, formatter)
    return [nodes.raw('', parsed, format='html')]

pygments_directive.arguments = (1, 0, 1)
pygments_directive.content = 1
directives.register_directive('sourcecode', pygments_directive)


# Below here is the original rst2html.py code.
from docutils.core import publish_cmdline, default_description


description = ('Generates (X)HTML documents from standalone reStructuredText '
               'sources.  ' + default_description)

publish_cmdline(writer_name='html', description=description)
